<?php

use yii\db\Migration;

/**
 * Handles dropping pdf_and_djvu_and_ps_and_html_and_TeX from table `library`.
 */
class m180601_125610_drop_pdf_and_djvu_and_ps_and_html_and_TeX_column_from_library_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropColumn('library', 'pdf', $this->integer());
        $this->dropColumn('library', 'djvu', $this->integer());
        $this->dropColumn('library', 'ps', $this->integer());
        $this->dropColumn('library', 'html', $this->integer());
        $this->dropColumn('library', 'TeX', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->addColumn('library', 'pdf', $this->string());
        $this->addColumn('library', 'djvu', $this->string());
        $this->addColumn('library', 'ps', $this->string());
        $this->addColumn('library', 'html', $this->string());
        $this->addColumn('library', 'TeX', $this->string());
    }
}
