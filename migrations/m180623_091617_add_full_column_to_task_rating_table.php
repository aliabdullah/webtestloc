<?php

use yii\db\Migration;

/**
 * Handles adding full to table `task_rating`.
 */
class m180623_091617_add_full_column_to_task_rating_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('task_rating', 'full', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('task_rating', 'full');
    }
}
