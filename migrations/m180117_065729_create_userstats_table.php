<?php

use yii\db\Migration;

/**
 * Handles the creation of table `userstats`.
 */
class m180117_065729_create_userstats_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('userstats', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'blog_id' => $this->integer(),
            'quest_id' => $this->integer(),
            'select_id' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('userstats');
    }
}
