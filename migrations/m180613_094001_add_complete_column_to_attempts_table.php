<?php

use yii\db\Migration;

/**
 * Handles adding complete to table `attempts`.
 */
class m180613_094001_add_complete_column_to_attempts_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('attempts', 'complete', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('attempts', 'complete');
    }
}
