<?php

use yii\db\Migration;

/**
 * Handles adding djvu_and_pdf_and_ps_and_html_and_TeX to table `library`.
 */
class m180601_110539_add_djvu_and_pdf_and_ps_and_html_and_TeX_column_to_library_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('library', 'djvu', $this->integer());
        $this->addColumn('library', 'pdf', $this->integer());
        $this->addColumn('library', 'ps', $this->integer());
        $this->addColumn('library', 'html', $this->integer());
        $this->addColumn('library', 'TeX', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('library', 'djvu');
        $this->dropColumn('library', 'pdf');
        $this->dropColumn('library', 'ps');
        $this->dropColumn('library', 'html');
        $this->dropColumn('library', 'TeX');
    }
}
