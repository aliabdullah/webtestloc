<?php

use yii\db\Migration;

/**
 * Handles the creation of table `library`.
 */
class m180601_092854_create_library_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('library', [
            'id' => $this->primaryKey(),
            'author' => $this->string()->notNull(),
            'name' => $this->string()->notNull(),
            'year' => $this->integer(),
            'page' => $this->integer(),
            
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('library');
    }
}
