<?php

use yii\db\Migration;

/**
 * Handles adding predmet1 to table `statistics`.
 */
class m180119_091314_add_predmet1_column_to_statistics_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('statistics', 'predmet1', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('statistics', 'predmet1');
    }
}
