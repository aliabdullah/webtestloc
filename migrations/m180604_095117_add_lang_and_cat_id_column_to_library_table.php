<?php

use yii\db\Migration;

/**
 * Handles adding lang_and_cat_id to table `library`.
 */
class m180604_095117_add_lang_and_cat_id_column_to_library_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('library', 'lang', $this->string());
        $this->addColumn('library', 'cat_id', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('library', 'lang');
        $this->dropColumn('library', 'cat_id');
    }
}
