<?php

use yii\db\Migration;

/**
 * Handles adding date to table `tests`.
 */
class m171127_060820_add_date_column_to_tests_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('tests', 'date', $this->date());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('tests', 'date');
    }
}
