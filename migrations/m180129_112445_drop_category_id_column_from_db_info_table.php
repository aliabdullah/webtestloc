<?php

use yii\db\Migration;

/**
 * Handles dropping category_id from table `db_info`.
 */
class m180129_112445_drop_category_id_column_from_db_info_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropColumn('db_info', 'category_id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->addColumn('db_info', 'category_id', $this->integer());
    }
}
