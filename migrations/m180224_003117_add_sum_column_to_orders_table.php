<?php

use yii\db\Migration;

/**
 * Handles adding sum to table `orders`.
 */
class m180224_003117_add_sum_column_to_orders_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('orders', 'sum', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('orders', 'sum');
    }
}
