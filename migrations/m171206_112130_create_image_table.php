<?php

use yii\db\Migration;

/**
 * Handles the creation of table `image`.
 */
class m171206_112130_create_image_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('image', [
            'id' => $this->primaryKey(),
            'user_id'=>$this->integer(),
            'question_id' => $this->integer(),
            'blog_id'=>$this->integer(),
            'photo'=>$this->string(),
            'main'=>$this->integer()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('image');
    }
}
