<?php

use yii\db\Migration;

/**
 * Handles adding auth_key to table `users`.
 */
class m171129_103310_add_auth_key_column_to_users_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('users', 'auth_key', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('users', 'auth_key');
    }
}
