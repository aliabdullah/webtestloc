<?php

use yii\db\Migration;

/**
 * Handles the creation of table `statistics`.
 */
class m171203_105858_create_statistics_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('statistics', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'category_id' => $this->integer(),
            'cours_id' => $this->integer(),
            'start_time' => $this->time(),
            'stop_time' => $this->time(),
            'correct_answer' => $this->integer(),
            'wrong_answer' => $this->integer(),
            'level' => $this->string(),
            'date' => $this->date(),
            

        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('statistics');
    }
}
