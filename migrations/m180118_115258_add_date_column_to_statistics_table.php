<?php

use yii\db\Migration;

/**
 * Handles adding date to table `statistics`.
 */
class m180118_115258_add_date_column_to_statistics_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('statistics', 'date', $this->dateTime());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('statistics', 'date');
    }
}
