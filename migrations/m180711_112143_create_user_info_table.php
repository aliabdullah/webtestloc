<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_info`.
 */
class m180711_112143_create_user_info_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user_info', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'name' => $this->string(),
            'surname' => $this->string(),
            'email' => $this->string(),
            'gender' => $this->string(),
            'countries' => $this->string(),
            'birthday' => $this->string(),
            'phone_number' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user_info');
    }
}
