<?php

use yii\db\Migration;

/**
 * Handles adding lang to table `tests`.
 */
class m180309_102919_add_lang_column_to_tests_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('tests', 'lang', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('tests', 'lang');
    }
}
