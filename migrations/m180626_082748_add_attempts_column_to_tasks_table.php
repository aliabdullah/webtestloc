<?php

use yii\db\Migration;

/**
 * Handles adding attempts to table `tasks`.
 */
class m180626_082748_add_attempts_column_to_tasks_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('tasks', 'attempts', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('tasks', 'attempts');
    }
}
