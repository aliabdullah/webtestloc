<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_complaint`.
 */
class m180627_091933_create_user_complaint_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user_complaint', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'task_id' => $this->integer(),
            'complaint' => $this->text(),
            'checked' => $this->integer(),
            'date' => $this->date(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user_complaint');
    }
}
