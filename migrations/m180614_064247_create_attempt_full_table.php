<?php

use yii\db\Migration;

/**
 * Handles the creation of table `attempt_full`.
 */
class m180614_064247_create_attempt_full_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('attempt_full', [
            'id' => $this->primaryKey(),
            'attempt_id' => $this->integer(),
            'full_answ' => $this->text(),
            'file' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('attempt_full');
    }
}
