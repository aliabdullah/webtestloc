<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tests`.
 */
class m171122_134859_create_tests_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('tests', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer()->notNull(),
            'question' => $this->string()->notNull(),
            'photo_quest' => $this->string()->notNull(),
            'a' => $this->string()->notNull(),
            'b' => $this->string()->notNull(),
            'c' => $this->string()->notNull(),
            'd' => $this->string()->notNull(),
            'answer' => $this->string()->notNull(),
            'answer_photo' => $this->string()->notNull(),
            'score' => $this->integer()->notNull(),
            'level' => $this->string()->notNull(),

        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('tests');
    }
}
