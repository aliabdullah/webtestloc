<?php

use yii\db\Migration;

/**
 * Handles adding type_and_tournament_id to table `tasks`.
 */
class m180807_063831_add_type_and_tournament_id_column_to_tasks_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('tasks', 'type', $this->string());
        $this->addColumn('tasks', 'tournament', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('tasks', 'type');
        $this->dropColumn('tasks', 'tournament');
    }
}
