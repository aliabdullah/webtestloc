<?php

use yii\db\Migration;

/**
 * Handles adding name to table `tasks`.
 */
class m180608_125730_add_name_column_to_tasks_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('tasks', 'name', $this->string()->notNull());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('tasks', 'name');
    }
}
