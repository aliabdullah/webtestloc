<?php

use yii\db\Migration;

/**
 * Handles adding lang to table `cours`.
 */
class m180309_073622_add_lang_column_to_cours_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('cours', 'lang', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('cours', 'lang');
    }
}
