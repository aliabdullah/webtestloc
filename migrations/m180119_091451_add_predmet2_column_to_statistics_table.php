<?php

use yii\db\Migration;

/**
 * Handles adding predmet2 to table `statistics`.
 */
class m180119_091451_add_predmet2_column_to_statistics_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('statistics', 'predmet2', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('statistics', 'predmet2');
    }
}
