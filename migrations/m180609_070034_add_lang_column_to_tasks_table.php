<?php

use yii\db\Migration;

/**
 * Handles adding lang to table `tasks`.
 */
class m180609_070034_add_lang_column_to_tasks_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('tasks', 'lang', $this->string()->notNull());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('tasks', 'lang');
    }
}
