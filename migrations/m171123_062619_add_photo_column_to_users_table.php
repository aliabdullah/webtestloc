<?php

use yii\db\Migration;

/**
 * Handles adding photo to table `users`.
 */
class m171123_062619_add_photo_column_to_users_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('users', 'photo', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('users', 'photo');
    }
}
