<?php

use yii\db\Migration;

/**
 * Handles dropping cours_id from table `statistics`.
 */
class m180118_114736_drop_cours_id_column_from_statistics_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropColumn('statistics', 'cours_id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->addColumn('statistics', 'cours_id', $this->integer());
    }
}
