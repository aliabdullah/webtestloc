<?php

use yii\db\Migration;

/**
 * Handles adding test_count to table `category`.
 */
class m180710_063856_add_test_count_column_to_category_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('category', 'test_count', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('category', 'test_count');
    }
}
