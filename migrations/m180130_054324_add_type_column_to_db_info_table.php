<?php

use yii\db\Migration;

/**
 * Handles adding type to table `db_info`.
 */
class m180130_054324_add_type_column_to_db_info_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('db_info', 'type', $this->string(15));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('db_info', 'type');
    }
}
