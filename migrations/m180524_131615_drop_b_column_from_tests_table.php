<?php

use yii\db\Migration;

/**
 * Handles dropping b from table `tests`.
 */
class m180524_131615_drop_b_column_from_tests_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropColumn('tests', 'b');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->addColumn('tests', 'b', $this->string());
    }
}
