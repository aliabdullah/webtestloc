<?php

use yii\db\Migration;

/**
 * Handles dropping answer_photo_and_photo_quest_and_blog_id from table `tests`.
 */
class m180524_132403_drop_answer_photo_and_photo_quest_and_blog_id_column_from_tests_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropColumn('tests', 'answer_photo');
        $this->dropColumn('tests', 'photo_quest');
        $this->dropColumn('tests', 'blog_id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->addColumn('tests', 'answer_photo', $this->string());
        $this->addColumn('tests', 'photo_quest', $this->string());
        $this->addColumn('tests', 'blog_id', $this->string());
    }
}
