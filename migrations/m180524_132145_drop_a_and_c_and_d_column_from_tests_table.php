<?php

use yii\db\Migration;

/**
 * Handles dropping a_and_c_and_d from table `tests`.
 */
class m180524_132145_drop_a_and_c_and_d_column_from_tests_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropColumn('tests', 'a');
        $this->dropColumn('tests', 'c');
        $this->dropColumn('tests', 'd');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->addColumn('tests', 'a', $this->string());
        $this->addColumn('tests', 'c', $this->string());
        $this->addColumn('tests', 'd', $this->string());
    }
}
