<?php

use yii\db\Migration;

/**
 * Handles adding user_id to table `db_info`.
 */
class m180314_080700_add_user_id_column_to_db_info_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('db_info', 'user_id', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('db_info', 'user_id');
    }
}
