<?php

use yii\db\Migration;

/**
 * Handles adding true to table `user_messages`.
 */
class m180702_100922_add_true_column_to_user_messages_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('user_messages', 'true', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('user_messages', 'true');
    }
}
