<?php

use yii\db\Migration;

/**
 * Handles adding lang to table `tournament`.
 */
class m180920_093021_add_lang_column_to_tournament_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('tournament', 'lang', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('tournament', 'lang');
    }
}
