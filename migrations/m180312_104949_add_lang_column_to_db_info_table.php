<?php

use yii\db\Migration;

/**
 * Handles adding lang to table `db_info`.
 */
class m180312_104949_add_lang_column_to_db_info_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('db_info', 'lang', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('db_info', 'lang');
    }
}
