<?php

use yii\db\Migration;

/**
 * Handles adding answer_and_score_and_date to table `attempts`.
 */
class m180613_101039_add_answer_and_score_and_date_column_to_attempts_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('attempts', 'answer', $this->text());
        $this->addColumn('attempts', 'score', $this->integer());
        $this->addColumn('attempts', 'date', $this->dateTime());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('attempts', 'answer');
        $this->dropColumn('attempts', 'score');
        $this->dropColumn('attempts', 'date');
    }
}
