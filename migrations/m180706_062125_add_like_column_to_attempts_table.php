<?php

use yii\db\Migration;

/**
 * Handles adding like to table `attempts`.
 */
class m180706_062125_add_like_column_to_attempts_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('attempts', 'like', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('attempts', 'like');
    }
}
