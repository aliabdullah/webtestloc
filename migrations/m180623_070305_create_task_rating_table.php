<?php

use yii\db\Migration;

/**
 * Handles the creation of table `task_rating`.
 */
class m180623_070305_create_task_rating_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('task_rating', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'attempt' => $this->integer(),
            'date' => $this->dateTime(),

        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('task_rating');
    }
}
