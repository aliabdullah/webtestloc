<?php

use yii\db\Migration;

/**
 * Handles adding day to table `tournament`.
 */
class m180807_094321_add_day_column_to_tournament_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('tournament', 'day', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('tournament', 'day');
    }
}
