<?php

use yii\db\Migration;

/**
 * Handles adding invalid to table `test_variant`.
 */
class m180910_123103_add_invalid_column_to_test_variant_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('test_variant', 'invalid', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('test_variant', 'invalid');
    }
}
