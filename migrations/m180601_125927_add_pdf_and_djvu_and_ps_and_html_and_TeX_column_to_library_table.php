<?php

use yii\db\Migration;

/**
 * Handles adding pdf_and_djvu_and_ps_and_html_and_TeX to table `library`.
 */
class m180601_125927_add_pdf_and_djvu_and_ps_and_html_and_TeX_column_to_library_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('library', 'djvu', $this->string());
        $this->addColumn('library', 'pdf', $this->string());
        $this->addColumn('library', 'ps', $this->string());
        $this->addColumn('library', 'html', $this->string());
        $this->addColumn('library', 'TeX', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('library', 'djvu');
        $this->dropColumn('library', 'pdf');
        $this->dropColumn('library', 'ps');
        $this->dropColumn('library', 'html');
        $this->dropColumn('library', 'TeX');
    }
}
