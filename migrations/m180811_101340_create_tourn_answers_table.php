<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tourn_answers`.
 */
class m180811_101340_create_tourn_answers_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('tourn_answers', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'tourn_id' => $this->integer(),
            'task_id' => $this->integer(),
            'answer' => $this->text(),
            'file' => $this->string(),
            'true' => $this->integer(),
            'time' => $this->time(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('tourn_answers');
    }
}
