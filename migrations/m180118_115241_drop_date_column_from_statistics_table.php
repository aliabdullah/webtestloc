<?php

use yii\db\Migration;

/**
 * Handles dropping date from table `statistics`.
 */
class m180118_115241_drop_date_column_from_statistics_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropColumn('statistics', 'date');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->addColumn('statistics', 'date', $this->integer());
    }
}
