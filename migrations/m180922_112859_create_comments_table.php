<?php

use yii\db\Migration;

/**
 * Handles the creation of table `comments`.
 */
class m180922_112859_create_comments_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('comments', [
            'id' => $this->primaryKey(),
            'task_id' => $this->integer(),
            'user_id' => $this->integer(),
            'text' => $this->text(),
            'status' => $this->integer(),
            'date' => $this->datetime(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('comments');
    }
}
