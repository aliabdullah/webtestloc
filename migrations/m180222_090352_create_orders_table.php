<?php

use yii\db\Migration;

/**
 * Handles the creation of table `orders`.
 */
class m180222_090352_create_orders_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('orders', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'tariff_id' => $this->integer(),
            'amount_sum' => $this->integer(),
            'date_start' => $this->date(),
            'date_end' => $this->date(),
            'status' => $this->boolean(),
            'tran_id' => $this->integer(),
            'tran_date' => $this->integer(),

        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('orders');
    }
}
