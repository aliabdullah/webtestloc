<?php

use yii\db\Migration;

/**
 * Handles the creation of table `test_group`.
 */
class m171213_060523_create_test_group_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('test_group', [
            'id' => $this->primaryKey(),
            'test_id' => $this->integer(),
            'blog_id' => $this->integer(),
            'sub_cat_id' => $this->integer(),
            'date' => $this->date(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('test_group');
    }
}
