<?php

use yii\db\Migration;

/**
 * Handles adding blog_id to table `tests`.
 */
class m171206_111316_add_blog_id_column_to_tests_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('tests', 'blog_id', $this->integer());

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('tests', 'blog_id');
    }
}
