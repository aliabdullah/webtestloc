<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_messages`.
 */
class m180628_105509_create_user_messages_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user_messages', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'task_id' => $this->integer(),
            'message' => $this->text(),
            'date' => $this->dateTime(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user_messages');
    }
}
