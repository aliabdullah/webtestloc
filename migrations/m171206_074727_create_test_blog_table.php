<?php

use yii\db\Migration;

/**
 * Handles the creation of table `test_blog`.
 */
class m171206_074727_create_test_blog_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('test_blog', [
            'id' => $this->primaryKey(),
            'cat_id' => $this->integer(),
            'name' => $this->string(),
            'description' => $this->text(),
            'type' => $this->string(),
            'date' => $this->date(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('test_blog');
    }
}
