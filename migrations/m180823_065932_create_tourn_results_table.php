<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tourn_results`.
 */
class m180823_065932_create_tourn_results_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('tourn_results', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'country' => $this->string(),
            'score' => $this->integer(),
            'place' => $this->integer(),
            'date' => $this->date()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('tourn_results');
    }
}
