<?php

use yii\db\Migration;

/**
 * Class m180129_044830_add_link_column_to_db_info
 */
class m180129_044830_add_link_column_to_db_info extends Migration
{
    /**
     * @inheritdoc
     */
    // public function safeUp()
    // {

    // }

    /**
     * @inheritdoc
     */
    // public function safeDown()
    // {
    //     echo "m180129_044830_add_link_column_to_db_info cannot be reverted.\n";

    //     return false;
    // }

    
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->addColumn('db_info', 'link', $this->string(255)->null());
    }

    public function down()
    {
        $this->dropColumn('db_info', 'link');
    }
    
}
