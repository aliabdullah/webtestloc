<?php

use yii\db\Migration;

/**
 * Handles adding start_time to table `userstats`.
 */
class m180118_110437_add_start_time_column_to_userstats_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('userstats', 'start_time', $this->time());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('userstats', 'start_time');
    }
}
