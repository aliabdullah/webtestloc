<?php

use yii\db\Migration;

/**
 * Handles adding date to table `db_info`.
 */
class m180314_082636_add_date_column_to_db_info_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('db_info', 'date', $this->dateTime());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('db_info', 'date');
    }
}
