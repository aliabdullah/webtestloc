<?php

use yii\db\Migration;

/**
 * Handles adding date to table `attempt_full`.
 */
class m180622_105415_add_date_column_to_attempt_full_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('attempt_full', 'date', $this->dateTime());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('attempt_full', 'date');
    }
}
