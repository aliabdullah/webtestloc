<?php

use yii\db\Migration;

/**
 * Handles dropping day from table `tournament`.
 */
class m180807_094146_drop_day_column_from_tournament_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropColumn('tournament', 'day');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->addColumn('tournament', 'day', $this->date());
    }
}
