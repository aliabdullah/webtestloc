<?php

use yii\db\Migration;

/**
 * Class m180129_044912_add_likes_column_to_db_info
 */
class m180129_044912_add_likes_column_to_db_info extends Migration
{
    /**
     * @inheritdoc
     */
    // public function safeUp()
    // {

    // }

    /**
     * @inheritdoc
     */
    // public function safeDown()
    // {
    //     echo "m180129_044912_add_likes_column_to_db_info cannot be reverted.\n";

    //     return false;
    // }

    
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->addColumn('db_info', 'likes', $this->integer()->null());
    }

    public function down()
    {
        $this->dropColumn('db_info', 'likes');
    }
    
}
