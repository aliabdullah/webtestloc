<?php

use yii\db\Migration;

/**
 * Handles adding task_id to table `task_rating`.
 */
class m180623_092050_add_task_id_column_to_task_rating_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('task_rating', 'task_id', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('task_rating', 'task_id');
    }
}
