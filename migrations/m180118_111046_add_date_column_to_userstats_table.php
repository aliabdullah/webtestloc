<?php

use yii\db\Migration;

/**
 * Handles adding date to table `userstats`.
 */
class m180118_111046_add_date_column_to_userstats_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('userstats', 'date', $this->dateTime());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('userstats', 'date');
    }
}
