<?php

use yii\db\Migration;

/**
 * Handles adding checked to table `attempt_full`.
 */
class m180614_065224_add_checked_column_to_attempt_full_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('attempt_full', 'checked', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('attempt_full', 'checked');
    }
}
