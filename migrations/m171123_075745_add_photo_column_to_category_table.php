<?php

use yii\db\Migration;

/**
 * Handles adding photo to table `category`.
 */
class m171123_075745_add_photo_column_to_category_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('category', 'photo', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('category', 'photo');
    }
}
