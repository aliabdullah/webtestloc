<?php

use yii\db\Migration;

/**
 * Handles adding status to table `cours`.
 */
class m171126_115923_add_status_column_to_cours_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('cours', 'status', $this->string(25));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('cours', 'status');
    }
}
