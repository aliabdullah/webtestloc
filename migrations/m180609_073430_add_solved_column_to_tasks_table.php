<?php

use yii\db\Migration;

/**
 * Handles adding solved to table `tasks`.
 */
class m180609_073430_add_solved_column_to_tasks_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('tasks', 'solved', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('tasks', 'solved');
    }
}
