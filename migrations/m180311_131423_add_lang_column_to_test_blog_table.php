<?php

use yii\db\Migration;

/**
 * Handles adding lang to table `test_blog`.
 */
class m180311_131423_add_lang_column_to_test_blog_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('test_blog', 'lang', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('test_blog', 'lang');
    }
}
