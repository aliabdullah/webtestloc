<?php

use yii\db\Migration;

/**
 * Handles adding link to table `library_formatts`.
 */
class m180601_095849_add_link_column_to_library_formatts_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('library', 'link', $this->string()->notNull());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('library', 'link');
    }
}
