<?php

use yii\db\Migration;

/**
 * Handles adding interval to table `statistics`.
 */
class m180122_093419_add_interval_column_to_statistics_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('statistics', 'interval', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('statistics', 'interval');
    }
}
