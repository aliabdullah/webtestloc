<?php

use yii\db\Migration;

/**
 * Handles adding date_and_level_and_public to table `user_info`.
 */
class m180712_064441_add_date_and_level_and_public_column_to_user_info_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('user_info', 'date', $this->dateTime());
        $this->addColumn('user_info', 'level', $this->string());
        $this->addColumn('user_info', 'public', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('user_info', 'date');
        $this->dropColumn('user_info', 'level');
        $this->dropColumn('user_info', 'public');
    }
}
