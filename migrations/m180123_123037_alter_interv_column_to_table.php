<?php

use yii\db\Migration;

/**
 * Class m180123_123037_alter_interv_column_to_table
 */
class m180123_123037_alter_interv_column_to_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('statistics', 'interval', $this->integer(32));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn('statistics', 'interval', $this->string(255));
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180123_123037_alter_interv_column_to_table cannot be reverted.\n";

        return false;
    }
    */
}
