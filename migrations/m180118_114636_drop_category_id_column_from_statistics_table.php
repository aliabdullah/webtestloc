<?php

use yii\db\Migration;

/**
 * Handles dropping category_id from table `statistics`.
 */
class m180118_114636_drop_category_id_column_from_statistics_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropColumn('statistics', 'category_id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->addColumn('statistics', 'category_id', $this->integer());
    }
}
