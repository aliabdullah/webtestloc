<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tasks`.
 */
class m180607_090330_create_tasks_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('tasks', [
            'id' => $this->primaryKey(),
            'cat_id' => $this->integer(),
            'sentBy_id' => $this->string()->notNull(),
            'task' => $this->text()->notNull(),
            'short_answ' => $this->string(),
            // 'file_answ' => $this->string(),
            'source' => $this->string(),
            'level' => $this->integer(),
            'class' => $this->integer(),
            'bal' => $this->integer(),
            'theme' => $this->string(),
            'date' => $this->dateTime(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('tasks');
    }
}
