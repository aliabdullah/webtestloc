<?php

use yii\db\Migration;

/**
 * Handles adding type to table `tests`.
 */
class m171124_091022_add_type_column_to_tests_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('tests', 'type', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('tests', 'type');
    }
}
