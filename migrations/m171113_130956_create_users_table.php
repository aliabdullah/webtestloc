<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users`.
 */
class m171113_130956_create_users_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('users', [
            'id' => $this->primaryKey(),
            'role' => $this->string(15),
            'status' => $this->string(15),
            'username' => $this->string(25),
            'password' => $this->string(32),
            'firstname' => $this->string(25),
            'lastname' => $this->string(25),
            'date' => $this->dateTime()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('users');
    }
}
