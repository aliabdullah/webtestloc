<?php

use yii\db\Migration;

/**
 * Handles the creation of table `cours`.
 */
class m171125_101639_create_cours_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('cours', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'test_id' => $this->integer()->notNull(),
            'title' => $this->string()->notNull(),
            'content' => $this->string()->notNull(),
            'cost' => $this->integer()->notNull(),
            'photo' => $this->string()->notNull(),

        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('cours');
    }
}
