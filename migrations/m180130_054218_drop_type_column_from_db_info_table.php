<?php

use yii\db\Migration;

/**
 * Handles dropping type from table `db_info`.
 */
class m180130_054218_drop_type_column_from_db_info_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropColumn('db_info', 'type');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->addColumn('db_info', 'type', $this->string());
    }
}
