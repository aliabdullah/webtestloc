<?php

use yii\db\Migration;

/**
 * Handles dropping category_id from table `statistics`.
 */
class m180118_114419_drop_category_id_column_from_statistics_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
    }
}
