<?php

use yii\db\Migration;

/**
 * Handles adding true to table `attempt_full`.
 */
class m180620_083232_add_true_column_to_attempt_full_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('attempt_full', 'true', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('attempt_full', 'true');
    }
}
