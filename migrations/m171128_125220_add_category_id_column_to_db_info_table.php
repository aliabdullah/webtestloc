<?php

use yii\db\Migration;

/**
 * Handles adding category_id to table `db_info`.
 */
class m171128_125220_add_category_id_column_to_db_info_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('db_info', 'category_id', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('db_info', 'category_id');
    }
}
