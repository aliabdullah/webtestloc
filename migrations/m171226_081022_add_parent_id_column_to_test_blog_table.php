<?php

use yii\db\Migration;

/**
 * Handles adding parent_id to table `test_blog`.
 */
class m171226_081022_add_parent_id_column_to_test_blog_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('test_blog', 'parent_id', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('test_blog', 'parent_id');
    }
}
