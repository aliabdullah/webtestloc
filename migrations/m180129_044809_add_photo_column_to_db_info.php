<?php

use yii\db\Migration;

/**
 * Class m180129_044809_add_photo_column_to_db_info
 */
class m180129_044809_add_photo_column_to_db_info extends Migration
{
    /**
     * @inheritdoc
     */
    // public function safeUp()
    // {
    //     $this->alterColumn('db_info', 'photo', $this->string(255)->null());
    // }

    /**
     * @inheritdoc
     */
    // public function safeDown()
    // {
    //     $this->alterColumn('db_info', 'photo', $this->string(255));
    // }

    
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->addColumn('db_info', 'photo', $this->string(255)->null());
    }

    public function down()
    {
        $this->dropColumn('db_info', 'photo');
    }
    
}
