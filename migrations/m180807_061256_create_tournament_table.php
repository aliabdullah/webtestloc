<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tournament`.
 */
class m180807_061256_create_tournament_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('tournament', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer(),
            'name' => $this->string(),
            'descrip' => $this->text(),
            'day' => $this->date(),
            'time' => $this->string(),
            'status' => $this->string(),
            'difficulty_lev' => $this->integer(),
            'rules' => $this->string(),
            'sponsors' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('tournament');
    }
}
