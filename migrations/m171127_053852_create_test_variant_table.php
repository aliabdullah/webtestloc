<?php

use yii\db\Migration;

/**
 * Handles the creation of table `test_variant`.
 */
class m171127_053852_create_test_variant_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('test_variant', [
            'id' => $this->primaryKey(),
            'test_id' => $this->integer()->notNull(),
            'type' => $this->string()->notNull(),
            'correct' => $this->boolean(),
            'content' => $this->string(255)->notNull(),
            'photo' => $this->string()->notNull()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('test_variant');
    }
}
