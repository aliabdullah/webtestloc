<?php

use yii\db\Migration;

/**
 * Handles adding checked to table `user_messages`.
 */
class m180628_111149_add_checked_column_to_user_messages_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('user_messages', 'checked', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('user_messages', 'checked');
    }
}
