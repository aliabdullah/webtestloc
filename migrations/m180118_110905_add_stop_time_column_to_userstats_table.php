<?php

use yii\db\Migration;

/**
 * Handles adding stop_time to table `userstats`.
 */
class m180118_110905_add_stop_time_column_to_userstats_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('userstats', 'stop_time', $this->time());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('userstats', 'stop_time');
    }
}
