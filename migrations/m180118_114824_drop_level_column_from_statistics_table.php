<?php

use yii\db\Migration;

/**
 * Handles dropping level from table `statistics`.
 */
class m180118_114824_drop_level_column_from_statistics_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropColumn('statistics', 'level');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->addColumn('statistics', 'level', $this->integer());
    }
}
