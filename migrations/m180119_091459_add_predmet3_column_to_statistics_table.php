<?php

use yii\db\Migration;

/**
 * Handles adding predmet3 to table `statistics`.
 */
class m180119_091459_add_predmet3_column_to_statistics_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('statistics', 'predmet3', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('statistics', 'predmet3');
    }
}
