<?php 

return [
	// main layout 
	"Tests" => "Тесты",
	"My_results" => "Мои результаты",
	"Materials" => "Учебные материалы",
	"Orders" => "Тарифы и подписки",
	"Cabinet" => "Личный кабинет",
	"Signin" => "Войти",
	"Logout" => "Выйти",
	"Home" => "Главная",

	"About_sistem" => "О системе",
	"Tariffs" => "Тарифы",
	"Payment_for_services" => "Оплата услуг",
	"Offer" => "Оферта",
	"Security_policy" => "Политика безопасности",
	"Partners" => "Партнерам",
	"Contacts" => "Контакты",
	"Сайт разработан в студии" => "Ushbu sayt",
	"comments" => "Комментарии",
	//

	// site/index page
	"Pull_up_your_skills" => "Прокачай свои навыки",
	"Take_the_tests_online_and_find_out_your_rating" => "Пройди тесты онлайн и узнай свой рейтинг",
	"Learn_more_about_the_service" => "Узнать подробнее о сервисе",
	"Start_using" => "Начать пользоваться",

	"ONLINE_TESTS_ON_DIRECTIONS" => "ОНЛАЙН ТЕСТЫ ПО НАПРАВЛЕНИЯМ",
	"Математика" => "Matematika",
	"Физика" => "Fizika",
	"Биология" => "Biologiya",
	"Химия" => "Kimyo",
	"Русский язык" => "Rus tili",
	"История" => "Tarix",
	"English" => "Ingliz tili",
	"Родной язык" => "Ona tili va Adabiyot",

	"tariffs_for_testing" => "ТАРИФЫ НА ТЕСТИРОВАНИЕ",
	"Базовый" => "TAYANCH (БАЗОВЫЙ) BILIM",
	"Продвинутый" => "DAVOM ETTIRUVCHILAR",
	"Премиум" => "PREMIUM (BILIMGA SARMOYA)",
	"Платинум" => "PLATINUM",
	//	

	// test/modal page
	"Check_status" => "Выберите Статус",
	"Choose_the_right_direction_for_yourself" => "Выберите правильное направление для себя",
	"free_tests" => "Бесплатные тесты",
	"Choose_a_free_service_to_learn_how_the_test_system_works" => "Выберите бесплатный сервис, чтобы узнать как работает система тестов!",
	"paid_tests" => "Платные тесты",
	"Choose_a_paid_service_to_switch_to_more_common_testing_questions" => "Выберите платный сервис, чтобы переключиться на более распространенные вопросы тестирования",

	// test/free-test page
	"questions" => "Вопросы",
	"result" => "Результат",
	"test_questions_for_your_imagination" => "Тестовые вопросы для вашего воображения",
	//

	// test/premium-test page
	"Ваш результат" => "Sizning natijangiz",
	"go_to_the_next_test" => "Перейти на следущий тест",
	"print" => "Печатать",
	//

	// test/premium-filter
	"premium_tests" => "Премиум тесты",
	"in_direction_and_thematic" => "по направлению и тематический",
	"we_serve_you_for_your_dreams!" => "Мы служим вам за ваши мечты!",
	"select_destination" => "Выберите направлению",
	"Direction" => "Направлении",
	"Items" => "Предметы",
	"Период" => "Oldinga",
	"school_for_schoolchildren" => "Школа (для школьников)",
	"applicant_college_lyceum" => "Абитуриент (колледж / лицей)",
	"Студент (университет)" => "Talaba (Institut)",
	"start_generating_tests" => "Начать генерировать тесты",
	"Complexity" => "Сложность",
	"Легко" => "Yengil",
 	"Нормальный" => "O'rta",
	"Сложный" => "Qiyin",
	"Эксперт" => "Mutaxassis",
	"Ваши результаты сохранены, и вы можете увидеть свои результаты в разделе «Мои результаты»." => "Sizning natijangiz saqlandi o'z natijalaringizni «Mening natijalarim» bo'limida ko'rishingiz mumkin.",
	"Результаты теста удалены, вы можете увидеть сохраненные результаты в разделе «Мои результаты»" => "Sizning test natijalaringiz o'chirildi, avval saqlangan natijalaringizni «Mening natijalarim» bo'limidan ko'rishingiz mumkin",

	// test/index page
	"visit_the_test_questions_page_to_select_a_convenient_tariff_plan" => "Посетите страницу «Вопросы теста»</br>выберите удобный тариф",
	"online_tests" => "Онлайн-тесты",
	"types_of_payments" => "Типы Платежей",

	//

	// results/result page
	"uncorrect" => "Не верны ответы",
	"correct" => "Правильны ответы",
	"question" => "Вопросы",
	"dateres" => "Время",
	"savedres" => "Результаты сохранены!",
	"Результат удален" => "Natija o'chirildi",
	"deleteres" => "Удалит результат",
	"saveres" => "Сохранить результат",
	"you_are_dont_answered_no_one_questions" => "Вы не ответили ни на один вопрос!",
	"Сохранить результат" => "Natijani saqlash",
	"Удалит результат" => "Natijani o'chirish",
	"to_home" => "На главную",
	"Страница тарифов" => "Tariflar sahifasiga",
	"Неправильно" => "No to'g'ri",
	//

	// results/index page 
	"your_statistics" => "Ваши статистики",
	"more" => "Подробно",
	"rating" => "Рейтинг",
	"records" => "Рекорды",
	"statistic_your_tests" => "Статистика ваших тестов",
	"learn_more_info" => "Вы узнаете больше о коротких и расширенных результатах ваших тестов, времени и результатах.",
	"statstics_of_test" => "Статистика тестов",
	// "Items" => "Предметы",
	"corrects" => "Правильный",
	"uncorrects" => "Не правильный",
	"progress" => "Прогресс",
	"percent" => "Процент",
	"date" => "ДАТА",
	"you_have_not_tried_any_tests_yet" => "Вы еще не пробовали никаких тестов",
	"register_to_see_your_results" => "Зарегистрируйтесь, чтобы увидеть свои результаты!",
	"rating_your_results" => "Рейтинг ваше резултатов",
	"diogramma" => "С помощью конкретной диаграммы вы можете отслеживать темпы роста вашего результата.",

	// materials/index page 
	"Привет" => "Salom",
	"Categories" => "Категории",
	"О странице" => "Bo'lim haqida",
	"В этом разделе вы можете использовать разные учебники, раздел также включает видео, аудио, электронные книги, статьи и является открытым исходным кодом для пользователей с статусом Premium" => "Bu bo'limda turli hildagi darsliklardan foydalanishingiz mumkin, bo'limda shuningdek video, audio, elektron kitoblar, maqolalar o'rin olgan bo'lib faqat Premium maqomiga ega bo'lgan foydalanuvchilar uchun ochiq manba hisoblanadi",
	"ОБЛАКО ТЕГОВ" => "TEGLAR BULUTI",
	"Video" => "Video",
	"Library" => "Kutubxona",
	"Audio" => "Audio",
	"Рекомендации" => "Maslahatlar",
	"Источники" => "Manbalar",
	"Видеоматериалы" => "Video maqolalar",
	"News" => "Новости",
	"Ratings" => "Рейтинги",
	"Tournaments" => "Турниры",
	"Tasks" => "Задачи",
	"Events" => "События",

	//

	// materials/video page
	"В текущем моменте такую информация нету в базе" => "Hozirda bunday ma'lumot yo'q tez orada yuklanadi...",
	//

	// materials/find page
	"Учебные центры" => "O'quv markazlari",
	"Просмотреть все" => "Hammasini ko'rish",
	"Видео уроки" => "Video darsliklar",
	"Новые" => "Yangi",
	//

	// tarrifs/index page
	"Типы Платежей" => "To'lov turlari",
	"Тарифы" => "Tariflar",
	"Инфо" => "Info",
	"Статус" => "Maqomi",
	"Коротка" => "Qisqacha",
	"Подробно" => "Batafsil",
	"Стоимость" => "Narx",
	"Осталось" => "Qoldiq",
	"Время Окончания" => "Tugash vaqti",
	"Время Начала" => "Boshlangan vaqti",
	"Текущий Подписка" => "Joriy obuna",
	"Тарифны Статистики" => "Tarif statistikasi",
	"Дата" => "Sana",
	"Вы не активировали какие-либо тарифы в настоящее время" => "Hozirgi vaqtda siz hechqanday tarifni faollashtirmagansiz",
	"Внимание" => "Diqqat",
	"Перейти на страницу тарифов" => "Tariflar sahifasiga o'tish",
	// 

	// test/modal page 
	"Это описание недоступно" => "Bunday Tarif mavjud emas",
	"O тарифе" => "Tarif haqida",
	"Параметры" => "Parametrlar",
	"Названия" => "Nomi",
	"Заказать" => "Buyurtma qilish",
	"user" => "Пользователь",
	"settings" => "Настройка",
	//

	// site/login page
	"Войдите, чтобы начать сеанс" => "Seansni boshlash uchun tizimga kiring",
	"Зарегистрировать" => "Ro'yhattan o'tish",
	"Запомни меня" => "Meni eslabqol",
	//

	// site/error page
	"Вернуться на главную" => "Bosh sahifaga qaytish",
	//

	// library/index page
	"скачать" => "yuklash",
	"empty_data" => "Эта информация временно недоступна в базе данных!",
	"complaint" => "жалобы",
	"idea_for_advice" => "Здесь вы можете оставить свою жалобу по этому поводу!",
	"Афтор" => "Muallif",
	"Названия" => "Nomi",
	"Год" => "Yil",
	"Страницы" => "Sahifa",
	"Скачать" => "Yuklash",
	"Книги" => "Kitoblar",
	"Фильтр" => "Saralash",
	//

	// site/user-info 
	"Пожалуйста, введите свою дополнительную информацию" => "Iltimos, qo'shimcha ma'lumotlaringizni kiriting",

	//

	// task/events page
	"events" => "Лента событий",
	"attempts" => "Попытки",
	"scores" => "Полученный балл",
	"answer_task" => "ответил   на   задачу",
	"show" => "Показывать",
	"all" => "все",
	"unsolved" => "нерешенный",
	"solved" => "решена",
	"Tournament" => "Турнир",
	"unable" => "Эта информация временно недоступна в базе данных!",
	"Published" => "Опубликована",
	"Sent_by" => "Прислал",
	"Complexity" => "Сложность",
	"grade" => "класс",
	"score" => "баллы",
	"topics" => "tемы",
	"task" => "Задачa",
	"task_solved" => "задачу решили",
	"all_attempts" => "всего попыток",
	"answer_sent" => "Ответ отправлен",
	"do_not_select_answer" => "Вы не дали ответа. Вы можете добавить полный текст или файл.",
	"full_answer" => "Полный ответ",
	"scrinshot_or_file" => "Вы можете загрузить ScrinShot или Фото",
	"choose_file" => "выберите файл",
	"send" => "послать",
	"congratulation!" => "Поздравляем! Вы решили эту Задачу!",
	"answer" => "Ответ:",
	"attempt" => "попыток:",
	"gived_scores" => "Полученный балл:",
	"fulled_attempt" => "Эта задача не была оценена, потому что вы проработали более 5 попыток!",

	//

	// task/tournament page
	"part_one_the_end" => "субботний тур закончился ... >> следующий раунд будет завтра",
	"the_end" => "турнир закончился, и вы можете узнать результаты в понедельник",
	"old" => "турнир закончился",
	"the_date_of_the" => "Дата проведения",
	"start_at" => "время с",
	"to" => "до",
	"level_temp" => "уровень сложности",
	"status_temp" => "статус",
	"active" => "активный",
	"past" => "прошлое",
	"boss" => "Организаторы",
	"sponsor" => "Спонсоры",
	"short_answ" => "короткий ответ",
	//

	// task/rating page 
	"name" => "Имя",
	"country" => "Страна",
	//

	// materials/news page
	"about_page" => "О странице",
	"desc_page" => "В этом разделе вы можете использовать разные учебники, раздел также включает видео, аудио, электронные книги, статьи и является открытым исходным кодом для пользователей с статусом Premium",
	"cloud" => "ОБЛАКО ТЕГОВ",
	"video" => "Видео",
	"library" => "Библиотека",
	"audio" => "Аудио",
	"recomended" => "Рекомендации",
	"source" => "Источники",
	//

	// library/index page
	"filter" => "Фильтр",
	"books" => "Книги",
	"author" => "Афтор",
	"title" => "Названия",
	"year" => "Год",
	"pages" => "Страницы",
	"download" => "Скачать",
	//

	// tariffs/index page
	"stats_tariff" => "Тарифны Статистики",
	"off_time" => "Конец срока!",
	"current_order" => "Текущий Подписка",
	"start_time" => "Время Начала",
	"end_time" => "Время Окончания",
	"residue" => "Осталось",
	"status" => "Статус",
	"attention" => "Внимание",
	"You_have_not_activated_any_tariffs_at_the_moment" => "Вы не активировали какие-либо тарифы в настоящее время",
	"info" => "Инфо",
	"statusUp" => "Статус",
	"short" => "Коротка",
	"cost" => "Стоимость",
	"enter_to_page" => "Перейти на страницу тарифов",
	//

	// site/info page
	"change_info" => "Изменить информацию",
	"username" => "Имя пользователя",
	"password" => "Пароль",
	"update" => "Обновить",
	"settings_account" => "Настройка Профиля",



];