<?php 

return [
	// main layout 
	"Tests" => "Testlar",
	"My_results" => "Mening natijalarim",
	"Materials" => "O'quv darsliklari",
	"Orders" => "Ta`riflar",
	"Cabinet" => "Shaxsiy bo'lim",
	"Signin" => "Kirish",
	"Logout" => "Saytdan chiqish",
	"Home" => "Asosiy",

	"About_sistem" => "Tizim haqida",
	"Tariffs" => "Ta`riflar",
	"Payment_for_services" => "Xizmatga to'lov",
	"Offer" => "Oferta",
	"Security_policy" => "Xavfsizlik siyosati",
	"Partners" => "Hamkorlarga",
	"Contacts" => "Bizga bog'lanish",
	"Сайт разработан в студии" => "Ushbu sayt",
	"comments" => "Izohlar",
	//

	// site/index page
	"Pull_up_your_skills" => "Qobilyatingizni sinab ko'ring",
	"Take_the_tests_online_and_find_out_your_rating" => "Test sinovlarini online tarzda yeching va o'z reytingingizni bilib oling",
	"Learn_more_about_the_service" => "Xizmat haqida batavsil ma'lumot",
	"Start_using" => "Foydalanishni boshlang",

	"ONLINE_TESTS_ON_DIRECTIONS" => "YO'NALISHLAR BO'YICHA ONLINE TESTLAR",
	"Математика" => "Matematika",
	"Физика" => "Fizika",
	"Биология" => "Biologiya",
	"Химия" => "Kimyo",
	"Русский язык" => "Rus tili",
	"История" => "Tarix",
	"English" => "Ingliz tili",
	"Родной язык" => "Ona tili va Adabiyot",

	"tariffs_for_testing" => "TA'RIFLAR",
	"Базовый" => "TAYANCH (БАЗОВЫЙ) BILIM",
	"Продвинутый" => "DAVOM ETTIRUVCHILAR",
	"Премиум" => "PREMIUM (BILIMGA SARMOYA)",
	"Платинум" => "PLATINUM",
	//	

	// test/modal page
	"Check_status" => "Holatni tanlang",
	"Choose_the_right_direction_for_yourself" => "O'zingiz uchun qulay bo'lgan yo'nalishni tanlang",
	"free_tests" => "Bepul testlar",
	"Choose_a_free_service_to_learn_how_the_test_system_works" => "Bepul хizmatni tanlang, savol-javob tizimi qanday ishlashi haqida tushuncha hosil qilish uchun!",
	"paid_tests" => "Pullik xizmat",
	"Choose_a_paid_service_to_switch_to_more_common_testing_questions" => "Pullik xizmatni tanlang, savol javob tizimidan to'liq ravishda foydalanish va kengroq imkoniyatlarni qo'lga kiritish uchun",

	// test/free-test page
	"questions" => "Savollar",
	"result" => "Natijalar",
	"test_questions_for_your_imagination" => "Tasavvur hosil qilish uchun testlar",
	//

	// test/premium-test page
	"Ваш результат" => "Sizning natijangiz",
	"go_to_the_next_test" => "Keyingi testga o'tish",
	"Результат" => "Natija",
	"print" => "Taxrirlash",
	//

	// test/premium-filter
	"premium_tests" => "Premium testlar",
	"in_direction_and_thematic" => "yo'nalish va mavzu bo'yicha",
	"we_serve_you_for_your_dreams!" => "Biz sizning orzularingiz uchun xizmat qilamiz!",
	"select_destination" => "Yo'nalish tanlang",
	"Direction" => "Yo'nalishlar",
	"Items" => "Fanlar",
	"Период" => "Oldinga",
	"school_for_schoolchildren" => "Maktab (maktab o'quvchilari uchun)",
	"applicant_college_lyceum" => "Talaba (Kollej/Litsey)",
	"Студент (университет)" => "Talaba (Institut)",
	"start_generating_tests" => "Test savollariga o'tish",
	"Complexity" => "Murakkablik",
	"Легко" => "Yengil",
 	"Нормальный" => "O'rta",
	"Сложный" => "Qiyin",
	"Эксперт" => "Mutaxassis",
	"Ваши результаты сохранены, и вы можете увидеть свои результаты в разделе «Мои результаты»." => "Sizning natijangiz saqlandi o'z natijalaringizni «Mening natijalarim» bo'limida ko'rishingiz mumkin.",
	"Результаты теста удалены, вы можете увидеть сохраненные результаты в разделе «Мои результаты»" => "Sizning test natijalaringiz o'chirildi, avval saqlangan natijalaringizni «Mening natijalarim» bo'limidan ko'rishingiz mumkin",

	// test/index page
	"visit_the_test_questions_page_to_select_a_convenient_tariff_plan" => "Test savollari sahifasiga kirish uchun o'zingizga </br>
	qulay bo'lgan ta'rifni tanlang",
	"online_tests" => "Online testlar",
	"types_of_payments" => "To'lov turlari",
	//

	// results/result page
	"uncorrect" => "Noto'g'ri javoblar",
	"correct" => "To'g'ri javoblar",
	"question" => "Savollar",
	"dateres" => "Vaqt",
	"savedres" => "Natija saqlandi",
	"Результат удален" => "Natija o'chirildi",
	"deleteres" => "Natijani o'chirish",
	"saveres" => "Natijani saqlash",
	"you_are_dont_answered_no_one_questions" => "Siz hechqaysi savolga javob kiritmadingiz!",
	"Сохранить результат" => "Natijani saqlash",
	"Удалит результат" => "Natijani o'chirish",
	"to_home" => "Bosh sahifaga",
	"Страница тарифов" => "Ta'riflar sahifasiga",
	"Неправильно" => "No to'g'ri",
	//

	// results/index page 
	"your_statistics" => "Sizning statistikangiz",
	"more" => "Batafsil",
	"rating" => "Reyting",
	"records" => "Rekordlar",
	"statistic_your_tests" => "Testlaringiz statistikasi",
	"learn_more_info" => "Yechgan testlaringizni kengroq va batafsilroq bilib olish imkoniyati",
	"statstics_of_test" => "Testlar statistikasi",
	"Предметы" => "Fanlar",
	"corrects" => "To'g'ri",
	"uncorrects" => "Noto'g'ri",
	"progress" => "Jarayon",
	"percent" => "Foiz",
	"date" => "Vaqt",
	"you_have_not_tried_any_tests_yet" => "Siz hali hechqanday test yechmagansiz",
	"register_to_see_your_results" => "Natijalaringizni ko'rish uchun Ro'yhattan o'ting!",
	"rating_your_results" => "Natijalaringizni reytingi",
	"diogramma" => "Aniq diogramma yordamida o'z natijalaringizni qanchalik o'sib yoki pasayib borayotganini ko'rishingiz mumkin",

	// materials/index page 
	"Привет" => "Salom",
	"Categories" => "Kategoryalar",
	"О странице" => "Bo'lim haqida",
	"В этом разделе вы можете использовать разные учебники, раздел также включает видео, аудио, электронные книги, статьи и является открытым исходным кодом для пользователей с статусом Premium" => "Bu bo'limda turli hildagi darsliklardan foydalanishingiz mumkin, bo'limda shuningdek video, audio, elektron kitoblar, maqolalar o'rin olgan bo'lib faqat Premium maqomiga ega bo'lgan foydalanuvchilar uchun ochiq manba hisoblanadi",
	"ОБЛАКО ТЕГОВ" => "TEGLAR BULUTI",
	"Video" => "Video",
	"Library" => "Kutubxona",
	"Audio" => "Audio",
	"Рекомендации" => "Maslahatlar",
	"Источники" => "Manbalar",
	"Видеоматериалы" => "Video maqolalar",
	"News" => "Yangiliklar",
	"Ratings" => "Reytinglar",
	"Tournaments" => "Turnirlar",
	"Events" => "Tadbirlar",
	"Tasks" => "Masalalar",
	//

	// materials/video page
	"В текущем моменте такую информация нету в базе" => "Hozirda bunday ma'lumot yo'q tez orada yuklanadi...",
	//

	// materials/find page
	"Учебные центры" => "O'quv markazlari",
	"Просмотреть все" => "Hammasini ko'rish",
	"Видео уроки" => "Video darsliklar",
	"Новые" => "Yangi",
	//

	// tarrifs/index page
	"Типы Платежей" => "To'lov turlari",
	"Тарифы" => "Tariflar",
	"Инфо" => "Info",
	"Статус" => "Maqomi",
	"Коротка" => "Qisqacha",
	"Подробно" => "Batafsil",
	"Стоимость" => "Narx",
	"Осталось" => "Qoldiq",
	"Время Окончания" => "Tugash vaqti",
	"Время Начала" => "Boshlangan vaqti",
	"Текущий Подписка" => "Joriy obuna",
	"Тарифны Статистики" => "Tarif statistikasi",
	"Дата" => "Sana",
	"Вы не активировали какие-либо тарифы в настоящее время" => "Hozirgi vaqtda siz hechqanday tarifni faollashtirmagansiz",
	"Внимание" => "Diqqat",
	"Перейти на страницу тарифов" => "Tariflar sahifasiga o'tish",
	// 

	// test/modal page 
	"Это описание недоступно" => "Bunday Tarif mavjud emas",
	"O тарифе" => "Tarif haqida",
	"Параметры" => "Parametrlar",
	"Названия" => "Nomi",
	"Заказать" => "Buyurtma qilish",
	"user" => "Foydalanuvchi",
	"settings" => "Sozlash",
	//

	// site/login page
	"Войдите, чтобы начать сеанс" => "Seansni boshlash uchun tizimga kiring",
	"Зарегистрировать" => "Ro'yhattan o'tish",
	"Запомни меня" => "Meni eslabqol",
	//

	// site/error page
	"Вернуться на главную" => "Bosh sahifaga qaytish",
	//

	// library/index page
	"скачать" => "yuklash",
	"empty_data" => "Ma'lumotlar bazasida bunday ma'lumot mavjud emas! Vaqtincha...",
	"Афтор" => "Muallif",
	"Названия" => "Nomi",
	"Год" => "Yil",
	"Страницы" => "Sahifa",
	"Скачать" => "Yuklash",
	"Книги" => "Kitoblar",
	"Фильтр" => "Saralash",
	//

	// site/user-info 
	"Пожалуйста, введите свою дополнительную информацию" => "Iltimos, qo'shimcha ma'lumotlaringizni kiriting",

	//

	// task/events page
	"attempts" => "Urunishlar",
	"events" => "Tadbirlar lentasi",
	"scores" => "Jamg'arilgan ball",
	"answer_task" => "Masalaga javob berdi",
	"show" => "Ko'rsatish",
	"all" => "hammasi",
	"unsolved" => "yechilmagan",
	"solved" => "yechilgan",
	"Tournament" => "Turnir",
	"unable" => "Bu ma'lumot hozircha bazada mavjud emas!",
	"Published" => "Namoyish qilingan",
	"Sent_by" => "Joyladi",
	"Complexity" => "Murakkablik",
	"grade" => "sinf",
	"score" => "ball",
	"topics" => "tema",
	"task" => "Masala",
	"task_solved" => "masalani yechildi",
	"all_attempts" => "barcha urunishlar",
	"answer_sent" => "Javob yuborilgan",
	"do_not_select_answer" => "Javobni kiritmadingiz Javobni to'liq matn yoki fayl ko'rinishida kiritishingiz mumkin",
	"full_answer" => "To'liq javob",
	"scrinshot_or_file" => "ScrinShot yoki Rasm yuklashingiz mumkin",
	"choose_file" => "fayl tanlang",
	"send" => "yuborish",
	"congratulation!" => "Tabriklaymiz! Masalani yechdingiz!",
	"answer" => "Javob:",
	"attempt" => "urinish:",
	"gived_scores" => "Jamg'arilgan ball:",
	"fulled_attempt" => "Bu masala inobatga olinmadi, chunki siz urunishlarni amalga oshirib bo'lgansiz!",
	"complaint" => "shikoyat",
	"idea_for_advice" => "Bu yerda siz masala bo'yicha o'z shikoyatingizni qoldirishingiz mumkin!",
	//

	// task/tournament page
	"part_one_the_end" => "shanba kungi bosqich yakunlandi... >> keyingi bosqich ertaga bo'lib o'tadi",
	"the_end" => "turnir nixoyasiga yetti natijalarni Dushanba kuni bilishingiz mumkin",
	"old" => "turnir yakunlangan",
	"the_date_of_the" => "O'tkashilish kuni",
	"start_at" => "boshlanadi",
	"to" => "gacha",
	"level_temp" => "murakkablik",
	"status_temp" => "holat",
	"active" => "faol",
	"past" => "o'tgan",
	"boss" => "tashkillashtruvchi",
	"sponsor" => "hamkor",
	//

	// task/rating page 
	"country" => "Mamlakat",
	"name" => "Ism",
	//

	// materials/news page
	"about_page" => "Sahifa haqida",
	"desc_page" => "Ushbu bo'limda siz turli xil qo'llanmalardan foydalanishingiz mumkin, shuningdek, bo'limda video, audio, elektron kitoblar, maqola va Premium maqomiga ega bo'lgan foydalanuvchilar uchun ochiq manba mavjud.",
	"cloud" => "TEGLAR BULUTI",
	"video" => "Video",
	"library" => "Kutubxona",
	"audio" => "Audio",
	"recomended" => "Tafsiyalar",
	"source" => "Manba",
	//

	// library/index page
	"filter" => "Saralash",
	"books" => "Kitoblar",
	"author" => "Muallif",
	"title" => "Nomi",
	"year" => "Yili",
	"pages" => "Sahifasi",
	"download" => "Yuklash",
	//

	// tariffs/index page
	"stats_tariff" => "Tarif Ma'lumoti",
	"off_time" => "Tugash Vaqti!",
	"current_order" => "Hozirgi Obuna",
	"start_time" => "Boshlanish Vaqti",
	"end_time" => "Tugash Vaqti",
	"residue" => "Qoldiq",
	"status" => "Holat",
	"attention" => "Diqqat",
	"You_have_not_activated_any_tariffs_at_the_moment" => "Hozirgi holatda tarifga obuna bo'lmagansiz",
	"info" => "info",
	"statusUp" => "Holat",
	"short" => "Qisqacha",
	"cost" => "Narxi",
	"enter_to_page" => "Tariflar sahifasiga o'tish",
	//

	// site/info page
	"change_info" => "Malumotni o'zgartirish",
	"username" => "Foydalanuvchi nomi",
	"password" => "Parol",
	"update" => "Yangilash",
	"settings_account" => "Profilni Sozlash",


];