<?php
namespace app\assets;

use yii2assets\printthis\PrintThisAsset as PrintThisParent;

class PrintThisChildAsset extends PrintThisParent
{
    public $sourcePath = '@bower/printThis';

    public $css = [
        '/css/styles.css',
	  ];

    public $js = [
        '//code.jquery.com/jquery-migrate-1.3.0.min.js',
		    'printThis.js',
	  ];

    public $depends = [
        'yii\web\JqueryAsset'
    ];
}