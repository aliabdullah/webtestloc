<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class TaskAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        // '/css/bootstrap.min.css',
        '/css/animate.css',
        '/css/owl.carousel.min.css',
        '/css/flaticon.css',
        '/css/task-page/styles.css',
        '/css/jquery.circliful.css',
        '/css/plugin-selectizing/dom.selectizing.css',
        '/css/ionicons.min.css',
        '/css/stylesDropDown.css',

        '/css/admin/adminstyles.css',
        '/css/admin/font-awesome.min.css',
        '/css/admin/ionicons.min.css',
        '/css/admin/AdminLTE.min.css',
        '/css/admin/_all-skins.min.css',

        '/css/material-page/style.css',
        // 'http://www.jqueryscript.net/css/jquerysctipttop.css',
        "/css/material-page/blog-sidebar/css/stylesec.css",
        // "/css/material-page/blog-sidebar/css/responsive.css",
        // "/css/material-page/single-post-2/css/resp.css",
        "/css/material-page/single-post-2/css/sty.css",

    ];
    public $js = [
        '/js/owl.carousel.min.js',
        '/js/scripts.js',
        'https://code.jquery.com/jquery-migrate-3.0.0.min.js',
        "https://unpkg.com/sweetalert/dist/sweetalert.min.js",
        '/js/jquery.circliful.min.js',
        '/js/jquery.foggy.js',
        '/js/jquery-blockUi.js',
        '/js/plugin-selectizing/dom.selectizing.js',
        // '/js/wow.js',
        '/js/scriptDropDown.js',
        

        '/js/admin/jquery-ui.js',
        // '/js/admin/raphael.min.js',
        '/js/admin/jquery.sparkline.min.js',
        '/js/admin/jquery-jvectormap-1.2.2.min.js',
        '/js/admin/jquery-jvectormap-world-mill-en.js',
        // '/js/admin/jquery.knob.min.js',
        // '/js/admin/moment.min.js',
        // '/js/admin/daterangepicker.js',
        // '/js/admin/bootstrap-datepicker.min.js',
        // '/js/admin/bootstrap3-wysihtml5.all.min.js',
        // '/js/admin/jquery.slimscroll.min.js',
        // '/js/admin/fastclick.js',
        '/js/admin/adminlte.min.js',
        // '/js/admin/dashboard.js',
        // '/js/fusioncharts.js',
        '/js/admin/demo.js',
        // '/js/admin/admin.js',

        '/js/matrial-page/script.js',
        '/js/matrial-page/timezz.js',
        '/js/matrial-page/jquery.marquee.min.js',
        // "/js/matrial-page/common-js/tether.min.js",
        // "/js/matrial-page/common-js/scripts.js",

    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
