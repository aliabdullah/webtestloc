<?php 

namespace app\models\user;

use Yii;
use yii\db\ActiveRecord;

class Auth extends ActiveRecord
{

	public function getUser()
	{
		return $this->hasOne(User::className(), ['id' => 'user_id']);
	}
}

