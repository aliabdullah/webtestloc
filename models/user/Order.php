<?php 

namespace app\models\user;

use Yii;
use yii\db\ActiveRecord;
use app\models\cours\Cours;

class Order extends ActiveRecord
{
	public static function tableName()
	{
		return 'orders';
	}

	public function rules()
	{
		return [
			['status', 'boolean'],
		];
	}

	const UNIQUE_KEY = "0486546a1265d2226861857d853249201a903c42161f8d48dda131ea819650ea079c18f3e679e9f4d1244717abc362a252a9918f6f47bf956ba1170da636715b";
    const MERCHANT_ID = "76b264627035af923b63e8272b6b5d9e70929141";

	public function getUser()
	{
		return $this->hasOne(User::className(), ['id' => 'user_id']);
	}

	public function getTarif()
	{
		return $this->hasOne(Cours::className(), ['id' => "tariff_id"]);
	}
}

