<?php 

namespace app\models\user;

use Yii;
use mdm\admin\models\User as UserModel;
use app\models\statistics\Statistics;

class User extends UserModel
{

	public function getResults()
	{
		return $this->hasMany(Statistics::className(), ['user_id' => 'id']);
	}

	public function getUseBlogs()
	{
		return $this->hasMany(UserStats::className(), ['user_id' => 'id']);
	}

	public function uploadFile()
    {
        if (is_object($this->photo)) {
            $name = 'uploads/users/' . md5($this->photo->baseName.rand())  . '.' . $this->photo->extension;
            if ($this->photo->saveAs($name)) {
                
                // if ($this->getOldAttribute("photo")) {
                //     is_file($this->getOldAttribute("photo")) && unlink($this->getOldAttribute("photo"));
                // }

                chmod($name, 0777);
                $this->photo = $name;
                return true;
            }
        }
        return false;
    }

}