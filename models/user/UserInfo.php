<?php

namespace app\models\user;

use Yii;

/**
 * This is the model class for table "user_info".
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property string $surname
 * @property string $email
 * @property string $gender
 * @property string $countries
 * @property string $birthday
 * @property int $phone_number
 */
class UserInfo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_info';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'phone_number'], 'integer'],
            [['name', 'surname', 'email', 'gender', 'countries', 'birthday'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'name' => Yii::t('app', 'Name'),
            'surname' => Yii::t('app', 'Surname'),
            'email' => Yii::t('app', 'Email'),
            'gender' => Yii::t('app', 'Gender'),
            'countries' => Yii::t('app', 'Countries'),
            'birthday' => Yii::t('app', 'Birthday'),
            'phone_number' => Yii::t('app', 'Phone Number'),
        ];
    }
}
