<?php 

namespace app\models\user;

use yii\db\ActiveRecord;
use app\models\statistics\Statistics;

Class UserStats extends ActiveRecord
{
	public static function tableName()
    {
        return 'userstats';
    }

    public function getPredmet()
    {
    	$this->hasMany(Statistics::className(), ['start_time' => 'start_time']);
    }
    
}