<?php

namespace app\models\user;

use Yii;


/**
 * This is the model class for table "users".
 *
 * @property integer $id
 * @property string $role
 * @property string $status
 * @property string $username
 * @property string $password
 * @property string $firstname
 * @property string $lastname
 * @property string $date
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'username', 'password', 'firstname', 'lastname', 'date', 'photo'], 'required'],
            [['date'], 'default', 'value' => date("Y-m-d H:1")],
            [['status'], 'string', 'max' => 15],
            ['role', 'safe'],
            [['username', 'firstname', 'lastname'], 'string', 'max' => 25],
            [['password'], 'string', 'max' => 32],
            ['photo', 'file', 'extensions' => 'png, jpeg, jpg'],
            ['rememberMe', 'boolean'],
        ];
    }

    public $rememberMe = false;

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'role' => Yii::t('app', 'Role'),
            'status' => Yii::t('app', 'Status'),
            'username' => Yii::t('app', 'Username'),
            'password' => Yii::t('app', 'Password'),
            'firstname' => Yii::t('app', 'Firstname'),
            'lastname' => Yii::t('app', 'Lastname'),
            'date' => Yii::t('app', 'Date'),
            'photo' => Yii::t('app', 'Foto')
        ];
    }

    const STATUS_NEW = 'new';

     /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return self::findOne(['id' => $id]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return self::findOne(['username' => $username]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === $password;
    }

    

    public function reg() 
    {
        $this->status = self::STATUS_NEW;
        $this->role = 'admin';
        $this->photo = 'foto';
        $this->date = date("Y-m-d H:i");
        $this->password = self::generatePassword($this->password);
        if ($this->insert()) {
            return Yii::$app->user->login($this);
        } else 
            return false;
    }

    public function login() 
    {
        $user = static::findOne(['username' => $this->username, 'password' => self::generatePassword($this->password)]);

        if ($user) {
            return Yii::$app->user->login($user, $this->rememberMe ? 3600*24*30 : 0);
        } else {
            $this->addError("password", "Parol yoki login notugri");
            return false;
        }
    }

     /**
     * @param $authKey
     * @return string
     */
    public static function generatePassword($password)
    {
        return md5($password);
    }

    public function uploadFile()
    {
        if (is_object($this->photo)) {
            $name = 'uploads/users/' . md5($this->photo->baseName.rand())  . '.' . $this->photo->extension;
            if ($this->photo->saveAs($name)) {

                /**
                 * @var $img SimpleImage
                 */
                $img = \Yii::$app->image;

                $img->load($name)->thumbnail(150, 150)->save($name);

                if ($this->getOldAttribute("photo")) {
                    is_file($this->getOldAttribute("photo")) && unlink($this->getOldAttribute("photo"));
                }

                chmod($name, 0777);
                $this->photo = $name;
                return true;
            }
        }
        return false;
    }

    public static function authUlogin($data)
    {
        $user = new self();
        $user->auth_key = self::generatePassword($data['network']."-".$data['uid']);
        $user->username = isset($data['email']) ? $data['email'] : false;

        // check user by auth username
        if ($user->username) {
            $oldUser = self::findOne(['username' => $user->username]);
            if ($oldUser) {
                Yii::$app->user->login($oldUser);
                return $oldUser;
            }
        }

        // check user by auth key
        $oldUser = self::findOne(['auth_key' => $user->auth_key]);
        if ($oldUser) {
            Yii::$app->user->login($oldUser);
            return $oldUser;
        }

        $user->status = self::STATUS_NEW;
        $user->date = date("Y-m-d H:i");
        $user->username = $data['email'];
        $user->firstname = $data['first_name'];
        $user->lastname = $data['first_name'];
        $user->lastname = $data['first_name'];
        $user->password = self::generatePassword($data['network']."-".$data['uid']);

        if ($data['photo_big']) {
            $file = "uploads/users/".$data['network']."-".$data['uid'].".jpg";
            file_put_contents($file, file_get_contents($data['photo_big']));
            $user->photo = $file;
        }

        if ($user->save(false)) {
            return $user;
        } else {
            return false;
        }
    }
}
