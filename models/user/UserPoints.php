<?php

namespace app\models\user;

use Yii;

/**
 * This is the model class for table "user_points".
 *
 * @property int $id
 * @property int $user_id
 * @property string $subject
 * @property int $point
 */
class UserPoints extends \yii\db\ActiveRecord
{

    const SUBJECT_TOURNAMENT = 'tournament';
    const SUBJECT_TASK = 'task';
    const SUBJECT_TEST = 'test';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_points';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'point'], 'integer'],
            [['subject'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'subject' => Yii::t('app', 'Subject'),
            'point' => Yii::t('app', 'Point'),
        ];
    }
}
