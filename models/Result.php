<?php 

namespace app\models;

use yii\db\ActiveRecord;

Class Result extends ActiveRecord
{
	public static function tableName(){
		return 'statistics';
	}

	public function addResults($quest_id, $pred_name, $var_id, $user)
	{

		if (isset($_SESSION['result'][$user->username][$pred_name][$quest_id])) {
			$old = $_SESSION['result'][$user->username][$pred_name][$quest_id];
			$_SESSION['result'][$user->username][$pred_name][$quest_id] = $var_id;

		}elseif (isset($_SESSION['result'][$user->username][$pred_name])) {
			$_SESSION['result'][$user->username][$pred_name][$quest_id] = $var_id;
		
		}elseif (!isset($_SESSION['result'][$user->username][$pred_name])) {
			$_SESSION['result'][$user->username][$pred_name] = [$quest_id => $var_id];

		}else {
			$_SESSION['result'] = [
                'user_id' => $user->id,
                'startTime' => date('H:i:s'),
                'stopTime' => 0,
                'max_ball' => 0,
                $user->username => [
                        $pred_name => [ $quest_id => $var_id ],
                    ],
            ];
		}
		return $_SESSION['result'];

	}
	
}