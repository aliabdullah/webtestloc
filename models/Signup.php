<?php 

namespace app\models;

use Yii;
use mdm\admin\models\form\Signup as SignupForm;

class Signup extends SignupForm
{
    public $photo;
    public $username;
    public $email;
	public $password;


	public function uploadFile()
    {
        if (is_object($this->photo)) {
            $name = 'uploads/users/' . md5($this->photo->baseName.rand())  . '.' . $this->photo->extension;
            if ($this->photo->saveAs($name)) {
                
                // if ($this->getOldAttribute("photo")) {
                //     is_file($this->getOldAttribute("photo")) && unlink($this->getOldAttribute("photo"));
                // }

                chmod($name, 0777);
                $this->photo = $name;
                return true;
            }
        }
        return false;
    }


    public function signup()
    {
        if ($this->validate()) {
            $user = new User();
            $user->username = $this->username;
            $user->email = $this->email;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            $user->photo = $this->photo;
            if ($user->save()) {
                return $user;
            }
        }

        return null;
    }


    public function upload($user)
    {
        if ($this->validate()) {
            $user = User::findOne(['id' => $user->id]);
            $user->username = $this->username;
            $user->email = $this->email;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            $user->photo = $this->photo;
            if ($user->save()) {
                return $user;
            }
        }

        return null;
    }
	
}