<?php  

namespace app\models;

use app\models\Comments;
use yii\base\Model;

Class CommentForm extends Model
{

	public $comment;

	public function rules()
	{
		return [
			['comment', 'safe'],
			[['comment'], 'string', 'length' => [3,250]]

		];
	}

	public function saveComment($task_id)
	{
		$comment = new Comments();
		$comment->text = $this->comment;
		$comment->user_id = \Yii::$app->user->id;
		$comment->task_id = $task_id;
		$comment->status = 1;
		$comment->date = date('Y-m-d H:i:s');
		return $comment->save();
	}
}