<?php

namespace app\models\task;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\task\Tournament;

/**
 * TournamentSearch represents the model behind the search form of `app\models\task\Tournament`.
 */
class TournamentSearch extends Tournament
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'category_id', 'difficulty_lev'], 'integer'],
            [['name', 'descrip', 'day', 'time', 'status', 'rules', 'sponsors'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Tournament::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'category_id' => $this->category_id,
            'day' => $this->day,
            'difficulty_lev' => $this->difficulty_lev,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'descrip', $this->descrip])
            ->andFilterWhere(['like', 'time', $this->time])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'rules', $this->rules])
            ->andFilterWhere(['like', 'sponsors', $this->sponsors]);

        return $dataProvider;
    }
}
