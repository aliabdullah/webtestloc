<?php

namespace app\models\task;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\task\Tasks;

/**
 * TaskSearch represents the model behind the search form of `app\models\task\Tasks`.
 */
class TaskSearch extends Tasks
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'cat_id', 'level', 'class', 'bal'], 'integer'],
            [['sentBy_id', 'task', 'short_answ', 'source', 'theme', 'date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $id=null)
    {
        if ($id) {
            $query = Tasks::find()->where(['tournament' => $id])->andWhere(['type' => Tasks::TYPE_TOURNAMENT]);
        }else{
            $query = Tasks::find()->where(['type' => Tasks::TYPE_TASK]);
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'cat_id' => $this->cat_id,
            'level' => $this->level,
            'class' => $this->class,
            'bal' => $this->bal,
            'date' => $this->date,
        ]);

        $query->andFilterWhere(['like', 'sentBy_id', $this->sentBy_id])
            ->andFilterWhere(['like', 'task', $this->task])
            ->andFilterWhere(['like', 'short_answ', $this->short_answ])
            ->andFilterWhere(['like', 'source', $this->source])
            ->andFilterWhere(['like', 'theme', $this->theme]);

        return $dataProvider;
    }
}
