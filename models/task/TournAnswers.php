<?php

namespace app\models\task;

use Yii;

/**
 * This is the model class for table "tourn_answers".
 *
 * @property int $id
 * @property int $user_id
 * @property int $tourn_id
 * @property int $task_id
 * @property string $answer
 * @property string $file
 * @property int $true
 * @property string $time
 */
class TournAnswers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tourn_answers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'tourn_id', 'task_id', 'true'], 'integer'],
            [['answer'], 'string'],
            [['time'], 'safe'],
            [['file'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'tourn_id' => Yii::t('app', 'Tourn ID'),
            'task_id' => Yii::t('app', 'Task ID'),
            'answer' => Yii::t('app', 'Answer'),
            'file' => Yii::t('app', 'File'),
            'true' => Yii::t('app', 'True'),
            'time' => Yii::t('app', 'Time'),
        ];
    }

    public function uploadFile()
    {
        if (is_object($this->file)) {
            $name = 'uploads/task/answer/' . md5($this->file->baseName.rand())  . '.' . $this->file->extension;

            // /**
            //  * @var $img SimpleImage
            //  */
            // $img = \Yii::$app->image;

            // $img->load($name)->thumbnail(150, 150)->save($name);

            if ($this->file->saveAs($name)) {
                if ($this->getOldAttribute("file")) {
                    is_file($this->getOldAttribute("file")) && unlink($this->getOldAttribute("file"));
                }

                chmod($name, 0777);
                $this->file = $name;
                return true;
            }
        }
        return false;
    }
}
