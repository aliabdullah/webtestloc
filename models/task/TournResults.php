<?php

namespace app\models\task;

use Yii;

/**
 * This is the model class for table "tourn_results".
 *
 * @property int $id
 * @property int $user_id
 * @property string $country
 * @property int $score
 * @property int $place
 * @property string $date
 */
class TournResults extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tourn_results';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'score', 'place'], 'integer'],
            [['date'], 'safe'],
            [['country'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'country' => Yii::t('app', 'Country'),
            'score' => Yii::t('app', 'Score'),
            'place' => Yii::t('app', 'Place'),
            'date' => Yii::t('app', 'Date'),
        ];
    }
}
