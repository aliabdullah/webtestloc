<?php

namespace app\models\task;

use Yii;

/**
 * This is the model class for table "user_complaint".
 *
 * @property int $id
 * @property int $user_id
 * @property int $task_id
 * @property string $complaint
 * @property int $checked
 * @property string $date
 */
class UserComplaint extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_complaint';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'task_id', 'checked'], 'integer'],
            [['complaint'], 'string'],
            [['date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'task_id' => Yii::t('app', 'Task ID'),
            'complaint' => Yii::t('app', 'Complaint'),
            'checked' => Yii::t('app', 'Checked'),
            'date' => Yii::t('app', 'Date'),
        ];
    }
}
