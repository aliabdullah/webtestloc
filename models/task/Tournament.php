<?php

namespace app\models\task;

use Yii;

/**
 * This is the model class for table "tournament".
 *
 * @property int $id
 * @property int $category_id
 * @property string $name
 * @property string $descrip
 * @property string $day
 * @property string $time
 * @property string $status
 * @property int $difficulty_lev
 * @property string $rules
 * @property string $sponsors
 */
class Tournament extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tournament';
    }

    const SCORE_0 = '0';
    const SCORE_1 = '1';
    const SCORE_2 = '2';
    const SCORE_3 = '3';
    const SCORE_4 = '4';
    const SCORE_5 = '5';
    const SCORE_6 = '6';
    const SCORE_7 = '7';
    const SCORE_8 = '8';
    const SCORE_9 = '9';
    const SCORE_10 = '10';

    const RULE = 'Inspiring Education';
    const SPONSOR = 'Inspiring Education';

    const STATUS_ACTIVE = "active";
    const STATUS_DESACTIVE = "desactive";
    const STATUS_PAST = "past";

    const LANG_UZ = 'uz';
    const LANG_RU = 'ru';
    const LANG_CRYL = 'cryl';


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lang', 'name', 'time', 'status', 'day', 'descrip'], 'required'],
            [['category_id', 'difficulty_lev'], 'integer'],
            [['descrip'], 'string'],
            [['name', 'time', 'status', 'day', 'rules', 'sponsors'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'category_id' => Yii::t('app', 'Категория'),
            'name' => Yii::t('app', 'Имя'),
            'descrip' => Yii::t('app', 'Описание'),
            'day' => Yii::t('app', 'День'),
            'time' => Yii::t('app', 'Время'),
            'status' => Yii::t('app', 'Положение'),
            'difficulty_lev' => Yii::t('app', 'Сложный уровень'),
            'rules' => Yii::t('app', 'Правила'),
            'sponsors' => Yii::t('app', 'Спонсоры'),
        ];
    }

    public function getTasks()
    {
        return $this->hasMany(Tasks::className(), ['tournament' => 'id'])->andWhere(['type' => 'tournament']);
    }

    public static function getDiffLevel()
    {
        return [
            self::SCORE_0 => '0',
            self::SCORE_1 => '1',
            self::SCORE_2 => '2',
            self::SCORE_3 => '3',
            self::SCORE_4 => '4',
            self::SCORE_5 => '5',
            self::SCORE_6 => '6',
            self::SCORE_7 => '7',
            self::SCORE_8 => '8',
            self::SCORE_9 => '9',
            self::SCORE_10 => '10'
        ];
    }

    public function getStatus()
    {
        return [
            self::STATUS_ACTIVE => "active",
            self::STATUS_DESACTIVE => "desactive",
            // STATUS_PAST => "past",
        ];
        
    }

    public function getLang()
    {
        return [
            self::LANG_UZ => 'uz',
            self::LANG_RU => 'ru',
            self::LANG_CRYL => 'cryl',
        ];
    }
}
