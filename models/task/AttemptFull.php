<?php

namespace app\models\task;

use Yii;

/**
 * This is the model class for table "attempt_full".
 *
 * @property int $id
 * @property int $attempt_id
 * @property string $full_answ
 * @property string $file
 */
class AttemptFull extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'attempt_full';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['attempt_id'], 'integer'],
            [['full_answ'], 'string'],
            [['file'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'attempt_id' => Yii::t('app', 'Attempt ID'),
            'full_answ' => Yii::t('app', 'Full Answ'),
            'file' => Yii::t('app', 'File'),
        ];
    }

    public function uploadFile()
    {
        if (is_object($this->file)) {
            $name = 'uploads/task/answer/' . md5($this->file->baseName.rand())  . '.' . $this->file->extension;

            // /**
            //  * @var $img SimpleImage
            //  */
            // $img = \Yii::$app->image;

            // $img->load($name)->thumbnail(150, 150)->save($name);

            if ($this->file->saveAs($name)) {
                if ($this->getOldAttribute("file")) {
                    is_file($this->getOldAttribute("file")) && unlink($this->getOldAttribute("file"));
                }

                chmod($name, 0777);
                $this->file = $name;
                return true;
            }
        }
        return false;
    }

    public function getAttempt()
    {
        return $this->hasOne(Attempts::className(), ['id' => 'attempt_id']);
    }
}
