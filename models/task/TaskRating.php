<?php

namespace app\models\task;

use Yii;
use app\models\user\User;

/**
 * This is the model class for table "task_rating".
 *
 * @property int $id
 * @property int $user_id
 * @property int $attempt
 * @property string $date
 */
class TaskRating extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'task_rating';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'attempt'], 'integer'],
            [['date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'attempt' => Yii::t('app', 'Attempt'),
            'date' => Yii::t('app', 'Date'),
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
