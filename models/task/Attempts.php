<?php

namespace app\models\task;

use Yii;
use app\models\task\Tasks;
use app\models\user\User;

/**
 * This is the model class for table "attempts".
 *
 * @property int $id
 * @property int $user_id
 * @property int $task_id
 * @property int $attempt
 */
class Attempts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'attempts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'task_id'], 'required'],
            [['user_id', 'task_id', 'attempt'], 'integer'],
            ['attempt', 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'task_id' => Yii::t('app', 'Task ID'),
            'attempt' => Yii::t('app', 'Attempt'),
        ];
    }

    public function getFullAttempts()
    {
        return $this->hasMany(AttemptFull::className(), ['attempt_id' => 'id']);
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getTask()
    {
        return $this->hasOne(Tasks::className(), ['id' => 'task_id']);
    }
}
