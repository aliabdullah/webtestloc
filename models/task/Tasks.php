<?php

namespace app\models\task;

use Yii;
use app\models\user\User;
use app\models\category\Category;
use app\models\Comments;

/**
 * This is the model class for table "tasks".
 *
 * @property int $id
 * @property int $cat_id
 * @property string $sentBy_id
 * @property string $task
 * @property string $short_answ
 * @property string $source
 * @property int $level
 * @property int $class
 * @property int $bal
 * @property string $theme
 * @property string $date
 */
class Tasks extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tasks';
    }

    const TYPE_TOURNAMENT = 'tournament';
    const TYPE_TASK = 'task';

    const LANG_UZ = 'uz';
    const LANG_RU = 'ru';
    const LANG_CRYL = 'cryl';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cat_id', 'level', 'sentBy_id', 'class', 'bal', 'tournament'], 'integer'],
            [['sentBy_id', 'task', 'type', 'name', 'lang'], 'required'],
            [['task'], 'string'],
            [['date'], 'safe'],
            [['short_answ', 'source', 'theme'], 'string', 'max' => 255],
            [['tournament'], 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'cat_id' => Yii::t('app', 'Категория'),
            'sentBy_id' => Yii::t('app', 'Отправлено от'),
            'task' => Yii::t('app', 'Задача'),
            'short_answ' => Yii::t('app', 'Короткий ответ'),
            'source' => Yii::t('app', 'Источник'),
            'level' => Yii::t('app', 'Уровень'),
            'class' => Yii::t('app', 'Класс'),
            'bal' => Yii::t('app', 'Балл'),
            'theme' => Yii::t('app', 'Тема'),
            'date' => Yii::t('app', 'Дата'),
            'type' => Yii::t('app', 'Тип'),
            'tournament' => Yii::t('app', 'Турнир ID'),
        ];
    }

    public function getEditor()
    {
        return $this->hasOne(User::className(), ['id' => 'sentBy_id']);
    }

    public static function getMainCat($id)
    {
        $cat = Category::findOne($id);
        if (!empty($cat)) {
            if ($cat->parent_id == 0) {
                $catName = $cat->name;
            }elseif ($cat->parent_id != 0) {
                $parent = Category::findOne($cat->parent_id);
                if ($parent->parent_id == 0) {
                    $catName = $parent->name;
                }elseif ($parent->parent_id != 0) {
                    $sub_parent = Category::findOne($parent->parent_id);
                    if ($sub_parent->parent_id == 0) {
                        $catName = $sub_parent->name;
                    }
                }
            }else {
                return false;
            }
            return $catName;
        }
    }

    public function getComments()
    {
        return $this->hasMany(Comments::className(), ['task_id' => 'id'])->orderBy('id DESC')->andWhere(['status' => 1]);
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'sentBy_id']);
    }

    public function getLang()
    {
        return [
            self::LANG_UZ => 'uz',
            self::LANG_RU => 'ru',
            self::LANG_CRYL => 'cryl',
        ];
    }
}
