<?php

namespace app\models\image;

use Yii;

/**
 * This is the model class for table "image".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $question_id
 * @property integer $blog_id
 * @property string $photo
 * @property integer $main
 */
class Image extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'image';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'question_id', 'blog_id', 'main'], 'integer'],
            [['photo'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'question_id' => Yii::t('app', 'Question ID'),
            'blog_id' => Yii::t('app', 'Blog ID'),
            'photo' => Yii::t('app', 'Photo'),
            'main' => Yii::t('app', 'Main'),
        ];
    }
}
