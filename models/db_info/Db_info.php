<?php

namespace app\models\db_info;

use Yii;
use app\models\category\Category;
use app\models\user\User;

/**
 * This is the model class for table "db_info".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property string $name
 * @property string $keywords
 * @property string $description
 */
class Db_info extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'db_info';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'name', 'keywords', 'description', 'type', 'lang'], 'required'],
            [['parent_id'], 'integer'],
            ['photo', 'string'],
            [['name', 'keywords', 'description', 'type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'parent_id' => Yii::t('app', 'Родительский ID'),
            'category_id' => Yii::t('app', 'Категория ID'),
            'name' => Yii::t('app', 'Имя'),
            'keywords' => Yii::t('app', 'Ключевые слова'),
            'description' => Yii::t('app', 'Описание'),
        ];
    }


    const DATA_VIDEO = "video";
    const DATA_AUDIO = "audio";
    const DATA_POST = "post";
    const DATA_LIBRARY = "library";
    const DATA_LINK = "link";
    const DATA_RECOMENDED = "recomended";

    const RU = "ru";
    const UZ = "uz";

    public static function getLang()
    {
        return [
            self::RU => "ru",
            self::UZ => "uz",
        ];
    }
    

    public function getCat() {
        return $this->hasOne(Category::className(), ['id' => 'parent_id']);
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }


    public function uploadFile()
    {
        if (is_object($this->photo)) {
            $name = 'uploads/materials/' . md5($this->photo->baseName.rand())  . '.' . $this->photo->extension;
            if ($this->photo->saveAs($name)) {
                
                if ($this->getOldAttribute("photo")) {
                    is_file($this->getOldAttribute("photo")) && unlink($this->getOldAttribute("photo"));
                }

                chmod($name, 0777);
                $this->photo = $name;
                return true;
            }
        }
        return false;
    }

    public static function getTypes()
    {
        return [
            self::DATA_POST => "post",
            self::DATA_LIBRARY => "library",
            self::DATA_LINK => "link",
            self::DATA_VIDEO => "video",
            self::DATA_AUDIO => "audio",
            self::DATA_RECOMENDED => "recomended",
        ];
    }


}
