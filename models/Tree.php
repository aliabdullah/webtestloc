<?php

namespace app\models;

use Yii;
use \kartik\tree\models\Tree as MainTree;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * This is the model class for table "tree".
 *
 * @property integer $id
 * @property integer $root
 * @property integer $lft
 * @property integer $rgt
 * @property integer $lvl
 * @property string $name
 * @property string $icon
 * @property integer $icon_type
 * @property integer $active
 * @property integer $selected
 * @property integer $disabled
 * @property integer $readonly
 * @property integer $visible
 * @property integer $collapsed
 * @property integer $movable_u
 * @property integer $movable_d
 * @property integer $movable_l
 * @property integer $movable_r
 * @property integer $removable
 * @property integer $removable_all
 */
class Tree extends MainTree
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tree';
    }

    public function attributeLabels()
    {
        return [
            'name' => Yii::t("app", "tree_name"),
            'icon' => Yii::t("app", "tree_icon")
        ];
    }

    /**
     * @return Post[]
     */
    public function getLastPosts()
    {
        if (Yii::$app->language == "uz-UZ") {
            return $this->hasMany(Post::className(), ['cat_id' => 'id'])
                ->where(['lang' => 'uz'])
                ->limit(3)
                ->orderBy("id desc");
        } else {
            return $this->hasMany(Post::className(), ['cat_id' => 'id'])
                ->where(['lang' => 'ru'])
                ->limit(3)
                ->orderBy("id desc");
        }
    }

    public function getLink() {
        return Url::toRoute(['post/index', 'category' => $this->id]);
    }

    public function getMobLink()
    {
        return Url::toRoute(['post/mobil', 'category' => $this->id]);
    }

    public function getName() {
        if (Yii::$app->language == "uz-UZ") {
            return $this->name;
        } else {
            return $this->icon;
        }
    }

    // relations
    public function getPosts(){
        return $this->hasMany(Post::className(), ['cat_id'=>'id']);
    }

    public function getCats() {
        return ArrayHelper::map(\app\models\Tree::findOne(['name' => 'Cats'])->children()->all(), 'id', 'name');
    }
}
