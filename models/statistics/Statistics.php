<?php

namespace app\models\statistics;

use Yii;

/**
 * This is the model class for table "statistics".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $category_id
 * @property integer $cours_id
 * @property string $start_time
 * @property string $stop_time
 * @property integer $correct_answer
 * @property integer $wrong_answer
 * @property string $level
 * @property string $date
 */
class Statistics extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'statistics';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'correct_answer', 'wrong_answer'], 'integer'],
            [['start_time', 'stop_time', 'date'], 'safe'],
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'Пользователь ID'),
            'start_time' => Yii::t('app', 'Время начала'),
            'stop_time' => Yii::t('app', 'Остановить время'),
            'correct_answer' => Yii::t('app', 'Правильный ответ'),
            'wrong_answer' => Yii::t('app', 'Неверный ответ'),
            'date' => Yii::t('app', 'Дата'),
        ];
    }
}
