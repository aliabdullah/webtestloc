<?php

namespace app\models\statistics;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\statistics\Statistics;

/**
 * StatisticsSearch represents the model behind the search form about `app\models\statistics\Statistics`.
 */
class StatisticsSearch extends Statistics
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'category_id', 'cours_id', 'correct_answer', 'wrong_answer'], 'integer'],
            [['start_time', 'stop_time', 'level', 'date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Statistics::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'category_id' => $this->category_id,
            'cours_id' => $this->cours_id,
            'start_time' => $this->start_time,
            'stop_time' => $this->stop_time,
            'correct_answer' => $this->correct_answer,
            'wrong_answer' => $this->wrong_answer,
            'date' => $this->date,
        ]);

        $query->andFilterWhere(['like', 'level', $this->level]);

        return $dataProvider;
    }
}
