<?php

namespace app\models\category;

use Yii;
use app\components\helpers\SimpleImage;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use app\models\tests\Tests;
use app\models\category\SortList;


/**
 * This is the model class for table "Category".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property string $name
 * @property string $keywords
 * @property string $description
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'name', 'keywords', 'description'], 'required'],
            [['parent_id'], 'default', 'value' => 0],
            ['test_count', 'safe'],
            [['name', 'keywords', 'description'], 'string', 'max' => 255],
            ['photo', 'file', 'extensions' => 'png, jpeg, jpg'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'parent_id' => Yii::t('app', 'Родительский ID'),
            'name' => Yii::t('app', 'Имя'),
            'keywords' => Yii::t('app', 'Ключевые слова'),
            'description' => Yii::t('app', 'Описание'),
        ];
    }

    public function uploadFile()
    {
        if (is_object($this->photo)) {
            $name = 'uploads/category/' . md5($this->photo->baseName.rand())  . '.' . $this->photo->extension;
            if ($this->photo->saveAs($name)) {
                
                if ($this->getOldAttribute("photo")) {
                    is_file($this->getOldAttribute("photo")) && unlink($this->getOldAttribute("photo"));
                }

                chmod($name, 0777);
                $this->photo = $name;
                return true;
            }
        }
        return false;
    }

    public function getParentCats() 
    {

        return ArrayHelper::map(\app\models\category\Category::findAll(['parent_id' => '0']), 'id', 'name');
    }

    public function getParent()
    {
        return $this->hasOne(self::className(), ['id' => $this->parent_id]);
    }

    public static function getList()
    {
        $data = static::find()
            ->select(['id', 'parent_id', 'name'])
            ->orderBy('parent_id ASC')
            ->asArray()
            ->all();

        $sort = new SortList([
                'data' => $data,
                'prefix' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
        ]);
        $sortList = ArrayHelper::map($sort->getList(), 'id', 'name');
        
        return $sortList;
    }

    public static function findCatPosition($id)
    {
        $cat = static::findOne($id);
        if (!empty($cat)) {
            if ($cat->parent_id == 0) {
                $position = 'main';
            }elseif ($cat->parent_id != 0) {
                $parent = static::findOne($cat->parent_id);
                if ($parent->parent_id == 0) {
                    $position = 'firstSub';
                }elseif ($parent->parent_id != 0) {
                    $sub_parent = static::findOne($parent->parent_id);
                    if ($sub_parent->parent_id == 0) {
                        $position = 'twoSub';
                    }
                }
            }else {
                return false;
            }
            return $position;
        }
    }


    public function getTree()
    {
        $cats = Category::find()->indexBy('id')->asArray()->all();
        $tree = [];
        foreach ($cats as $id => &$cat) {
            if (!$cat['parent_id']) {
                $tree[$id] = &$cat;
            }else {
                $cats[$cat['parent_id']]['childs'][$cat['id']] = &$cat;
            }
        }
        
        return $tree;
    }

    public function getCats() 
    {
        return self::find()->where(['parent_id' => 0])->all();
    }

    // public function getAllCats() 
    // {
    //     return $this->hasMany(Category::className(), ['parent_id' => 'id']);
    // }

    public function getLink()
    {
        return Url::toRoute(['/test/index', 'category_id' => $this->id]);
    }

    public function getLinkAdmin()
    {
        return Url::toRoute(['/admin/testblog/catview', 'category_id' => $this->id]);
    }

    public function getPhoto()
    {
        return is_file($this->photo) ? "/".$this->photo : false;
    }

    public function getCatTests()
    {
        return $this->hasMany(Tests::className(), ['category_id' => $this->id]);
    }

    public function getTests()
    {
        return $this->hasMany(Tests::className(), ['category_id' => $this->id]);
    }


}
