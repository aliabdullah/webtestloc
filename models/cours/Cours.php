<?php

namespace app\models\cours;

use Yii;

/**
 * This is the model class for table "cours".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $test_id
 * @property string $title
 * @property string $content
 * @property integer $cost
 * @property string $photo
 */
class Cours extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cours';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'status', 'title', 'content', 'cost', 'photo', 'lang'], 'required'],
            [['cost'], 'integer'],
            [['test_id'], 'safe'],
            [['title',  'content'], 'string', 'max' => 255],
            ['photo', 'file', 'extensions' => 'png, jpeg, jpg']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'Пользователь ID'),
            'test_id' => Yii::t('app', 'Тест ID'),
            'lang' => Yii::t('app', 'Язык'),
            'title' => Yii::t('app', 'Заглавие'),
            'content' => Yii::t('app', 'Содержание'),
            'cost' => Yii::t('app', 'Стоимость'),
            'photo' => Yii::t('app', 'Фото'),
        ];
    }

    const TYPE_BASE = "Базовый";
    const TYPE_ADVANCED = "Продвинутый";
    const TYPE_PREMIUM = "Премиум";
    const TYPE_PLATINUM = "Платинум";

    const RU = "ru";
    const UZ = "uz";

     public function uploadFile()
    {
        if (is_object($this->photo)) {
            $name = 'uploads/cours/' . md5($this->photo->baseName.rand())  . '.' . $this->photo->extension;
            if ($this->photo->saveAs($name)) {

                /**
                 * @var $img SimpleImage
                 */
                

                if ($this->getOldAttribute("photo")) {
                    is_file($this->getOldAttribute("photo")) && unlink($this->getOldAttribute("photo"));
                }

                chmod($name, 0777);
                $this->photo = $name;
                return true;
            }
        }
        return false;
    }

    public static function getStatus()
    {
        return [
            self::TYPE_BASE => "Базовый",
            self::TYPE_ADVANCED => "Продвинутый",
            self::TYPE_PREMIUM => "Премиум",
            self::TYPE_PLATINUM => "Платинум",
        ];
    }

    public static function getLang()
    {
        return [
            self::RU => "ru",
            self::UZ => "uz",
        ];
    }

    public function getCours()
    {   
        if (Yii::$app->language == 'ru') {
            return self::find()->where(['lang' => 'ru'])->all();
        }else {
            return self::find()->where(['lang' => 'uz'])->all();
        }
    }
}
