<?php

namespace app\models\library;

use Yii;
use app\models\category\Category;


/**
 * This is the model class for table "library".
 *
 * @property int $id
 * @property string $author
 * @property string $name
 * @property int $year
 * @property int $page
 * @property string $link
 */
class Library extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'library';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['author', 'name', 'lang'], 'required'],
            [['year', 'page', 'cat_id'], 'integer'],
            [['author', 'name', 'lang', 'link', 'djvu', 'pdf', 'ps', 'html', 'TeX'], 'string', 'max' => 255],
            ['djvu', 'file', 'extensions' => ['djvu', 'djv']],
            ['pdf', 'file', 'extensions' => ['pdf']],
            ['ps', 'file', 'extensions' => ['ps']],
            ['html', 'file', 'extensions' => ['html']],
            ['TeX', 'file', 'extensions' => ['TeX']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'author' => Yii::t('app', 'Автор'),
            'name' => Yii::t('app', 'Имя'),
            'year' => Yii::t('app', 'Год'),
            'page' => Yii::t('app', 'Страница'),
            'link' => Yii::t('app', 'Ссылка'),
        ];
    }

    const RU = 'ru';
    const UZ = 'uz';
    const OTHERS = 'others';

    public function getLang()
    {
        return [
            self::RU => 'ru',
            self::UZ => 'uz',
            self::OTHERS => 'others'
        ];
    }

    public function getCats()
    {
    //     $cats = Category::find()->where(['parent_id' => 0])->all();
    //     $arr = [];
    //     foreach ($cats as $key => $cat) {
    //         $subCats = Category::find()->where(['parent_id' => $cat->id])->all();
    //         // $arr[$cat->id] = $cat->name;
    //         foreach ($subCats as $key => $value) {
    //             $arr[[$cat->id] = $cat->name][$value->id] = $value->name;
    //         }
    //     }
    //     // echo '<pre>';
    //     // print_r($arr);
    //     // echo '</pre>';
    //     // exit();
    //     return $arr;
    }

    public function uploadFile()
    {
        if (is_object($this->djvu) || is_object($this->pdf) || is_object($this->ps) || is_object($this->html) || is_object($this->TeX)) {
            
            if (is_object($this->djvu) || is_object($this->djv)) {
                $djvu = 'uploads/library/djvu/' . md5($this->djvu->baseName.rand())  . '.' . $this->djvu->extension;
            }    
            if (is_object($this->pdf)) {
                $pdf = 'uploads/library/pdf/' . md5($this->pdf->baseName.rand())  . '.' . $this->pdf->extension;
            }
            if (is_object($this->ps)) {
                $ps = 'uploads/library/ps/' . md5($this->ps->baseName.rand())  . '.' . $this->ps->extension;
            }
            if (is_object($this->html)) {
                $html = 'uploads/library/html/' . md5($this->html->baseName.rand())  . '.' . $this->html->extension;
            }
            if (is_object($this->TeX)) {
                $TeX = 'uploads/library/TeX/' . md5($this->TeX->baseName.rand())  . '.' . $this->TeX->extension;
            }
            if ((($this->djvu != '') && ($this->djvu->saveAs($djvu))) || (($this->pdf != '') && ($this->pdf->saveAs($pdf))) || (($this->ps != '') && ($this->ps->saveAs($ps))) || (($this->html != '') && ($this->html->saveAs($html))) || (($this->TeX != '') && ($this->TeX->saveAs($TeX)))) {

                if ($this->getOldAttribute("pdf") && $this->getOldAttribute("djvu") && $this->getOldAttribute("ps") && $this->getOldAttribute("html") && $this->getOldAttribute("TeX")) {
                    is_file($this->getOldAttribute("djvu")) && unlink($this->getOldAttribute("djvu"));
                    is_file($this->getOldAttribute("pdf")) && unlink($this->getOldAttribute("pdf"));
                    is_file($this->getOldAttribute("ps")) && unlink($this->getOldAttribute("ps"));
                    is_file($this->getOldAttribute("html")) && unlink($this->getOldAttribute("html"));
                    is_file($this->getOldAttribute("TeX")) && unlink($this->getOldAttribute("TeX"));
                }

                if (is_file($pdf)) chmod($pdf, 0777);
                if (is_file($djvu)) chmod($djvu, 0777);
                if(is_file($ps)) chmod($ps, 0777);
                if (is_file($html)) chmod($html, 0777);
                if (is_file($TeX)) chmod($TeX, 0777);
                // echo '<pre>';
                // print_r($pdf.'-'.$djvu.'-'.$ps);
                // echo '</pre>';
                // exit();
                $this->pdf = $pdf ? $pdf : '';
                $this->djvu = $djvu ? $djvu : '';
                $this->ps = $ps ? $ps : '';
                $this->html = $html ? $html : '';
                $this->TeX = $TeX ? $TeX : '';
                
                return true;
            }
        }
        return false;
    }
}
