<?php

namespace app\models\tests;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\tests\Tests;

/**
 * testsSearch represents the model behind the search form about `app\models\tests\tests`.
 */
class TestsSearch extends Tests
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'category_id', 'score'], 'integer'],
            [['question', 'photo_quest', 'a', 'b', 'c', 'd', 'answer', 'answer_photo', 'level'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        // echo '<pre>';
        // print_r($params['invalid']);
        // echo '</pre>';
        // exit();
        $query = tests::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (empty($params['invalid'])) {
            // grid filtering conditions
            $query->andFilterWhere([
                'id' => $this->id,
                'category_id' => $this->category_id,
                'score' => $this->score,
            ]);

            $query->andFilterWhere(['like', 'question', $this->question])
                ->andFilterWhere(['like', 'answer', $this->answer])
                ->andFilterWhere(['like', 'level', $this->level]);

            $query->orderBy('id DESC');
        }else {
            // get invalid variants of tests and push id to array
            $variants = TestVariant::find()->where(['invalid' => 1])->groupBy('test_id')->all();
            $idArr = [];
            foreach ($variants as $key => $value) {
                $idArr[$value->test_id] = $value->test_id;   
            }
            $query->where(['in', 'id', $idArr])->all();
        }
        

        return $dataProvider;
    }
}
