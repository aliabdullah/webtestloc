<?php

namespace app\models\tests;

use Yii;
use yii\helpers\ArrayHelper;
use app\models\category\Category;
use app\models\testblog\TestGroup;

/**
 * This is the model class for table "tests".
 *
 * @property integer $id
 * @property integer $category_id
 * @property string $question
 * @property string $photo_quest
 * @property string $a
 * @property string $b
 * @property string $c
 * @property string $d
 * @property string $answer
 * @property string $answer_photo
 * @property integer $score
 * @property string $level
 */
class Tests extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tests';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'question', 'answer', 'score', 'level', 'type', 'lang'], 'required'],
            ['score', 'integer'],
            [['category_id', 'date', 'a', 'b', 'c', 'd'], 'safe'],
            ['date', 'default', 'value' => date("Y-m-d H:i")],
            [['question', 'answer', 'level', 'type'], 'string', 'max' => 5000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'category_id' => Yii::t('app', 'Категория'),
            'question' => Yii::t('app', 'Вопрос'),
            'answer' => Yii::t('app', 'Ответ'),
            'answer_photo' => Yii::t('app', 'Ответьте фото'),
            'score' => Yii::t('app', 'Балл'),
            'level' => Yii::t('app', 'Level'),
            'lang' => Yii::t('app', 'Язык'),
        ];
    }
    const SCORE_0 = '0';
    const SCORE_1 = '1';
    const SCORE_2 = '2';
    const SCORE_3 = '3';
    const SCORE_4 = '4';
    const SCORE_5 = '5';
    const SCORE_6 = '6';
    const SCORE_7 = '7';
    const SCORE_8 = '8';
    const SCORE_9 = '9';
    const SCORE_10 = '10';

    const LEVEL_EASY = 'Легко';
    const LEVEL_NORMAL = 'Нормальный';
    const LEVEL_HARD = 'Сложный';
    const LEVEL_EXPERT = 'Эксперт';

    const TYPE_BASE = 'Базовый';
    const TYPE_ADVANCED = 'Продвинутый';
    const TYPE_PREMIUM = 'Премиум';
    const TYPE_PLATINUM = 'Платинум';

    const RU = 'ru';
    const UZ = 'uz';

    public $testcount = 0;

    public function uploadFile()
    {
        if (is_object($this->photo_quest) && is_object($this->answer_photo)) {
            $name = 'uploads/tests/' . md5($this->photo_quest->baseName.rand())  . '.' . $this->photo_quest->extension;
            $name = 'uploads/tests/' . md5($this->answer_photo->baseName.rand())  . '.' . $this->answer_photo->extension;

            if ($this->photo_quest->saveAs($name) && $this->answer_photo->saveAs($name)) {

                /**
                 * @var $img SimpleImage
                 */
                $img = \Yii::$app->image;

                $img->load($name)->thumbnail(150, 150)->save($name);

                if ($this->getOldAttribute("answer_photo") && $this->getOldAttribute("photo_quest")) {
                    is_file($this->getOldAttribute("photo_quest")) && unlink($this->getOldAttribute("photo_quest"));
                    is_file($this->getOldAttribute("answer_photo")) && unlink($this->getOldAttribute("answer_photo"));
                }

                chmod($name, 0777);
                $this->answer_photo = $name;
                $this->photo_quest = $name;
                return true;
            }
        }
        return false;
    }

    public static function getLang()
    {
        return [
            self::RU => "ru",
            self::UZ => "uz"
        ];
    }

    public function getVariants()
    {
        return $this->hasMany(TestVariant::className(), ['test_id' => 'id']);
    }

     public static function getLevels()
    {
        return [
            self::LEVEL_EASY => 'Легко',
            self::LEVEL_NORMAL => 'Нормальный',
            self::LEVEL_HARD => 'Сложный',
            self::LEVEL_EXPERT => 'Эксперт',
        ];
    }

    public static function getScores()
    {
        return [
            self::SCORE_0 => '0',
            self::SCORE_1 => '1',
            self::SCORE_2 => '2',
            self::SCORE_3 => '3',
            self::SCORE_4 => '4',
            self::SCORE_5 => '5',
            self::SCORE_6 => '6',
            self::SCORE_7 => '7',
            self::SCORE_8 => '8',
            self::SCORE_9 => '9',
            self::SCORE_10 => '10'
        ];
    }


    // Change encoding to ASCII
    public function changeEncodingOption($str)
    {
        if (mb_detect_encoding($str) == 'ASCII') {
            if (strlen($str) == 1){ 
                $varType = mb_strtoupper($str); 
            } else {
                $varType = mb_strtoupper(substr($str, 0, -1)); 
            }
        }elseif (mb_detect_encoding($str) == 'UTF-8') {
            if (strlen($str) == 2) {
                $varType = mb_strtoupper($str);
            } else {
                $varType = mb_strtoupper(substr($str, 0, -1)); }
        }else { return false; }
        
        return $varType;
    }

    public static function getTypes()
    {
        return [
            self::TYPE_BASE => 'Базовый',
            self::TYPE_ADVANCED => 'Продвинутый',
            self::TYPE_PREMIUM => 'Премиум',
            self::TYPE_PLATINUM => 'Платинум',
        ];
    }

    public function getCats() {
        return ArrayHelper::map(Category::find()->all(), 'id', 'name');
    }

    public function getTgroup(){
        return $this->hasMany(TestGroup::className(), ['test_id' => 'id']);
    }

    public function getTests($predmet_id, $level, $lang, $used_ids_1 = null, $used_ids_2 = null){
        $returnBox = [];
        $testBox = [];
        $mainCat = Category::findOne($predmet_id);
        $secCats = Category::find()->where(['parent_id' => $predmet_id])->all();
        foreach ($secCats as $key => $secCat) {
            if ($secCat->test_count > 0) {
                $testsSec = self::find()
                    ->with('variants')
                    ->where(['category_id' => $secCat->id])
                    ->andFilterWhere(['not in', 'id', $used_ids_1])
                    ->andFilterWhere(['not in', 'id', $used_ids_2])
                    ->andWhere(['lang' => $lang])
                    ->andWhere(['level' => $level])
                    ->limit($secCat->test_count)
                    ->orderBy('rand()')
                    ->all();
                $testBox = ArrayHelper::merge($testsSec, $testBox);
            }
            $thirdCats = Category::find()->where(['parent_id' => $secCat->id])->all();
            foreach ($thirdCats as $key => $thirdCat) {
                if ($thirdCat->test_count > 0) {
                    $testsThird = self::find()
                        ->with('variants')
                        ->where(['category_id' => $thirdCat->id])
                        ->andFilterWhere(['not in', 'id', $used_ids_1])
                        ->andFilterWhere(['not in', 'id', $used_ids_2])
                        ->andWhere(['lang' => $lang])
                        ->andWhere(['level' => $level])
                        ->limit($thirdCat->test_count)
                        ->orderBy('rand()')
                        ->all();
                    $testBox = ArrayHelper::merge($testsThird, $testBox);
                }
            }
        }
        $returnBox['mainCat'] = $mainCat;
        $returnBox['tests'] = $testBox;
        return $returnBox;
    }

}
