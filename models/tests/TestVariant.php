<?php

namespace app\models\tests;

use Yii;

/**
 * This is the model class for table "test_variant".
 *
 * @property integer $id
 * @property integer $test_id
 * @property string $type
 * @property integer $correct
 * @property string $content
 * @property string $photo
 */
class TestVariant extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'test_variant';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            
            [['test_id', 'correct'], 'integer'],
            [['type', 'photo'], 'safe'],   
            [['type', 'content'], 'string', 'max' => 5000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'test_id' => Yii::t('app', 'Test ID'),
            'type' => Yii::t('app', 'Type'),
            'correct' => Yii::t('app', 'Correct'),
            'content' => Yii::t('app', 'Content'),
            'photo' => Yii::t('app', 'Photo'),
        ];
    }

    public function uploadFile()
    {
        if (is_object($this->photo)) {
            $name = 'uploads/tests/test_variant/' . md5($this->photo->baseName.rand())  . '.' . $this->photo->extension;
           

            if ($this->photo->saveAs($name)) {

                /**
                 * @var $img SimpleImage
                 */
                $img = \Yii::$app->image;

                $img->load($name)->thumbnail(150, 150)->save($name);

                if ($this->getOldAttribute("photo")) {
                    is_file($this->getOldAttribute("photo")) && unlink($this->getOldAttribute("photo"));
                    
                }

                chmod($name, 0777);
                $this->photo = $name;
                return true;
            }
        }
        return false;
    }
}
