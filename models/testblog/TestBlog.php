<?php

namespace app\models\testblog;

use Yii;
use app\models\tests\Tests;
use app\models\testblog\TestGroup;
use app\models\category\Category;
    

/**
 * This is the model class for table "test_blog".
 *
 * @property integer $id
 * @property integer $cat_id
 * @property string $name
 * @property string $description
 * @property string $type
 * @property string $date
 */
class TestBlog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'test_blog';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['lang', 'required'],
            [['cat_id'], 'integer'],
            [['description'], 'string'],
            [['date'], 'safe'],
            [['name', 'type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'cat_id' => Yii::t('app', 'Cat ID'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'type' => Yii::t('app', 'Type'),
            'date' => Yii::t('app', 'Date'),
        ];
    }

    public $testcount = 0;
    public $countcattest = 0;

    const LEVEL_EASY = 'Легко';
    const LEVEL_NORMAL = 'Нормальный';
    const LEVEL_HARD = 'Сложный';

    const RU = 'ru';
    const UZ = 'uz';

    public static function getLang()
    {
        return [
            self::RU => 'ru',
            self::UZ => 'uz'
        ];
    }

    public static function getLevels()
        {
            return [
                self::LEVEL_EASY => 'Легко',
                self::LEVEL_NORMAL => 'Нормальный',
                self::LEVEL_HARD => 'Сложный',
            ];
        }

    public function getTests(){
        return $this->hasMany(Tests::className(), ['blog_id'=>'id']);  
    }

    public function getGroup(){
        return $this->hasMany(Tests::className(), ['id'=>'test_id'])
        ->viaTable('test_group', ['blog_id' => 'id']);  
    }

    public function getCat()
    {
        return $this->hasOne(Category::className(), ['id' => 'cat_id']);
    }

    public function getParentCat()
    {
        return $this->hasOne(Category::className(), ['id' => 'cat_id']);
    }

    public function getMainCat()
    {
        return $this->hasOne(Category::className(), ['id' => 0]);
    }

    public function getSubcats()
    {
        return $this->hasMany(Category::className(), ['parent_id' => 'cat_id']);
    }

    public function getTestCount()
    {
        return Tests::findAll(['blog_id' => $this->id]);
    }

    // public static function getTestCount($provider, $columnName)
    // {
    //     $testcount = 0;

    //     foreach ($provider as $item) {
    //         $testcount += $item[$columnName];
    //     }
        

    //     return $testcount;
    // }
}
