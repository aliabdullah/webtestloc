<?php

namespace app\models\testblog;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\testblog\TestBlog;

/**
 * TestBlogSearch represents the model behind the search form about `app\models\testblog\TestBlog`.
 */
class TestBlogSearch extends TestBlog
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'cat_id'], 'integer'],
            [['name', 'description', 'type', 'date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TestBlog::find()->addSelect('test_blog.*, (SELECT COUNT(*) FROM test_group WHERE test_blog.id = test_group.blog_id) AS testcount');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        

        if (isset($params['parent_id'])) {
            $query->where(['parent_id' => intval($params['parent_id'])]);
        }
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'cat_id' => $this->cat_id,
            'date' => $this->date,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'type', $this->type]);

        return $dataProvider;
    }
}
