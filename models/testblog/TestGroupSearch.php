<?php

namespace app\models\testblog;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\testblog\TestGroup;
use app\models\tests\Tests;
use yii\helpers\ArrayHelper;

/**
 * testsSearch represents the model behind the search form about `app\models\tests\tests`.
 */
class TestGroupSearch extends Tests
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'category_id', 'score'], 'integer'],
            [['question', 'photo_quest', 'a', 'b', 'c', 'd', 'answer', 'answer_photo', 'level'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $id = Yii::$app->request->get('id');
        $group = TestGroup::find()->where(['blog_id' => $id])->indexBy('test_id')->all();
        $testId = ArrayHelper::map($group, 'test_id', 'test_id');
       
        $query = tests::find()->where(['in', 'id', $testId]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'category_id' => $this->category_id,
            'score' => $this->score,
        ]);

        $query->andFilterWhere(['like', 'question', $this->question])
            ->andFilterWhere(['like', 'photo_quest', $this->photo_quest])
            ->andFilterWhere(['like', 'a', $this->a])
            ->andFilterWhere(['like', 'b', $this->b])
            ->andFilterWhere(['like', 'c', $this->c])
            ->andFilterWhere(['like', 'd', $this->d])
            ->andFilterWhere(['like', 'answer', $this->answer])
            ->andFilterWhere(['like', 'answer_photo', $this->answer_photo])
            ->andFilterWhere(['like', 'level', $this->level]);

        return $dataProvider;
    }
}
