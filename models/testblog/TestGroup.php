<?php

namespace app\models\testblog;

use Yii;
use app\models\tests\Tests;

/**
 * This is the model class for table "test_group".
 *
 * @property integer $id
 * @property integer $test_id
 * @property integer $blog_id
 * @property integer $sub_cat_id
 * @property string $date
 */
class TestGroup extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'test_group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['test_id', 'blog_id', 'sub_cat_id'], 'integer'],
            [['date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'test_id' => Yii::t('app', 'Test ID'),
            'blog_id' => Yii::t('app', 'Blog ID'),
            'sub_cat_id' => Yii::t('app', 'Sub Cat ID'),
            'date' => Yii::t('app', 'Date'),
        ];
    }

    public function getTests(){
        return $this->hasMany(Tests::className(), ['category_id'=>'blog_id']);  
    }
}
