<?php

/* @var $this yii\web\View */
use app\models\user\Order;
use yii\helpers\Url;


$this->title = 'My Yii Application';
$user = Yii::$app->user->identity;
?>
<?php 
  $userOrder = Order::find()->where(['user_id' => $user->id, 'status' => 1])->all();
    if (!empty($userOrder)) {
      $path = Url::to(['/test/premium-filter']);
    }else {
      $path = Url::to(['/test/index']);
    }
?>

<?php if (Yii::$app->session->hasFlash('user_info')) {
$script = <<< JS
  $('#clickmodal').click();
JS;
$this->registerJs($script);
 }  ?>
<button type="button" id="clickmodal" class="btn btn-info btn-lg hide" data-toggle="modal" data-target="#myModal">Open Modal</button>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Diqqat!</h4>
      </div>
      <div class="modal-body">
        <p style="font-size: 18px;"><?= Yii::$app->user->identity->username ?> saytga qo'shimcha ma'lumotlaringizni kiritib qo'yishingizni <strong>tafsiya qilamiz!</strong></p>
        <p style="font-size: 14px;"><i>Albatta malumotlaringiz Reyting va Statistikalar uchun kerak bo'ladi.</i> <strong><a href="/site/user-info" style="font-size: 17px;">Ma'lumotni kiritish</a></strong></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Hozir emas...</button>
      </div>
    </div>

  </div>
</div>
<?php if (Yii::$app->language == 'cryl') { $class = 'slide__image__cryl';}else { $class = 'slide__image';} ?>
<div class="main__slider slider owl-carousel">

  <div class="slider__slide slide">
    <div class="<?=$class?>">
      <img src="/images/slide1.png" class="slide__img">
    </div>
    <div class="slide__description">
      <h1 class="slide__title"><?= Yii::t("lang", "Pull_up_your_skills")?></h1>
      <span class="slide__subtitle"><?= Yii::t("lang", "Take_the_tests_online_and_find_out_your_rating")?></span>
      <div class="slide__buttons">
        <a href="<?= Url::to(['/footer/index'])?>" class="slide__button slide__button_simple"><?= Yii::t("lang", "Learn_more_about_the_service")?></a>
        <a href="<?= $path ?>" class="slide__button slide__button_border"><?= Yii::t("lang", "Start_using")?></a>
      </div>
    </div>
  </div>

  <div class="slider__slide slide">
    
    <div class="<?=$class?>">
      <img src="/images/slide1.png" class="slide__img">
    </div>
    
    <div class="slide__description">
      <h1 class="slide__title"><?= Yii::t("lang", "Pull_up_your_skills")?></h1>
      <span class="slide__subtitle"><?= Yii::t("lang", "Take_the_tests_online_and_find_out_your_rating")?></span>
      <div class="slide__buttons">
        <a href="<?= Url::to(['/footer/index'])?>" class="slide__button slide__button_simple"><?= Yii::t("lang", "Learn_more_about_the_service")?></a>
        <a href="<?= $path ?>" class="slide__button slide__button_border"><?= Yii::t("lang", "Start_using")?></a>
      </div>
    </div>
  </div>
</div>
        
<div class="main__services services">
  <h2 class="services__caption"><?= Yii::t("lang", "Online_tests_on_directions")?></h2>

  <div class="services__banner banner">
    <img src="/images/banner.jpg" class="banner__img">
  </div>

  <div class="services__wrapper">
    <?php foreach ($model as $key => $category) { ?>
      <div class="services__box">
        <div class="services__service service">
          <div class="service__box">
            <?php $userOrder = Order::find()->where(['user_id' => $user->id, 'status' => 1])->all();
              if (!empty($userOrder)) { ?>
                <a href="<?= Url::to(['/test/premium-filter']) ?>" class="service__link"></a>
            <?php }else { ?>
                <a href="<?= $category->getLink() ?>" class="service__link"></a>
            <?php } ?>
            <img src="<?= $category->Photo ?>" class="service__icon">
            <div class="service__name"><?= Yii::t("lang", $category->name ) ?></div>
          </div>
        </div>
      </div>  
    <?php } ?>
  </div>
</div>

<div class="main__tarrifs tarrifs">
  <h2 class="tarrifs__caption"><?= Yii::t("lang", "tariffs_for_testing")?></h2>
  <div class="tarrifs__wrapper">
  <?php foreach ($model_cours as $key => $cours) { ?>
      <div class="tarrifs__box">
      <div class="tarrifs__tarrif tarrif">
        <div class="tarrif__head">
          <img src="<?= '/'.$cours->photo ?>" class="tarrif__bg">
          <div class="tarrif__box">
            <h1 class="tarrif__price"><?= number_format($cours->cost).' сум'?> </h1>
            <span class="tarrif__name"><?= $cours->status ?></span>
          </div>
        </div>
        <div class="tarrif__body">
          <h3 class="tarrif__level"><?= $cours->title ?></h3>
          <p class="tarrif__desc"><?= $cours->content ?></p>
        </div>
      </div>
    </div>

  <?php  } ?>


  </div>
</div>


      