<?php 

use yii\bootstrap\ActiveForm;
use borales\extensions\phoneInput\PhoneInput;
use kartik\date\DatePicker;

?>

<div class="register-box" style="margin: 4% auto;">
  <div class="register-logo">
    <a href="../../index2.html"><b>Дополнительная</b><br>информация</a>
  </div>

  <div class="register-box-body" style="height: 582px;">
    <p class="login-box-msg"><?=Yii::t('lang', 'Пожалуйста, введите свою дополнительную информацию')?></p>

    <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
      <div class="form-group has-feedback">
        <?= $form->field($model, 'name', ['options' => [
              'tag' => 'div',
              'class' => 'form-group field-loginform-username has-feedback required'
              ],
              'template' => '{input}<span class="glyphicon glyphicon-user form-control-feedback"></span>
                {error}{hint}'
            ])->textInput(['placeholder' => 'Имя']) ?>
      </div>
      <div class="form-group has-feedback">
        <?= $form->field($model, 'surname', ['options' => [
              'tag' => 'div',
              'class' => 'form-group field-loginform-username has-feedback required'
              ],
              'template' => '{input}<span class="glyphicon glyphicon-user form-control-feedback"></span>
                {error}{hint}'
            ])->textInput(['placeholder' => 'Фамилия']) ?>
        <!-- <input type="email" class="form-control" placeholder="Email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span> -->
      </div>
      <div class="form-group has-feedback">
        <?= $form->field($model, 'email', ['options' => [
              'tag' => 'div',
              'class' => 'form-group field-loginform-username has-feedback required'
              ],
              'template' => '{input}<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                {error}{hint}'
            ])->textInput(['placeholder' => 'Email', 'type' => 'email']) ?>
        <!-- <input type="password" class="form-control" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span> -->
      </div>

      <div class="form-group has-feedback">
        <div class="form-group field-userinfo-gender has-success">
          <div class="checkbox">
            <label for="userinfo-gender">
              <!-- <input type="hidden" name="UserInfo[gender]" value="1"> -->
              <input type="radio" id="userinfo-gender" checked name="UserInfo[gender]" value="men">
              мужчина
            </label>
            <p class="help-block help-block-error"></p>
          </div>
        </div>
        <div class="form-group field-userinfo-gender has-success">
          <div class="checkbox">
            <label for="userinfo-gender">
              <!-- <input type="hidden" name="UserInfo[gender]" value="2"> -->
              <input type="radio" id="userinfo-gender" name="UserInfo[gender]" value="woman">
              женщина
            </label>
            <p class="help-block help-block-error"></p>
          </div>
        </div>
      </div>

      <div class="form-group has-feedback">
        <?= $form->field($model, 'countries', ['options' => [
              'tag' => 'div',
              'class' => 'form-group field-loginform-username has-feedback required'
              ],
              'template' => '{input}<span class="glyphicon glyphicon-globe form-control-feedback"></span>
                {error}{hint}'
            ])->textInput(['placeholder' => 'Страна']) ?>
      </div>
      <div class="form-group has-feedback">
        <?php echo '<label>Дата рождения</label>';
              echo DatePicker::widget([
                'name' => 'UserInfo[birthday]', 
                'value' => date('d-M-Y', strtotime('+2 days')),
                'options' => ['placeholder' => 'Select issue date ...'],
                'pluginOptions' => [
                  'format' => 'dd-M-yyyy',
                  'todayHighlight' => true
                ]
              ]);
        ?>
      </div>

      <div class="form-group has-feedback">
        <?= $form->field($model, 'phone_number', ['options' => [
              'tag' => 'div',
              'class' => 'form-group field-loginform-username has-feedback required user-info',
              ],
              'template' => '{input}<span class="glyphicon glyphicon-phone form-control-feedback"></span>
                {error}{hint}'
            ])->textInput()->widget(PhoneInput::className(), [
                'jsOptions' => [
                    'preferredCountries' => ['uz', 'us', 'ru'],
                ]]) ?>
      </div>
      <div class="form-group has-feedback">
        <div class="form-group field-userinfo-gender has-success">
          <div class="checkbox">
            <label for="userinfo-gender">
              <input type="hidden" name="UserInfo[public]" value="1">
              <input type="checkbox" id="userinfo-public" value="1" name="UserInfo[public]">
              Вы хотите, чтобы ваша информация отображалась другим пользователям
            </label>
            <p class="help-block help-block-error"></p>
          </div>
        </div>
      </div>
      <div class="col-xs-12">
        <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
      </div>
      <?php ActiveForm::end(); ?>
    </div>
  <!-- /.form-box -->
</div>
 