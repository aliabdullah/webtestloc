<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \mdm\admin\models\form\Signup */

$this->title = 'Signup';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="register-box">
  <div class="register-logo">
    <a href="../../index2.html"><b><?= Html::encode($this->title) ?></b></a>
  </div>

  <div class="register-box-body">
    <p class="login-box-msg">Register a new membership</p>

    <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
            
      <div class="form-group has-feedback">
        <?= $form->field($model, 'username', ['options' => [
              'tag' => 'div',
              'class' => 'form-group field-loginform-username has-feedback required'
              ],
              'template' => '{input}<span class="glyphicon glyphicon-user form-control-feedback"></span>
                {error}{hint}'
            ])->textInput(['placeholder' => 'Username']) ?>
      </div>

      <div class="form-group has-feedback">
        <?= $form->field($model, 'email', ['options' => [
              'tag' => 'div',
              'class' => 'form-group field-loginform-username has-feedback required'
              ],
              'template' => '{input}<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                {error}{hint}'
            ])->textInput(['placeholder' => 'Email']) ?>
      </div>

      <div class="form-group has-feedback">
        <?= $form->field($model, 'password', ['options' => [
              'tag' => 'div',
              'class' => 'form-group field-loginform-username has-feedback required'
              ],
              'template' => '{input}<span class="glyphicon glyphicon-lock form-control-feedback"></span>
                {error}{hint}'
            ])->passwordInput(['placeholder' => 'Password']) ?>
      </div>

      <div class="form-group has-feedback">
        <?= $form->field($model, 'photo')->fileInput() ?>
      </div>
      
      <div class="row">
        <div class="col-xs-8">
          
        </div>
        <!-- /.col -->
        <div class="form-group col-xs-4">
            <?= Html::submitButton('Signup', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
        </div>
        <!-- /.col -->
      </div>
    <?php ActiveForm::end(); ?>
  </div>
  <!-- /.form-box -->
</div>