<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \mdm\admin\models\form\Login */

$this->title = Yii::t('lang', 'Войти');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="login-box">
  <div class="login-logo">
    <a href="../../index2.html"><b><?= Html::encode($this->title) ?></b></a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg"><?=Yii::t('lang', 'Войдите, чтобы начать сеанс')?></p>

    <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
      <div class="form-group has-feedback">
        <?= $form->field($model, 'username', ['options' => [
              'tag' => 'div',
              'class' => 'form-group field-loginform-username has-feedback required'
              ],
              'template' => '{input}<span class="glyphicon glyphicon-user form-control-feedback"></span>
                {error}{hint}'
            ])->textInput(['placeholder' => 'Имя пользователя']) ?>
      </div>
      <div class="form-group has-feedback">
        <?= $form->field($model, 'password',['options' => [
              'tag' => 'div',
              'class' => 'form-group field-loginform-username has-feedback required'
              ],
              'template' => '{input}<span class="glyphicon glyphicon-lock form-control-feedback"></span>
                {error}{hint}'
            ])->passwordInput(['placeholder' => 'Пароль']) ?>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <?= $form->field($model, 'rememberMe')->checkbox()->label(Yii::t('lang', 'Запомни меня')) ?>
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <?= Html::submitButton(Yii::t('lang', 'Войти'), ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
        </div>
        <!-- /.col -->
      </div>
    <?php ActiveForm::end(); ?>

    <?= yii\authclient\widgets\AuthChoice::widget([
     'baseAuthUrl' => ['site/auth'],
     'popupMode' => false,
]) ?>

    <!-- /.social-auth-links -->
    <a href="<?=Url::to(['/site/signup'])?>" class="text-center"><?=Yii::t('lang', 'Зарегистрировать')?></a>

  </div>
  <!-- /.login-box-body -->
</div>