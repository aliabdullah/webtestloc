<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="site-error text-center" style="margin-top: 100px;">
    <div class="col-md-12 " style="min-height: 650px;">
            <div class="error-template">
                <h1>
                    <?= nl2br(Html::encode($message)) ?>!</h1>
                <h2>
                   <?= Html::encode($this->title) ?></h2>
                <div class="error-details" style="margin-bottom: 50px;">
                   
                </div>
                <div class="error-actions">
                    <a href="/" class="btn btn-primary btn-lg"><span class="glyphicon glyphicon-home"></span>
                        <?= Yii::t('lang', 'Вернуться на главную')?></a>
                </div>
            </div>
        </div>

    <!-- <h1><?= Html::encode($this->title) ?></h1>

    <div class="alert alert-danger">
        <?= nl2br(Html::encode($message)) ?>
    </div>

    <p>
        The above error occurred while the Web server was processing your request.
    </p>
    <p>
        Please contact us if you think this is a server error. Thank you.
    </p> -->

</div>
