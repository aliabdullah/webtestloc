<?php 

use yii\helpers\Url;
use zxbodya\yii2\tinymce\TinyMce;
use zxbodya\yii2\elfinder\TinyMceElFinder;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

// if (Yii::$app->session->hasFlash('send-answer')) {
// $script = <<< JS
// $(document).ready(function(){
// 	swal({
// 		title: "Javob yuborildi!",
// 		text: "Tez orada javobingiz ko'rib chiqiladi va sizga ma'lum qilinadi",
// 		icon: "success"
// 	});
// });
// JS;

// }elseif (Yii::$app->session->hasFlash('unchecked') || $answer != '') {
// 	$script = '';

// }elseif (Yii::$app->session->hasFlash('incorrect')) {
// $incorrect = Yii::$app->session->getFlash('incorrect');
// $script = <<< JS
// $(document).ready(function(){
// 	swal({
// 		title: "Javob noto'g'ri!",
// 		text: "$incorrect",
// 		icon: "error"
// 	});
// });
// JS;

// }elseif (Yii::$app->session->hasFlash('complaint')) {
// $complaint = Yii::$app->session->getFlash('complaint');
// $script = <<< JS
// $(document).ready(function(){
// 	swal({
// 		title: "Sizning shikoyatingiz yuborildi!",
// 		text: "Tez orada ko'ribchiqiladi",
// 		icon: "info"
// 	});
// });
// JS;

// }else{
// $script = <<< JS
// $(document).ready(function(){
// 	swal({
// 		title: "Ответ будет засчитываться в зависимости от того, как часто вы пытались!",
// 		text: "1 - 100 балл;   2 - 90 балл;   3 - 70 балл;   4 - 50 балл;   5 - 20 балл;   6 - 10 балл;   7 - 0 балл",
// 		icon: "warning"
// 	});
// });
// JS;
// }
// $this->registerJs($script);

?>
<div class="col-md-12">
<div class="col-md-9" style="min-height: 640px;">
	<div class="box" style="border-top: none; height: auto; margin-top: 10px;">
	    <div class="box-header">
	    	<?php if (!empty($task)){ ?>
	        	<div class="col-md-12">
	        		<div class="col-md-12" style=" width: 100%; border-radius: 3px; border: 0px solid #ddd; background: #f3f3f3; padding: 10px;">
	        		<span style="width:100%; font-size: 22px; font-family: 'Source Sans Pro',sans-serif;"><?= Yii::t('lang', 'task')?> <?=$num++?>. <span style="color: #2980b9;"><?= $task->name?></span></span>
	        			<ul  class="task__info">
	        				<li><strong><?=Yii::t('lang', 'show').':'?></strong> <?=$task->date?></li>
	        				<li style="margin-left: 10px;"><strong><?=Yii::t('lang', 'Sent_by').':'?></strong> <?=$task->editor->username?></li>
	        			</ul>
	        			<ul style="font-family: Times new roman;">
	        				<li><strong><?=Yii::t('lang', 'Complexity').':'?></strong> <?=$task->level?>, <strong style="margin-left: 20px;"><?=Yii::t('lang', 'grade').':'?></strong> <?=$task->class?></li>
	        				<li style="margin-left: 20px;"><strong><?=Yii::t('lang', 'score').':'?></strong> <?=$task->bal?></li>
	        				<li style="margin-left: 20px;"><strong><?=Yii::t('lang', 'topics').':'?></strong> <?=$task->theme?></li>
	        			</ul>
	        		</div>
	        		<div class="col-md-12" style=" padding: 5px 0px 0 15px; border: 0px solid #ddd;">
	        			<p><?= $task->task?>
						</p>
	        		</div>
	        		<hr class="col-md-12" style="width:97%;">
	      
	        		<?php if (!Yii::$app->session->hasFlash('send-answer-$task->id')) { ?>
	        			<div class="col-md-12">
	        				<?php if (Yii::$app->session->hasFlash('empty-inputs')): ?>
				            	<p class="well" style="color: red;"><?=Yii::t('lang', 'do_not_select_answer')?></p>	
				            <?php endif ?>
		        			<form method="post" action="<?=Url::to("/ru/task/answ-tourn")?>" enctype="multipart/form-data">
		        				<?= Html::hiddenInput(\Yii :: $app->getRequest()->csrfParam, \Yii :: $app->getRequest()->getCsrfToken(), []);?>
		        			<!-- Task params -->
		        				<input type="hidden" name="user" value="<?=Yii::$app->user->identity->id?>">
								<input type="hidden" name="tourn_id" value="<?=$task->tournament?>">
		        				<input type="hidden" name="task_id" value="<?=$task->id?>">
		        				<input type="hidden" name="task_name" value="<?=$task->name?>">
		        				<input type="hidden" name="task_task" value="<?=$task->task?>">
		        				<input type="hidden" name="task_cat" value="<?=$task->cat_id?>">
		        			<!-- end -->
		        			
		        			<div class="text-center" style="margin-top: 5px; min-height: 20px; padding: 7px;  background-color: #739bb5;   border: 1px solid #2980b9;   border-top-left-radius: 4px; border-top-right-radius: 4px;   -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.05);   box-shadow: inset 0 1px 1px rgba(0,0,0,.05);">
		        				<span class="" style="color: white;"><?=Yii::t('lang', 'full_answer')?></span>
		        			</div>
		        			
		        			<!-- <input class="form-control" style="border-top-left-radius: 0px !important; border-top-right-radius: 0px !important;" type="text" name="Answer[short_answ]"> -->
		        			<?= TinyMce::widget([
		        				'name' => 'answer[full_answ]',
				                'settings' => [
				                    'plugins' => ["advlist autolink lists link image charmap print preview hr anchor pagebreak ",
				                    "searchreplace visualblocks visualchars code fullscreen",
				                    "insertdatetime media nonbreaking save table contextmenu directionality",
				                    "template paste textcolor "],
				                    'toolbar' => "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media | forecolor backcolor | fontsizeselect",
				                    'fontsize_formats' => '8px 10px 12px 14px 18px 24px 36px',],
				                'fileManager' => [
				                    'class' => TinyMceElFinder::className(),
				                    'connectorRoute' => '/admin/tests/connector_test'
				                ],
				            ])
				            ?>
	            	
							<div class="input-file-container text-center" style="margin-top: 15px;">
								<h6><?=Yii::t('lang', 'scrinshot_or_file')?></h6>
							    <input class="input-file" id="my-file" type="file" style="display: none;" name="answer[file]">
							    <label tabindex="0" for="my-file" class="input-file-trigger"><?=Yii::t('lang', 'choose_file')?></label>
							    <p class="file-return"></p>
							</div>

							<div class="form-group" style="margin-top: 15px;">
							  	<?= Html::submitButton(Yii::t('lang', 'send'), ['class' => 'btn btn-success']) ?>
							</div>
							  
							</form>
		        		</div>
	        		<?php }else { ?>
	        			<div class="col-md-12" style=" width: 100%; border-radius: 3px; border: 0px solid #ddd; background: #d7ffce; padding: 10px;">
	        				<h4 style="color: #2980b9;"><?= Yii::t('lang', 'congratulation!')?></h4>
	        				<span style="color: #444444;"><strong style="margin-right: 10px;"><?= Yii::t('lang', 'answer')?></strong><?= $answer->answer?></span><br>
	        				<span style="color: #444444;"><strong style="margin-right: 10px;"><?= Yii::t('lang', 'attempt')?></strong><?= $count = ((6 - $answer->attempt) + 1)?></span><br>
	        				<span style="color: #444444;"><strong style="margin-right: 10px;"><?= Yii::t('lang', 'gived_scores')?></strong><?= $answer->score ?>  <? if ($answer->score == 0) { ?>
	        					<span style=" margin-left: 15px;color: red; font-size: 14px;"><?=Yii::t('lang', 'fulled_attempt')?></span>
	        					<?php } ?></span><br>
	        				<span style="color: #444444;"><strong style="margin-right: 10px;"><?= Yii::t('lang', 'date')?></strong><?= $answer->date?></span><br>
	        			</div>
	        		<?php } ?>
	        	</div>
	    	<?php }else { ?>
	    		<div class="col-md-12 text-center ">
	                <span style="font-size: 20px;"><?=Yii::t('lang', 'empty_data')?></span>
	              </div>
	    	<?php } ?>
	    </div>
	</div>
</div>
  <span id="complaint" class="btn btn-warning col-md-3" style="margin-top:10px; "><i class="fa fa-info-circle" aria-hidden="true">  <?=Yii::t('lang', 'complaint')?></i></span>
  <div id="complaint_area" class="col-md-3 hide">
  	<p><?=Yii::t('lang', 'idea_for_advice')?></p>
  	<form action="<?= Url::to(['/task/complaint', 'id' => $task->id])?>" method="post">
  		<?= Html::hiddenInput(\Yii :: $app->getRequest()->csrfParam, \Yii :: $app->getRequest()->getCsrfToken(), []);?>
  		<input type="hidden" name="user" value="<?=Yii::$app->user->identity->id?>">
  		<input type="hidden" name="task" value="<?=$task->id?>">
  		<textarea type="textarea" class="form-control" name="complaint" style="height: 115px;"> </textarea>
  		<button class="btn btn-primary" style="margin: 10px 0;">yuborish</button>
  	</form>
  </div>
</div>