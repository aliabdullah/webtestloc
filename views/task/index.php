<?php 

use yii\helpers\Url;
use app\models\task\Attempts;
use app\models\task\TournAnswers;

$user = Yii::$app->user->identity;
?>

<div class="col-md-9" style="min-height: 640px;">
	<?php if (!empty($attempt->task) || $attempt->task->name != ''): ?>
		<marquee  direction="left" bgcolor="#fff">
	   		<span style="color: blue;"><?=$attempt->user->username ?></span>  <?=Yii::t('lang', 'answer_task')?>   -- "<span style="color: green;"><?= $attempt->task->name?></span>"  <span style="font-size: 13px;">(<?=$attempt->date?>)</span>!
	  	</marquee>	
	<?php endif ?>
	
	<div class="box" style="border-top: none; height: auto;">
        <div class="box-header">
        	<?php if (!$tournModel || empty($tournModel)) { ?>
        		<div class="col-md-6"> <h4 style=""><?= Yii::t('lang', 'Tasks'). ': '?><span style="color: #2980b9;"><?=$category->name ? $category->name.' / ' : 'общий / '?><?= count($tasks)?></span></h4></div>
        		<div class="col-md-6 pull-right head-filter">
            	<ul class="pull-right">
            		<li><h5><?=Yii::t('lang', 'show').':'?></h5></li>
            		<li class="li" style="border-right: 1px solid #ddd; padding-right: 5px; color: #9393ea;"><a href="<?=Url::to(['/task/index', 'all' => true])?>"><?=Yii::t('lang', 'all').':'?></a></li>
            		<li class="li" style="border-right: 1px solid #ddd; padding-right: 5px; color: #9393ea;"><a href="<?=Url::to(['/task/index', 'solved' => false])?>"><?=Yii::t('lang', 'unsolved').':'?></a></li>
            		<li class="li"style="border-right: 1px solid #ddd; padding-right: 5px; color: #9393ea;"><a href="<?=Url::to(['/task/index', 'solved' => true])?>"><?=Yii::t('lang', 'solved').':'?></a></li>
            		<!-- <li class="li"><a href="">filter</a></li> -->
            	</ul>
            </div>	
        	<?php }else { ?>
        		<div class="col-md-6"> <h4 style=""><?= Yii::t('lang', 'Tournament'). ': '?><span style="color: #2980b9;"><?=$tournModel->name ? $tournModel->name.' / ' : 'общий / '?><?= count($tasks)?></span></h4></div>
        	<?php } ?>
            
        </div>
    </div>

	<div class="box" style="border-top: none; height: auto; margin-top: 10px;">
        <div class="box-header">
        	<?php if (!empty($tasks)){ ?>
        		<?php $num=1; foreach ($tasks as $key => $task) {
        			$complated = Attempts::find()->where(['task_id' => $task->id])->andWhere(['complete' => 1])->count();
        			// Check to answered tasks
        			$answer = TournAnswers::find()->where(['user_id' => $user->id])->andWhere(['tourn_id' => $task->tournament])->andWhere(['task_id' => $task->id])->one();
        			if (!empty($answer)) { $htmlClass = 'blocked-task';	}else {	$htmlClass = ''; }
        		?>
		        <div class="col-md-12">
		        	<?php if (!empty($answer)){ ?>
		        		<span style="width:100%; font-size: 22px; font-family: 'Source Sans Pro',sans-serif;"><?=Yii::t('lang', 'task')?> <?=$num++?>. <span style="color: #2980b9;"><?= $task->name?></span> <span class="pull-right" style="color: green; font-size: 20px; "><?=Yii::t('lang', 'answer_sent')?>...</span></span>
		        	<?php }else { ?>
		        		<span style="width:100%; font-size: 22px; font-family: 'Source Sans Pro',sans-serif;"><?=Yii::t('lang', 'task')?> <?=$num++?>. <a href="<?=Url::to(['/task/view', 'id' => $task->id])?>"><span style="color: #2980b9;"><?= $task->name?></span></a> <span class="pull-right" style="color: darkgray; font-size: 12px; margin-top: 10px;"><?=Yii::t('lang', 'task_solved')?>: <?=$complated ? $complated : 0;?>,    <?=Yii::t('lang', 'all_attempts')?>: <?= $task->attempts ? $task->attempts : 0;?></span></span>
		        	<?php } ?>	
		        		<div class="pull-left" style="height: 170px; width: 230px;     border-radius: 3px; border: 0px solid #ddd; background: #f3f3f3; padding: 10px;">
		        			<ul  class="task__info">
		        				<li><strong><?=Yii::t('lang', 'Published')?>:</strong> <span class="task_li"><?= explode(' ', $task->date)[0]?></span></li>
		        				<li><strong><?=Yii::t('lang', 'Sent_by')?>: </strong> <span class="task_li"><?=$task->editor->username?></span></li>
		        				<li><strong><?=Yii::t('lang', 'Complexity')?>:</strong> <span class="task_li"><?=$task->level?></span>,<br> <strong><?=Yii::t('lang', 'grade')?>:</strong> <span class="task_li"><?=$task->class?></span></li><br>
		        				<li><strong><?=Yii::t('lang', 'score')?>:</strong> <span class="task_li"><?=$task->bal?></span></li><br>
		        				<li><strong><?=Yii::t('lang', 'topics')?>:</strong> <span class="task_li"><?=$task->theme?></span></li>
		        				<!-- <li></li> -->
		        			</ul>
		        			
		        		</div>
		        		<div class="pull-right" style="height: 150px; width: 69%; padding: 5px 0px 0 15px; border: 0px solid #ddd;">
		        			<p><?= $task->task?>
							</p>
		        		</div>
		        		<div class="col-md-12">
		        			<span style="color: darkgray; margin-top: 20px;">Комментарии: 25</span>
		        			<?php if (empty($answer)): ?>
		        				<span class="pull-right" style="color:lightseagreen;     margin-right: 20px; margin-top: 20px;"><a href="<?=Url::to(['/task/view', 'id' => $task->id])?>"><?=Yii::t('lang', 'more')?>...</a></span>	
		        			<?php endif ?>
							
		        		</div>
		        		<hr class="col-md-12" style="width:97%;">
		        	
		        </div>
		        <?php } ?>
        	<?php }else { ?>
        		<div class="col-md-12 text-center">
                    <span style="font-size: 20px;"><?=Yii::t('lang', 'uneble')?></span>
                  </div>
        	<?php } ?>
        </div>
    </div>
</div>
