<?php 

use app\models\user\User;
use app\models\user\UserInfo;

?>

<div class="col-xs-12" style="padding-top: 15px; min-height: 900px;">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title"><?= Yii::t('lang', 'rating');?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>ID</th>
                  <th><?= Yii::t('lang', 'name');?></th>
                  <th><?= Yii::t('lang', 'country');?></th>
                  <th><?= ucfirst(Yii::t('lang', 'score'));?></th>
                </tr>
                </thead>
                <tbody>
                <?php $num=1; foreach ($points as $key => $point) { 
                  $user = User::findOne($point->user_id);
                  $userInfo = UserInfo::find()->where(['user_id' => $point->user_id])->one();
                ?>
                  <tr>
                  <td><?=$num?></td>
                    <td><?=$user->username?>
                    </td>
                    <td><?=$userInfo->country?></td>
                    <td><?=$point->point?></td>
                  </tr>  
                <?php $num++; } ?>
                </tbody>
                <tfoot>
                <!-- <tr>
                  <th>Rendering engine</th>
                  <th>Browser</th>
                  <th>Platform(s)</th>
                  <th>Engine version</th>
                  <th>CSS grade</th>
                </tr> -->
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          
        </div>