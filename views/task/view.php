<!-- Check task type and rendering to necessity view -->

<?php if ($task->type == 'tournament' ) {
   	echo $this->render('answ-tourn', [
		'task' => $task
	]);
}elseif ($task->type == 'task') {
	echo $this->render('answer', [
		'task' => $task,
        'rating' => $rating,
        'commentForm' => $commentForm,
        'complated' => $complated,
	]);
} ?>
