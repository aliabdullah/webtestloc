<?php 
use yii\helpers\Url;
	$lang = Yii::$app->language;
?>
<div class="content-event col-md-12" style="height: 917px; padding-top: 20px;">
	<h3 style="    color: grey;"><?=Yii::t('lang', 'events')?></h3>
	<div class="col-md-9" style="box-shadow: 0 0 16px #ccc; height: auto; border-radius: 4px; background: white;">
		<div class="col-md-12">
          <!-- Box Comment -->
          <?php foreach ($events as $key => $event) { ?>
          	<div class="box box-widget">
            <div class="box-header with-border">
              <div class="user-block">
                <img class="img-circle" src="<?= '/'.$event->user->photo?>" alt="User Image">
                <span class="username"><a href="#"><?=$event->user->username ?> </a>    <span style="font-size: 12px; color: silver;">решил</span></span><span class="description" style="color: #2980b9;">            <a href="<?=Url::to(['/task/view', 'id' => $event->task->id])?>"><h4>  "<?=$event->task->name?>"</h4></a><span class="pull-right" style="color: silver"><?=$event->date?></span></span>
                
              </div>
              <!-- /.user-block -->
              <div class="box-tools">
                
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <!-- post text -->
              <p><?= substr($event->task->task, 0, 320)?>...</p>
              <!-- Attachment -->
              <div class="attachment-block clearfix">
                

                
                  <span class="" style="font-weight: unset; font-size: 13px;"><?=Yii::t('lang', 'attempts')?>:   <span style="color: #2980b9; margin-left: 5px;"><?= (6 - $event->attempt) ?></span></span>
                  <span class="" style="font-weight: unset; font-size: 13px; margin-left: 20px;"><?=Yii::t('lang', 'scores')?>:   <span style="color: #2980b9; margin-left: 5px;"><?= $event->score ?></span></span>

                  
              </div>
              <!-- /.attachment-block -->

              <!-- Social sharing buttons -->
              <button type="button" class="btn btn-default btn-xs"><i class="fa fa-share"></i> Share</button>
              <button type="button" class="btn btn-default btn-xs addlike" data-id="<?=$event->id?>" ><i class="fa fa-thumbs-o-up"></i> Like</button>
              <span class="pull-right text-muted"><?= $event->like?> likes - 2 comments</span>
            </div>
            <!-- /.box-body -->
            
            <!-- /.box-footer -->
            <!-- <div class="box-footer">
              <form action="#" method="post">
                <img class="img-responsive img-circle img-sm" src="../dist/img/user4-128x128.jpg" alt="Alt Text">
                .img-push is used to add margin to elements next to floating images
                <div class="img-push">
                  <input type="text" class="form-control input-sm" placeholder="Press enter to post comment">
                </div>
              </form>
            </div> -->
            <!-- /.box-footer -->
          </div>
          <?php } ?>
          
          <!-- /.box -->
        </div>
	</div>
  <div class="services__banner__inner banner" style="float: right;
  /*padding: 10px;*/
  background-color: #efefef;
  border-radius: 4px;
  /*margin-right: 15px;*/
  position: relative;">
    <img src="/images/banner.jpg" class="banner__img">
  </div>
</div>