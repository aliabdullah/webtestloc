<?php 

use yii\helpers\Url;
use app\models\task\Tournament;
?>

<div class="col-md-9" style="min-height: 640px;">
	<?php if (!empty($attempt->task) || $attempt->task->name != ''): ?>
		<marquee  direction="left" bgcolor="#fff">
	   		<span style="color: blue;"><?=$attempt->user->username ?></span><?=Yii::t('lang', 'answer_task')?>  -- "<span style="color: green;"><?= $attempt->task->name?></span>"  <span style="font-size: 13px;">(<?=$attempt->date?>)</span>!
	  	</marquee>	
	<?php endif ?>
	
	<div class="box" style="border-top: none; height: auto;">
        <div class="box-header">
        	<div class="col-md-6"> <h4 style=""><?=Yii::t('lang', 'Tournaments')?></h4></div>
        </div>
    </div>

	<div class="box" style="border-top: none; height: auto; margin-top: 10px;">
        <div class="box-header">
        	<?php if (!empty($tournModels)){  ?>
        		<?php $num=1; foreach ($tournModels as $key => $tournModel) {
        		$from = explode(',', $tournModel->time) ?>
        			<div class="" style="width: 100%; padding:10px;">
        			<?php
        				$first = date('Y-m-d', strtotime($tournModel->day));
        				$secDay = date('Y-m-d', strtotime($first.'+1 day'));
        			?>
		        		<span style="width:100%; font-size: 22px; font-family: Times New Roman;">
		        			<?php if ((date('Y-m-d') == $first || date('Y-m-d') == $secDay) && $from[0] <= date('H:i') && $from[1] >= date('H:i')){ ?>
			        			<a href="<?=Url::to(['/task/tourn-view', 'id' => $tournModel->id])?>">
		        			<?php } ?>
		        			<?php if (date('Y-m-d') == $first && $from[1] < date('H:i')) { $timeOut = '&nbsp;&nbsp;&nbsp;&nbsp;'.Yii::t('lang', 'part_one_the_end');
		        			}elseif ((date('Y-m-d') == $first || date('Y-m-d') == $secDay) && $from[0] > date('H:i')) {
		        				$timeOut = '&nbsp;&nbsp;&nbsp;&nbsp;boshlanish vaqti '.$from[0].'...';
		        			}elseif ((date('Y-m-d') > $first && date('Y-m-d') == $secDay) && $from[1] < date('H:i')) { $timeOut = '&nbsp;&nbsp;&nbsp;&nbsp;'.Yii::t('lang', 'the_end');
		        			}elseif ((date('Y-m-d') == $first || date('Y-m-d') == $secDay) && $from[0] <= date('H:i') && $from[1] > date('H:i')) {
		        				$timeOut = "&nbsp;&nbsp;&nbsp;&nbsp;<span style='color:green;'>online...</span>";
		        			}elseif (date('Y-m-d') < $first) {
		        				$timeOut = '';
		        			} else { $timeOut = '&nbsp;&nbsp;&nbsp;&nbsp;'.Yii::t('lang', 'old'); } ?>
			        			<span><?=$num++?>. </span>  
			        			<span style="color: #ff6e07;"><?= $tournModel->name?><span style="color: grey;font-size: 15px;"><?=$timeOut?></span></span>
		        			<?php if ((date('Y-m-d') == $first || date('Y-m-d') == $secDay) && $from[0] <= date('H:i') && $from[1] >= date('H:i')): ?>
		        				</a>
		        			<?php endif ?>
		        		</span>
		        		<div class="" style="min-height: 70px; width:100%; padding: 5px 0px 0 9px; border: 0px solid #ddd; font-family: Times New Roman; margin-bottom: 10px;">
		        				<p><?= substr($tournModel->descrip, 3, 552)?></p>
						</div>
		        		<div class="col-md-12" style="height: auto; border-radius: 3px; border: 0px solid #ddd; background: #fff0d9; padding: 10px;">
		        			<ul  class="task__info">
		        				<li class="<?= (date('Y-m-d') < $first && $tournModel->status == Tournament::STATUS_ACTIVE) ? 'date' : ''?>"><?=Yii::t('lang', 'the_date_of_the')?>: <span><?=$tournModel->day?></span></li>
		        				<li><?=Yii::t('lang', 'start_at')?> <span><?php echo $from[0]?></span>  <?=Yii::t('lang', 'to')?> <span><?=$from[1]?></span></li>
		        				<li><?=Yii::t('lang', 'level_temp')?>: <span><?=$tournModel->difficulty_lev?></span></li>
		        				<li><?=Yii::t('lang', 'status_temp')?>: <span><?=($tournModel->status == 'past') ? Yii::t('lang', 'past') : Yii::t('lang', 'active')?></span></li>
		        				<li><?=Yii::t('lang', 'boss')?>: <span><a href=""><?=$tournModel->rules?></a></span></li>
		        				<li><?=Yii::t('lang', 'sponsor')?>: <span><a href=""><?=$tournModel->sponsors?></a></span></li>
		        			</ul>
		        		</div>
		        		<hr class="col-md-12" style="width:97%;">
		        	</div>

		        <?php } ?>
        	<?php }else { ?>
        		<div class="col-md-12 text-center">
                    <span style="font-size: 20px;"><?=Yii::t('lang', 'empty_data')?></span>
                  </div>
        	<?php } ?>
        </div>
    </div>
</div>
