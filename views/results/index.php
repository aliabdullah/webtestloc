<?php 

use dosamigos\chartjs\ChartJs;
use yii\widgets\LinkPager;

?>

<div class="page_content" style="min-height: 631px;
    height: auto;">
<div class="page" style="margin-bottom: 30px;">
	<section class="content-header">
      <h1>
        <?= $user->username ?>
        <small><?=Yii::t('lang', 'your_statistics')?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i><?=Yii::t('lang', 'Home')?></a></li>
        <li class="active"><?= $this->title ?></li>
      </ol>
    </section>
	<!-- Top Menu -->
	<div class="row index__result">
	    
	    <!-- ./col -->
	    <div class="col-lg-4  col-xs-6">
	      <!-- small box -->
	      <div class="small-box bg-red">
	        <div class="inner">
	          <!-- <h3><?= count($modelUser->results) ? count($modelUser->results) : 0 ?></h3> -->

	          <p style="font-size: 25px;"><?=Yii::t('lang', 'Tests')?></p>
	        </div>
	        <div class="icon">
	          <i class="ion ion-pie-graph"></i>
	        </div>
	        <a href="#" id="test" class="small-box-footer">
	          <?=Yii::t('lang', 'more')?> <i class="fa fa-arrow-circle-right"></i>
	        </a>
	      </div>
	    </div>
	    <!-- ./col -->
	    <div class="col-lg-4  col-xs-6">
	      <!-- small box -->
	      <div class="small-box bg-green">
	        <div class="inner">
	          <!-- <h3>00<sup style="font-size: 20px">%</sup></h3> -->

	          <p style="font-size: 25px;"><?=Yii::t('lang', 'rating')?></p>
	        </div>
	        <div class="icon">
	          <i class="ion ion-stats-bars"></i>
	        </div>
	        <a href="#" id="rating" class="small-box-footer">
	          <?=Yii::t('lang', 'more')?> <i class="fa fa-arrow-circle-right"></i>
	        </a>
	      </div>
	    </div>
	    <!-- ./col -->
	    <div class="col-lg-4  col-xs-6">
	      <!-- small box -->
	      <div class="small-box bg-yellow">
	        <div class="inner">
	          <!-- <h3>44</h3> -->

	          <p style="font-size: 25px;"><?=Yii::t('lang', 'records')?></p>
	        </div>
	        <div class="icon">
	          <i class="ion ion-person-add"></i>
	        </div>
	        <a href="#" id="resurs" class="small-box-footer">
	          <?=Yii::t('lang', 'more')?> <i class="fa fa-arrow-circle-right"></i>
	        </a>
	      </div>
	    </div>
	    <!-- ./col -->
	<!-- End Top Menu -->
	<!-- Sections -->

	<!-- End Sections -->
	</div>
</div>

<!-- /////////////////////////// -->

<!-- Tests info start -->
<section class="test">

    <div class="col-md-10" style="margin-top: 15px;">
		<blockquote style="border-left: 4px solid #00c0ef;">
			<h2 style="margin-top: 0px;"><?=Yii::t('lang', 'statistic_your_tests')?></h2>
		   <p><?=Yii::t('lang', 'learn_more_info')?></p>
	  	</blockquote>
	</div>

	<div class="col-md-12">
		<div class="box">
		    <div class="box-header with-border">
		      <h3 class="box-title"><?=Yii::t('lang', 'statstics_of_test')?></h3>
		    </div>
		    <!-- /.box-header -->
		    <div class="box-body">
		      <table class="table table-bordered">

		        <tr style="background-color: #dff6ff;" >
		          <th style="width: 10px">#</th>
		          <!-- <th style="width: 70px">Тесты</th> -->
		          <th style="width: 130px; text-align: center; "><?=Yii::t('lang', 'Items')?></th>
		          <th style="width: 10px; text-align: center;"><?=Yii::t('lang', 'corrects')?></th>
		          <th style="width: 10px; text-align: center;"><?=Yii::t('lang', 'uncorrects')?></th>
		          <th style="width: 150px; text-align: center;"><?=Yii::t('lang', 'progress')?></th>
		          <th style="width: 10px; text-align: center;"><?=Yii::t('lang', 'percent')?></th>
		          <th style="width: 10px; text-align: center;"><?=Yii::t('lang', 'date')?></th>
		        </tr>
	<?php if (isset(\Yii::$app->user->identity)) { ?>
		 <?php if (!empty($modelUser->results)) {  ?>
			<?php $id = 1; foreach ($models as $key => $value) { ?>
		        <tr style="background-color: <?php if($key % 2) { echo '#f0feff'; } ?>">
		        	<td><?= $id++ ?>.</td>
		          <td>
		          	<span  data-pred1-<?= $value->id ?>><?= $value->predmet1 ?></span> 
		          	&nbsp;  |  &nbsp;
		          	<?php $value->predmet2 ? $color = '' : ($color = '#b3acac')?>
		          	<span  data-pred1-<?= $value->id ?> style="color: <?= $color ?>;">
		          		<?= $value->predmet2 ? $value->predmet2 : 'Не выбран'  ?></span>
		          	&nbsp;  |  &nbsp;
		          	<?php $value->predmet2 ? $colore = '' : ($colore = '#b3acac')?>
		          	<span  data-pred1-<?= $value->id ?> style="color: <?=$colore?>;">
		          		<?= $value->predmet3 ? $value->predmet3 : 'Не выбран' ?>
		          	</span>
		          </td>
		          <td class="text-center"><span class=""><?= $value->correct_answer ?></span></td>
		          <td class="text-center"><span class=""><?= $value->wrong_answer ?></span></td>
		          <td>
		          	<?php
		          		$totalCount = $value->correct_answer + $value->wrong_answer;
		          		$protsent = $value->correct_answer * 100 / $totalCount;
		          	 ?>
		            <div class="progress progress-xs progress-striped ">
		              <div class="progress-bar progress-bar-<?php if(intval($protsent) < 25) {
		          		echo 'danger';
		          }elseif(intval($protsent) < 50 && intval($protsent) >= 25) {
		          	echo 'warning';
		          }elseif(intval($protsent) < 75 && intval($protsent) >= 50) {
		          	echo 'primary';
		          }else {
		          	echo 'success';
		          } ?>" style="width: <?= intval($protsent)?>%;"></div>
		            </div>
		          </td>
		          <td><span class="badge bg-<?php if(intval($protsent) < 25) {
		          		echo 'red';
		          }elseif(intval($protsent) < 50 && intval($protsent) >= 25) {
		          	echo 'yellow';
		          }elseif(intval($protsent) < 75 && intval($protsent) >= 50) {
		          	echo 'blue';
		          }else {
		          	echo 'green';
		          } ?>"><?= intval($protsent)?>%</span></td>
		          <td><span class=""><?= $value->date ?></span></td>
		        </tr>
		     <?php  } ?>
		<?php }else { ?>
				<tr>
					<th><?=Yii::t('lang', 'you_have_not_tried_any_tests_yet')?></th>
				</tr>
		<?php } ?>       	
	<?php }else { ?>	        
		<H2 class="well col-md-12 text-center"><?=Yii::t('lang', 'register_to_see_your_results')?></H2>
	<?php } ?>
		      </table>
		    </div>
		    <!-- /.box-body -->
			<?= LinkPager::widget([
			    'pagination' => $pages,
			]); ?>
		  </div>
	</div>
</section>
<!-- Tests info end -->

<!-- ///////////////////// -->

<!-- rating body start -->
<section class=" col-md-12 rating hide ">
	<?php if (isset(\Yii::$app->user->identity)) { ?>
		<div class="col-md-10" style="margin-top: 15px;">
			<blockquote style="border-left: 4px solid #00c0ef;">
				<h2 style="margin-top: 0px;"><?=Yii::t('lang', 'rating_your_results')?></h2>
			   <p><?=Yii::t('lang', 'diogramma')?></p>
		  	</blockquote>
		</div>

		<div id="userChart" style="min-width: 310px; height: 500px; margin: 0 auto; margin-bottom: 150px;"></div>
	<?php }else { ?>
		<H2 class="well col-md-12 text-center"><?=Yii::t('lang', 'register_to_see_your_results')?></H2>
	<?php } ?>
</section>
<!-- rating body end -->

<!-- ////////////////////////// -->

<!-- resurs body start -->
<section class="resurs hide">
	<div class="row">
		<div class="col-md-10 col-md-offset-1 text-center well">
			<h1>Hello I'm resurs page!</h1>
			<p>Here is containing records of all users in website WebTest.uz <strong>SOON!</strong></p>
		</div>
	</div>
</section>
<!-- resurs body end -->

<!-- ////////////////////////// -->

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/series-label.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>

<script type="text/javascript">
	
</script>

</div>