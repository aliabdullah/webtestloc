<?php
use yii\helpers\Url;
	$user = \Yii::$app->user->identity;

	// get time
	$date1 = new DateTime($_SESSION['result']['startTime']);
	$date2 = new DateTime($_SESSION['result']['stopTime']);
	$interval = $date2->diff($date1);
	$hour = $interval->format("%h");
	$min = $interval->format("%i");
	$sec = $interval->format("%s");

	$true = $_SESSION['result']['all']['true'] ? $_SESSION['result']['all']['true'] : 1;
	$false = $_SESSION['result']['all']['false'] ? $_SESSION['result']['all']['false'] : 1;
	$percentTrue = ($true * 100) / ($false + $true);
	$percentFalse = 100 - $percentTrue;	
?>
<style type="text/css">
.GaugeMeter {
  position: relative;
  text-align: center;
  overflow: hidden;
  cursor: default;
  display: inline-block;
}

.GaugeMeter span, .GaugeMeter b {
  width: 54%;
  position: absolute;
  text-align: center;
  display: inline-block;
  color: rgba(0,0,0,.8);
  font-weight: 100;
  font-family: "Open Sans", Arial;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
  margin: 0 23%;
}

.GaugeMeter[data-style="Semi"] b {
  width: 80%;
  margin: 0 10%;
}

.GaugeMeter s, .GaugeMeter u {
  text-decoration: none;
  font-size: .60em;
  font-weight: 200;
  opacity: .6;
}

.GaugeMeter b {
  color: #000;
  font-weight: 200;
  opacity: .8;
}
</style>
<?php if (!isset($_SESSION['result']['saved']) && isset($_SESSION['result'])) { ?>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	<div class="">
	<div class="col-md-10 col-md-offset-1" style="min-height: 637px; margin-top: 70px;">
		<div class="col-md-7 col-md-offset-0">
			<div class="col-lg-6 col-md-6 col-xs-12 col-sm-6" style="">
	            <div class="GaugeMeter gaugeMeter" id="PreviewGaugeMeter_4" 
	            data-percent="<?= intval($percentFalse) ?>" 
	            data-append="%" 
	            data-size="190" 
	            data-theme="green" 
	            data-back="RGBa(22,22,22,.1)" 
	            data-animate_gauge_colors="1" 
	            data-animate_text_colors="1" 
	            data-width="5"
	            data-label_color="#FFF" 
	            data-stripe="2" 
	            data-id="PreviewGaugeMeter_4" 
	            style="width: 180px; margin-left: 29px; "><span style="line-height: 193px;  color: #2980b9; font-size: 39.6px;"><?= intval($percentFalse) ?><u>%</u></span><canvas width="180" height="180"></canvas></div>
	            <span class="text-center col-md-11 col-sm-8 col-xs-8 col-lg-10" style="margin-left: 10%; color: #696969;">
	            	<span class=" text-center" style="font-size: 25px; color:#D83C3B; margin: 10px auto;"><?= $_SESSION['result']['all']['false'] ? $_SESSION['result']['all']['false'] : 0 ?>
	            	</span><br>
	            	<span style="margin: 10px auto;"><?=Yii::t('lang', 'uncorrect')?></span>
	            </span>
	        </div>
		
			<div class="col-lg-6 col-md-6 col-xs-12 col-sm-6" style="">
	            <div class="GaugeMeter gaugeMeter" id="PreviewGaugeMeter_4" 
	            data-percent="<?= intval($percentTrue) ?>" 
	            data-append="%" 
	            data-size="190" 
	            data-theme="green" 
	            data-back="RGBa(22,22,22,.1)" 
	            data-animate_gauge_colors="1" 
	            data-animate_text_colors="1" 
	            data-width="5"
	            data-label_color="#696969"
	            data-stripe="2" 
	            data-id="PreviewGaugeMeter_4" 
	            style="width: 180px; margin-left: 29px;"><span style="line-height: 193px;  color: #2980b9; font-size: 39.6px;"><?= intval($percentTrue) ?><u>%</u></span><canvas width="180" height="180"></canvas></div>
	            <span class="text-center col-md-11 col-sm-8 col-xs-8 col-lg-10" style="margin-left: 10%; color: #696969;">
	            	<span class=" col-lg-12 text-center" style="font-size: 25px; color:#93de93;"><?= $_SESSION['result']['all']['true'] ? $_SESSION['result']['all']['true'] : 0?></span>
	            	<br>
	            	<span style=" margin: 10px auto;"><?=Yii::t('lang', 'correct')?></span>
	            </span>
	        </div>
		</div>
		<div class="col-lg-5 col-md-5 col-xs-12 col-sm-8" style="">
            <div class="" style="border-left: 1px dashed #ccc; height: 210px; padding: 15px 0 0 40px; margin-left: 30px; margin-top: 40px;  ">
            	<h4 style="color: #696969;">
            		<?=Yii::t('lang', 'question')?> : <span style="color:#3498db; font-size: 21px;"><?= $_SESSION['result']['all']['true'] + $_SESSION['result']['all']['false']?></span>
            	</h4>
            	<h4 style="color: #696969;">
            		<?=Yii::t('lang', 'correct')?> : <span style="color:#3498db; font-size: 21px;"><?= $_SESSION['result']['all']['true'] ? $_SESSION['result']['all']['true'] : 0?></span>
            	</h4>
            	<h4 style="color: #696969;">
            		<?=Yii::t('lang', 'uncorrect')?> : <span style="color:#3498db; font-size: 21px;"><?= $_SESSION['result']['all']['false'] ? $_SESSION['result']['all']['false'] : 0 ?></span>
            	</h4>
            	<h4 style="color: #696969;">
            		<?=Yii::t('lang', 'dateres')?> : <span style="color:#3498db; font-size: 21px;">
            		<?= '0'.$hour.' : '.(($min < 10 ) ? '0'.$min : $min).' : '. (($sec < 10 ) ? '0'.$sec : $sec) ?> </span>
            	</h4>
            	<!-- <h4 style="color: #696969;">
            		Бал : <span style="color:#3498db; font-size: 21px;"></span>
            	</h4> -->
            </div>
        </div>

        <div class="col-md-11 col-lg-11 col-sm-11 col-xs-11" style="margin-top: 70px;">
          <table class="table table-striped text-center">
            <thead>
              <tr>
              	<?php foreach ($_SESSION['result']['checked'] as $predName => $resArr) { ?>
	                <th style="font-size: 20px;"><?= mb_substr($predName, 0, -1); ?></th>
	            <?php } ?>
              </tr>
            </thead>
            <tbody>
            	<tr>
            	<?php foreach ($_SESSION['result']['checked'] as $predName => $resArr) { ?>
	  	            <td><span><?=Yii::t('lang', 'correct')?> -</span> <?= $resArr['true'] ? $resArr['true'] : 0 ?> </td>
  	          	<?php } ?>
				</tr>
				<tr>
            	<?php foreach ($_SESSION['result']['checked'] as $predName => $resArr) { ?>
	  	            <td><span><?=Yii::t('lang', 'uncorrect')?> -</span> <?= $resArr['false'] ? $resArr['false'] : 0 ?> </td>
  	          	<?php } ?>
				</tr>              
            </tbody>
          </table>
        </div>
        <?php 
        	$lang = \Yii::$app->language;
        	if ($_SESSION['result']['type'] != 'free') { ?>
		        <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12" style="margin-top: 70px; margin-bottom: 30px;">
		        	<a href='<?= Url::to(["$lang/results/delete"])?>'>
		        	<button class="btn btn-danger" ><?= Yii::t('lang', 'deleteres') ?></button></a>
		        	<a href='<?= Url::to(["$lang/results/save"])?>'>
					<button class="btn btn-success"><?= Yii::t('lang', 'saveres') ?></button></a>
				</div>
		<?php }else { ?>
			<div class="col-md-12" style="margin-top: 70px; margin-bottom: 30px;">
	        	<a href='<?= Url::to(["/"])?>'>
	        	<button class="btn btn-default" ><?= Yii::t('lang', 'to_home') ?></button></a>
	        	<a href='<?= Url::to(["/test/index"])?>'>
				<button class="btn btn-success"><?= Yii::t('lang', 'tariffs') ?></button></a>
			</div>
		<?php } ?>
	</div>
</div>

<?php } elseif ($_SESSION['result']['saved'] == true) { ?>
	<div class="row">
		<div class="col-md-10 col-md-offset-1 text-center " style=" padding-top: 20px; padding-bottom: 20px;" >
			<img src="/images/save-1.png" style="width: 200px; height: 200px;">
			<span style="color: #7d7c7c; font-size: 25px; margin-left: 25px; font-weight: bold; margin-top: 10px;"><?=Yii::t('lang', 'savedres')?></span>
		</div>
	</div>	
<?php  }elseif (empty($_SESSION['result'])) { ?>
	<div class="row">
		<div class="col-md-10 col-md-offset-1 text-center" >
			<img src="/images/delete-1.png" style="width: 200px; height: 200px;">
			<span style="color: #7d7c7c; margin-top: 10px;font-size: 25px; margin-left: 20px; font-weight: bold;"><?=Yii::t('lang', 'deleteres')?></span>
		</div>
	</div>	
<?php }elseif (empty($_SESSION['result'][Yii::$app->user->identity->username])) { ?>
	<div class="row">
		<div class="col-md-10 col-md-offset-1 text-center" >
			<!-- <img src="/images/delete-1.png" style="width: 200px; height: 200px;"> -->
			<span style="color: #7d7c7c; margin-top: 10px;font-size: 25px; margin-left: 20px; font-weight: bold;"><?=Yii::t('lang', 'you_are_dont_answered_no_one_questions')?></span>
		</div>
	</div>	
<?php } ?>
