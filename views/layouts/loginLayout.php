<?php

use yii\helpers\Html;
use app\assets\LoginAsset;
use yii\helpers\Url;
use yii\bootstrap\Modal;

LoginAsset::register($this);
$this->title = 'WebTest.uz';
$user = Yii::$app->user->identity;
if (!empty($user->photo)) {
  $userPhoto = '/'.$user->photo;
}else {
  $userPhoto = '/uploads/user1.png';
}
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
  <meta charset="<?= Yii::$app->charset ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <?=Html::csrfMetaTags() ?>
  <link rel="icon" type="/image/x-icon" href="favicon.ico">
</head>

<body>
<?php $this->beginBody() ?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.12&appId=927137210778831&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div class="wrapper">
  <div class="container" style="min-height: 650px !important; max-height: 650px !important;">
    <div class="header">
      <button class="hidden-sm hidden-md hidden-lg rightBox__mobMenu">
        <img src="/images/menu.png" class="rightBox__menuIcon">
      </button>
      <div class="header__leftBox leftBox">
        <div class="leftBox__logotype">
          <a href="<?= Url::to(['/']) ?>">
          <img class="leftBox__logo" src="/images/logo.png">
          </a>
        </div>
        <div class="leftBox__balance">
          <!-- Мой баланс: 154.000 сум -->
        </div>
      </div>
      <div class="header__rightBox rightBox ">
        <div class="rightBox__subscribe">
          <!-- <span class="rightBox__period">Подписка действует до 11.12.17</span> -->
          <!-- <a href="#" class="rightBox__renew">Продлить <i class="fi flaticon-right-arrow"></i></a> -->
        </div>
        <div class="rightBox__user user" role="button">
          <div class="user__inner">
            <div class="user__avatar" style=" overflow: hidden; background-size: cover; border: 1px solid white;">
              <!-- Изображение пользователя -->
              <img src="<?= $userPhoto ?>" style="width: 100%; height: 100%;">
            </div>
            <div class="user__name">
              <?= Yii::$app->user->identity->username ?> <i class="fi flaticon-down-arrow"></i>
            </div>
          </div>
        </div>
        
        <?= $this->render('language'); ?>
        
        <?php if (Yii::$app->user->isGuest) { ?>
          <!-- <a href="<?= Url::to(['/site/login'])?>"><span class="lang-link" ><?= Yii::t("lang", "Войти") ?></span></a>   -->
        <?php }else { ?> 
          <!-- <a href="<?= Url::to(['/site/logout'])?>"><span class="lang-link" ><?= Yii::t("lang", "Выйти") ?></span></a> -->
        <?php } ?>
      </div>
    </div>

    <div class="content" style="background-color: #e9eeef;">
        <?= $content ?>
        <div class="main__nav nav">
          <ul class="nav__list">
            <li class="nav__item">
              <a href="<?= Url::to(['footer/index'])?>" class="nav__link">О системе</a>
            </li>
            <li class="nav__item">
              <a href="<?= Url::to(['footer/tarrif'])?>" class="nav__link">Тарифы</a>
            </li>
            <li class="nav__item">
              <a href="<?= Url::to(['footer/payment'])?>" class="nav__link">Оплата услуг</a>
            </li>
            <li class="nav__item">
              <a href="<?= Url::to(['footer/oferta'])?>" class="nav__link">Оферта</a>
            </li>
            <li class="nav__item">
              <a href="<?= Url::to(['footer/politic'])?>" class="nav__link">Политика безопасности</a>
            </li>
            <li class="nav__item">
              <a href="<?= Url::to(['footer/partner'])?>" class="nav__link">Партнерам</a>
            </li>
            <li class="nav__item">
              <a href="<?= Url::to(['footer/contact'])?>" class="nav__link">Контакты</a>
            </li>
          </ul>
          <ul class="nav__social social">
            <li class="social__item">
              <a href="#" class="social__link">
                <img src="/images/fb.png" class="social__icon">
              </a>
            </li>
            <li class="social__item">
              <a href="#" class="social__link">
                <img src="/images/vk.png" class="social__icon">
              </a>
            </li>
            <li class="social__item">
              <a href="#" class="social__link">
                <img src="/images/rss.png" class="social__icon">
              </a>
            </li>
          </ul>
        </div>

        <div class="main__footer footer">
          <div class="footer__copyright">&copy; OOO "E-Services house"</div>
          <div class="footer__developed">Сайт разработан в студии <a href="http://qwerty.uz" class="footer__qwerty">QWERTY</strong></div>
        </div>
    </div>
      </div>
    </div>
  </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>