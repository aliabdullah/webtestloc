<?php

use yii\helpers\Html;
use app\assets\MaterialsAsset;
use yii\helpers\Url;
use yii\bootstrap\Modal;

MaterialsAsset::register($this);
$this->title = 'WebTest.uz';
$user = Yii::$app->user->identity;
if (!empty($user->photo)) {
  if (substr($user->photo, 0, 4) == 'http') {
      $userPhoto = $user->photo;  
  }else{
      $userPhoto = '/'.$user->photo;
  }
}else {
  $userPhoto = '/uploads/user1.png';
}
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    
  <meta charset="<?= Yii::$app->charset ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <?=Html::csrfMetaTags() ?>
  <!-- <link rel="icon" type="/image/x-icon" href="favicon.ico"> -->
  <link rel="icon" href="/favicon.ico" type="image/x-icon">
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon"> 

</head>

<body>
<?php $this->beginBody() ?>
<div class="wrapper">
  <div class="container">
    <div class="header">
      <button class="hidden-sm hidden-md hidden-lg rightBox__mobMenu">
        <img src="/images/menu.png" class="rightBox__menuIcon">
      </button>
      <div class="header__leftBox leftBox">
        <div class="leftBox__logotype">
          <a href="<?= Url::to(['/']) ?>">
          <img class="leftBox__logo" src="/images/logo.png">
          </a>
        </div>
        <div class="leftBox__balance">
          <!-- Мой баланс: 154.000 сум -->
        </div>
      </div>
      <div id='cssmenu' class="col-md-3">
         <ul>
            <li class='active has-sub'><a href='#'><i class="fa fa-fw fa-bars"></i><span>Категории</span></a>
               <ul>
                <?php  foreach (app\models\category\Category::getTree() as $key => $parentCat) {
                  if(isset($parentCat['childs'])) { $childs = 'has-sub'; }else $childs = '';
                    if (Yii::$app->controller->action->id == 'view') {
                          $currentUrl = Yii::$app->controller->id.'/index';
                     }else $currentUrl = Yii::$app->controller->route; ?>
                  <li class='<?=$childs?>'><a href='<?= Url::to([$currentUrl, 'id' => $key]) ?>'><span><?= Yii::t('lang', $parentCat['name']) ?></span></a>
                    <?php if (isset($parentCat['childs'])): ?>
                      <ul>
                        <?php foreach ($parentCat['childs'] as $key => $child) { 
                          if(isset($parentCat['childs'])) { $childs = 'has-sub'; }else $childs = ''; ?>
                            <li class="<?=$childs?>"><a href='<?= Url::to([$currentUrl, 'id' => $key]) ?>'><span><?= Yii::t('lang', $child['name']) ?></span></a>
                              <ul>
                                <?php if (isset($child['childs'])): ?>
                                <?php foreach ($child['childs'] as $id => $subchild) { 
                                  if(isset($subchild['childs'])) { $childs = 'has-sub'; }else $childs = ''; ?>
                                    <li class="<?=$childs?>"><a href='<?= Url::to([$currentUrl, 'id' => $id]) ?>'><span><?= Yii::t('lang', $subchild['name']) ?></span></a></li>
                                 <?php } ?>
                                 <?php endif ?>
                              </ul>
                            </li>
                         <?php } ?>
                      </ul>
                  <?php endif ?>
                  </li>
                  <?php } ?>
               </ul>
            </li>
         </ul>
      </div>

      <div class="header__rightBox rightBox ">
        <div class="rightBox__subscribe">
          <!-- <span class="rightBox__period">Подписка действует до 11.12.17</span>
          <a href="#" class="rightBox__renew">Продлить <i class="fi flaticon-right-arrow"></i></a> -->
        </div>
        <div class="rightBox__user user" role="button" style="margin-right: 30px;">
          <div class="user__inner">
            <div class="user__avatar" style=" overflow: hidden; background-size: cover; border: 1px solid white;">
              <!-- Изображение пользователя -->
              <img src="<?= $userPhoto ?>" style="width: 100%; height: 100%;">
            </div>
            <div class="user__name">
             <?= Yii::$app->user->identity->username ?> <i class="fi flaticon-down-arrow"></i>
            </div>
          </div>
        </div>
        <!-- <div class="user__dropdown">
          <span><?//=Yii::t('lang', "Привет")?></span>
        </div> -->
        <?php if (Yii::$app->language == 'ru') {
          $active_ru = "lang__active";
        }else {
          $active_uz = "lang__active";
        } ?>
        <div class="lang__div">
          <ul>
            <li class="<?= $active_uz ?>">
              <?= Html::a('O\'z', array_merge(Yii::$app->request->get(), [Yii::$app->controller->route, 'language' => 'uz']), ['class' => "$active_uz"]); ?>
              <!-- <a class="" href="/uz">O`z</a> -->
            </li>
            <li class="<?= $active_ru ?>">
              <?= Html::a('Ру', array_merge(Yii::$app->request->get(), [Yii::$app->controller->route, 'language' => 'ru']), ['class' => '$active_ru']); ?>
              <!-- <a class="" href="/ru">Ру</a> -->
            </li>
          </ul>
        </div>
        <?php if (Yii::$app->user->isGuest) { ?>
          <a class="login" href="<?= Url::to(['/site/login'])?>"><span class="lang-link" style="color: white !important;
            font-size: 17px;
            margin-left: 20px;" ><?= Yii::t("lang", "Signin") ?></span></a>  
        <?php }else { ?> 
          <a class="login" href="<?= Url::to(['/site/logout'])?>"><span class="lang-link" style="color: white !important;
            font-size: 17px;
            margin-left: 20px;" ><?= Yii::t("lang", "Logout") ?></span></a>
        <?php } ?>
      </div>
    </div>
    <!-- <div class="col-md-12" style="background: #fff; width: 100%; height: 40px;">
    </div> -->
    <div class="content">
      <div class="content__navigation navigation">
        <nav>
          <div id="menu" class="menu white">
            <div class="menu-header"><a href="<?= Url::to(['materials/index']) ?>"><?=Yii::t('lang', "Categories")?></a></div>
            <ul>
              <?php  foreach (app\models\category\Category::getTree() as $key => $parentCat) { ?>
                <li><a href="<?= Url::to([Yii::$app->controller->route, 'id' => $key]) ?>"><i class="fa fa-folder-open"> </i><?= Yii::t('lang', $parentCat['name']) ?></a>
                  <?php if (isset($parentCat['childs'])): ?>
                    <?php foreach ($parentCat['childs'] as $key => $child) { ?>
                      <ul class="submenu">
                        <li><a href="<?= Url::to([Yii::$app->controller->route, 'id' => $key]) ?>"> <?= Yii::t('lang', $child['name']) ?></a>
                        </li>
                      </ul>
                    <?php } ?>
                  <?php endif ?>
                </li>
              <?php } ?>
            </ul>
            <div class="menu-footer"> @ Test</div>
          </div>

        </nav>
      </div>
    </div>

    <div class="content__main main ">
        <?= $content ?>

        <div class="main__nav nav">
          <ul class="nav__list">
            <li class="nav__item">
              <a href="<?= Url::to(['footer/index'])?>" class="nav__link"><?= Yii::t('lang', 'About_sistem') ?></a>
            </li>
            <!-- <li class="nav__item">
              <a href="#" class="nav__link">Как пополнить счет</a>
            </li> -->
            <li class="nav__item">
              <a href="<?= Url::to(['footer/tarrif'])?>" class="nav__link"><?= Yii::t('lang', 'Tariffs') ?></a>
            </li>
            <li class="nav__item">
              <a href="<?= Url::to(['footer/payment'])?>" class="nav__link"><?= Yii::t('lang', 'Payment_for_services') ?></a>
            </li>
            <li class="nav__item">
              <a href="<?= Url::to(['footer/oferta'])?>" class="nav__link"><?= Yii::t('lang', 'Offer') ?></a>
            </li>
            <li class="nav__item">
              <a href="<?= Url::to(['footer/politic'])?>" class="nav__link"><?= Yii::t('lang', 'Security_policy') ?></a>
            </li>
            <li class="nav__item">
              <a href="<?= Url::to(['footer/partner'])?>" class="nav__link"><?= Yii::t('lang', 'Partners') ?></a>
            </li>
            <li class="nav__item">
              <a href="<?= Url::to(['footer/contact'])?>" class="nav__link"><?= Yii::t('lang', 'Contacts') ?></a>
            </li>
          </ul>
          <ul class="nav__social social">
            <li class="social__item">
              <a href="#" class="social__link">
                <img src="/images/fb.png" class="social__icon">
              </a>
            </li>
            <li class="social__item">
              <a href="#" class="social__link">
                <img src="/images/vk.png" class="social__icon">
              </a>
            </li>
            <li class="social__item">
              <a href="#" class="social__link">
                <img src="/images/rss.png" class="social__icon">
              </a>
            </li>
          </ul>
        </div>

       <div class="main__footer footer">
          <div class="footer__copyright">&copy; OOO "E-Services house"</div>
          <?php if (Yii::$app->language == "ru") { ?>
            <div class="footer__developed">Сайт разработан в студии <a href="https://Qwerty.uz" class="footer__qwerty"><strong>QWERTY</strong></a></div>
          <?php }else { ?>
            <div class="footer__developed">Ushbu sayt <a href="https://Qwerty.uz" class="footer__qwerty"><strong>QWERTY</strong></a> da ishlangan</div>
          <?php } ?>
        </div>
    </div>

    
    </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>