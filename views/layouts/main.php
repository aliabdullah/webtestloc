<?php

use yii\helpers\Html;
use app\assets\AppAsset;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use app\models\user\Order;


AppAsset::register($this);
$this->title = 'WebTest.uz';
$user = Yii::$app->user->identity;
if (!empty($user->photo)) {
  if (substr($user->photo, 0, 4) == 'http') {
      $userPhoto = $user->photo;  
  }else{
      $userPhoto = '/'.$user->photo;
  }
}else {
  $userPhoto = '/uploads/user1.png';
}
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    
  <meta charset="<?= Yii::$app->charset ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <?=Html::csrfMetaTags() ?>
  <link rel="icon" type="/image/x-icon" href="favicon.ico">
  <link rel="stylesheet" type="text/css" href="/css/site.css" media="print" />

</head>

<body>
<?php $this->beginBody() ?>

<div class="wrapper">
  <div class="container">
    <div class="header">
      <button class="hidden-sm hidden-md hidden-lg rightBox__mobMenu">
        <img src="/images/menu.png" class="rightBox__menuIcon">
      </button>
      <div class="header__leftBox leftBox">
        <div class="leftBox__logotype">
          <a href="<?= Url::to(['/']) ?>">
            <?php if (Yii::$app->language == "ru") { ?>
              <img class="leftBox__logo" src="/images/logo.png">     
            <?php }else { ?>
              <img class="leftBox__logo" src="/images/logo-uz.png">
            <?php } ?>
          
          </a>
        </div>
        <div class="leftBox__balance">
          <!-- Мой баланс: 154.000 сум -->
        </div>
      </div>
      <div class="header__rightBox rightBox ">
        <div class="rightBox__subscribe">
         <!--  <span class="rightBox__period">Подписка действует до 11.12.17</span>
          <a href="#" class="rightBox__renew">Продлить <i class="fi flaticon-right-arrow"></i></a> -->
        </div>
        <div class="rightBox__user user" role="button">
          <?php if (!\Yii::$app->user->isGuest): ?>
            <div class="user__inner">
            <div class="user__avatar" style=" overflow: hidden; background-size: cover; border: 2px solid white;">
              <!-- Изображение пользователя -->
              <img src="<?= $userPhoto ?>" style="width: 100%; height: 100%;">
            </div>
            <div class="user__name">
              <?= Yii::$app->user->identity->username ?>
            </div>
          </div>
          <?php endif ?>
          
        </div>
        <!-- turn on language -->
        <?= $this->render('language'); ?>
        
        
        <?php if (Yii::$app->user->isGuest) { ?>
          <a href="<?= Url::to(['/site/login'])?>"><span class="log-link" ><?= Yii::t("lang", "Signin") ?></span></a>  
        <?php }else { ?> 
          <a href="<?= Url::to(['/site/logout'])?>"><span class="log-link" ><?= Yii::t("lang","Logout") ?></span></a>
        <?php } ?>
      </div>
    </div>

    <div class="content">
      <div class="content__navigation navigation">
        <ul class="navigation__list">
          <li class="navigation__item">
            <?php $userOrder = Order::find()->where(['user_id' => $user->id, 'status' => 1])->all();
            if (!empty($userOrder)) { ?>
              <a href="<?= Url::to(['/test/premium-filter'])?>" class="navigation__link">
                <div class="navigation__icon">
                  <img class="navigation__img" src="/images/test.png">
                </div>
                <span class="navigation__name"> <?= Yii::t("lang", "Tests") ?> </span>
              </a>
            <?php }else { ?>
              <a type="button" class="navigation__link" data-toggle="modal" data-target="#choose_test_type">
                <div class="navigation__icon">
                  <img class="navigation__img" src="/images/test.png">
                </div>
                <span class="navigation__name"> <?= Yii::t("lang", "Tests") ?> </span>
              </a>
            <?php } ?>
          </li>
          <?php if (empty($user)) { 
            $url = Url::to(['/site/login']);
          }else{
            $url = Url::to(['/results/index']);
          } ?>
          <li class="navigation__item">
            <a href="<?= $url?>" class="navigation__link">
              <div class="navigation__icon">
                <img class="navigation__img" src="/images/presentation.png">
              </div>
              <span class="navigation__name"><?= Yii::t('lang', 'My_results') ?></span>
            </a>
          </li>
          <li class="navigation__item">
            <a href="<?= Url::to(['/task/events'])?>" class="navigation__link">
              <div class="navigation__icon">
                <img class="navigation__img" src="/images/agenda.png">
              </div>
              <span class="navigation__name"><?= Yii::t('lang', 'Materials') ?></span>
            </a>
          </li>
          <li class="navigation__item">
            <a href="<?= Url::to(['/tariffs/index'])?>" class="navigation__link">
              <div class="navigation__icon">
                <img class="navigation__img" src="/images/credit-card.png">
              </div>
              <span class="navigation__name"><?= Yii::t('lang', 'Orders') ?></span>
            </a>
          </li>
           <li class="navigation__item">
            <a href="<?= Url::to(['/cabinet/index'])?>" class="navigation__link">
              <div class="navigation__icon">
                <img class="navigation__img" src="/images/user.png">
              </div>
              <span class="navigation__name"><?= Yii::t('lang', 'Cabinet') ?></span>
            </a>
          </li>
        </ul>
        <span class="navigation__years">2017-2018</span>
      </div>
      <div class="content__main main ">
        <?= $content ?>

        <div class="main__nav nav">
          <ul class="nav__list">
            <li class="nav__item">
              <a href="<?= Url::to(['footer/index'])?>" class="nav__link"><?= Yii::t('lang', 'About_sistem') ?></a>
            </li>
            <!-- <li class="nav__item">
              <a href="#" class="nav__link">Как пополнить счет</a>
            </li> -->
            <li class="nav__item">
              <a href="<?= Url::to(['footer/tarrif'])?>" class="nav__link"><?= Yii::t('lang', 'Tariffs') ?></a>
            </li>
            <li class="nav__item">
              <a href="<?= Url::to(['footer/payment'])?>" class="nav__link"><?= Yii::t('lang', 'Payment_for_services') ?></a>
            </li>
            <li class="nav__item">
              <a href="<?= Url::to(['footer/oferta'])?>" class="nav__link"><?= Yii::t('lang', 'Offer') ?></a>
            </li>
            <li class="nav__item">
              <a href="<?= Url::to(['footer/politic'])?>" class="nav__link"><?= Yii::t('lang', 'Security_policy') ?></a>
            </li>
            <li class="nav__item">
              <a href="<?= Url::to(['footer/partner'])?>" class="nav__link"><?= Yii::t('lang', 'Partners') ?></a>
            </li>
            <li class="nav__item">
              <a href="<?= Url::to(['footer/contact'])?>" class="nav__link"><?= Yii::t('lang', 'Contacts') ?></a>
            </li>
          </ul>
          <ul class="nav__social social">
            <li class="social__item">
              <a href="#" class="social__link">
                <img src="/images/fb.png" class="social__icon">
              </a>
            </li>
            <li class="social__item">
              <a href="#" class="social__link">
                <img src="/images/vk.png" class="social__icon">
              </a>
            </li>
            <li class="social__item">
              <a href="#" class="social__link">
                <img src="/images/rss.png" class="social__icon">
              </a>
            </li>
          </ul>
        </div>

        <div class="main__footer footer">
          <div class="footer__copyright">&copy; OOO "E-Services house"</div>
          <?php if (Yii::$app->language == "ru") { ?>
            <div class="footer__developed">Сайт разработан в студии <a href="https://Qwerty.uz" class="footer__qwerty"><strong>QWERTY</strong></a></div>
          <?php }else { ?>
            <div class="footer__developed">Ushbu sayt <a href="https://Qwerty.uz" class="footer__qwerty"><strong>QWERTY</strong></a> da ishlangan</div>
          <?php } ?>
        </div>
      </div>
        

      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="choose_test_type" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog lg md xs" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 style="  text-align:center; color:darkseagreen;" class="modal-title" id="myModalLabel">
        <?= Yii::t('lang', 'Check_status')?><br>
        <span style="font-size:16px;"><?= Yii::t('lang', 'Choose_the_right_direction_for_yourself')?></span></h3>
      </div>
      <div class="modal-body">
        <?=$this->render('/test/modal')?>
      </div>
    </div>
  </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>