<?php

use yii\helpers\Html;
// use app\assets\AppAsset;
use app\assets\TaskAsset;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use app\models\user\Order;
use app\models\task\AttemptFull;
use app\models\task\UserMessages;
use app\models\task\Tournament;
use app\models\task\Tasks;


// AppAsset::register($this);
TaskAsset::register($this);
$this->title = 'WebTest.uz';
$user = Yii::$app->user->identity;
if (!empty($user->photo)) {
  if (substr($user->photo, 0, 4) == 'http') {
      $userPhoto = $user->photo;  
  }else{
      $userPhoto = '/'.$user->photo;
  }
}else {
  $userPhoto = '/uploads/user1.png';
}
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    
  <meta charset="<?= Yii::$app->charset ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <?=Html::csrfMetaTags() ?>
  <link rel="icon" type="/image/x-icon" href="favicon.ico">
  <link rel="stylesheet" type="text/css" href="/css/site.css" media="print" />
  <!-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> -->

</head>

<style>
  body {
    font-family: Arial;
  }

  .timer,
  /*.timer2 {
    font-size: 70px;
    text-align: center;
    margin-top: 100px;
    color: #555;
  }*/
  
  .timer i {
    color: #fff;
    font-size: 28px;
  }

  /*.timer2 span {
    font-weight: normal;
    text-transform: uppercase;
    color: #bbb;
  }*/
</style>

<body>
<?php $this->beginBody() ?>
<div class="wrapper">
  <div class="container">
    <div class="header">
      <button class="hidden-sm hidden-md hidden-lg rightBox__mobMenu">
        <img src="/images/menu.png" class="rightBox__menuIcon">
      </button>
      <div class="header__leftBox leftBox">
        <div class="leftBox__logotype">
          <a href="<?= Url::to(['/']) ?>">
            <?php if (Yii::$app->language == "ru") { ?>
              <img class="leftBox__logo" src="/images/logo.png">     
            <?php }else { ?>
              <img class="leftBox__logo" src="/images/logo-uz.png">
            <?php } ?>
          
          </a>
        </div>
        <div class="leftBox__balance">
          <!-- Мой баланс: 154.000 сум -->
        </div>
      </div>
      <?php
          $tourn = Tournament::find()->where(['status' => 'active'])->one();
          $tournDate = date('Y-m-d', strtotime($tourn->day));
          $tournTime = explode(',', $tourn->time);
          if (!empty($tourn) && $tournDate >= date('Y-m-d')) {
            $trueDate = $tournDate;
            // echo '<pre>';
            // print_r(date('d-m-Y', $trueDate));
            // echo '</pre>';
            // exit();
          }
          $currentController = Yii::$app->controller->id;
          $currentAction = Yii::$app->controller->action->id;
          if ($currentAction == 'tournament' || $currentAction == 'tourn-view' || $currentAction == 'answ-tourn') { ?>
            <div id='cssmenu' class="col-md-3" style="background: #2980b9;  margin-left: 100px; margin-top: 0; margin-bottom: 0;">
              <h4 id="text-date" class=" text-center">TURNIR FAOLLASHTIRILISHI</h4>
              <div id="time" class="hide text-center"></div>
              <div class="timer j-timer-second text-center"></div>
            </div>
      <?php }else { ?>
          <div id='cssmenu' class="col-md-3">
             <ul>
                <li class='active has-sub'><a href='#'><i class="fa fa-fw fa-bars"></i><span>Категории</span></a>
                   <ul>
                    <?php  foreach (app\models\category\Category::getTree() as $key => $parentCat) {
                      if(isset($parentCat['childs'])) { $childs = 'has-sub'; }else $childs = '';
                        if (Yii::$app->controller->action->id == 'view') {
                              $currentUrl = Yii::$app->controller->id.'/index';
                         }else $currentUrl = Yii::$app->controller->route; ?>
                      <li class='<?=$childs?>'><a href='<?= Url::to([$currentUrl, 'id' => $key]) ?>'><span><?= Yii::t('lang', $parentCat['name']) ?></span></a>
                        <?php if (isset($parentCat['childs'])): ?>
                          <ul>
                            <?php foreach ($parentCat['childs'] as $key => $child) { 
                              if(isset($parentCat['childs'])) { $childs = 'has-sub'; }else $childs = ''; ?>
                                <li class="<?=$childs?>"><a href='<?= Url::to([$currentUrl, 'id' => $key]) ?>'><span><?= Yii::t('lang', $child['name']) ?></span></a>
                                  <ul>
                                    <?php if (isset($child['childs'])): ?>
                                    <?php foreach ($child['childs'] as $id => $subchild) { 
                                      if(isset($subchild['childs'])) { $childs = 'has-sub'; }else $childs = ''; ?>
                                        <li class="<?=$childs?>"><a href='<?= Url::to([$currentUrl, 'id' => $id]) ?>'><span><?= Yii::t('lang', $subchild['name']) ?></span></a></li>
                                     <?php } ?>
                                     <?php endif ?>
                                  </ul>
                                </li>
                             <?php } ?>
                          </ul>
                      <?php endif ?>
                      </li>
                      <?php } ?>
                   </ul>
                </li>
             </ul>
          </div>
      <?php } ?>
      <div class="header__rightBox rightBox ">

        <div class="rightBox__subscribe">
          <li class="dropdown messages-menu pull-right">
            <a href="#" class="dropdown-toggle alertbox" id="alertbox">
              <i class="fa fa-envelope-o"></i>
              <?php 
                $user = Yii::$app->user->identity;
                $messages = UserMessages::find()->where(['user_id' => $user->id])->andWhere(['checked' => null])->all();
              ?>
              <?php if (count($messages) > 0): ?>
                <span class="label label-success " id="count_message"></span>
              <?php endif ?>
              <?//= $messages ? "<span class='label label-success'>".count($messages)."</span>" : '' ?>
            </a>
            <ul class="preview_message">
              <li class="header_message" style="margin: 10px;
    font-size: 13px;"><?=count($messages)?> ta javob</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu notify_list" id="ul">
                </ul>
              </li>
              <!-- <li class="footer"><a href="<?=Url::to('/admin/task/messages')?>">See All Messages</a></li> -->
            </ul>
          </li>
         <!--  <span class="rightBox__period">Подписка действует до 11.12.17</span>
          <a href="#" class="rightBox__renew">Продлить <i class="fi flaticon-right-arrow"></i></a> -->
        </div>
        <div class="rightBox__user user" role="button">
          <?php if (!\Yii::$app->user->isGuest): ?>
            <div class="user__inner">
            <div class="user__avatar" style=" overflow: hidden; background-size: cover; border: 2px solid white;">
              <!-- Изображение пользователя -->
              <img src="<?= $userPhoto ?>" style="width: 100%; height: 100%;">
            </div>
            <div class="user__name">
              <?= Yii::$app->user->identity->username ?>
            </div>
          </div>
          <?php endif ?>
          
        </div>
        <!-- turn on language -->
        <?= $this->render('language'); ?>

        
        <?php if (Yii::$app->user->isGuest) { ?>
          <a style="margin-left: 15px;" href="<?= Url::to(['/site/login'])?>"><span class="log-link" ><?= Yii::t("lang", "Signin") ?></span></a>  
        <?php }else { ?> 
          <a style="margin-left: 15px;" href="<?= Url::to(['/site/logout'])?>"><span class="log-link" ><?= Yii::t("lang","Logout") ?></span></a>
        <?php } ?>
      </div>
    </div>

    <div class="content task">
      <div class="content__navigation navigation"  style="box-shadow: 1px 0px 2px #888888;">
        <nav>
          <div id="menu" class="menu white">
            <div class="menu-header"><a href="<?= Url::to(['materials/index']) ?>"><?=Yii::t('lang', "Categories")?></a></div>
            <ul>
                <li><a href="<?= Url::to(['/task/events']) ?>"><i class="fa fa-calendar"> </i><?= Yii::t('lang', 'Events') ?></a>
                </li>
                <li><a href="<?= Url::to(['/task/index'])?>"><i class="fa fa-tasks"> </i><?= Yii::t('lang', 'Tasks') ?></a>
                </li>
                <li><a href="<?= Url::to(['/task/tournament'])?>"><i class="fa fa-edit"> </i><?= Yii::t('lang', 'Tournaments') ?></a>
                </li>
                <li><a href="<?= Url::to(['/task/rating'])?>"><i class="fa fa-star"> </i><?= Yii::t('lang', 'Ratings') ?></a>
                </li>
                <li><a href="<?= Url::to(['/materials/index'])?>"><i class="fa fa-newspaper-o"> </i><?= Yii::t('lang', 'News') ?></a>
                </li>
                <li><a style="color:#328423;" href="<?= Url::to(['/library/index'])?>"><i class="fa fa-book"> </i><?= Yii::t('lang', 'Library') ?></a>
                  <!-- <ul class="submenu">
                    <li><a href="<?//= Url::to(['/library/index', 'lang' => 'uz']) ?>"> <?//= Yii::t('lang', 'O\'zbek(lotin)') ?></a>
                    </li>
                    <li><a href="<?//= Url::to(['/library/index', 'lang' => 'krl']) ?>"> <?//= Yii::t('lang', 'Ўзбек(кирил)') ?></a>
                    </li>
                  </ul> -->
                </li>
              </ul>
            <div class="menu-footer"> @ Test</div>
          </div>

        </nav>
        <span class="navigation__years">2017-2018</span>
      </div>
    </div>
      <div class="content__main main ">
        <?= $content ?>

        <div class="main__nav nav">
          <ul class="nav__list">
            <li class="nav__item">
              <a href="<?= Url::to(['footer/index'])?>" class="nav__link"><?= Yii::t('lang', 'About_sistem') ?></a>
            </li>
            <!-- <li class="nav__item">
              <a href="#" class="nav__link">Как пополнить счет</a>
            </li> -->
            <li class="nav__item">
              <a href="<?= Url::to(['footer/tarrif'])?>" class="nav__link"><?= Yii::t('lang', 'Tariffs') ?></a>
            </li>
            <li class="nav__item">
              <a href="<?= Url::to(['footer/payment'])?>" class="nav__link"><?= Yii::t('lang', 'Payment_for_services') ?></a>
            </li>
            <li class="nav__item">
              <a href="<?= Url::to(['footer/oferta'])?>" class="nav__link"><?= Yii::t('lang', 'Offer') ?></a>
            </li>
            <li class="nav__item">
              <a href="<?= Url::to(['footer/politic'])?>" class="nav__link"><?= Yii::t('lang', 'Security_policy') ?></a>
            </li>
            <li class="nav__item">
              <a href="<?= Url::to(['footer/partner'])?>" class="nav__link"><?= Yii::t('lang', 'Partners') ?></a>
            </li>
            <li class="nav__item">
              <a href="<?= Url::to(['footer/contact'])?>" class="nav__link"><?= Yii::t('lang', 'Contacts') ?></a>
            </li>
          </ul>
          <ul class="nav__social social">
            <li class="social__item">
              <a href="#" class="social__link">
                <img src="/images/fb.png" class="social__icon">
              </a>
            </li>
            <li class="social__item">
              <a href="#" class="social__link">
                <img src="/images/vk.png" class="social__icon">
              </a>
            </li>
            <li class="social__item">
              <a href="#" class="social__link">
                <img src="/images/rss.png" class="social__icon">
              </a>
            </li>
          </ul>
        </div>

        <div class="main__footer footer">
          <div class="footer__copyright">&copy; OOO "E-Services house"</div>
          <?php if (Yii::$app->language == "ru") { ?>
            <div class="footer__developed">Сайт разработан в студии <a href="https://Qwerty.uz" class="footer__qwerty"><strong>QWERTY</strong></a></div>
          <?php }else { ?>
            <div class="footer__developed">Ushbu sayt <a href="https://Qwerty.uz" class="footer__qwerty"><strong>QWERTY</strong></a> da ishlangan</div>
          <?php } ?>
        </div>
      </div>
        

      </div>
    </div>

<!-- Modal -->
<div class="modal fade" id="choose_test_type" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog lg md xs" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 style="  text-align:center; color:darkseagreen;" class="modal-title" id="myModalLabel">
        <?= Yii::t('lang', 'Check_status')?><br>
        <span style="font-size:16px;"><?= Yii::t('lang', 'Choose_the_right_direction_for_yourself')?></span></h3>
      </div>
      <div class="modal-body">
        <?=$this->render('/test/modal')?>
      </div>
    </div>
  </div>
</div>
<?php
//$date = date('F d, Y', strtotime($tourn->day));
  // echo '<pre>';
  // print_r();
  // echo '</pre>';
  // exit();
?>
<!-- <script src='/js/matrial-page/timezz.js'></script> -->
<script>
  
  // new TimezZ('.j-timer-second', {
  //   date: 'Jan 1, 2019',
  //   tagNumber: 'b',
  //   tagLetter: 'span',
  // });
</script>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>