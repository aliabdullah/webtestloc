<?php 

use yii\bootstrap\Html;

// if (Yii::$app->language == 'ru') {
// 	echo Html::a('O\'z', array_merge(Yii::$app->request->get(), [Yii::$app->controller->route, 'language' => 'uz']), ['class' => 'lang-link']);
// 	echo Html::a('Ру', array_merge(Yii::$app->request->get(), [Yii::$app->controller->route, 'language' => 'ru']), ['class' => 'lang-link']);
// 	echo Html::a('Cryl', array_merge(Yii::$app->request->get(), [Yii::$app->controller->route, 'language' => 'cryl']), ['class' => 'lang-link']);
// }elseif (Yii::$app->language == 'uz') {
// 	echo Html::a('O\'z', array_merge(Yii::$app->request->get(), [Yii::$app->controller->route, 'language' => 'uz']), ['class' => 'lang-link']);
// 	echo Html::a('Ру', array_merge(Yii::$app->request->get(), [Yii::$app->controller->route, 'language' => 'ru']), ['class' => 'lang-link']);
// 	echo Html::a('Cryl', array_merge(Yii::$app->request->get(), [Yii::$app->controller->route, 'language' => 'cryl']), ['class' => 'lang-link']);
// }else {
// 	echo Html::a('O\'z', array_merge(Yii::$app->request->get(), [Yii::$app->controller->route, 'language' => 'uz']), ['class' => 'lang-link']);
// 	echo Html::a('Ру', array_merge(Yii::$app->request->get(), [Yii::$app->controller->route, 'language' => 'ru']), ['class' => 'lang-link']);
// 	echo Html::a('Cryl', array_merge(Yii::$app->request->get(), [Yii::$app->controller->route, 'language' => 'cryl']), ['class' => 'lang-link']);
// }

 if (Yii::$app->language == 'ru') {
          $active_ru = "lang__active";
        }elseif (Yii::$app->language == 'uz') {
          $active_uz = "lang__active";
        }elseif (Yii::$app->language == 'cryl') {
          $active_cryl = "lang__active";
        } ?>
        <div class="lang__div">
          <ul>
            <li class="<?= $active_uz ?>">
              <?= Html::a('O\'z', array_merge(Yii::$app->request->get(), [Yii::$app->controller->route, 'language' => 'uz']), ['class' => "$active_uz"]); ?>
              <!-- <a class="" href="/uz">O`z</a> -->
            </li>
            <li class="<?= $active_ru ?>">
              <?= Html::a('Ру', array_merge(Yii::$app->request->get(), [Yii::$app->controller->route, 'language' => 'ru']), ['class' => '$active_ru']); ?>
              <!-- <a class="" href="/ru">Ру</a> -->
            </li>
            <li class="<?= $active_cryl ?>">
              <?= Html::a('Cryl', array_merge(Yii::$app->request->get(), [Yii::$app->controller->route, 'language' => 'cryl']), ['class' => '$active_cryl']); ?>
              <!-- <a class="" href="/ru">Ру</a> -->
            </li>
          </ul>
        </div>