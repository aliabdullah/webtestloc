<div class="col-md-12">
	<!-- Main content -->
    <section class="invoice">
      <!-- title row -->
      <?php if (!empty($userOrder)) { ?>
        <div class="row">
          <div class="col-xs-12">
            <h2 class="page-header">
              <i class="fa fa-globe"></i><?=Yii::t('lang', 'stats_tariff')?>, Inc.
              <small class="pull-right"><?=Yii::t('lang', 'date')?>: <?= date('d/m/Y')?></small>
            </h2>
          </div>
        <!-- /.col -->
        </div>
        <!-- info row -->
        <div class="row invoice-info" style="margin-bottom: 30px;">
          <div class="col-sm-3 invoice-col">
            From
            <img style="max-width: 263px;" src="/<?=$userOrder->tarif->photo?>">
          </div>
          <!-- /.col -->
          <div class="col-sm-3 invoice-col">
            To
            <address>
              <strong><?=$userOrder->tarif->title?></strong><br>
              <?=$userOrder->tarif->content?>
            </address>
          </div>
          <!-- /.col -->
          <?php 

            $date1 = new DateTime(date('Y-m-d'));
            $date2 = new DateTime($userOrder->date_end);
            if ($date1 < $date2) {
              $interval = $date2->diff($date1);
              $year = $interval->format("%Y");
              $moon = $interval->format("%m");
              $day = $interval->format("%d");

              $diff = (($day < 10 ) ? '0'.$day : $day).'<span style="color:black; font-size:17px;">  день осталось</span>';
            }else {
              $diff = "<a class='time__out' href='/test/index' style='' >".Yii::t('lang', 'off_time')."</a>";
            }
            
           ?>
          <div class="col-xs-6">
            <p class="lead"><?=Yii::t('lang', 'current_order')?> </p>
            <div class="table-responsive">
              <table class="table">
                <tr>
                  <th style="width:50%"><?=Yii::t('lang', 'start_time')?>:</th>
                  <td style=" font-size: 15px;"><?= $userOrder->date_start ?></td>
                </tr>
                <tr>
                  <th><?=Yii::t('lang', 'end_time')?></th>
                  <td style=" font-size: 15px;"><?= $userOrder->date_end ?></td>
                </tr>
                <tr>
                  <th><?=Yii::t('lang', 'residue')?>:</th>
                  <?php if ($diff >= 20) { 
                      $color = 'green';
                  }elseif ($diff >= 10 && $diff < 20) {
                      $color = '#f4bd21';
                  }else {
                      $color = 'red';
                  } ?>
                  <td style="color:<?= $color ?>; font-size: 22px;"><?= $diff ?></td>
                </tr>
                <tr>
                  <th><?=Yii::t('lang', 'status')?>:</th>
                  <td style=" font-size: 15px;"><?= Yii::t('lang', $userOrder->tarif->status) ?></td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      <!-- /.row -->
      <?php }else { ?>
          <div class="col-md-10 col-md-offset-1 text-center well">
            <h1 style="color:#757575;"><?=Yii::t('lang', 'attention')?>!</h1>
            <p><?=Yii::t("lang", "You_have_not_activated_any_tariffs_at_the_moment")?>...</p>
          </div>
      <?php } ?>
      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive" style="margin-bottom: 50px;">
        	<div class="col-xs-12">
          <h2 class="page-header">
            <span><?=Yii::t('lang','Tariffs')?>, <small><?=Yii::t('lang', 'info')?>.</small></span>
            
          </h2>
        </div>
          <table class="table table-striped">
            <thead>
              <tr>
                <th>Qty</th>
                <th><?=Yii::t('lang', 'statusUp')?></th>
                <th><?=Yii::t('lang', 'short')?></th>
                <th><?=Yii::t('lang', 'more')?></th>
                <th><?=Yii::t('lang', 'cost')?></th>
              </tr>
            </thead>
            <tbody>
              <?php $num=1; foreach ($tarrifs as $key => $tarrif) { ?>
          		  <tr>
  	              <td><?= $num ?></td>
  	              <td><?= $tarrif->status ?></td>
  	              <td style="width: 263px;"><?= $tarrif->title ?></td>
  	              <td style="width: 550px;"><?= $tarrif->content ?></td>
  	              <td><?= number_format($tarrif->cost).' сум' ?></td>
  	            </tr>
              <?php $num++; } ?>
            </tbody>
          </table>
           <?php if (empty($userOrder)) { ?>
              <a href="/test/index" class="btn btn-warning"><?=Yii::t('lang', 'enter_to_page')?></a>
          <?php } ?>
        </div>
        <!-- /.col -->
      </div>
     
      <!-- /.row -->

      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6">
          <p class="lead"><?=Yii::t('lang', 'types_of_payments')?>:</p>
          <!-- <img src="/images/credit/visa.png" alt="Visa">
          <img src="/images/credit/mastercard.png" alt="Mastercard"> -->
          <img src="/images/credit/american-express.png" alt="American Express">
          <!-- <img src="/images/credit/paypal2.png" alt="Paypal"> -->

          <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
            Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem plugg
            dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.
          </p>
        </div>
        <!-- /.col -->
        
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- this row will not appear when printing -->
      <!-- <div class="row no-print">
        <div class="col-xs-12">
          <a href="invoice-print.html" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
          <button type="button" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Submit Payment
          </button>
          <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
            <i class="fa fa-download"></i> Generate PDF
          </button>
        </div>
      </div> -->
    </section>
    <!-- /.content -->
</div>