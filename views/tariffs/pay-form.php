<?php 

use yii\bootstrap\Modal;

$user = \Yii::$app->user->identity;
 ?>

<div class="col-md-12" style="min-height: 638px;">
	<!-- Main content -->
    <section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-globe"></i> <?=Yii::t('lang', 'O тарифе')?>, Inc.
            <small class="pull-right"><?=Yii::t('lang', 'Дата')?>: <?= date('d/m/Y')?></small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info" style="margin-bottom: 30px;">
        <div class="col-sm-6 invoice-col">
          <?=Yii::t('lang', 'Инфо')?>
          <address>
            <strong><?= $tarifModel->title?></strong><br>
            <?= $tarifModel->content ?>
          </address>
        </div>
        <!-- /.col -->
        <div class="col-xs-6">
          <p class="lead"><?=Yii::t('lang', 'Параметры')?></p>
          <div class="table-responsive">
            <table class="table">
              <tr>
                <th style="width:50%"><?=Yii::t('lang', 'Названия')?>:</th>
                <td><?= $tarifModel->title ?></td>
              </tr>
              <tr>
                <th><?=Yii::t('lang', 'Статус')?>:</th>
                <td><?= $tarifModel->status ?></td>
              </tr>
              <tr>
                <th><?=Yii::t('lang', 'Стоимость')?>:</th>
                <td><?= number_format($tarifModel->cost).' сум'?></td>
              </tr>
            </table>
          </div>
        </div>
        <!-- /.col -->
        <!-- /.col -->
      </div>
      <!-- /.row -->
      <!-- Table row -->
      <div class="row">
        <div class="col-xs-6 col-md-offset-3 table-responsive" style="margin-bottom: 50px;">
            <button type="button"
                  id="paymodal"
                  data-tariff_id="<?= $tarifModel->id ?>"
                  style="border: 1px solid #eaaf07; background-color: white; color: #eaaf07;" 
                  class="btn btn-lg btn-block"> <?=Yii::t('lang', 'Заказать')?>
            </button>
        </div>
      </div>
      <!-- /.row -->
      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6">
          <p class="lead"><?=Yii::t('lang', 'Типы Платежей')?>:</p>
          <!-- <img src="/images/credit/visa.png" alt="Visa">
          <img src="/images/credit/mastercard.png" alt="Mastercard"> -->
          <img src="/images/credit/american-express.png" alt="American Express">
          <!-- <img src="/images/credit/paypal2.png" alt="Paypal"> -->
          <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
            Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem plugg
            dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.
          </p>
        </div>
        <!-- /.col -->
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<?php

    Modal::begin([
      'header' => '<h3 style="  text-align:center; color:#333333;">
        Ваше заказ</h3>',
      'id' => 'payModal',
      'size' => 'modal-md',
      
    ]);
    Modal::end();

?>