<?php 

use yii\widgets\ActiveForm;
use yii\helpers\Url;

 ?>
<?php if (!empty($tariff)) { ?>
	<div class="row">
	<div class="col-md-10 col-md-offset-1">
		<div class="box box-defoult">
			<div class="col-lg-10 col-md-offset-1 text-center" style="margin-bottom: 50px; padding-top: 50px;">
          		<p class="col-md-6 col-md-offset-3 text-center" style="border: 1px solid #6f5757; height: 35px; line-height: 1.8; border-radius: 5px; font-size: 18px;"><?= $tariff->status ?></p>
          		<p class="col-md-12" style="color: #ef6300; font-size: 18px;"><?= number_format($tariff->cost).' сум' ?></p>
          		<p class="col-md-12" style="font-size: 18px;"><?= $tariff->title ?></p>
          		<p class="col-md-12"><?= $tariff->content ?></p>
        	</div>
        	<!-- Setting Woy wo payment sistem -->
        	<?php
        		$key = "0486546a1265d2226861857d853249201a903c42161f8d48dda131ea819650ea079c18f3e679e9f4d1244717abc362a252a9918f6f47bf956ba1170da636715b";
        		$merchant_id = "76b264627035af923b63e8272b6b5d9e70929141";
        		$date = time() * 1000;
        		$arr = $key. $merchant_id. $orderModel->id. $date. $tariff->cost;
        		$mac = sha1($arr);
        		$lang = \Yii::$app->language;
        	 ?>
        		<?php 
        		// $form = ActiveForm::begin(['action' => Url::to(["/tariffs/modal-form"]), 'method' => 'post']); 
        		?>
	            <form action="https://pay.woy-wo.uz/v1" method="POST">
		            <input type="hidden" name="account[order_id]" value="<?= $orderModel->id ?>">
		            <input type="hidden" name="amount" value="<?= $tariff->cost ?>">
		            <input type="hidden" name="merchant_id" value="<?= $merchant_id ?>">
		            <input type="hidden" name="note" value="<?= $tariff->title ?>">
		            <input type="hidden" name="date" value="<?= $date ?>">
		            <input type="hidden" name="mac" value="<?= $mac ?>">
		            <input type="hidden" name="redirect" value="https://webtest.uz/<?= $lang ?>/test/premium-filter">
		        	<?php if (Yii::$app->language == 'ru') { ?>
		        		<button type="submit"
			                style="margin-top: 100px; margin-bottom: 50px; border: 1px solid #5cb85c; background-color: white; color: #5cb85c;" 
			                class="btn btn-lg btn-block">Оплатить через<strong> Way Wo</strong>
			            </button>
		        	<?php }else { ?>
			        	<button type="submit"
			                style="margin-top: 100px; margin-bottom: 50px; border: 1px solid #5cb85c; background-color: white; color: #5cb85c;" 
			                class="btn btn-lg btn-block"><strong> Way Wo</strong> orqali to'lash
			            </button>
		            <?php } ?>
	            </form>
	            <?php 
	            // $form = ActiveForm::end() 
	            ?>
         	<!-- End setting -->
	    </div>
	</div>
</div>
<?php }else { ?>
	<div class="row">
		<div class="col-md-12">
			<h4><?=Yii::t('lang', 'Это описание недоступно')?></h4>
		</div>
	</div>
<?php } ?>
