<?php 
use yii\widgets\LinkPager;
use yii\helpers\Url;

if (Yii::$app->language == 'uz') {
  $alphabet = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
}else {
  $alphabet = array(
    'А', 'Б', 'В', 'Г', 'Д', 'Е', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Ж',  'Ч',  'Щ',   'Ш',  'Ю', 'Х', 'Ц', 'Я');
}
?>
<div class="col-md-12" style="min-height: 640px;">
  <div class="col-xs-12">
    <div class="box" style="margin-bottom: 5px !important;">
            <div class="box-header">
              <h5 style="font-size: 14px;" class=""><?= Yii::t('lang', 'filter').': '.$alphabet[0].' - '.end($alphabet); ?></h5>
              <div class="col-md-12 text-center">
                <?php foreach ($alphabet as $key => $value) { ?>
                  <a href="<?= Url::to(['library/index', 'letter' => $value])?>" style="margin:5px;"><?=$value;?></a>
                <?php } ?>
              </div>
            </div>
            </div>
          <div class="box" style="border-top: none;">
            <div class="box-header">
              <h5 style="font-size: 14px;" class=""><?= Yii::t('lang', 'books').': '.count($books); ?></h5>
            </div>
            
            <!-- /.box-header -->
            <div class="box-body">
              <?php if (!empty($books)) { ?>
                <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr style="background: #f7f7f7;">
                  <th rowspan="2" width="30" style="line-height: 4;">ID</th>
                  <th rowspan="2" style="line-height: 4;"><?=Yii::t('lang', 'author')?></th>
                  <th rowspan="2" style="line-height: 4;"><?=Yii::t('lang', 'title')?></th>
                  <th rowspan="2" width="30" style="line-height: 4;"><?=Yii::t('lang', 'year')?></th>
                  <th rowspan="2" width="30" style="line-height: 4;"><?=Yii::t('lang', 'pages')?></th>
                  <th colspan="5" style="text-align: center;"><?=Yii::t('lang', 'download')?></th>
                  <!-- <th>Djvu</th>
                  <th>Pdf</th>
                  <th>Ps</th>
                  <th>Html</th>
                  <th>Tex</th> -->
                </tr>
                <tr style="background: #f7f7f7;">
                  <!-- <th rowspan="2" width="30">ID</th>
                  <th rowspan="2">Author</th>
                  <th rowspan="2">Name</th>
                  <th rowspan="2" width="30">Year</th>
                  <th rowspan="2" width="30">Pages</th>
                  <th colspan="5">Download</th> -->
                  <th>Djvu</th>
                  <th>Pdf</th>
                  <th>Ps</th>
                  <th>Html</th>
                  <th>Tex</th>
                </tr>
                </thead>
                <tbody>
                
                  <?php $num = 1; foreach ($books as $key => $book) { 
                    if (($num % 2) == 0 ) {$color = '#f7f7f7';} else { $color = '';} ?>
                      <tr style="background: <?= $color?>;">
                        <td><?=$num++?></td>
                        <td width="185"><h6><?=ucfirst($book->author)?></h6></td>
                        <td><h6><?=ucfirst($book->name)?></h6></td>
                        <td width="55"><?=$book->year?></td>
                        <td width="55"><?=$book->page?></td>
                        <td width="25"><h6 style="color:blue;"><a href="<?= '/'.$book->djvu?>" download><?=$book->djvu ? Yii::t('lang', 'download') : '--'?></a></h6></td>
                        <td width="25"><h6 style="color:blue;"><a href="<?= '/'.$book->pdf?>" download><?=$book->pdf ? Yii::t('lang', 'download') : '--'?></a></h6></td>
                        <td width="25"><h6 style="color:blue;"><a href="<?= '/'.$book->ps?>" download><?=$book->ps ? Yii::t('lang', 'download') : '--'?></a></h6></td>
                        <td width="25"><h6 style="color:blue;"><a href="<?= '/'.$book->html?>" download><?=$book->html ? Yii::t('lang', 'download') : '--'?></a></h6></td>
                        <td width="25"><h6 style="color:blue;"><a href="<?= '/'.$book->TeX?>" download><?=$book->TeX ? Yii::t('lang', 'download') : '--'?></a></h6></td>
                      </tr>
                  <?php  } ?>
                  
                
                
                </tbody>
                <tfoot>
                  <?= LinkPager::widget([
                      'pagination' => $pages,
                  ]); ?>
                </tfoot>
              </table>
                  
              <?php }else { ?>
                  <div class="col-md-12 text-center">
                    <span style="font-size: 20px;"><?=Yii::t('lang', 'empty_data')?></span>
                  </div>
              <?php } ?>
            </div>
            <!-- /.box-body -->
            
          </div>
  </div>
</div>

