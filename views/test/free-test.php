<?php 

use yii\helpers\Url;
use yii\bootstrap\Modal;

?>
<div class="col-md-8 col-md-push-2" style="margin-top: 45px;">	
    <div class="col-md-8 col-md-push-1 text-center pred__3 desactive__predmet" style="margin: 0;">
      <span style="font-size: 24px;"><?= Yii::t('lang', 'test_questions_for_your_imagination') ?></span>
    </div>
</div>	
<div id="print__area">
	<div class="col-md-12" style="min-height: 591px;">
			<div class="col-md-10" style="margin-left: 71px;">
				<hr class="col-md-3 hr"> <h4 class="col-md-1 text-center"><span class="glyphicon glyphicon-education"></span></h4> <hr class="col-md-3">
			</div>
			<!-- // Predmet-1 -->
			<div class="predmet__1 predmet__opened" >
				<?php if (Yii::$app->language == 'ru') { ?>
					<div class=" col-md-5  text-center" style="margin-left: 285px; margin-top: 20px; height: 49px; line-height: 2;">
						<span style="font-size: 20px;"><?= Yii::t('lang', 'questions') ?></span>
					</div>	
				<?php }else { ?>
				<div class=" col-md-5 col-md-offset-4 text-center" style="margin-left: 285px; margin-top: 20px; height: 49px; line-height: 2;">
					<span style="font-size: 24px;"><?= Yii::t('lang', 'questions') ?></span>
				</div>
				<?php } ?>
				<div class="col-md-10 col-md-offset-1">
					<div class="col-md-11 text-center" >
						<ul class="count-ul">
							<?php $num = 1; if (isset($test)) { ?>
								<?php foreach ($test as $key => $quest) { ?>
									<a href="#" class="scrollTo" data-scrollTo="question<?= ($key + $quest->id) ?>">
										<li class="count-li text-center">
											<?= $num++ ?>
										</li>
									</a>
								<?php } ?>
							<?php } ?>
							
						</ul>
					</div>
				</div>

				<!-- QUESTIONS START-->	
				<?php if (isset($test)) { ?>
					<?php $num = 1; foreach ($test as $key => $quest) { ?>
						 <div  class="test panel col-md-9 col-md-offset-1 " id="question<?= ($key + $quest->id) ?>" data-key="<?= $quest->id?>">
						 	<div class="col-md-12 text-center col-md-offset-0 panel-div question<?=$quest->id ?>" >
						 		<div class="col-md-12 panel-area">
							 		<div class="panel-quest ">
										<?= $num++?>
									</div>
								</div>
								<div style="margin-top: 41px; padding: 15px 30px; height: auto; font-size: 18px;"><?= $quest->question ?></div>
								<div class="col-md-12" style="">
									<hr class="col-md-3 col-md-offset-1" style="margin-left: 154px;"> <h4 class="col-md-1 text-center"><span class="glyphicon glyphicon-education"></span></h4> <hr class="col-md-3">
								</div>
							</div>
							<!-- VARIANTS START -->
							<div class="col-md-12" style="margin-top: 10px;">
								<?php foreach ($quest->variants as $key => $variant) { ?>
									<div class="list-group-item col-md-12  question<?=$variant->test_id ?>">
											<a href="<?//= Url::to(['/results/add-to-results'])?>"
												data-pred_name="<?= $test['mainCat']['name'].'1' ?>"
												data-quest_id="<?=$variant->test_id?>" 
												data-var-id="<?=$variant->id?>" 
												data-type="<?=$variant->type?>" 
												class="variant">
												<table>	                
								                    <tr>
								                    	<td>
								                    		<h4><?= mb_strtoupper($variant->type)?></h4>
								                    	</td>
								                    	<td>
								                    		<h4><?=$variant->content?></h4>
								                    	</td>
							            			</tr>
							            		</table>
						            		</a>
					        		</div>		
								<?php  } ?>
							</div>
							<!-- VARIANTS END -->
							<div class=' col-md-2 pull-right text-center foggy-comparison-manual-wrapper' style="margin-top: 20px; margin-left: 10px; ">
								<div  class="col-md-1 update updatebtn" id="down<?=$key ?>" data-id="<?=$quest->id ?>">
									<a   class=" glyphicon glyphicon-repeat
									" ></a>
								</div>
								<div  class="  col-md-1 to-down arrow" id="down<?=$key ?>">
									<a  id="<?= $key ?>" data-scrollTo="<?= $key ?>" data-id="<?=$quest->id ?>" class=" scrollToDown glyphicon glyphicon-menu-down	Try it
									" aria-hidden="true"></a>
								</div>
							</div>
						</div>
					<?php } ?>
				<?php } ?>
			</div>
			<!-- QUESTIONS END -->
			<?php $lang_uri = Yii::$app->language;
				(!empty($test2['mainCat']['name']) && !empty($test3['mainCat']['name'])) ? ($offset = 2) : ($offset = 3);
			?>
			<div class="col-md-offset-<?= $offset ?> col-md-9 wow fadeInRight" style="margin-bottom: 25px;">
				<a href='<?= Url::to("/$lang_uri/results/result")?>' id="resModalx" class="btn btn-success col-md-3 col-md-offset-0" style="margin-bottom: 15px; margin-left: 15px;"><?=Yii::t('lang', 'result' )?> <i class="glyphicon glyphicon-signal" style="margin-left: 10px; margin-top: 3px; font-size: 15px;"></i></a>

				<a id="toprint" class="btn btn-primary col-md-3 col-md-offset-0" style="margin-bottom: 15px; margin-left: 15px;"><?=Yii::t('lang', 'print' )?><i class="glyphicon glyphicon-print" style="margin-left: 10px; margin-top: 3px; font-size: 15px;"></i>
				</a>
			</div>
		</div>

	<?php
	    // Modal::begin([
	    //   'header' => '<h2 style="  text-align:center; color:darkseagreen;">
	    //     Ваш результат<br>
	    //     </h2>',
	    //   'id' => 'resultModal',
	    //   'size' => 'modal-lg',
	    //   'footer' => '<button class="btn btn-danger" onclick="resDelete()">Удалит 						результат</button>
	    //   				<button class="btn btn-success" onclick="resSave()">Сохранить результат</button>',
	    // ]);
	    // Modal::end();
	?>
</div>