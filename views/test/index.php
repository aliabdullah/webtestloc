<?php 

use yii\helpers\Url;


 ?>

<div class="container " style="background-color: #f6f7f9;">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h1 class="mt-5"><?=Yii::t('lang', "online_tests")?></h1>
        <p class="lead"><?=Yii::t('lang', "visit_the_test_questions_page_to_select_a_convenient_tariff_plan")?></p>
        <ol class="breadcrumb no-margin">
					<li><a href="/"><?= Yii::t('lang', 'Home')?></a></li>
					<li class="active"><?= Yii::t('lang', 'Tariffs')?></li>
				</ol>
      </div>
    </div>
    <div class="main__tarrifs tarrifs">
      <h2 class="tarrifs__caption"><?= Yii::t("lang", "Tariffs")?></h2>
      <div class="tarrifs__wrapper">

      <?php foreach ($model_cours as $key => $cours) { ?>
          <div class="tarrifs__box">
            <a href="<?= Url::to(['/tariffs/pay-form', 'id' => $cours->id]) ?>" style="text-decoration: none; color: #4c4c4c;">
              <div class="tarrifs__tarrif tarrif">
                <div class="tarrif__head" style="position: sticky;">
                  <img src="<?= '/'.$cours->photo ?>" class="tarrif__bg">
                  <div class="tarrif__box">
                    <h1 class="tarrif__price"><?= number_format($cours->cost).' сум'?> </h1>
                    <span class="tarrif__name"><?= $cours->status ?></span>
                  </div>
                </div>
                <div class="tarrif__body">
                  <h3 class="tarrif__level"><?= $cours->title ?></h3>
                  <p class="tarrif__desc"><?= $cours->content?></p>
                </div>
              </div>
          </a>
        </div>
    
      <?php  } ?>


      </div>
    </div>

    <!-- accepted payments column -->
    <div class="col-xs-6" style="margin-left: 20px; margin-top: 10px;">
      <p class="lead"><?=Yii::t('lang', "types_of_payments")?>:</p>
      <!-- <img src="/images/credit/visa.png" alt="Visa">
      <img src="/images/credit/mastercard.png" alt="Mastercard"> -->
      <img src="/images/credit/american-express.png" alt="Woy-wo">
      <!-- <img src="/images/credit/paypal2.png" alt="Paypal"> -->
      <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
        Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem plugg
        dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.
      </p>
    </div>
    <!-- /.col -->
</div>
