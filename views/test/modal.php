<?php 

use yii\helpers\Html;
use yii\helpers\Url;

 ?>

<form class=" form-group">
	<div class="row">
		<div class="col-md-12 ">
			<div class="col-md-6 col-xs-6">
			  	<h3 style="  text-align:center; color:darkseagreen;">
		        	<?= Yii::t('lang', 'free_tests')?><br>
		        	<span style="font-size:16px;"><?= Yii::t('lang', 'Choose_a_free_service_to_learn_how_the_test_system_works')?></span>
		    	</h3>
					  <a type="button" style="border: 1px solid #5cb85c; background-color: white; color: #5cb85c; margin-top: 36px;" data-id="2" href="<?=Url::to('/test/free-test');?>" class="btn btn-lg btn-block " ><?= Yii::t('lang', 'free_tests')?></a>
			</div>
	  	
	  		<div class="col-md-6 col-xs-6" style="border-left: 1px solid #e6e6e6;">
				<h3 style=" text-align:center; color:#e8732e;"><?= Yii::t('lang', 'paid_tests')?>
				<br><span style="font-size:16px;"><?= Yii::t('lang', 'Choose_a_paid_service_to_switch_to_more_common_testing_questions')?></span></h3>
				<a type="button" style="border: 1px solid #e8732e; background-color: white; color: #e8732e;" data-id="3" href="<?=Url::to('/test/index');?>" value="premium" class="btn btn-lg btn-block" ><?= Yii::t('lang', 'paid_tests')?></a>
			</div>
		</div>
</form>