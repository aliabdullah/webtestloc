<?php 
use yii\bootstrap\ActiveForm;
use app\models\category\Category;
use yii\helpers\ArrayHelper;

$lang = \Yii::$app->language;

?>

	<div class="col-md-12 col-xs-12" style="min-height: 650px; background-image: url('/images/blur2.jpg');">
		<?php if (Yii::$app->session->getFlash('success')) { ?>
			<div class="col-md-8 col-md-offset-2" style="text-align: center; font-size: 18px; color: #478647; margin-top: 20px; border: 1px solid #478647; border-radius: 1px; height: 57px; line-height: 3;"><?= Yii::$app->session->getFlash('success'); ?></div>
		<?php } ?>

		<?php if (Yii::$app->session->getFlash('deleted')) { ?>
			<div class="col-md-8 col-md-offset-2" style="text-align: center; font-size: 16px; color: #fff57c; margin-top: 20px; border: 1px solid #fff57c; border-radius: 1px; height: 50px; line-height: 3;"><?= Yii::$app->session->getFlash('deleted'); ?></div>
		<?php } ?>
		
		<fieldset class="yow-banner">
			<legend class="" style="border:0px solid white;  margin: 0 22% 10px 22%; width: 310px; height: auto;">
				<img style="margin: 5px 11px 5px 11px;" src="/images/logo.png">
			</legend>
			<div class="" style="border: 0px solid white; padding: 0; height: 80px; max-width: 515px;
    margin: auto;">
				<span class="count__test">
					<?=$test_count?>
				</span>
				<div class="col-md-7 col-xs-7" style="border: 0px solid white; float: right; height: 50px; font-size: 38px; color: white; /*padding: 0;*/ ">
					<?=Yii::t('lang', 'premium_tests')?>
				</div>
				<span class="col-md-7 col-xs-7" style="color: white; float: right; font-size: 18px; /*margin-left: 3px;*/ line-height: -1;">	<?=Yii::t('lang', 'in_direction_and_thematic')?>
				</span>
			</div>
			<span class="col-md-8 col-xs-8 wish">
				<?=Yii::t('lang', 'we_serve_you_for_your_dreams!')?>
			</span>

		</fieldset>
		<div class="col-md-6 col-md-offset-3" >
			<img src="/images/shadow.png">
		</div>


		<div class="menu-overlay"></div>


		<div class="col-md-12">
			<div class="col-md-6 col-md-offset-3 text-center">
				<h3 class="col-md-8 col-md-offset-2" style="color: white;">
					<?=Yii::t('lang', 'select_destination')?>
				</h3>
				<div class="menu col-md-12" style="margin-top: 20px;">
				  <div class="hamburger-menu-wrapper">
				    <button class="hamburger-menu"> <span>toggle menu</span> </button>
				  </div>
				  <div class="menu-list"> 
				  	<span id="student1" ><?=Yii::t('lang', 'school_for_schoolchildren')?></span><br>
				    <span id="student2" desabled ><?=Yii::t('lang', 'applicant_college_lyceum')?></span><br>
				    <!-- <span id="student3" ><?//=Yii::t('lang', 'Студент (университет)')?></span><br> -->
				    <!-- <a href="#" id="student4">Barcha...</a><br> -->
				  </div>
				</div>
			</div>
		</div>

		<section id="student1-pane" style="display: none;">

			<div class="col-md-12 col-md-offset-0 filter" style="border: 0px solid white; height: auto; border-radius: 50px; margin-bottom: 50px;">
				<div class="col-md-6 col-md-offset-3 text-center" style="margin-top: 20px;">
					<h4 class="col-md-12  col-md-offset-0" style="color: white;">
						<?=Yii::t('lang', 'Items')?>
					</h4>
				</div>
				
				<?php $form = ActiveForm:: begin(['action' => yii\helpers\Url::to("/$lang/test/premium-test")]);?>
					<div class="container" style="min-height:auto !important">
					<div class="col-md-6 col-md-offset-3">
						<select class="predmet-1 pred" name="predmet1">
					      	<?php foreach (\app\models\category\Category::getList() as $key => $pred) {
					      	 ?>
					      		<option value="<?= $key ?>"><?= $pred ?></option>				
							<?php } ?>
					    </select>
					</div>
				</div>
			<h4 class="col-md-12 text-center col-md-offset-0" style="color: white; margin-top: 50px;">
				<?=Yii::t('lang', 'Complexity')?>
			</h4>
			
				<div class="container" style="min-height:auto !important; margin-bottom: 20px;">
					<div class="col-md-6 col-md-offset-3">
						<select class="level" name="level">
					      	<?php foreach (\app\models\tests\Tests::getLevels() as $key => $level) { ?>
					      		<option value="<?= $key ?>"><?= Yii::t('lang', $level) ?></option>				
							<?php } ?>
					    </select>
					</div>
				</div>
				<div class="container" style="min-height:auto !important">
					<div class="col-md-6 col-md-offset-3">
						<select class="fakultet" name="lang">
					      	<?php foreach (\app\models\tests\Tests::getLang() as $key => $lang) { ?>
					      		<option value="<?= $key ?>"><?= ucfirst($lang)  ?></option>
							<?php } ?>
					    </select>
					</div>
				</div>
				<button class="col-md-6 col-md-push-3 btn button " type="submit">
					<span class="text text-center" style="color: white;">
						<?=Yii::t('lang', 'start_generating_tests')?>
					</span>
				</button>
				<?php ActiveForm:: end();?>
				<!-- </form> -->
			</div>
		</section>
		<?php $lang = Yii::$app->language; ?>
		<section id="student2-pane" style="display: none;">
			<div class="col-md-12 col-md-offset-0 filter" style="border: 0px solid white; height: auto; border-radius: 50px; margin-bottom: 50px;">
				<?php $form = ActiveForm:: begin(['action' => yii\helpers\Url::to("/$lang/test/premium-test")])?>
				<div class="col-md-6 col-md-offset-3 text-center" style="margin-top: 20px;">
					<h4 class="col-md-12  col-md-offset-0" style="color: white;">
						<?=Yii::t('lang', 'Items')?>
					</h4>
				</div>
				<div class="container" style="min-height:auto !important">
					<div class="col-md-4">
						<select class="predmet-1 pred" name="predmet1">
					      	<?php foreach (\app\models\category\Category::getParentCats() as $key => $pred) {
					      	 ?>
					      		<option value="<?= $key ?>"><?= $pred ?></option>				
							<?php } ?>
					    </select>
					</div>
					<div class="col-md-4">
						<select class="predmet-2 pred" name="predmet2">
					      	<?php foreach (\app\models\category\Category::getParentCats() as $key => $pred) { ?>
					      		<option value="<?= $key ?>"><?= $pred ?></option>				
							<?php } ?>
					    </select>
					</div>
					<div class="col-md-4">
						<select class="predmet-3 pred" name="predmet3">
					      	<?php foreach (\app\models\category\Category::getParentCats() as $key => $pred) { ?>
					      		<option value="<?= $key ?>"><?= $pred ?></option>				
							<?php } ?>
					    </select>
					</div>
				</div>
				<h4 class="col-md-12 text-center col-md-offset-0" style="color: white; margin-top: 50px;">
					<?=Yii::t('lang', 'Direction')?>
				</h4>
				<div class="container" style="min-height:auto !important">
					
					<div class="col-md-6">
						<select class="fakultet" name="lang">
					      	<?php foreach (\app\models\tests\Tests::getLang() as $key => $lang) { ?>
					      		<option value="<?= $key ?>"><?= ucfirst($lang)  ?></option>				
							<?php } ?>
					    </select>
					</div>
					<div class="col-md-6">
						<select class="level" name="level">
					      	<?php foreach (\app\models\tests\Tests::getLevels() as $key => $level) { ?>
					      		<option value="<?= $key ?>"><?= Yii::t('lang', $level) ?></option>				
							<?php } ?>
					    </select>
					</div>
				</div>
				<button class="col-md-6 col-md-push-3 btn button " type="submit">
					<span class="text text-center" style="color: white;">
						<?=Yii::t('lang', 'start_generating_tests')?>
					</span>
				</button>
				<?php ActiveForm:: end();?>
				<!-- </form> -->
			</div>
		</section>
	</div>

