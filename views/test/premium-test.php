<?php 

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii2assets\printthis\PrintThis;
use app\models\tests\Tests;

// $this->registerJsFile('@web/js/jQuery.print.js', ['position' => '\yii\web\View::POS_HEAD']);

?>

<script src="/js/wow.js"></script>
<script>
	new WOW().init();
</script>

<?php if (!empty($test1)) { ?>
	<section class="fdb-block">
	  	<div class="col-md-8 col-md-push-1" style="margin-top: 45px;">
	  		<?php if (!empty($test1['mainCat']['name'])) { 
	  			if (empty($test2['mainCat']['name']) && empty($test3['mainCat']['name'])) {
	  				$push_class = 5; } else { $push_class = 2; } ?>
	  			<div class="col-md-3 col-md-push-<?= $push_class ?> text-center pred__1 active__predmet" style="margin: 0;">
		          <h4><?= $test1['mainCat']['name'] ?></h4>
		        </div>
		    <?php } ?>
	        <?php if (!empty($test2['mainCat']['name'])) {
	        	if (empty($test1['mainCat']['name']) && empty($test3['mainCat']['name'])) {
	  				$push_class = 5; } else { $push_class = 2; } ?>
		        <div class="col-md-3 col-md-push-<?= $push_class ?> text-center pred__2 desactive__predmet" style="margin: 0;">
		          <h4><?= $test2['mainCat']['name'] ?></h4>
		        </div>
	        <?php } ?>
	        <?php if (!empty($test3['mainCat']['name'])){
	        	if (empty($test1['mainCat']['name']) && empty($test2['mainCat']['name'])) {
	  				$push_class = 5; } else { $push_class = 2; } ?>
		        <div class="col-md-3 col-md-push-<?= $push_class ?> text-center pred__3 desactive__predmet" style="margin: 0;">
		          <h4><?= $test3['mainCat']['name'] ?></h4>
		        </div>
		    <?php } ?>
	    </div>    
	</section>
<?php }else { ?>
	<div class="col-md-8 col-md-push-2" style="margin-top: 45px;">
		<?php if (!empty($test['mainCat']['name'])){  ?>
		        <div class="col-md-6 col-md-push-2 text-center pred__3 desactive__predmet" style="margin: 0;">
		          <span style="font-size: 24px;"><?= $test['mainCat']['name'] ?></span>
		        </div>
		    <?php } ?>
	</div>
<?php } ?>


<div id="print__area">
	<!-- For abiturient -->
	<?php if (empty($test)) { ?>
		<div class="col-md-12" style="min-height: 591px;">
			<div class="col-md-10" style="margin-left: 71px;">
				<hr class="col-md-3 hr"> <h4 class="col-md-1 text-center"><span class="glyphicon glyphicon-education"></span></h4> <hr class="col-md-3">
			</div>
			<!-- // Predmet-1 -->
			<div class="predmet__1 predmet__opened" >
				<?php if (Yii::$app->language == 'ru') { ?>
					<div class=" col-md-5  text-center" style="margin-left: 285px; margin-top: 20px; height: 49px; line-height: 2;">
						<span style="font-size: 24px;">Вопросы по <b><?= $test1['mainCat']['name'] ?></b></span>
					</div>	
				<?php }else { ?>
				<div class=" col-md-5 col-md-offset-4 text-center" style="margin-left: 285px; margin-top: 20px; height: 49px; line-height: 2;">
					<span style="font-size: 24px;"><b><?= $test1['mainCat']['name'] ?></b> bo'yicha savollar</span>
				</div>
				<?php } ?>
				<div class="col-md-10 col-md-offset-1">
					<div class="col-md-11 text-center" >
						<ul class="count-ul">
							<?php $num = 1; if (isset($test1['tests'])) { ?>
								<?php foreach ($test1['tests'] as $key => $quest) { ?>
									<a href="#" class="scrollTo" data-scrollTo="question<?= ($key + $quest->id) ?>">
										<li class="count-li text-center " id="<?= $quest->id ?>">
											<?= $num++ ?>
										</li>
									</a>
								<?php } ?>
							<?php } ?>
							
						</ul>
					</div>
				</div>

				<!-- QUESTIONS START-->	
				<?php if (isset($test1['tests'])) { ?>
					<?php $num = 1; foreach ($test1['tests'] as $key => $quest) { ?>
						 <div  class="panel col-md-9 col-md-offset-1 " id="question<?= ($key + $quest->id) ?>" data-key="<?= $quest->id?>">
						 	<div class="col-md-12 text-center col-md-offset-0 panel-div question<?=$quest->id ?>" >
						 		<div class="col-md-12 panel-area">
							 		<div class="panel-quest ">
										<?= $num++?>
									</div>
								</div>
								<div style="margin-top: 41px; padding: 15px 30px; height: auto; font-size: 18px;"><?= $quest->question ?></div>
								<div class="col-md-12" style="">
									<hr class="col-md-3 col-md-offset-1" style="margin-left: 154px;"> <h4 class="col-md-1 text-center"><span class="glyphicon glyphicon-education"></span></h4> <hr class="col-md-3">
								</div>
							</div>
							<!-- VARIANTS START -->
							<div class="col-md-12" style="margin-top: 10px;">
								<?php foreach ($quest->variants as $key => $variant) { ?>
									<div class="list-group-item col-md-12  question<?=$variant->test_id ?>">
											<a href="<?//= Url::to(['/results/add-to-results'])?>"
												data-pred_name="<?= $test1['mainCat']['name'].'1' ?>"
												data-quest_id="<?=$variant->test_id?>" 
												data-var-id="<?=$variant->id?>" 
												data-type="<?=$variant->type?>" 
												class="variant">
												<table>	                
								                    <tr>
								                    	<td>
								                    		<h4><?= Tests::changeEncodingOption($variant->type)?></h4>
								                    	</td>
								                    	<td>
								                    		<h4><?=$variant->content?></h4>
								                    	</td>
							            			</tr>
							            		</table>
						            		</a>
					        		</div>		
								<?php  } ?>
							</div>
							<!-- VARIANTS END -->
							<div class=' col-md-2 pull-right text-center foggy-comparison-manual-wrapper' style="margin-top: 20px; margin-left: 10px; ">
								<div  class="col-md-1 update updatebtn" id="down<?=$key?>" data-id="<?=$quest->id ?>">
									<a   class=" glyphicon glyphicon-repeat
									" ></a>
								</div>
								<div  class="  col-md-1 to-down arrow" id="down<?=$key ?>">
									<a  id="<?= $key ?>" data-scrollTo="<?= $key ?>" data-id="<?=$quest->id ?>" class=" scrollToDown glyphicon glyphicon-menu-down	Try it
									" aria-hidden="true"></a>
								</div>
							</div>
						</div>
					<?php } ?>
				<?php } ?>
			</div>
			<!-- // END Predmet-1 -->

			<!-- // Predmet-2 -->
			<div class="predmet__2 predmet__close" >
				<?php if (Yii::$app->language == 'ru') { ?>
					<div class=" col-md-5  text-center" style="margin-left: 285px; margin-top: 20px; height: 49px; line-height: 2;">
						<span style="font-size: 24px;">Вопросы по <b><?= $test2['mainCat']['name'] ?></b></span>
					</div>	
				<?php }else { ?>
				<div class=" col-md-5 col-md-offset-4 text-center" style="margin-left: 285px; margin-top: 20px; height: 49px; line-height: 2;">
					<span style="font-size: 24px;"><b><?= $test2['mainCat']['name'] ?></b> bo'yicha savollar</span>
				</div>
				<?php } ?>
				<div class="col-md-10 col-md-offset-1">
					<div class="col-md-11" >
						<ul class="count-ul">
							<?php $num = 1; if (isset($test2['tests'])) { ?>
								<?php foreach ($test2['tests'] as $key => $quest) { ?>
									<a href="#" class="scrollTo" data-scrollTo="question<?= ($key + $quest->id) ?>">
										<li class="count-li text-center" id="<?= $quest->id ?>">
											<?= $num++ ?>
										</li>
									</a>
								<?php } ?>
							<?php } ?>
						</ul>
					</div>
				</div>

				<!-- QUESTIONS START-->
				<?php $num = 1; if (isset($test2['tests'])) { ?>
					<?php foreach ($test2['tests'] as $key => $quest) { ?>
						 <div  class="panel col-md-9 col-md-offset-1 " id="question<?= ($key + $quest->id) ?>" data-key="<?= $quest->id?>">
						 	<div class="col-md-12 text-center col-md-offset-0 panel-div question<?=$quest->id ?>" >
						 		<div class="col-md-12 panel-area">
							 		<div class="panel-quest ">
										<?= $num++?>
									</div>
								</div>
								<div style="margin-top: 41px; padding: 15px 30px; height: auto; font-size: 18px;"><?= $quest->question ?></div>
								<div class="col-md-12" style="">
									<hr class="col-md-3 col-md-offset-2 "> <h4 class="col-md-1 text-center"><span class="glyphicon glyphicon-education"></span></h4> <hr class="col-md-3">
								</div>
							</div>
							<!-- VARIANTS START -->
							<div class="col-md-12" style="margin-top: 10px;">
								<?php foreach ($quest->variants as $key => $variant) { ?>
									<div class="list-group-item col-md-12  question<?=$variant->test_id ?>">
										<a href="<?//= Url::to(['/results/add-to-results'])?>"
											data-pred_name="<?= $test2['mainCat']['name'].'2' ?>"
											data-quest_id="<?=$variant->test_id?>" 
											data-var-id="<?=$variant->id?>" 
											data-type="<?=$variant->type?>" 
											class="variant">
											<table>	                
							                    <tr>
							                    	<td>
							                    		<h4><?= Tests::changeEncodingOption($variant->type)?></h4>
							                    	</td>
							                    	<td>
							                    		<h4><?=$variant->content?></h4>
							                    	</td>
						            			</tr>
						            		</table>
					            		</a>
					            	</div>		
								<?php  } ?>
							</div>
							<!-- VARIANTS END -->
							<div class=' col-md-2 pull-right text-center foggy-comparison-manual-wrapper' style="margin-top: 20px; margin-left: 10px; ">
								<div  class="col-md-1 update updatebtn" id="down<?=$key ?>" data-id="<?=$quest->id ?>">
									<a class="glyphicon glyphicon-repeat"></a>
								</div>
								<div  class="  col-md-1 to-down arrow" id="down<?=$key ?>">
									<a id="<?= $key ?>" data-scrollTo="<?= $key ?>" data-id="<?=$quest->id ?>" class=" scrollToDown glyphicon glyphicon-menu-down	Try it
									" aria-hidden="true"></a>
								</div>
							</div>
						</div>
					<?php } ?>
				<?php } ?>
			</div>
			<!-- // END Predmet-2 -->

			<!-- // Predmet-3 -->
			<div class="predmet__3 predmet__close" >
				<?php if (Yii::$app->language == 'ru') { ?>
					<div class=" col-md-5  text-center" style="margin-left: 285px; margin-top: 20px; height: 49px; line-height: 2;">
						<span style="font-size: 24px;">Вопросы по <b><?= $test3['mainCat']['name'] ?></b></span>
					</div>	
				<?php }else { ?>
				<div class=" col-md-5 col-md-offset-4 text-center" style="margin-left: 285px; margin-top: 20px; height: 49px; line-height: 2;">
					<span style="font-size: 24px;"><b><?= $test3['mainCat']['name'] ?></b> bo'yicha savollar</span>
				</div>
				<?php } ?>
				<div class="col-md-10 col-md-offset-1">
					<div class="col-md-11" >
						<ul class="count-ul">
							<?php $num = 1; if (isset($test3['tests'])) { ?>
								<?php foreach ($test3['tests'] as $key => $quest) { ?>
									<a href="#" class="scrollTo" data-scrollTo="question<?= ($key + $quest->id) ?>">
										<li class="count-li text-center" id="<?= $quest->id ?>">
											<?= $num++ ?>
										</li>
									</a>
								<?php } ?>
							<?php } ?>
						</ul>
					</div>
				</div>

				<!-- QUESTIONS START-->
				<?php if (isset($test3['tests'])) { ?>
					<?php $num = 1; foreach ($test3['tests'] as $key => $quest) { ?>
						<div  class="panel col-md-9 col-md-offset-1 " id="question<?= ($key + $quest->id) ?>" data-key="<?= $quest->id?>">
						 	<div class="col-md-12 text-center col-md-offset-0 panel-div question<?=$quest->id ?>" >
						 		<div class="col-md-12 panel-area">
							 		<div class="panel-quest ">
										<?= $num++?>
									</div>
								</div>
								<div style="margin-top: 41px; padding: 15px 30px; height: auto; font-size: 18px;"><?= $quest->question ?></div>
								<div class="col-md-12" style="">
									<hr class="col-md-3 col-md-offset-2 "> <h4 class="col-md-1 text-center"><span class="glyphicon glyphicon-education"></span></h4> <hr class="col-md-3">
								</div>
							</div>
							<!-- VARIANTS START -->
							<div class="col-md-12" style="margin-top: 10px;">
								<?php foreach ($quest->variants as $key => $variant) { ?>
									<div class="list-group-item col-md-12  question<?=$variant->test_id ?>">
										<a href="<?//= Url::to(['/results/add-to-results'])?>"
											data-pred_name="<?= $test3['mainCat']['name'].'3' ?>"
											data-quest_id="<?=$variant->test_id?>" 
											data-var-id="<?=$variant->id?>" 
											data-type="<?=$variant->type?>" 
											class="variant">
											<table>	                
							                    <tr>
							                    	<td>
							                    		<h4><?= Tests::changeEncodingOption($variant->type)?></h4>
							                    	</td>
							                    	<td>
							                    		<h4><?=$variant->content?></h4>
							                    	</td>
						            			</tr>
						            		</table>
					            		</a>
					        		</div> 		
								<?php  } ?>
								<!-- VARIANTS END -->
							</div>
							<div class=' col-md-2 pull-right text-center foggy-comparison-manual-wrapper' style="margin-top: 20px; margin-left: 10px; ">
								<div  class="col-md-1 update updatebtn" id="down<?=$key ?>" data-id="<?=$quest->id ?>">
									<a class="glyphicon glyphicon-repeat"></a>
								</div>
								<div class="col-md-1 to-down arrow" id="down<?=$key ?>">
									<a id="<?= $key ?>" data-scrollTo="<?= $key ?>" data-id="<?=$quest->id ?>" class="scrollToDown glyphicon glyphicon-menu-down Try it" aria-hidden="true"></a>
								</div>
							</div>
						</div>
					<?php } ?>
				<?php } ?>
			</div>
			<!-- // END Predmet-3 -->
			<!-- QUESTIONS END -->
			<?php $lang_uri = Yii::$app->language;
				(!empty($test2['mainCat']['name']) && !empty($test3['mainCat']['name'])) ? ($offset = 2) : ($offset = 3);
			?>
			<div class="col-md-offset-<?= $offset ?> col-md-9 wow fadeInRight" style="margin-bottom: 25px;">
			<?php if (!empty($test2['mainCat']['name']) && !empty($test3['mainCat']['name'])) { ?>
				<a onclick="nextPredmet()"  class="col-md-3 btn btn-default"><?=Yii::t('lang', 'go_to_the_next_test' )?><i class="glyphicon glyphicon-arrow-right" style="margin-left: 10px; margin-top: 3px; font-size: 15px;"></i>
				</a>
			<?php } ?>
				<a href='<?= Url::to("/$lang_uri/results/result")?>' id="resModal" class="btn btn-success col-md-3 col-md-offset-0" style="margin-bottom: 15px; margin-left: 15px;"><?=Yii::t('lang', 'result' )?> <i class="glyphicon glyphicon-signal" style="margin-left: 10px; margin-top: 3px; font-size: 15px;"></i></a>

				<a id="toprint" class="btn btn-primary col-md-3 col-md-offset-0" style="margin-bottom: 15px; margin-left: 15px;"><?=Yii::t('lang', 'print' )?><i class="glyphicon glyphicon-print" style="margin-left: 10px; margin-top: 3px; font-size: 15px;"></i>
				</a>
			</div>
		</div>
	<?php }else { ?>
		<!-- End abitutient -->
		<!-- For school -->
		<div class="col-md-12" style="min-height: 591px;">
			<div class="col-md-10" style="margin-left: 71px;">
				<hr class="col-md-3 hr"> <h4 class="col-md-1 text-center"><span class="glyphicon glyphicon-education"></span></h4> <hr class="col-md-3">
			</div>
			<!-- // Predmet-1 -->
			<div class="predmet__1 predmet__opened" >
				<?php if (Yii::$app->language == 'ru') { ?>
					<div class=" col-md-5  text-center" style="margin-left: 285px; margin-top: 20px; height: 49px; line-height: 2;">
						<span style="font-size: 20px;">Вопросы по <b><?= $test['mainCat']['name'] ?></b></span>
					</div>	
				<?php }else { ?>
				<div class=" col-md-5 col-md-offset-4 text-center" style="margin-left: 285px; margin-top: 20px; height: 49px; line-height: 2;">
					<span style="font-size: 24px;"><b><?= $test['mainCat']['name'] ?></b> bo'yicha savollar</span>
				</div>
				<?php } ?>
				<div class="col-md-10 col-md-offset-1">
					<div class="col-md-11" >
						<ul class="count-ul">
							<?php $num = 1; if (isset($test['tests'])) { ?>
								<?php foreach ($test['tests'] as $key => $quest) { ?>
									<a href="#" class="scrollTo" data-scrollTo="question<?= ($key + $quest->id) ?>">
										<li class="count-li text-center" id="<?= $quest->id ?>">
											<?= $num++ ?>
										</li>
									</a>
								<?php } ?>
							<?php } ?>
							
						</ul>
					</div>
				</div>

				<!-- QUESTIONS START-->	
				<?php if (isset($test['tests'])) { ?>
					<?php $num = 1; foreach ($test['tests'] as $key => $quest) { ?>
						 <div  class="panel col-md-9 col-md-offset-1 " id="question<?= ($key + $quest->id) ?>" data-key="<?= $quest->id?>">
						 	<div class="col-md-12 text-center col-md-offset-0 panel-div question<?=$quest->id ?>" >
						 		<div class="col-md-12 panel-area">
							 		<div class="panel-quest">
										<?= $num++?>
									</div>
								</div>
								<div style="margin-top: 41px; padding: 15px 30px; height: auto; font-size: 18px;"><h4><?= $quest->question ?></h4></div>
								<div class="col-md-12" style="">
									<hr class="col-md-3 col-md-offset-1" style="margin-left: 154px;"> <h4 class="col-md-1 text-center"><span class="glyphicon glyphicon-education"></span></h4> <hr class="col-md-3">
								</div>
							</div>
							<!-- VARIANTS START -->
							<div class="col-md-12" style="margin-top: 10px;">
								<?php foreach ($quest->variants as $key => $variant) { ?>
									<div class="list-group-item col-md-12  question<?=$variant->test_id ?>">
											<a href="<?//= Url::to(['/results/add-to-results'])?>"
												data-pred_name="<?= $test['mainCat']['name'].'1' ?>"
												data-quest_id="<?=$variant->test_id?>" 
												data-var-id="<?=$variant->id?>" 
												data-type="<?=$variant->type?>" 
												class="variant">
												<table>	                
								                    <tr>
								                    	<td>
								                    		<h4><?=  Tests::changeEncodingOption($variant->type)?></h4>
								                    	</td>
								                    	<td>
								                    		<h4><?=$variant->content?></h4>
								                    	</td>
							            			</tr>
							            		</table>
						            		</a>
					        		</div>		
								<?php  } ?>
							</div>
							<!-- VARIANTS END -->
							<div class=' col-md-2 pull-right text-center foggy-comparison-manual-wrapper' style="margin-top: 20px; margin-left: 10px; ">
								<div  class="col-md-1 update updatebtn" id="down<?=$key ?>" data-id="<?=$quest->id ?>">
									<a   class=" glyphicon glyphicon-repeat
									" ></a>
								</div>
								<div  class="  col-md-1 to-down arrow" id="down<?=$key ?>">
									<a  id="<?= $key ?>" data-scrollTo="<?= $key ?>" data-id="<?=$quest->id ?>" class=" scrollToDown glyphicon glyphicon-menu-down	Try it
									" aria-hidden="true"></a>
								</div>
							</div>
						</div>
					<?php } ?>
				<?php } ?>
			</div>
			<div class="col-md-12 ">
				<a href="#" class="button-up pull-right glyphicon glyphicon-menu-up" style="margin-right: 30px; margin-top: 0px;
    margin-bottom: 5px;
    margin-left: 10px;
    width: 40px;
    height: 40px;
    border: 1px solid #61b4ea;
    background: white;
    padding-top: 5px;
    /*padding-left: 12px;*/
    border-radius: 50px;
    position: relative;
    z-index: 1001;
    box-shadow: rgba(0,0,0,0.2) 0px 0px 1px;
    line-height: 30px;
    text-align: center;
    transition: all .3s;"></a>
			</div>
			<!-- QUESTIONS END -->
			<?php $lang_uri = Yii::$app->language;
				(!empty($test2['mainCat']['name']) && !empty($test3['mainCat']['name'])) ? ($offset = 2) : ($offset = 3);
			?>
			<div class="col-md-offset-<?= $offset ?> col-md-9 wow fadeInRight" style="margin-bottom: 25px;">
				<a href='<?= Url::to("/$lang_uri/results/result")?>' id="resModalx" class="btn btn-success col-md-3 col-md-offset-0" style="margin-bottom: 15px; margin-left: 15px;"><?=Yii::t('lang', 'result' )?> <i class="glyphicon glyphicon-signal" style="margin-left: 10px; margin-top: 3px; font-size: 15px;"></i></a>

				<a id="toprint" class="btn btn-primary col-md-3 col-md-offset-0" style="margin-bottom: 15px; margin-left: 15px;"><?=Yii::t('lang', 'print' )?><i class="glyphicon glyphicon-print" style="margin-left: 10px; margin-top: 3px; font-size: 15px;"></i>
				</a>
			</div>
		</div>
	<?php } ?>
	<!-- End school -->
</div>