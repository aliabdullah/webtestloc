<!-- Page Content -->
    <div class="container-fluid">

      <!-- Introduction Row -->
      <h1 class="my-4">About Us
        <small>It's Nice to Meet You!</small>
      </h1>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sint, explicabo dolores ipsam aliquam inventore corrupti eveniet quisquam quod totam laudantium repudiandae obcaecati ea consectetur debitis velit facere nisi expedita vel?</p>

      <!-- Team Members Row -->
      <div class="row">
        <div class="col-lg-12">
          <h2 class="my-4">Our Team</h2>
        </div>
        <div class="col-lg-3 col-sm-4 text-center mb-4" style="margin-bottom: 70px;">
          <img class="rounded-circle img-fluid d-block mx-auto well" src="<?= Yii::getAlias('@web/images/math.png')?>" alt="">
          <h3>John Smith
            
          </h3>
          <p>lorem ipsum qisqacha kategorya haqida</p>
        </div>
        <div class="col-lg-3 col-sm-4 text-center mb-4" style="margin-bottom: 70px;">
          <img class="rounded-circle img-fluid d-block mx-auto well" src="<?= Yii::getAlias('@web/images/math.png')?>" alt="">
          <h3>John Smith
            
          </h3>
          <p>lorem ipsum qisqacha kategorya haqida</p>
        </div>
        <div class="col-lg-3 col-sm-4 text-center mb-4" style="margin-bottom: 70px;">
          <img class="rounded-circle img-fluid d-block mx-auto well" src="<?= Yii::getAlias('@web/images/math.png')?>" alt="">
          <h3>John Smith
            
          </h3>
          <p>lorem ipsum qisqacha kategorya haqida</p>
        </div>
        <div class="col-lg-3 col-sm-4 text-center mb-4" style="margin-bottom: 70px;">
          <img class="rounded-circle img-fluid d-block mx-auto well" src="<?= Yii::getAlias('@web/images/math.png')?>" alt="">
          <h3>John Smith
            
          </h3>
          <p>lorem ipsum qisqacha kategorya haqida</p>
        </div>
        <div class="col-lg-3 col-sm-4 text-center mb-4" style="margin-bottom: 70px;">
          <img class="rounded-circle img-fluid d-block mx-auto well" src="<?= Yii::getAlias('@web/images/math.png')?>" alt="">
          <h3>John Smith
            
          </h3>
          <p>lorem ipsum qisqacha kategorya haqida</p>
        </div>
        <div class="col-lg-3 col-sm-4 text-center mb-4" style="margin-bottom: 70px;">
          <img class="rounded-circle img-fluid d-block mx-auto well " src="<?= Yii::getAlias('@web/images/math.png')?>" alt="">
          <h3>John Smith
            
          </h3>
          <p>lorem ipsum qisqacha kategorya haqida</p>
        </div>

        <div class="col-lg-3 col-sm-4 text-center mb-4" style="margin-bottom: 70px;">
          <img class="rounded-circle img-fluid d-block mx-auto well " src="<?= Yii::getAlias('@web/images/math.png')?>" alt="">
          <h3>John Smith
            
          </h3>
          <p>lorem ipsum qisqacha kategorya haqida</p>
        </div>

        <div class="col-lg-3 col-sm-4 text-center mb-4" style="margin-bottom: 70px;">
          <img class="rounded-circle img-fluid d-block mx-auto well " src="<?= Yii::getAlias('@web/images/math.png')?>" alt="">
          <h3>John Smith
            
          </h3>
          <p>lorem ipsum qisqacha kategorya haqida</p>
        </div>
      </div>

    </div>
    <!-- /.container -->