<?php 

use yii\widgets\ActiveForm;
use yii\helpers\Html;

 ?>

<div class="row">
	<div class="col-md-12" style="height: 850px; background-image: url('/images/blur2.jpg');">
		<fieldset class="col-md-6 col-md-offset-3 yow-banner">

			<legend class="" style="border:0px solid white;  margin-left: 140px; width: 310px; height: auto;"><img style="margin: 5px 11px 5px 11px;" src="/images/logo.png"></legend>

			<div class="col-md-8 col-md-offset-2" style="border: 0px solid white; padding: 0; height: 80px; margin-top: 0px;">
				<div class="col-md-4" style="height: 100%;
											    font-size: 82px;
											    line-height: 99%;
											    color: white;
											    padding: 0;
											    /*width: 130px;*/
											    border: 0px solid white;">
					575
				</div>
				<div class="col-md-8" style="border: 0px solid white; height: 50px; font-size: 38px; color: white; padding: 0;">
					Премиум тесты
				</div>
				<span class="" style="color: white; font-size: 18px; margin-left: 3px; line-height: -1;">по направлению и тематический</span>
				<!-- <div class="" style="width: 100px; float: right; height: 20px; border: 1px solid white; position: relative;">
					fddfdfdfdfdf
				</div> -->
				
			</div>
			<h4 class="col-md-8 " style="margin-top: 40px; margin-left: 29%; color: white; ">Мы служим вам за ваши мечты!</h4>

		</fieldset>
		<div class="col-md-6 col-md-offset-3" >
			<img src="/images/shadow.png">
		</div>

		<div class="col-md-12">
			<div class="col-md-6 col-md-offset-3 text-center">
				
				<h3 class="col-md-8 col-md-offset-2" style="color: white;">
					<!-- <?= $tariffsModal->title?> -->
						Выберите направлению
					</h3>
				<h4 class="col-md-12  col-md-offset-0" style="color: white;">
					<!-- <?= $tariffsModal->content?> -->
						Направлении
					</h4>
				
			</div>
		</div>

		<div class="col-md-12 col-md-offset-0 filter" style="border: 0px solid white; height: auto; border-radius: 50px;">
			<?php $form = ActiveForm::begin(); ?>

			<div class="container" style="min-height:auto !important">
				<div class="col-md-4">
					<select class="Vuz">
						<!-- <?php foreach (\app\models\tests\Tests::getLevels() as $key => $level) { ?>
				      		<option value="0"><?= $level ?></option>				
						<?php } ?> -->
				      	<option value="1">Testing my custom select</option>
				      	<option value="1">Testing my custom select</option>
				      	<option value="2">This is another option</option>
				    </select>
				</div>


				<div class="col-md-4">
					
				        <select  class="level " >
				            
				        </select>
				</div>


				<div class="col-md-4">
					<select class="level" name="level">
				      	<?php foreach (\app\models\tests\Tests::getLevels() as $key => $level) { ?>
				      		<option value="<?= $key ?>"><?= $level ?></option>				
						<?php } ?>
				    </select>
				</div>

			</div>
			
			<div class="col-md-6 col-md-offset-3 text-center" style="margin-top: 20px;">
				<h4 class="col-md-12  col-md-offset-0" style="color: white;">
				<!-- <?= $tariffsModal->content?> -->
					Предметы
				</h4>
			</div>

			<div class="container" style="min-height:auto !important">

				<div class="col-md-4">
					<select class="predmet-1 pred" name="predmet1">
				      	<?php foreach (\app\models\category\Category::getParentCats() as $key => $level) { ?>
				      		<option value="<?= $key ?>"><?= $level ?></option>				
						<?php } ?>
				    </select>
				</div>

				<div class="col-md-4">
					<select class="predmet-2 pred" name="predmet2">
				      	<?php foreach (\app\models\category\Category::getParentCats() as $key => $level) { ?>
				      		<option value="<?= $key ?>"><?= $level ?></option>				
						<?php } ?>
				    </select>
				</div>

				<div class="col-md-4">
					<select class="predmet-3 pred" name="predmet3">
				      	<?php foreach (\app\models\category\Category::getParentCats() as $key => $level) { ?>
				      		<option value="<?= $key ?>"><?= $level ?></option>				
						<?php } ?>
				    </select>
				</div>

			</div>

			

			

			<a href="<?= yii\helpers\Url::to('/test/premium-test')?>"  class="col-md-6 			col-md-push-3 btn button ">
					
				<span class="text text-center" style="color: white;">Период</span> 
				
			</a>

<?= Html::submitButton('Создать', ['class' => 'btn btn-success']) ?>        
<?php ActiveForm::end(); ?>
		</div>
	</div>
</div>
