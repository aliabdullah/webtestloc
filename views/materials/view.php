
	<div class="" style="margin-top: 90px;">
		
		<div class="col-md-8">
			<div class="main-post" style="margin-bottom: 30px;">
            <div class="post-top-area">
              <h5 class="pre-title"><?= strtoupper($model->cat->name) ?></h5>
              <h3 class="title"><a href="#" ><b><?= ucfirst($model->name) ?></b></a></h3>
              <div class="post-info">
                <div class="left-area">
                  <a class="avatar" href="#"><img src="<?= '/'.$model->photo ?>" alt="Profile Image"></a>
                </div>
                <div class="middle-area">
                  <a class="name" href="#"><b><?=$model->user->username?></b></a>
                  <h6 class="datee"><?=$model->date?></h6>
                </div>
              </div><!-- post-info -->
              <p class="para"><?= ucfirst($model->description) ?></p>
            </div><!-- post-top-area -->
            <!-- <div class="post-image"><img src="/images/images/blog-1-1000x600.jpg" alt="Blog Image"></div> -->
            <div class="post-bottom-area">
              
              <ul class="tags">
                <li><a href="#">Mnual</a></li>
                <li><a href="#">Liberty</a></li>
                <li><a href="#">Recommendation</a></li>
                <li><a href="#">Inspiration</a></li>
              </ul>
              <div class="post-icons-area">
                <ul class="post-icons">
                  <li><a style="cursor: pointer;" id="like" data-id="<?= $model->id ?>"><i class="ion-heart"></i><?= $model->likes ? $model->likes : 0 ?></a></li>
                  <!-- <li><a href="#"><i class="ion-chatbubble"></i>6</a></li> -->
                  <li><a href="#"><i class="ion-eye"></i><?= $model->views ? $model->views : 0 ?></a></li>
                </ul>
                <ul class="icons">
                  <li>SHARE : </li>
                  <li><a href="#"><i class="ion-social-facebook"></i></a></li>
                  <li><a href="#"><i class="ion-social-twitter"></i></a></li>
                  <li><a href="#"><i class="ion-social-pinterest"></i></a></li>
                </ul>
              </div>
            </div><!-- post-bottom-area -->

          </div><!-- main-post -->
		</div>
    <div class="col-md-4">
      <div class="col-md-12 pull-right col-xs-12">
          <!-- USERS LIST -->
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title"><?=Yii::t('lang', 'Видео уроки')?></h3>

              <div class="box-tools pull-right">
                <span class="label label-danger">8 <?=Yii::t('lang', 'Новые')?></span>
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                </button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <ul class="users-list clearfix">
                <li>
                  <img src="/images/user1-128x128.jpg" alt="User Image">
                  <a class="users-list-name" href="#">Alexander Pierce</a>
                  <span class="users-list-date">Математик</span>
                </li>
                <li>
                  <img src="/images/user8-128x128.jpg" alt="User Image">
                  <a class="users-list-name" href="#">Norman</a>
                  <span class="users-list-date">Физик</span>
                </li>
                <li>
                  <img src="/images/user7-128x128.jpg" alt="User Image">
                  <a class="users-list-name" href="#">Jane</a>
                  <span class="users-list-date">Биолог</span>
                </li>
                <li>
                  <img src="/images/user6-128x128.jpg" alt="User Image">
                  <a class="users-list-name" href="#">John</a>
                  <span class="users-list-date">Программист</span>
                </li>
                <li>
                  <img src="/images/user2-160x160.jpg" alt="User Image">
                  <a class="users-list-name" href="#">Alexander</a>
                  <span class="users-list-date">13 Jan</span>
                </li>
                <li>
                  <img src="/images/user5-128x128.jpg" alt="User Image">
                  <a class="users-list-name" href="#">Sarah</a>
                  <span class="users-list-date">14 Jan</span>
                </li>
                <li>
                  <img src="/images/user4-128x128.jpg" alt="User Image">
                  <a class="users-list-name" href="#">Nora</a>
                  <span class="users-list-date">15 Jan</span>
                </li>
                <li>
                  <img src="/images/user3-128x128.jpg" alt="User Image">
                  <a class="users-list-name" href="#">Nadia</a>
                  <span class="users-list-date">15 Jan</span>
                </li>
              </ul>
              <!-- /.users-list -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer text-center">
              <a href="javascript:void(0)" class="uppercase"><?=Yii::t('lang', 'Просмотреть все')?></a>
            </div>
            <!-- /.box-footer -->
          </div>
          <!--/.box -->
      </div>
      <div class="col-md-12 pull-right col-xs-12">
        <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title"><?=Yii::t('lang', 'Учебные центры')?></h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <ul class="products-list product-list-in-box">
                  <li class="item" style="display: block;">
                    <div class="product-img">
                      <img src="/images/default-50x50.gif" alt="Product Image">
                    </div>
                    <div class="product-info">
                      <a href="javascript:void(0)" class="product-title">Samsung TV
                        <span class="label label-warning pull-right">$1800</span></a>
                      <span class="product-description">
                            Samsung 32" 1080p 60Hz LED Smart HDTV.
                          </span>
                    </div>
                  </li>
                  <!-- /.item -->
                  <li class="item" style="display: block;">
                    <div class="product-img">
                      <img src="/images/default-50x50.gif" alt="Product Image">
                    </div>
                    <div class="product-info">
                      <a href="javascript:void(0)" class="product-title">Bicycle
                        <span class="label label-info pull-right">$700</span></a>
                      <span class="product-description">
                            26" Mongoose Dolomite Men's 7-speed, Navy Blue.
                          </span>
                    </div>
                  </li>
                  <!-- /.item -->
                  <li class="item" style="display: block;">
                    <div class="product-img">
                      <img src="/images/default-50x50.gif" alt="Product Image">
                    </div>
                    <div class="product-info">
                      <a href="javascript:void(0)" class="product-title">Xbox One <span
                          class="label label-danger pull-right">$350</span></a>
                      <span class="product-description">
                            Xbox One Console Bundle with Halo Master Chief Collection.
                          </span>
                    </div>
                  </li>
                  <!-- /.item -->
                  <li class="item">
                    <div class="product-img">
                      <img src="/images/default-50x50.gif" alt="Product Image">
                    </div>
                    <div class="product-info">
                      <a href="javascript:void(0)" class="product-title">PlayStation 4
                        <span class="label label-success pull-right">$399</span></a>
                      <span class="product-description">
                            PlayStation 4 500GB Console (PS4)
                          </span>
                    </div>
                  </li>
                  <!-- /.item -->
                </ul>
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-center">
                <a href="javascript:void(0)" class="uppercase"><?=Yii::t('lang', 'Просмотреть все')?></a>
              </div>
              <!-- /.box-footer -->
            </div>
      </div>
    </div>
	</div>
<style type="text/css">
  .like-color {
    color:red;
  }
</style>