<?php 

use yii\helpers\Url;

?>
<section class="blog-area section" style="margin-top: 30px;">
    <div class="container">
      <!-- <div class="row"> -->
        <div class="col-lg-8 col-md-12">
          <div class="row">
		        <?php if ($audios) { ?>
		          <?php foreach ($audios as $key => $audio) { ?>
		            <div class="col-md-6 col-sm-12">
		              <div class="card h-100">
		                <div class="single-post post-style-1">
		                  <div class="blog-image"><img src="<?= '/'.$audio->photo ?>" alt="Blog Image"></div>
		                  <a class="avatar" href="#"><img src="/images/youtube.png" alt="Profile Image"></a>
		                  <div class="blog-info">
		                    <h4 class="title"><a href="#"><b><?= $audio->name ?></b></a></h4>
		                    <ul class="post-footer">
		                      <li><a href="#"><i class="ion-heart"></i><?= $audio->likes ?></a></li>
		                      <!-- <li><a href="#"><i class="ion-chatbubble"></i>6</a></li> -->
		                      <li><a href="#"><i class="ion-eye"></i><?= $audio->views ?></a></li>
		                    </ul>
		                  </div><!-- blog-info -->
		                </div><!-- single-post -->
		              </div><!-- card -->
		            </div><!-- col-md-6 col-sm-12 -->
		          <?php } ?>
		        <?php }else { ?>
		        	<section class="resurs ">
    						<div class="row">
    							<div class="col-md-10 col-md-offset-1 text-center well">
                    <h1>Ooops!</h1>
                    <p><?=Yii::t("lang", "В текущем моменте такую информация нету в базе")?>...<strong> SOON!</strong></p>
                  </div>
    						</div>
    					</section>
		        <?php } ?>
          </div><!-- row -->
          <!-- <a class="load-more-btn" href="#"><b>LOAD MORE</b></a> -->
          <!-- section Video -->
          <!-- / Video -->
        </div><!-- col-lg-8 col-md-12 -->
        <div class="col-lg-4 col-md-12 ">
          <div class="single-post info-area ">
            <div class="about-area">
              <h4 class="title"><b><?=Yii::t('lang', 'О странице')?></b></h4>
              <p><?=Yii::t('lang', 'В этом разделе вы можете использовать разные учебники, раздел также включает видео, аудио, электронные книги, статьи и является открытым исходным кодом для пользователей с статусом Premium')?></p>
            </div>
           <!--  <div class="subscribe-area">
              <h4 class="title"><b>ПОДПИСЫВАТЬСЯ</b></h4>
              <div class="input-area">
                <form>
                  <input class="email-input" type="text" placeholder="Enter your email">
                  <button class="submit-btn" type="submit"><i class="ion-ios-email-outline"></i></button>
                </form>
              </div>
            </div> --><!-- subscribe-area -->
            <div class="tag-area">
              <h4 class="title"><b><?=Yii::t('lang', 'ОБЛАКО ТЕГОВ')?></b></h4>
              <ul>
                <li><a href="<?= Url::to('/materials/video') ?>"><?=Yii::t('lang', 'Видео')?></a></li>
                <li><a href="<?= Url::to('/library/index') ?>"><?=Yii::t('lang', 'Библиотека')?></a></li>
                <li><a href="<?= Url::to('/materials/audio') ?>"><?=Yii::t('lang', 'Аудио')?></a></li>
                <li><a href="<?= Url::to('/materials/recomended') ?>"><?=Yii::t('lang', 'Рекомендации')?></a></li>
                <li><a href="<?= Url::to('/materials/link') ?>"><?=Yii::t('lang', 'Источники')?></a></li>
              </ul>
            </div><!-- subscribe-area -->
          </div><!-- info-area -->
        </div><!-- col-lg-4 col-md-12 -->
      <div class="col-md-12">  
      </div>
    </div><!-- container -->
  </section><!-- section -->

