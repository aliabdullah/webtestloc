<?php 

use yii\helpers\Url;

?>
<section class="blog-area section" style="margin-top: 20px;">
    <div class="container">
        <div class="col-lg-8 col-md-12">
          <div class="row">
          <?php foreach ($posts as $key => $data) { ?>
            <div class="col-md-6 col-sm-12">
              <div class="card h-100">
                <div class="single-post post-style-2 post-style-3">
                  <div class="blog-info">
                    <h6 class="pre-title"><a href="#"><b><?= strtoupper($data->cat->name) ?></b></a></h6>
                    <h4 class="title"><a href="<?= Url::to(['/materials/view', 'id' => $data->id])?>"><b><?= ucfirst($data->name) ?></b></a>
                    </h4>
                    <p><?= mb_strimwidth(ucfirst($data->description), 0, 110, "...") ?></p>
                    <ul class="post-footer">
                      <li><a ><i class="ion-heart"></i><?= $data->likes?></a></li>
                      <!-- <li><a href="#"><i class="ion-chatbubble"></i>6</a></li> -->
                      <li><a ><i class="ion-eye"></i><?= $data->views?></a></li>
                    </ul>
                  </div><!-- blog-right -->
                </div><!-- single-post extra-blog -->
              </div><!-- card -->
            </div><!-- col-md-6 col-sm-12 -->
          <?php } ?>
          </div><!-- row -->
          <!-- <a class="load-more-btn" href="#"><b>LOAD MORE</b></a> -->
          <!-- section Video -->
          <!-- / Video -->
        </div><!-- col-lg-8 col-md-12 -->
        <div class="col-lg-4 col-md-12 ">
          <div class="single-post info-area ">
            <div class="about-area">
              <h4 class="title"><b><?=Yii::t('lang', 'about_page')?></b></h4>
              <p><?=Yii::t('lang', 'desc_page')?></p>
            </div>
           <!--  <div class="subscribe-area">
              <h4 class="title"><b>ПОДПИСЫВАТЬСЯ</b></h4>
              <div class="input-area">
                <form>
                  <input class="email-input" type="text" placeholder="Enter your email">
                  <button class="submit-btn" type="submit"><i class="ion-ios-email-outline"></i></button>
                </form>
              </div>
            </div> --><!-- subscribe-area -->
            <div class="tag-area">
              <h4 class="title"><b><?=Yii::t('lang', 'cloud')?></b></h4>
              <ul>
                <li><a href="<?= Url::to('/materials/video') ?>"><?=Yii::t('lang', 'video')?></a></li>
                <li><a href="<?= Url::to('/library/index') ?>"><?=Yii::t('lang', 'library')?></a></li>
                <li><a href="<?= Url::to('/materials/audio') ?>"><?=Yii::t('lang', 'audio')?></a></li>
                <li><a href="<?= Url::to('/materials/recomended') ?>"><?=Yii::t('lang', 'recomended')?></a></li>
                <li><a href="<?= Url::to('/materials/link') ?>"><?=Yii::t('lang', 'source')?></a></li>
              </ul>
            </div><!-- subscribe-area -->
          </div><!-- info-area -->
        </div><!-- col-lg-4 col-md-12 -->
      <?php if ($videos) { ?>
        <div class="col-md-12" style="padding-top: 30px;">  
          <span class="text-center" style="font-size: 25px; width: 100%; color: #555555; padding-bottom: 30px;"><?=Yii::t('lang', 'Видеоматериалы')?></span>
          <?php foreach ($videos as $key => $video) { ?>
              <div class="col-md-4 col-sm-12">
                <div class="card h-100">
                  <div class="single-post post-style-1">
                    <!-- <div class="blog-image"><img src="<?= '/'.$video->photo ?>" alt="Blog Image"></div> -->
                    <a class="avatar" href="#"><img src="/images/youtube.png" alt="Profile Image"></a>
                    <div class="blog-info">
                      <h4 class="title"><a href="<?= Url::to(['/materials/view', 'id' => $video->id])?>"><b><?= $video->name ?></b></a>
                      </h4>
                      <ul class="post-footer">
                        <li><a><i class="ion-heart"></i><?= $video->likes ? $video->likes : 0 ?></a></li>
                        <!-- <li><a href="#"><i class="ion-chatbubble"></i>6</a></li> -->
                        <li><a><i class="ion-eye"></i><?= $video->views ? $video->views : 0 ?></a></li>
                      </ul>
                    </div><!-- blog-info -->
                  </div><!-- single-post -->
                </div><!-- card -->
              </div><!-- col-md-6 col-sm-12 -->
            <?php } ?>
        </div>
      <?php } ?>
    </div><!-- container -->
  </section><!-- section -->