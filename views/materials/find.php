<?php 

use yii\helpers\Url;

 ?>
	<div class="" style="margin-top: 90px; min-height: 637px;">
		<div class="col-md-8">
			<div class="row">
        <div class="col-xs-12">
          <?php if ($catFindes) { ?>
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Учебные материали по <strong><?= $catFindes[0]->cat->name?></strong></h3>
              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">
                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th>ID</th>
                  <th>User</th>
                  <!-- <th>Date</th> -->
                  <th>Status</th>
                  <th>Reason</th>
                </tr>
                  <?php foreach ($catFindes as $key => $data) { ?>
                      <tr>
                        <td><?= $key ?></td>
                        <td style="width: 150px;"><a href="<?= Url::to(['/materials/view', 'id' => $data->id])?>"><?= $data->name ?></a></td>
                        <!-- <td>11-7-2014</td> -->
                        <td style="width: 80px;"><span class="label label-success"><?= $data->type ?></span></td>
                        <td><?= mb_strimwidth(ucfirst($data->description), 0, 110, "...") ?></td>
                      </tr>
                  <?php  } ?>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          <?php }else { ?>
              <section class="resurs ">
                <div class="row">
                  <div class="col-md-10 col-md-offset-1 text-center well">
                    <h1 style="color:#757575;">Ooops!</h1>
                    <p><?=Yii::t("lang", "В текущем моменте такую информация нету в базе")?>...<strong> SOON!</strong></p>
                  </div>
                </div>
              </section>
          <?php } ?>
        </div>
      </div>
		</div>

    <div class="col-md-4">
      <div class="col-md-12 pull-right col-xs-12">
                <!-- USERS LIST -->
                <div class="box box-danger">
                  <div class="box-header with-border">
                    <h3 class="box-title"><?=Yii::t('lang', 'Видео уроки')?></h3>
                    <div class="box-tools pull-right">
                      <span class="label label-danger">8 <?=Yii::t('lang', 'Новые')?></span>
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                      </button>
                    </div>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body no-padding">
                    <ul class="users-list clearfix">
                      <li>
                        <img src="/images/user1-128x128.jpg" alt="User Image">
                        <a class="users-list-name" href="#">Alexander Pierce</a>
                        <span class="users-list-date">Математик</span>
                      </li>
                      <li>
                        <img src="/images/user8-128x128.jpg" alt="User Image">
                        <a class="users-list-name" href="#">Norman</a>
                        <span class="users-list-date">Физик</span>
                      </li>
                      <li>
                        <img src="/images/user7-128x128.jpg" alt="User Image">
                        <a class="users-list-name" href="#">Jane</a>
                        <span class="users-list-date">Биолог</span>
                      </li>
                      <li>
                        <img src="/images/user6-128x128.jpg" alt="User Image">
                        <a class="users-list-name" href="#">John</a>
                        <span class="users-list-date">Программист</span>
                      </li>
                      <li>
                        <img src="/images/user2-160x160.jpg" alt="User Image">
                        <a class="users-list-name" href="#">Alexander</a>
                        <span class="users-list-date">13 Jan</span>
                      </li>
                      <li>
                        <img src="/images/user5-128x128.jpg" alt="User Image">
                        <a class="users-list-name" href="#">Sarah</a>
                        <span class="users-list-date">14 Jan</span>
                      </li>
                      <li>
                        <img src="/images/user4-128x128.jpg" alt="User Image">
                        <a class="users-list-name" href="#">Nora</a>
                        <span class="users-list-date">15 Jan</span>
                      </li>
                      <li>
                        <img src="/images/user3-128x128.jpg" alt="User Image">
                        <a class="users-list-name" href="#">Nadia</a>
                        <span class="users-list-date">15 Jan</span>
                      </li>
                    </ul>
                    <!-- /.users-list -->
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer text-center">
                    <a href="javascript:void(0)" class="uppercase"><?=Yii::t('lang', 'Просмотреть все')?></a>
                  </div>
                  <!-- /.box-footer -->
                </div>
                <!--/.box -->
      </div>
      <div class="col-md-12 pull-right col-xs-12">
        <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title"><?=Yii::t('lang', 'Учебные центры')?></h3>

                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <ul class="products-list product-list-in-box">
                  <li class="item" style="display: block;">
                    <div class="product-img">
                      <img src="/images/default-50x50.gif" alt="Product Image">
                    </div>
                    <div class="product-info">
                      <a href="javascript:void(0)" class="product-title">Samsung TV
                        <span class="label label-warning pull-right">$1800</span></a>
                      <span class="product-description">
                            Samsung 32" 1080p 60Hz LED Smart HDTV.
                          </span>
                    </div>
                  </li>
                  <!-- /.item -->
                  <li class="item" style="display: block;">
                    <div class="product-img">
                      <img src="/images/default-50x50.gif" alt="Product Image">
                    </div>
                    <div class="product-info">
                      <a href="javascript:void(0)" class="product-title">Bicycle
                        <span class="label label-info pull-right">$700</span></a>
                      <span class="product-description">
                            26" Mongoose Dolomite Men's 7-speed, Navy Blue.
                          </span>
                    </div>
                  </li>
                  <!-- /.item -->
                  <li class="item" style="display: block;">
                    <div class="product-img">
                      <img src="/images/default-50x50.gif" alt="Product Image">
                    </div>
                    <div class="product-info">
                      <a href="javascript:void(0)" class="product-title">Xbox One <span
                          class="label label-danger pull-right">$350</span></a>
                      <span class="product-description">
                            Xbox One Console Bundle with Halo Master Chief Collection.
                          </span>
                    </div>
                  </li>
                  <!-- /.item -->
                  <li class="item" style="display: block;">
                    <div class="product-img">
                      <img src="/images/default-50x50.gif" alt="Product Image">
                    </div>
                    <div class="product-info">
                      <a href="javascript:void(0)" class="product-title">PlayStation 4
                        <span class="label label-success pull-right">$399</span></a>
                      <span class="product-description">
                            PlayStation 4 500GB Console (PS4)
                          </span>
                    </div>
                  </li>
                  <!-- /.item -->
                </ul>
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-center">
                <a href="javascript:void(0)" class="uppercase"><?=Yii::t('lang', 'Просмотреть все')?></a>
              </div>
              <!-- /.box-footer -->
            </div>
      </div>
    </div>

    



	</div>
