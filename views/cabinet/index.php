<?php 

use yii\helpers\Url;

$user = Yii::$app->user->identity;
  if (!empty($user->photo)) {
    if (substr($user->photo, 0, 4) == 'http') {
      $userPhoto = $user->photo;  
    }else{
        $userPhoto = '/'.$user->photo;
    }
  }else {
    $userPhoto = '/uploads/user1.png';
  }
  
?>
<div class="col-md-12" style="min-height: 800px; background-color: #f2f6fd;">
<div class="col-md-6 col-md-push-3" style="margin-top: 80px;">
  <!-- Widget: user widget style 1 -->
  <div class="box box-widget widget-user" >
    <!-- Add the bg color to the header using any of the bg-* classes -->
    <div class="widget-user-header bg-black" style="height: 180px; background: url('/images/yu.jpg')center;">
      <h3 class="widget-user-username"><?= $user->username?></h3>
      <h5 class="widget-user-desc"><?=Yii::t('lang', 'user')?></h5>
    </div>
    <div class="widget-user-image" style="top:125px;">
      <img class="img-circle" style="height: 100px; width: 100px;" src="<?= $userPhoto ?>" alt="User Avatar">
    </div>

    <div class="box-footer" style="padding-top: 50px;">
      <ul class="nav nav-stacked" style="background-color: #fff;">
        <li><a href="<?= Url::to(['/results/index'])?>"><?= Yii::t('lang', 'My_results') ?></a></li>
        <li><a href="<?= Url::to(['/tariffs/index'])?>"><?= Yii::t('lang', 'Orders') ?></a></li>
        <li ><a href="<?= Url::to(['/site/upload'])?>">Account <?=Yii::t('lang', 'settings')?></a></li>
        <li class=""><a style="color:red;" href="<?= Url::to(['/site/logout', 'id' => $user->id])?>"><?=Yii::t('lang', 'Logout')?></a></li>
      </ul>
      <div class="row" style="margin-top: 20px; height: 30px;">
        
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
  </div>
  <!-- /.widget-user -->
</div>

</div>
