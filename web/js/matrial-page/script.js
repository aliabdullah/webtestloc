/* accordion menu plugin*/
;(function($, window, document, undefined) {
  var pluginName = "accordion";
  var defaults = {
    speed: 200,
    showDelay: 0,
    hideDelay: 0,
    singleOpen: true,
    clickEffect: true,
    indicator: 'submenu-indicator-minus',
    subMenu: 'submenu',
    event: 'click touchstart' // click, touchstart
  };

  function Plugin(element, options) {
    this.element = element;
    this.settings = $.extend({}, defaults, options);
    this._defaults = defaults;
    this._name = pluginName;
    this.init();
  }
  $.extend(Plugin.prototype, {
    init: function() {
      this.openSubmenu();
      this.submenuIndicators();
      if (defaults.clickEffect) {
        this.addClickEffect();
      }
    },
    openSubmenu: function() {
      $(this.element).children("ul").find("li").bind(defaults.event, function(e) {
        e.stopPropagation();
        e.preventDefault();
        var $subMenus = $(this).children("." + defaults.subMenu);
        var $allSubMenus = $(this).find("." + defaults.subMenu);
        if ($subMenus.length > 0) {
          if ($subMenus.css("display") == "none") {
            $subMenus.slideDown(defaults.speed).siblings("a").addClass(defaults.indicator);
            if (defaults.singleOpen) {
              $(this).siblings().find("." + defaults.subMenu).slideUp(defaults.speed)
                .end().find("a").removeClass(defaults.indicator);
            }
            return false;
          } else {
            $(this).find("." + defaults.subMenu).delay(defaults.hideDelay).slideUp(defaults.speed);
          }
          if ($allSubMenus.siblings("a").hasClass(defaults.indicator)) {
            $allSubMenus.siblings("a").removeClass(defaults.indicator);
          }
        }
        window.location.href = $(this).children("a").attr("href");
      });
    },
    submenuIndicators: function() {
      if ($(this.element).find("." + defaults.subMenu).length > 0) {
        $(this.element).find("." + defaults.subMenu).siblings("a").append("<span class='submenu-indicator'>+</span>");
      }
    },
    addClickEffect: function() {
      var ink, d, x, y;
      $(this.element).find("a").bind("click touchstart", function(e) {
        $(".ink").remove();
        if ($(this).children(".ink").length === 0) {
          $(this).prepend("<span class='ink'></span>");
        }
        ink = $(this).find(".ink");
        ink.removeClass("animate-ink");
        if (!ink.height() && !ink.width()) {
          d = Math.max($(this).outerWidth(), $(this).outerHeight());
          ink.css({
            height: d,
            width: d
          });
        }
        x = e.pageX - $(this).offset().left - ink.width() / 2;
        y = e.pageY - $(this).offset().top - ink.height() / 2;
        ink.css({
          top: y + 'px',
          left: x + 'px'
        }).addClass("animate-ink");
      });
    }
  });
  $.fn[pluginName] = function(options) {
    this.each(function() {
      if (!$.data(this, "plugin_" + pluginName)) {
        $.data(this, "plugin_" + pluginName, new Plugin(this, options));
      }
    });
    return this;
  };

  // $('#menu ul li').on('click', function(){
  //     var active = this;
  //     if (active) {active.addClass('active_item')}
  // });
})(jQuery, window, document);

var myUrl = window.location.pathname.split('/');
function mode() {
        if(myUrl[1] == 'ru') {
          $.ajax({
           url: '/ru/admin/task/user-message',
           success: function(response) {
            resrponse = JSON.parse(response);
            if (resrponse.length > 0) {
              var span = document.getElementById('count_message');
              span.innerHTML = resrponse.length;  
            }
           }
          });
        }else {
          $.ajax({
           url: '/uz/admin/task/user-message',
           success: function(response) {
            resrponse = JSON.parse(response);
            var span = document.getElementById('count_message');
            span.innerHTML = resrponse.length;
           }
          });
        }
      };
  $(document).ready(function(){
    mode();
  });

      


$('.alertbox').on('click', function(e){
  e.preventDefault();
  $('.dropdown .preview_message').addClass('show_preview').removeClass('preview_message');
  $.ajax({
    url: '/ru/admin/task/user-message',
    success: function(response) {
      data = JSON.parse(response);
      data.forEach(function(item){
        // console.log(item);
        var ul = document.getElementById('ul');
        var li = document.createElement('li');
        li.className = 'message_li';
        li.id = 'message_'+item['id'];

        var a = document.createElement('a');
        a.className = 'message_a';
        // a.id = 'message';
        a.setAttribute('data-id', item['id']);
        a.setAttribute('data-true', item['true']);
        a.setAttribute('data-message', item['message']);
        // a.setAttribute('data-title', item['title']);
        
        var div = document.createElement('div');
        div.className = 'message_div pull-left';
        
        var label = document.createElement('label');
        label.className = 'message_label';
        var id = item['task_id'];
        $.ajax({
          url: '/ru/admin/task/get-task',
          data: {id: id},
          type: 'GET',
          success: function(data) {
            task = JSON.parse(data);
            label.innerHTML = task['name'];
          }
        });

        var span = document.createElement('span');
        span.className = 'message_text';
        span.innerHTML = item['message'].substr(-0, 30)+'...';
        
        var small = document.createElement('small');
        // small.innerHTML = ''
        var i = document.createElement('i');
        i.className = 'fa fa-clock-o';

        ul.append(li);
        li.append(a);
        a.append(div);
        div.append(label);
        a.append(span);
        
      });
      
      
    }
  })
    mode();

  // swal({
  //   title: "Good job!",
  //   text: "You clicked the button!",
  //   icon: "success",
  //   button: "Aww yiss!",
  // });
});

$('.notify_list').on('click', '.message_a', function(e){
  e.preventDefault();
  var myUrl = window.location.pathname.split('/');
  var id = $(this).data('id');
  var title = $(this).data('title');
  var message = $(this).data('message');
  var t = $(this).data('true');
  // console.log(id);
  $.ajax({
    url: '/ru/task/read-message',
    data: {id: id},
    type: 'POST',
    success: function(res){
      var liDelete = document.getElementById('message_'+id);
          liDelete.remove();
      var countDelete = document.getElementById('count_message');
          countDelete.innerHTML--;
      var count_inner = document.getElementsByClassName('header_message');
          var n = Number(count_inner.innerHTML);
          // message degi ichki countni to'g'irlash kere
      console.log(count_inner);
      if (t) {
        swal({
          title: title,
          text: message,
          icon: "success",
          button: "Ok",
        });
        $(this).remove();
      }else {
        swal({
          title: title,
          text: message,
          icon: "error",
          button: "Ok",
        });
        $(this).remove();
      }
      
    }
  });
  
  
});

$('.addlike').on('click', function(){
  var myUrl = window.location.pathname.split('/');
  var id = $(this).data('id');
  $.ajax({
    url: '/ru/task/add-like',
    data: {id: id},
    type: 'POST',
    success: function(res){
      console.log(res);
    }
  })
});

// Down timer function
$(document).ready(function(){
  $.ajax({
    url: '/ru/task/get-tourn-date',
    type: 'GET',
    success: function(d){
      var tournData = JSON.parse(d);
      Data = new Date();
      Year = Data.getFullYear();
      Month = Data.getMonth();
      Day = Data.getDate();
      Hour = Data.getHours();
      Minutes = Data.getMinutes();
      Seconds = Data.getSeconds();
      var currDate = Date.parse(Year+'-'+(Month+1)+'-'+Day);
      var saveDate = Date.parse(tournData['date']);
      // add one day to saveDate
      D = new Date(tournData['date']);
      D.setDate(D.getDate()+1);
      console.log(D.getFullYear()+'-'+D.getMonth()+'-'+D.getDate());
      console.log(tournData['date']);
      // D.setDate(D.getDate() + 3);
      secDay = new Date(saveDate);
      if (saveDate > currDate || saveDate == currDate) {
       
        new TimezZ('.j-timer-second', {
          daysName: '  :  ',
          hoursName: '  :  ',
          minutesName: '   :  ',
          secondsName: '',
          date: tournData['date'],
          // tagNumber: 'b',
          tagLetter: 'span',
          // secondsName: ' : ',
          template: '<span>NUMBER  </span><small>LETTER</small> ',
        });
      }else if (secDay.setDate(secDay.getDate() +1) < currDate) {
        $('.j-timer-second').addClass('hide');
        $('#text-date').addClass('hide');
        var timeOut = $('#time').addClass('hide');
      }else {
        $('.j-timer-second').addClass('hide');
        $('#text-date').html('Boshlanish va tugash vaqti');
        var timeOut = $('#time').addClass('show').removeClass('hide');
        timeOut.html('<h4>'+tournData['timeUp']+'  /  '+tournData['timeDown']+'</h4>');
      }  
    }
  });
});




