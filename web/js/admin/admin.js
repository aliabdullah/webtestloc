 $(function () {
    //Enable iCheck plugin for checkboxes
    //iCheck for checkbox and radio inputs
    $('.mailbox-messages input[type="checkbox"]').iCheck({
      checkboxClass: 'icheckbox_flat-blue',
      radioClass: 'iradio_flat-blue'
    });

    //Enable check and uncheck all functionality
    $(".checkbox-toggle").click(function () {
      var clicks = $(this).data('clicks');
      if (clicks) {
        //Uncheck all checkboxes
        $(".mailbox-messages input[type='checkbox']").iCheck("uncheck");
        $(".fa", this).removeClass("fa-check-square-o").addClass('fa-square-o');
      } else {
        //Check all checkboxes
        $(".mailbox-messages input[type='checkbox']").iCheck("check");
        $(".fa", this).removeClass("fa-square-o").addClass('fa-check-square-o');
      }
      $(this).data("clicks", !clicks);
    });

    //Handle starring for glyphicon and font awesome
    $(".mailbox-star").click(function (e) {
      e.preventDefault();
      //detect type
      var $this = $(this).find("a > i");
      var glyph = $this.hasClass("glyphicon");
      var fa = $this.hasClass("fa");

      //Switch states
      if (glyph) {
        $this.toggleClass("glyphicon-star");
        $this.toggleClass("glyphicon-star-empty");
      }

      if (fa) {
        $this.toggleClass("fa-star");
        $this.toggleClass("fa-star-o");
      }
    });
  });


function viewDiv(){
  document.getElementById("div1").style.display = "block";
};
//  function showModal(subcat){
// 	 $('#CatsModal .modal-body').html(subcat);
//      $('#CatsModal').modal();
//      // $('.modal-body').addClass('.text-center');
// }

// $('.service__link').on('click', function (e) {
//         e.preventDefault();
//         var id = $(this).data('id');
//         $.ajax({
//             url: '/admin/testblog/sub',
//             data: {id: id},
//             type: 'GET',
//                 showModal(res);
//             success: function(res){
//                 // console.log(res);
//             },
//             error: function(){
//                 alert('Error!');
//             }
//         });
//     });


$('.editor').click(function(){
  var id = $(this).data('id');
  var small = $('small#date'+id).length;
  
  $('.inpu').remove();
  
  console.log(small);
  if ($('small#date'+id).length == 1) {
    // $('small#date'+id).addClass('hide');
    $('#edit_date'+id).append('<input type="date" name="new_date" class="inpu">');  
  }else {
    console.log('hello');
    $('small#date'+id).removeClass('hide');
    // $('#li-'+id).append('<small class="label label-danger" id="date<?=$tourn->id?>" style="font-size: 11px;"><?= $tourn->day?></small>');
  }
  
});