$(document).ready(function(){

  answers = [];

  $('.slider').owlCarousel({
    loop:true,
    margin:10,
    nav:false,
    dots: true,
    autoplay:true,
    autoplayTimeout: 4000,
    autoplaySpeed: 600,
    responsive:{
        0:{
            items:1
        }
    }
  })


  $('.user').click(function(){
    $('.user__dropdown').toggle();
  })

    
  var headerHeight = $('.header').outerHeight();


 
    $(window).scroll(function(){

      if($(window).scrollTop() > headerHeight) {
        $('.navigation').addClass('navigation_fixed');
      } else {
        $('.navigation').removeClass('navigation_fixed');
      }

    })
  
    $(document).mouseup(function (e) {
    var container = $(".navigation");
    if (container.has(e.target).length === 0){
        if($('.navigation').hasClass('navigation__opened')) {
          $('.navigation__opened').removeClass('navigation__opened');
          $('.body_noscroll').removeClass('body_noscroll');
          $('.main_menu-opened').removeClass('main_menu-opened');
        }
    }

  $('.rightBox__mobMenu').click(function(){
    $('.navigation').toggleClass('navigation__opened');
    $('body').toggleClass('body_noscroll');
    $('.main').toggleClass('main_menu-opened');
  })

});

})

/////////////////////////////////////////////

// For Premium-filter page

$(document).ready(function($) {
    var myUrl = window.location.pathname.split('/');
    var select;
    var lang_title;
    var level;
    if (myUrl[1] == 'ru') {
     select = 'выберите предмет...';
     lang_title = 'Язык';
     level = 'Сложность';
     }else { 
      select = 'elementni tanlang...';
      lang_title = 'Tili';
      level = 'Qiyinligi';
    }
      $('.predmet-1').selectizing({
        background: '#fff',
        color: "#737373",
        title: select,
        change: function(){
          var value = $(this).val();
              var text = $(this).find('option:selected').html();
          $('#event-change').html(value + ' : ' + text);
        }
      });

      $('.predmet-2').selectizing({
        background: '#fff',
        color: "#737373",
        title: select,
        change: function(){
          var value = $(this).val();
              var text = $(this).find('option:selected').html();
          $('#event-change').html(value + ' : ' + text);
        }
      });

      $('.predmet-3').selectizing({
        background: '#fff',
        color: "#737373",
        title: select,
        change: function(){
          var value = $(this).val();
              var text = $(this).find('option:selected').html();
          $('#event-change').html(value + ' : ' + text);
        }
      });

      $('.fakultet').selectizing({
        background: '#fff',
        color: "#737373",
        title: lang_title,
        change: function(){
          var value = $(this).val();
              var text = $(this).find('option:selected').html();
          $('#event-change').html(value + ' : ' + text);
        }
      });

      // $('.Vuz').selectizing({
      //   background: '#fff',
      //   color: "#737373",
      //   title: 'Вуз',
      //   change: function(){
      //     var value = $(this).val();
      //         var text = $(this).find('option:selected').html();
      //     $('#event-change').html(value + ' : ' + text);
      //   }
      // });

      $('.level').selectizing({
        background: '#fff',
        color: "#737373",
        title: level,
        change: function(){
          var value = $(this).val();
              var text = $(this).find('option:selected').html();
          $('#event-change').html(value + ' : ' + text);
        }
      });
    });

function premiumTest(){
  var level = $('.level').val();
      lang = $('.lang').val();
      predmet1 = $('.predmet-1').val();
      predmet2 = $('.predmet-2').val();
      predmet3 = $('.predmet-3').val();
      var myUrl = window.location.pathname.split('/');
      if (myUrl[1] == 'uz') {
        $.ajax({
            url: '/uz/test/premium-test',
            data: {
                    level: level,
                    lang: lang,
                    predmet1: predmet1,
                    predmet2: predmet2,
                    predmet3: predmet3,
                  },
            type: 'POST',
            
        });  
      }else {
        $.ajax({
            url: '/ru/test/premium-test',
            data: {
                    level: level,
                    lang: lang,
                    predmet1: predmet1,
                    predmet2: predmet2,
                    predmet3: predmet3,
                  },
            type: 'POST',
            
        });
      }
      $.ajax({
            url: '/test/premium-test',
            data: {
                    level: level,
                    lang: lang,
                    predmet1: predmet1,
                    predmet2: predmet2,
                    predmet3: predmet3,
                  },
            type: 'POST',
            
        });
  // console.log(predmet1, predmet2, predmet3);
}

/////////////////////////////////////////////////

function nextPredmet(){
  if ($('.predmet__1').hasClass('predmet__opened')) {
    // animate and scroll to the sectin 
    $('body, html').animate({
      
      // the magic - scroll to section
      "scrollTop": $('.predmet__2').position().top
    }, 200 );
    $('.predmet__opened').removeClass('predmet__opened').addClass('predmet__close');
    $('.pred__1').removeClass('active__predmet').addClass('desactive__predmet');
    $('.predmet__2').removeClass('predmet__close').addClass('predmet__opened');
    $('.pred__2').removeClass('desactive__predmet').addClass('active__predmet');
    
    // return false;
    
  }else {
    // animate and scroll to the sectin 
    $('body, html').animate({
      
      // the magic - scroll to section
      "scrollTop": $('.predmet__3').position().top
    }, 200 );
    $('.predmet__2').removeClass('predmet__opened').addClass('predmet__close');
    $('.predmet__3').removeClass('predmet__close').addClass('predmet__opened');
    $('.pred__2').removeClass('active__predmet').addClass('desactive__predmet');
    $('.pred__3').removeClass('desactive__predmet').addClass('active__predmet');
    
    // return false;
  }
  
}

/////////////////////////////////////////////////

$('#tarrif').on('click', function(e){
      e.preventDefault();
      $('.tarrif').removeClass('hide');
      $('.resurs').addClass('hide');
      $('.rating').addClass('hide');
      $('.test').addClass('hide');
});

$('#test').on('click', function(e){
      e.preventDefault();
      $('.test').removeClass('hide');
      $('.resurs').addClass('hide');
      $('.rating').addClass('hide');
      $('.tarrif').addClass('hide');
});

$('#rating').on('click', function(e){
      e.preventDefault();
      $('.rating').removeClass('hide');
      $('.resurs').addClass('hide');
      $('.test').addClass('hide');
      $('.tarrif').addClass('hide');

      $.ajax({
          type: 'POST',
          url: '/ru/results/stats',
          dataType : "json",
          
          success: function(data) {
            // total result Januar month
            var corrAddWro = parseInt(data[0].sum_correct) + parseInt(data[0].sum_wrong);
            var JanTotalRes = parseInt(data[0].sum_correct) * 100 / corrAddWro;

            console.log(data);
            var d;
            if (data[0].month == '2018 01') { Januar = data[0]; February = data[1]; March = data[2]; April = data[3]; May = data[4]; June = data[5]; Jule = data[6]; August = data[7]; September = data[8]; October = data[9]; November = data[10]; December = data[11];
              // console.log(February.month);
            }else if (data[0].month == '2018 02') { Januar = data[11]; February = data[0]; March = data[1]; April = data[2]; May = data[3]; June = data[4]; Jule = data[5]; August = data[6]; September = data[7]; October = data[8]; November = data[9]; December = data[10];
            }else if (data[0].month == '2018 03') { Januar = data[10]; February = data[11]; March = data[0]; April = data[1]; May = data[2]; June = data[3]; Jule = data[4]; August = data[5]; September = data[6]; October = data[7]; November = data[8]; December = data[9];
            }else if (data[0].month == '2018 04') { Januar = data[9]; February = data[10]; March = data[11]; April = data[0]; May = data[1]; June = data[2]; Jule = data[3]; August = data[4]; September = data[5]; October = data[6]; November = data[7]; December = data[8];
            }else if (data[0].month == '2018 05') { Januar = data[8]; February = data[9]; March = data[10]; April = data[11]; May = data[0]; June = data[1]; Jule = data[2]; August = data[3]; September = data[4]; October = data[5]; November = data[6]; December = data[7];
            }else if (data[0].month == '2018 06') { Januar = data[7]; February = data[8]; March = data[9]; April = data[10]; May = data[11]; June = data[0]; Jule = data[1]; August = data[2]; September = data[3]; October = data[4]; November = data[5]; December = data[6];
            }else if (data[0].month == '2018 07') { Januar = data[6]; February = data[7]; March = data[8]; April = data[9]; May = data[10]; June = data[11]; Jule = data[0]; August = data[1]; September = data[2]; October = data[3]; November = data[4]; December = data[5];
            }else if (data[0].month == '2018 08') { Januar = data[5]; February = data[6]; March = data[7]; April = data[8]; May = data[9]; June = data[10]; Jule = data[11]; August = data[0]; September = data[1]; October = data[2]; November = data[3]; December = data[4];
            }else if (data[0].month == '2018 09') { Januar = data[4]; February = data[5]; March = data[6]; April = data[7]; May = data[8]; June = data[9]; Jule = data[10]; August = data[11]; September = data[0]; October = data[1]; November = data[2]; December = data[3];
            }else if (data[0].month == '2018 10') { Januar = data[3]; February = data[4]; March = data[5]; April = data[6]; May = data[7]; June = data[8]; Jule = data[9]; August = data[10]; September = data[11]; October = data[0]; November = data[1]; December = data[2];
            }else if (data[0].month == '2018 11') { Januar = data[2]; February = data[3]; March = data[4]; April = data[5]; May = data[6]; June = data[7]; Jule = data[8]; August = data[9]; September = data[10]; October = data[11]; November = data[0]; December = data[1];
            }else if (data[0].month == '2018 12') { Januar = data[1]; February = data[2]; March = data[3]; April = data[4]; May = data[5]; June = data[6]; Jule = data[7]; August = data[8]; September = data[9]; October = data[10]; November = data[11]; December = data[0];
            }else {console.log('it is method false')}


            // Januar results
            var jan_total;
            if (Januar) {
              jan_corr = ((parseInt(Januar.sum_correct) * 100) / (parseInt(Januar.sum_correct) + parseInt(Januar.sum_wrong)));
              jan_wro =  100 - jan_corr;
              jan_inter = parseInt(Januar.sum_interval) / 60;

              jan_total = ((jan_corr * 100) / (jan_corr + jan_wro)) - jan_inter;

            } else {
              jan_corr = 0;
              jan_wro = 0;
              jan_inter = 0;
              jan_total = 0;
           }
           ////

           // February results
            var feb_total;
            if (February) {
              feb_corr = ((parseInt(February.sum_correct) * 100) / (parseInt(February.sum_correct) + parseInt(February.sum_wrong)));
              feb_wro =  100 - feb_corr;
              feb_inter = parseInt(February.sum_interval) / 60;

              feb_total = ((feb_corr * 100) / (feb_corr + feb_wro)) - feb_inter;

            } else {
              feb_corr = 0;
              feb_wro = 0;
              feb_inter = 0;
              feb_total = 0;
           }
           ////

           // March results
           var march_total;
            if (March) {
              march_corr = ((parseInt(March.sum_correct) * 100) / (parseInt(March.sum_correct) + parseInt(March.sum_wrong)));
              march_wro =  100 - march_corr;
              march_inter = parseInt(March.sum_interval) / 60;

              march_total = ((march_corr * 100) / (march_corr + march_wro)) - march_inter;

            } else {
              march_corr = 0;
              march_wro = 0;
              march_inter = 0;
              march_total = 0;
           }
           ////

           // April results
           var april_total;
            if (April) {
              april_corr = ((parseInt(April.sum_correct) * 100) / (parseInt(April.sum_correct) + parseInt(April.sum_wrong)));
              april_wro =  100 - april_corr;
              april_inter = parseInt(April.sum_interval) / 60;

              april_total = ((april_corr * 100) / (april_corr + april_wro)) - april_inter;

            } else {
              april_corr = 0;
              april_wro = 0;
              april_inter = 0;
              april_total = 0;
           }
           ////

           // May results
           var may;
            if (May) {
              may_corr = ((parseInt(May.sum_correct) * 100) / (parseInt(May.sum_correct) + parseInt(May.sum_wrong)));
              may_wro =  100 - may_corr;
              may_inter = parseInt(May.sum_interval) / 60;

              may_total = ((may_corr * 100) / (may_corr + may_wro)) - may_inter;

            } else {
              may_corr = 0;
              may_wro = 0;
              may_inter = 0;
              may_total = 0;
           }
           ////

           // June results
           var jun;
            if (June) {
              jun_corr = ((parseInt(June.sum_correct) * 100) / (parseInt(June.sum_correct) + parseInt(June.sum_wrong)));
              jun_wro =  100 - jun_corr;
              jun_inter = parseInt(June.sum_interval) / 60;

              jun_total = ((jun_corr * 100) / (jun_corr + jun_wro)) - jun_inter;

            } else {
              jun_corr = 0;
              jun_wro = 0;
              jun_inter = 0;
              jun_total = 0;

           }
           ////

           // Jule results
           var jul;
            if (Jule) {
              jul_corr = ((parseInt(Jule.sum_correct) * 100) / (parseInt(Jule.sum_correct) + parseInt(Jule.sum_wrong)));
              jul_wro =  100 - jul_corr;
              jul_inter = parseInt(Jule.sum_interval) / 60;

              jul_total = ((jul_corr * 100) / (jul_corr + jul_wro)) - jul_inter;

            } else {
              jul_corr = 0;
              jul_wro = 0;
              jul_inter = 0;
              jul_total = 0;
           }
           ////

           // August results
           var aug;
            if (August) {
              aug_corr = ((parseInt(August.sum_correct) * 100) / (parseInt(August.sum_correct) + parseInt(August.sum_wrong)));
              aug_wro =  100 - aug_corr;
              aug_inter = parseInt(August.sum_interval) / 60;

              aug_total = ((aug_corr * 100) / (aug_corr + aug_wro)) - aug_inter;

            } else {
              aug_corr = 0;
              aug_wro = 0;
              aug_inter = 0;
              aug_total = 0;
           }
           ////

           // September results
           var september;
            if (September) {
              september_corr = ((parseInt(September.sum_correct) * 100) / (parseInt(September.sum_correct) + parseInt(September.sum_wrong)));
              september_wro =  100 - september_corr;
              september_inter = parseInt(September.sum_interval) / 60;

              september_total = ((september_corr * 100) / (september_corr + september_wro)) - september_inter;

            } else {
              september_corr = 0;
              september_wro = 0;
              september_inter = 0;
              september_total = 0;
           }
           ////

           // October results
           var oct;
            if (October) {
              oct_corr = ((parseInt(October.sum_correct) * 100) / (parseInt(October.sum_correct) + parseInt(October.sum_wrong)));
              oct_wro =  100 - oct_corr;
              oct_inter = parseInt(October.sum_interval) / 60;

              oct_total = ((oct_corr * 100) / (oct_corr + oct_wro)) - oct_inter;

            } else {
              oct_corr = 0;
              oct_wro = 0;
              oct_inter = 0;
              oct_total = 0;
           }
           ////

          // November results
           var nov;
            if (November) {
              nov_corr = ((parseInt(November.sum_correct) * 100) / (parseInt(November.sum_correct) + parseInt(November.sum_wrong)));
              nov_wro =  100 - nov_corr;
              nov_inter = parseInt(November.sum_interval) / 60;

              nov_total = ((nov_corr * 100) / (nov_corr + nov_wro)) - nov_inter;

            } else {
              nov_corr = 0;
              nov_wro = 0;
              nov_inter = 0;
              nov_total = 0;
           }
           ////

           // December results
           var december;
            if (December) {
              december_corr = ((parseInt(December.sum_correct) * 100) / (parseInt(December.sum_correct) + parseInt(December.sum_wrong)));
              december_wro =  100 - december_corr;
              december_inter = parseInt(December.sum_interval) / 60;

              december_total = ((december_corr * 100) / (december_corr + december_wro)) - december_inter;

            } else {
              december_corr = 0;
              december_wro = 0;
              december_inter = 0;
              december_total = 0;
           }
           ////

            // start chart
            var chart = new Highcharts.chart('userChart', {
                  title: {
                      text: 'Годовой рейтинг'
                  },
                  xAxis: {
                      categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                          'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                      crosshair: true,
                  },

                  yAxis: [{ // Primary yAxis
                      labels: {
                          format: '{value}%',
                          tickInterval: 5,
                          style: {
                              color: Highcharts.getOptions().colors[1]
                          }
                        
                      },

                      minorTickInterval: 'auto',
                      minorTickLength: 10,
                      minorTickWidth: 1,
                      tickInterval: 10,

                      title: {
                          text: 'Процент Результата',
                          style: {
                              color: Highcharts.getOptions().colors[1]
                          }
                      }
                  }, 

                  { // Secondary yAxis
                      title: {
                          text: 'Rainfall',
                          style: {
                              color: Highcharts.getOptions().colors[0]
                          }
                      },
                      labels: {
                          format: '{value} %',
                          style: {
                              color: Highcharts.getOptions().colors[0]
                          }
                      },
                      opposite: true
                  }],


                  // labels: {
                  //     items: [{
                  //         html: 'Total fruit consumption',
                  //         style: {
                  //             left: '50px',
                  //             top: '50px',
                  //             color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
                  //         }
                  //     }]
                  // },

                  series: [{
                      type: 'column',
                      name: 'Правильный',
                      data: [parseInt(jan_corr),
                              parseInt(feb_corr),
                              parseInt(march_corr),
                              parseInt(april_corr),
                              parseInt(may_corr),
                              parseInt(jun_corr),
                              parseInt(jul_corr),
                              parseInt(aug_corr),
                              parseInt(september_corr),
                              parseInt(oct_corr),
                              parseInt(nov_corr),
                              parseInt(december_corr)],
                      color: Highcharts.getOptions().colors[7],

                  }, {
                      type: 'column',
                      name: 'Не правильный',
                      data: [parseInt(jan_wro),
                              parseInt(feb_wro), 
                              parseInt(march_wro),
                              parseInt(april_wro),
                              parseInt(may_wro),
                              parseInt(jun_wro),
                              parseInt(jul_wro),
                              parseInt(aug_wro),
                              parseInt(september_wro),
                              parseInt(oct_wro),
                              parseInt(nov_wro),
                              parseInt(december_wro)],
                      color: Highcharts.getOptions().colors[8],
                  }, {
                      type: 'column',
                      name: 'Скорость',
                      data: [jan_inter,
                              feb_inter,
                              march_inter,
                              april_inter,
                              may_inter,
                              jun_inter,
                              jul_inter,
                              aug_inter,
                              september_inter,
                              oct_inter,
                              nov_inter,
                              december_inter],
                      color: Highcharts.getOptions().colors[9],
                  }, {
                      type: 'spline',
                      name: 'Растижения',
                      data: [parseInt(jan_total),
                              parseInt(feb_total),
                              parseInt(march_total),
                              parseInt(april_total),
                              parseInt(may_total),
                              parseInt(jun_total),
                              parseInt(jul_total),
                              parseInt(aug_total),
                              parseInt(september_total),
                              parseInt(oct_total),
                              parseInt(nov_total),
                              parseInt(december_total)],
                      marker: {
                          lineWidth: 2,
                          lineColor: Highcharts.getOptions().colors[3],
                          fillColor: 'white'
                      }
                  }, {
                      type: 'pie',
                      name: 'Total consumption',
                      data: [{
                          name: 'Jane',
                          y: 13,
                          color: Highcharts.getOptions().colors[7] // Jane's color
                      }, {
                          name: 'John',
                          y: 23,
                          color: Highcharts.getOptions().colors[8] // John's color
                      }, {
                          name: 'Joe',
                          y: 19,
                          color: Highcharts.getOptions().colors[9] // Joe's color
                      }],
                      center: [100, 80],
                      size: 100,
                      showInLegend: false,
                      dataLabels: {
                          enabled: false
                      }
                  }]
              });

              console.log(data);
              // $('.div').html(data);
          }
      });

});

$('#resurs').on('click', function(e){
      e.preventDefault();
      $('.resurs').removeClass('hide');
      $('.test').addClass('hide');
      $('.rating').addClass('hide');
      $('.tarrif').addClass('hide');
});


/////////////////////////////////////////////////

function showModal(form){
   $('#testModal .modal-body').html(form);
     $('#testModal').modal();
}



$('#tmodal').on('click', function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        $.ajax({
            url: '/ru/test/modal',
            data: {id: id},
            type: 'POST',
            success: function(res){
                showModal(res);
                // console.log(res);
            },
            error: function(data){
              console.log(data);
                alert('Error!');
            }
        });
    });

//////////////////////////////////////

$(document).ready(function() {
  $('textarea#comment').click(function() {
    $('.form-hidden').show();
  });

  $(document).click(function(event) {
    var $target = $(event.target);
    if (!$target.parents('.form').length) {
      $('.form-hidden').hide();
    }
  });

});



/////////////////////////////////////////


$('.variant').on('click', function (e) {
        e.preventDefault();
        var pred_name = $(this).data('pred_name');
        var quest_id = $(this).data('quest_id');
        var var_id = $(this).data('var-id');

        // when select variant counter will be change style
        var id = '#'+quest_id;
        countNum = $(id).removeClass('count-li').addClass('checked');

        // to block panel
        var testToBlock = $(this).closest('.list-group-item');
        $(testToBlock).css("background", "#ccc");
        var panelClass = $(this).closest('.panel');
        console.log(countNum);
            $(panelClass).block({ 
                message: '<h2>Принято!</h2>',
                css: { border: '0px solid #a00',
                        backgroundColor: 'none',
                        // opacity:  0.6,
                        color:    '#fff',
                      } 
            });
            
        // update blocked panel after block
        var updateButton = $(this).closest('.update');
        $(updateButton).css("display", "inherit");

        $.ajax({
            url: '/ru/results/add-to-results',
            data: {
                    var_id: var_id,
                    quest_id: quest_id,
                    pred_name: pred_name
                  },
            type: 'GET',
            success: function(res){
                // showModal(res);
                console.log(res);
            },
            error: function(data){
              console.log(data);
                alert('Error!');
            }
        });
    });

/////////////////////////////////////////////

$('.one').click(function() {
    $('.two').slideToggle();    
});

/////////////////////////////////////////////

$('.count').on('click', function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        $('.panel').scroll(id);
        // console.log(id);

});


///////////////////////////////////////////
// on click event on all anchors with a class of scrollTo
$('a.scrollTo').on('click', function(){
  
  // data-scrollTo = section scrolling to name
  var scrollTo = $(this).attr('data-scrollTo');
  
  $( "a.scrollTo" ).each(function() {
    if(scrollTo == $(this).attr('data-scrollTo')){
      $(this).addClass('active');
    }else{
      $(this).removeClass('active');
    }
  });
  
  
  // animate and scroll to the sectin 
  $('body, html').animate({
    
    // the magic - scroll to section
    "scrollTop": $('#'+scrollTo).position().top
  }, 1000 );
  return false;
  
})

////////////////////////////////////

// $()

////////////////////////////////////

function showModalRes(form){
   $('#resultModal .modal-body').html(form);
     $('#resultModal').modal();
}

function resSave(){
    $.ajax({
            url: '/ru/results/save',
            type: 'POST',
            success: function(res){
                showModalRes(res);
                // console.log(res);
            },
            error: function(data){
              console.log(data);
                alert('Error!');
            }
        });
}

function resDelete(){
    $.ajax({
            url: '/ru/results/delete',
            type: 'POST',
            success: function(res){
                showModalRes(res);
                // console.log(res);
            },
            error: function(data){
              console.log(data);
                alert('Error!');
            }
        });
}

$(document).ready(function() {
  $(".GaugeMeter").gaugeMeter();
});


// $('#resModal').on('click', function (e) {
//         e.preventDefault();
//         var myUrl = window.location.pathname.split('/');
//         // var id = $(this).data('id');
//         console.log(myUrl);
//         if (myUrl == 'ru') {
//           $.ajax({
//             url: '/ru/results/result',
//             // data: {id: id},
//             type: 'POST',
//             success: function(res){
//                 showModalRes(res);
//                 // console.log(res);
//             },
//             error: function(data){
//               console.log(data);
//                 alert('Error!');
//             }
//         });
//         }else {
//           $.ajax({
//             url: '/uz/results/result',
//             // data: {id: id},
//             type: 'POST',
//             success: function(res){
//                 showModalRes(res);
//                 // console.log(res);
//             },
//             error: function(data){
//               console.log(data);
//                 alert('Error!');
//             }
//         });
//       }
//     });

//////////////////////////////////////

$('.arrow').click(function(){
  var test = $(this).closest('.panel').next('.panel');

  $('html, body').animate({
      scrollTop: test.position().top
  });

});

$(function(){
  //scroll to top
  $('.button-up').click(function(){
    $('html, body').animate({ scrollTop: 0 }, 1000);
    return false;
  });
});

// unblock panel
$('.updatebtn').click(function(e){
  e.preventDefault();
  var panel = $(this).closest('.panel');
  var questId = $(this).attr('data-id');
  
  $.ajax({
    url: '/ru/test/remove-variant',
    data: {questId: questId},
    type: 'POST',
    success: function(id){
    },
    error: function(err){
      console.log(err);
    }
  })

  $(panel).unblock();
  $(panel).find('.list-group-item').css("background", "#fff");

  // when unblock panel counter will be change to before style
  var id = '#'+questId;
  countNum = $(id).removeClass('checked').addClass('count-li');

});

$( ".item" ).hover(
  function() {
    $('.about').removeClass( "hide" );
  }, function() {
    $( ".about" ).addClass( "hide" );
  }
);



///////////////////////////

/// For materials page

$(document).ready(function($) {
  $("#menu").accordion();
  $(".colors a").click(function() {
    if ($(this).attr("class") != "default") {
      $("#menu").removeClass();
      $("#menu").addClass("menu").addClass($(this).attr("class"));
    } else {
      $("#menu").removeClass();
      $("#menu").addClass("menu");
    }
  });
}); 

////////////////////////


//// Humburger script 

(function() {
    'use strict';
    $('.hamburger-menu').click(function (e) {
        e.preventDefault();
        if ($(this).hasClass('active')){
            $(this).removeClass('active');
            $('.menu-overlay').fadeToggle( 'fast', 'linear' );
            $('.menu .menu-list').slideToggle( 'slow', 'swing' );
            $('.hamburger-menu-wrapper').toggleClass('bounce-effect');
        } else {
            $(this).addClass('active');
            $('.menu-overlay').fadeToggle( 'fast', 'linear' );
            $('.menu .menu-list').slideToggle( 'slow', 'swing' );
            $('.hamburger-menu-wrapper').toggleClass('bounce-effect');
        }
    })
})();

$('#student1').click(function(){
  $('.hamburger-menu').click();
  $('#student2-pane, #student3-pane').hide();
  $('#student1-pane').show();
});

$('#student2').click(function(){
  $('.hamburger-menu').click();
  $('#student1-pane, #student3-pane').hide();
  $('#student2-pane').show();
});

$('#student3').click(function(){
  $('.hamburger-menu').click();
  $('#student2-pane, #student1-pane').hide();
  $('#student3-pane').show();
});

/////////////////////////////////

/////  Print Page script
$(function() {
  $("#print__area").find('#toprint').on('click', function() {
  $.print("#print__area");
  });
});

///// end Print script


//////////////////////////////////


// FB.getLoginStatus(function(response) {
//     statusChangeCallback(response);
// });


/////////////////////////////////

$('#like').on('click', function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        $('#like i').addClass('like-color');
        $.ajax({
            url: '/ru/materials/add-link',
            data: {id: id},
            type: 'POST',
            success: function(res){
                // showModalRes(res);
                console.log(res);
            },
            error: function(data){
              console.log(data);
                alert('Error!');
            }
        });
    });


/////////////////////////////////

function showPayModal(form){
   $('#payModal .modal-body').html(form);
     $('#payModal').modal();
}

$('#paymodal').on('click', function (e) {
    e.preventDefault();
    var tariff_id = $(this).data('tariff_id');
    var myUrl = window.location.pathname.split('/');
    console.log(myUrl)
    if(myUrl[1] == 'uz'){
      $.ajax({
            url: '/uz/tariffs/modal',
            data: {tariff_id: tariff_id},
            type: 'GET',
            success: function(res){
                showPayModal(res);
                // console.log(res);
            },
            error: function(data){
              console.log(data);
                alert('Error!');
            }
        }) } else {
        $.ajax({
            url: '/ru/tariffs/modal',
            data: {tariff_id: tariff_id},
            type: 'GET',
            success: function(res){
                showPayModal(res);
                // console.log(res);
            },
            error: function(data){
              console.log(data);
                alert('Error!');
            }
        });
      }
        
    });

  document.querySelector("html").classList.add('js');

var fileInput  = document.querySelector( ".input-file" ),  
    button     = document.querySelector( ".input-file-trigger" ),
    the_return = document.querySelector(".file-return");
      
// button.addEventListener( "keydown", function( event ) {  
//     if ( event.keyCode == 13 || event.keyCode == 32 ) {  
//         fileInput.focus();  
//     }  
// });
// button.addEventListener( "click", function( event ) {
//    fileInput.focus();
//    return false;
// });  
// fileInput.addEventListener( "change", function( event ) {  
//     the_return.innerHTML = this.value;  
// });

$('#complaint').on('click', function(e){
  e.preventDefault();
  $('#complaint_area').addClass('show_area').removeClass('hide');
});

// $('#task_message').on('click', function(e){
//   e.preventDefault();
//   $('preview_message').addClass('show_preview').removeClass('preview_message');
// });

// $(window).load(function(){
//   $('.alertbox').click(function(){
//     alert('You Clicked on Click Here Button');
//     });
//   })