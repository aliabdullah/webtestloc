<?php

namespace app\components\helpers;

use Yii;

class TestParser
{
    public $result = [];

    public $images = [];

    public function load($path)
    {
        $xml = new \DOMDocument();
        $xml->load($path);
        $images = $xml->getElementsByTagName('binData');

        foreach ($images as $image) {
            $key = $image->getAttribute('w:name');
            $this->images[$key] = 'data:image/png;base64, '.$image->nodeValue;
            // echo '<pre>';
            // print_r($this->images[$key]);
            // echo '</pre>';
            // exit();
        }

        $tables = $xml->getElementsByTagName('tbl');
        foreach ($tables as $table) {
            $question = [
                'number' => '',
                'question' => '',
                'answers' => []
            ];
            foreach ($table->getElementsByTagName('tr') as $rowIndex => $rows) {
                $answer = [];
                foreach ($rows->getElementsByTagName('tc') as $cellIndex => $cell) {
                  
                     if ($rowIndex == 0 && $cellIndex == 0) {
                            $question['number'] = $this->getValue($cell);
                        } elseif ($rowIndex == 0 && $cellIndex == 1) {
                            $question['question'] = $this->getValue($cell);
                        } else {
                            if ($cellIndex == 0) {
                                $answer['key'] = $this->getValue($cell);
                                
                                if (strpos($answer['key'] , '*')) {
                                    $answer['correct'] = true;
                                } else {
                                    $answer['correct'] = false;
                                }
                            } else {
                                $answer['text'] = $this->getValue($cell);
                            }
                        }
                }
                if ($rowIndex != 0) {
                    $question['answers'][] = $answer;
                }
            }
            $this->result[] = $question;
        }
    }

    protected function getValue($item) {
        $text = '';
        static $image = 0;
        static $beforeImg = '';
        
        if ($item->childNodes && $item->childNodes->length > 0) {
            foreach ($item->childNodes as $node) {
                $text .= $this->getValue($node);
            }
        }
        
        if ($item->nodeName == 'v:imagedata') {
            if ($beforeImg != $this->images[$item->getAttribute('src')]) {
                $text .= '<img src="'.$this->images[$item->getAttribute('src')].'">';
            }
            $beforeImg = $this->images[$item->getAttribute('src')];

        } elseif ($item->nodeName == 'w:t') {
            $text .= $item->nodeValue;
        }
        return $text;
    }
};


