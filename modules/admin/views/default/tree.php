<?php

use app\models\Tree;
use kartik\tree\TreeView;

$this->title = 'Kategoriyalar';
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- Default box -->
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title"> <?=$this->title?> </h3>

        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fa fa-times"></i></button>
        </div>
    </div>

    <div class="box-body">
        <?=TreeView::widget([
            'query'             => Tree::find()->addOrderBy('root, lft'),
            'headingOptions'    => ['label' => 'Categories'],
            'isAdmin'           => false,                       // optional (toggle to enable admin mode)
            'displayValue'      => 1,                           // initial display value
        ])?>
    </div>
</div>
<!-- /.box -->