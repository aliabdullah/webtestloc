<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\category\Category;
/* @var $this yii\web\View */
/* @var $searchModel app\models\testblog\TestBlogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Test Blogs');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="test-blog-index" style="padding-left: 10px; padding-right: 10px;">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Test Blog'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'cat_id',
                'filter' => Html::activeDropDownList(
                      $searchModel,
                      'cat_id',
                      ArrayHelper::map(Category::find()->all(), 'id', 'name'),
                      ['class' => 'form-control']
                 ),
                 'value' => 'cat.name',
            ],
            'name',
            'description:ntext',
            'type',
            // 'date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
