<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\testblog\TestBlog */

$this->title = Yii::t('app', 'Create Test Blog');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Test Blogs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="test-blog-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
