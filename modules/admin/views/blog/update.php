<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\testblog\TestBlog */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Test Blog',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Test Blogs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="test-blog-update" style="padding-left: 10px; padding-right: 10px;">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
