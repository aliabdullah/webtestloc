<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\db_info\Db_info */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Базовая информация'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box container" style="padding-left: 30px; padding-right: 30px;">
    <div class="db-info-view">

        <h1><?= Html::encode($this->title) ?></h1>

        <p>
            <?= Html::a(Yii::t('app', 'Обновить'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) ?>
        </p>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                // 'category_id',
                'parent_id',
                'name',
                'keywords',
                'description:html',
                'photo',
                'type',
                'lang',
            ],
        ]) ?>

    </div>
</div>