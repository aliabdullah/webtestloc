<?php

use yii\helpers\Html;
use app\modles\Tree;
use yii\widgets\ActiveForm;
use zxbodya\yii2\elfinder\TinyMceElFinder;
use zxbodya\yii2\tinymce\TinyMce;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use app\models\db_info\Db_info;
/* @var $this yii\web\View */
/* @var $model app\models\db_info\Db_info */
/* @var $form yii\widgets\ActiveForm */
?>


	<div class="db-info-form">

	    <?php $form = ActiveForm::begin([
		    'id' => 'login-form',
		    'options' => [['class' => 'row'],['enctype' => 'multipart/form-data']],
		]); ?>

		<div class="col-sm-6">
		    <!-- <div class="form-group field-category-parent_id has-success">
		        <label class="control-label for-category_id">Raditelskaya kategoria</label>
		        <select id="category-parent_id" class="form-control" name="db_info[category_id]">
		            <?= \app\components\MenuWidget::widget(['tpl' => 'select']); ?>
		        </select>
		    </div> -->

		    <?php

		    	$category = app\models\category\Category::find()->all();

		    	$data = ArrayHelper::map($category, 'id', 'name');


		    	echo $form->field($model, 'parent_id')->widget(Select2::classname(), [
				    'name' => 'kv_theme_select2',
				    'data' => $data,
				    'theme' => Select2::THEME_BOOTSTRAP,
				    'options' => ['placeholder' => 'Select a state ...'],
				    'pluginOptions' => [
				        'allowClear' => true
				    ],
				]);
		    	
			    // echo $form->field($model, 'category_id')->dropDownList($data) 
			    ?>

	    </div>
	   
	    <div class="col-sm-6">
	    	<?= $form->field($model, 'type')->dropDownList(app\models\db_info\Db_info::getTypes()) ?>
		</div>

	    <div class="col-sm-6">
	    	<?= $form->field($model, 'keywords')->textInput(['maxlength' => true]) ?>
		</div>

		<div class="col-sm-6">
	    	<?= $form->field($model, 'lang')->dropDownList(Db_info::getLang()) ?>
		</div>

		<div class="col-sm-12">
	    	<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
		</div>
		
		<div class="col-md-2" style="float: left;">
			<?= $form->field($model, 'photo')->fileInput()?>
		</div>

		<div class="col-sm-12">
		    <?= $form->field($model, 'description')->widget(
		                TinyMce::className(), [
		                    'settings' => [   
		                        'plugins' => ["advlist autolink lists link image charmap print preview hr anchor pagebreak ",
				                "searchreplace visualblocks visualchars code fullscreen",
				                "insertdatetime media nonbreaking save table contextmenu directionality",
				                "template paste textcolor "],
				                'toolbar' => "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media | forecolor backcolor | fontsizeselect | tiny_mce_wiris_formulaEditor tiny_mce_wiris_formulaEditorChemistry tiny_mce_wiris_CAS",
		                        'fontsize_formats' => '8px 10px 12px 14px 18px 24px 36px',
		                    ],
		                    'fileManager' => [
		                        'class' => TinyMceElFinder::className(),
		                        'connectorRoute' => '/admin/db_info/connector_db-info',
		                    ],
		                ]
		            ) ?>
		</div>

		
		<div class="col-sm-3" style="float: left;">
		    <div class="form-group">
		        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
		    </div>
		</div>

	    <?php ActiveForm::end(); ?>

	</div>
