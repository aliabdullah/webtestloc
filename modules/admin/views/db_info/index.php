<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\db_info\Db_infoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Базовая информация');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box container" style="padding-left: 30px; padding-right: 30px;">       
    <div class="db-info-index">

        <h1><?= Html::encode($this->title) ?></h1>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <p>
            <?= Html::a(Yii::t('app', 'Создайте базовую информацию'), ['create'], ['class' => 'btn btn-success']) ?>
        </p>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                'parent_id',
                'name',
                'keywords',
                [
                    'attribute' => 'description',
                    'format' => 'html',
                    'value' => function($data) {
                      return substr($data->description, 0, 150);
                    },
                    'contentOptions' => ['style' => 'width:450px;'],
                ],
                'photo',
                'type',
                'views',
                'likes',
                'lang',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
</div>