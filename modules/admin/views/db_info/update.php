<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\db_info\Db_info */

$this->title = Yii::t('app', 'Обновить {modelClass}: ', [
    'modelClass' => 'Db Info',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Базовая информация'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Обновить');
?>
<div class="box container" style="padding-left: 30px; padding-right: 30px;">
	<div class="db-info-update">

	    <h1><?= Html::encode($this->title) ?></h1>

	    <?= $this->render('_form', [
	        'model' => $model,
	    ]) ?>

	</div>
</div>