<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\db_info\Db_info */

$this->title = Yii::t('app', 'Создайте Базовая информация');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Базовая информация'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box container" style="padding-left: 30px; padding-right: 30px;">
	<div class="db-info-create">

	    <h1><?= Html::encode($this->title) ?></h1>

	    <?= $this->render('_form', [
	        'model' => $model,
	    ]) ?>

	</div>
</div>
