<?php 

use app\models\user\User;
use app\models\task\Tasks;
use app\models\task\Tournament;
use yii\helpers\Url;
use yii\helpers\Html;

Url::remember();
$tourn = Tournament::findOne($tournAnswers[0]['tourn_id']);

// echo '<pre>';
// print_r($tournAnswers);
// echo '</pre>';
// exit();
?>
<?php if (!empty($tournAnswers)) { ?>
  <div class="col-md-4" style="padding-right: 5px; padding-left: 5px;">
  <div class="box" style="margin-top: 15px;">
      <div class="box-header with-border">
        <h3 class="box-title">Turnir: <?= $tourn->name ?></h3>
      </div>
      <div class="box-body">
        <ul class="products-list product-list-in-box">
          <?php foreach ($tournAnswers as $key => $answer) {
                $user = User::findOne($answer->user_id);
                $task = Tasks::findOne($answer->task_id);
                if ($answer->user_id == $selected[0]->user_id) {
                  $style = 'style="background: #dcf1d1;"';
                }
          ?>
            <li class="item" <?= $style?>>
              <div class="product-img">
                <div>
                  <img src="/<?= $user->photo ? $user->photo : 'uploads/user1.png'?>" alt="Product Image">
                </div>
              </div>
              <div class="product-info">
                <a href="<?=Url::to(['/admin/tournament/check-answers', 'user_id' => $user->id, 'tourn_id' => $answer->tourn_id])?>" class="product-title"><?=$user->username?>
                <?php if ($answer->true == null) { ?>
                  <span class="label label-warning pull-right">tekshirilmagan</span>
                <?php }else { ?>
                  <span class="label label-success pull-right">tekshirilgan</span>
                <?php } ?>
                  </a>
                <span class="product-description">
                      <?=$task->name?>
                    </span>
              </div>
            </li>
          <?php } ?>
        </ul>
      </div>
      <!-- /.box-body -->
      <div class="box-footer text-center">
        <a href="javascript:void(0)" class="uppercase">View All Products</a>
      </div>
      <!-- /.box-footer -->
  </div>
</div>

<div class="col-md-8" style="padding-right: 5px;
    padding-left: 5px;">
<?php if ($selected != ''): ?>
<?php foreach ($selected as $key => $item) {
      $task = Tasks::findOne($item->task_id);
?>
<section class="" style="min-height: auto;
    padding-top: 15px;
    display: inline-block;
    ">
    <div class="col-md-12" style="padding-right: 5px;
    padding-left: 5px;">
        <div class="box box-primary">
            <div class="box-header with-border">
              <!-- <h3 class="box-title"><?=$user->username?></h3> -->
              <div class="box-tools pull-right">
                <a href="#" class="btn btn-box-tool" data-toggle="tooltip" title="Previous"><i class="fa fa-chevron-left"></i></a>
                <a href="#" class="btn btn-box-tool" data-toggle="tooltip" title="Next"><i class="fa fa-chevron-right"></i></a>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <div class="mailbox-read-info">
                <h3><span style="color: #aaaaaa;">#<?=$task->id?></span>  <?=$task->name?></h3>
                <h5><span style="color: #aaaaaa;"><?=Yii::t('lang', 'Задача')?>:</span> <?=$task->task?>
                  <span class="mailbox-read-time pull-right">15 Feb. 2016 11:03 PM</span></h5>
              </div>
              <!-- /.mailbox-read-info -->
              <div class="mailbox-controls with-border text-center">
                <div class="btn-group">
                  <button type="button" class="btn btn-default btn-sm" data-toggle="tooltip" data-container="body" title="Delete">
                    <i class="fa fa-trash-o"></i></button>
                  <button type="button" class="btn btn-default btn-sm" data-toggle="tooltip" data-container="body" title="Reply">
                    <i class="fa fa-reply"></i></button>
                  <button type="button" class="btn btn-default btn-sm" data-toggle="tooltip" data-container="body" title="Forward">
                    <i class="fa fa-share"></i></button>
                </div>
                <!-- /.btn-group -->
                <button type="button" class="btn btn-default btn-sm" data-toggle="tooltip" title="Print">
                  <i class="fa fa-print"></i></button>
              </div>
              <!-- /.mailbox-controls -->
              <div class="mailbox-read-message" style="background: #fff;">
                <p><span style="color: #aaaaaa;"><?=Yii::t('lang', 'Ответ')?>:</span></p>
                 <?= $item->answer ?>
                <!-- <p>Thanks,<br>Jane</p> -->
              </div>
              
              <?php if (!empty($item->file)){ ?>
                <div class="mailbox-read-message" style="background: #fff;">
                  <p><span style="color: #aaaaaa;"><?=Yii::t('lang', 'Ответ (файл)')?>:</span></p>
                  <?php $file = "<img src='/$item->file' style='width: 100%;'>"; ?>
                   <?= $file ?>
                  <!-- <p>Thanks,<br>Jane</p> -->
              </div>
              <?php } ?>
            </div>
            <div class="box-footer">
              <!-- <div class="pull-right">
                <button type="button" class="btn btn-default"><i class="fa fa-reply"></i> Reply</button>
                <button type="button" class="btn btn-default"><i class="fa fa-share"></i> Forward</button>
              </div> -->
              <a href="<?=Url::to(['/admin/tournament/check', 'wrong' => $item->id])?>" class="btn btn-danger"><i class="fa fa-remove"></i> Неправильно</a>
              <!-- <a href="<?//=Url::to(['/admin/tournament/check', 'true' => $item->id])?>" class="btn btn-success"><i class="fa fa-check"></i>Правильно</a> -->
              <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal"><i class="fa fa-check"></i> Правильно</button>
            </div>
        </div>
      <!-- /. box -->
    </div>
</section>
<!-- modal start -->
  <div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <form method="post" action="<?= Url::to(['/admin/tournament/true-answer', 'id' => $item->id])?>">
          <?= Html::hiddenInput(\Yii :: $app->getRequest()->csrfParam, \Yii :: $app->getRequest()->getCsrfToken(), []);?>
        <div class="modal-body">
          <label>Оценка</label><br>
          <!-- <input type="radio" name="score" value="0"><span style="margin-left: 5px;">0 Балл</span><br> -->
          <input type="radio" name="score" value="1"><span style="margin-left: 5px;">1 Балл</span><br>
          <input type="radio" name="score" value="2"><span style="margin-left: 5px;">2 Балл</span><br>
          <input type="radio" name="score" value="3"><span style="margin-left: 5px;">3 Балл</span><br>
          <input type="radio" name="score" value="4"><span style="margin-left: 5px;">4 Балл</span><br>
          <input type="radio" name="score" value="5"><span style="margin-left: 5px;">5 Балл</span><br>
          <input type="radio" name="score" value="6"><span style="margin-left: 5px;">6 Балл</span><br>
          <input type="radio" name="score" value="7"><span style="margin-left: 5px;">7 Балл</span><br>
          <input type="radio" name="score" value="8"><span style="margin-left: 5px;">8 Балл</span><br>
          <input type="radio" name="score" value="9"><span style="margin-left: 5px;">9 Балл</span><br>
          <input type="radio" name="score" value="10" checked="checked"><span style="margin-left: 5px;">10 Балл</span>
        </div>
        <div class="modal-footer">
          <?= Html::submitButton(Yii::t('app', 'Send'), ['class' => 'btn btn-success']) ?>
        </div>
        </form>
      </div>
    </div>
  </div>
<!-- modal end -->
<?php } ?>
<?php endif ?>
</div>
<?php }else { ?>
  <div class="col-md-12">
    <h3>Javoblar qolmadi...</h3>
    <h5>Turnirlar sahifasiga <a href="/admin/tournament/">qaytish <i class="fa fa-angle-double-left"></i></a></h5>
  </div>
  <?php
    $currentTourn = Tournament::find()->where(['status' => Tournament::STATUS_ACTIVE])->one();
    $first = date('Y-m-d', strtotime($currentTourn->day));
    $secDay = date('Y-m-d', strtotime($first.'+1 day'));

    $time = explode(',', $currentTourn->time);

    // echo '<pre>';
    // print_r($currentTourn);
    // echo '</pre>';
    // exit();
    ?>
  <?php if (!empty($currentTourn) && $secDay <= date('Y-m-d') && $time[1] < date('H:i')): ?>
    
    <div class="col-md-12">
      <h3>Turnir vaqti yakunlandi!!!</h3>
      <h2><a class="btn btn-primary" href="<?=Url::to(['/admin/tournament/result', 'id' => $currentTourn->id])?>">Natijalarni hisoblash  <i class="fa fa-angle-double-right"></i></a></h2>
    </div>  
  <?php endif ?>
  
<?php } ?>