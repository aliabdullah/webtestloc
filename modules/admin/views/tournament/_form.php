<?php

use yii\helpers\Html;
use app\models\task\Tournament;
use yii\widgets\ActiveForm;
use zxbodya\yii2\elfinder\TinyMceElFinder;
use zxbodya\yii2\tinymce\TinyMce;
use kartik\date\DatePicker;
use pudinglabs\tagsinput\TagsinputWidget;


/* @var $this yii\web\View */
/* @var $model app\models\task\Tournament */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tournament-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-sm-6">
        <?//= $form->field($model, 'category_id')->dropDownList(\app\models\Tree::getCats()); ?>

        <div class="form-group field-category-parent_id has-success">
            <label class="control-label for-category_id">Raditelskaya kategoria</label>
            <select id="category-parent_id" class="form-control" name="Tournament[category_id]">
                <?= \app\components\MenuWidget::widget(['tpl' => 'select']); ?>
            </select>
        </div>
    </div>

    <div class="col-md-6">
        <?= $form->field($model, 'lang')->dropDownList(Tournament::getLang()) ?>
    </div>

    <div class="col-sm-12">
        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    </div>
    
    <div class="col-sm-12">
        <?= $form->field($model, 'descrip')->widget(
            TinyMce::className(), [
                'settings' => [
                    'plugins' => ["advlist autolink lists link image charmap print preview hr anchor pagebreak ",
                    "searchreplace visualblocks visualchars code fullscreen",
                    "insertdatetime media nonbreaking save table contextmenu directionality",
                    "template paste textcolor "],
                    'toolbar' => "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media | forecolor backcolor | fontsizeselect | tiny_mce_wiris_formulaEditor tiny_mce_wiris_formulaEditorChemistry tiny_mce_wiris_CAS",
                    'fontsize_formats' => '8px 10px 12px 14px 18px 24px 36px',],
                'fileManager' => [
                    'class' => TinyMceElFinder::className(),
                    'connectorRoute' => '/admin/tests/connector_test'
                ],
            ]
        ) ?>
    </div>

    <div class="col-sm-6">
        <?= $form->field($model, 'day')->widget(
            DatePicker::className(), [
                'name' => 'UserInfo[birthday]', 
                'value' => date("Y-m-d"),
                'options' => ['placeholder' => 'Select issue date ...'],
                'pluginOptions' => [
                  // 'dateFormat' => 'Y-m-d',
                  'todayHighlight' => true
                ],
        ]);?>
    </div>

    <div class="col-sm-6">
        <?= $form->field($model, 'time')->widget(TagsinputWidget::classname(), [
            'options' => ['class' => 'tagInput'],
            'clientOptions' => ['class' => 'tagInput'],
            'clientEvents' => []
         ]); ?>
    </div>

    <div class="col-sm-6">
        <?= $form->field($model, 'difficulty_lev')->dropDownList(Tournament::getDiffLevel()) ?>
    </div>

    <div class="col-sm-6">
        <?= $form->field($model, 'status')->dropDownList(Tournament::getStatus()); ?>
    </div>

    <div class="form-group col-sm-12">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
