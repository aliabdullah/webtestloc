<?php 

use yii\helpers\Url;

$flashM = explode(',', Yii::$app->session->getFlash('has-active'));
$selectedId = $flashM[0];
$selectedDate = $flashM[1];

?>

<div class="box box-primary">
  <?php if (Yii::$app->session->hasFlash('has-active')) { ?>
    <div class="box-body">
      <div class="callout callout-warning">
          <h4>Diqqat!</h4>
          <p>Hozirgi holatda faol turnir mavjud</p>
          <p><a href="/admin/tournament/">Shu holida qolsin</a></p>
          <a href="<?=Url::to(['/admin/tournament/desactive', 'checkedId' => $selectedId, 'checkedDate' => $selectedDate ])?>" class="btn btn-primary">Tanlangan turnir faollashsin!</a>
        </div>
    </div>
  <?php }elseif (Yii::$app->session->hasFlash('done-active')) { ?>
    <div class="box-body">
      <div class="callout callout-success">
          <h3>Turnir faollashtirildi!</h3>
          <h4><?= 'Nomi:  '.$tournAct->name?></h4>
          <h4><?= 'Kuni:  '.$tournAct->day?></h4>
          
        </div>
    </div>
  <?php } ?>
  <div class="box-header">
    <i class="ion ion-clipboard"></i>

    <h3 class="box-title">Aktivlashtirilmagan Turnirlar</h3>

    <div class="box-tools pull-right">
      <ul class="pagination pagination-sm inline">
        <li><a href="#">&laquo;</a></li>
        <li><a href="#">1</a></li>
        <li><a href="#">2</a></li>
        <li><a href="#">3</a></li>
        <li><a href="#">&raquo;</a></li>
      </ul>
    </div>
  </div>
  <!-- /.box-header -->
  <form action="<?= Url::to('/ru/admin/tournament/desactive')?>" method="post">
    <?= yii\helpers\Html::hiddenInput(\Yii::$app->getRequest()->csrfParam, \Yii::$app->getRequest()->getCsrfToken(), []);?>
  <div class="box-body">
    <!-- See dist/js/pages/dashboard.js to activate the todoList plugin -->
    <ul class="todo-list">
      <?php foreach ($tournDesAct as $key => $tourn) { ?>
          <li id="li-<?=$tourn->id?>">
            <input type="radio" name="tournId" class="editor" data-id="<?=$tourn->id?>" value="<?=$tourn->id?>">
            <span class="text" style="font-size: 15px;"><?= $tourn->name?></span>
            <!-- Emphasis label -->
            <small class="label label-danger" id="date<?=$tourn->id?>" style="font-size: 11px;"><!-- <i class="fa fa-clock-o"></i> --><?= $tourn->day?></small>
            <div class="col-md-3 pull-right" id="edit_date<?=$tourn->id?>"></div>
            <!-- General tools such as edit or delete-->
            <div class="tools">
              <!-- <i class="fa fa-edit" ></i> -->
              <!-- <i class="fa fa-trash-o"></i> -->
            </div>
          </li>
      <?php } ?>
    </ul>
  </div>
  <!-- /.box-body -->
  <div class="box-footer clearfix no-border">
    <button type="submit" class="btn btn-default pull-right"><i class="fa fa-plus"></i> Add item</button>
  </div>
  </form>
</div>