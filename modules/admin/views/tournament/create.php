<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\task\Tournament */

$this->title = Yii::t('app', 'Создать турнир');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Турнир'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box container">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
