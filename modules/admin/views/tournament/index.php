<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use app\models\task\TournAnswers;
use app\models\task\Tournament;

/* @var $this yii\web\View */
/* @var $searchModel app\models\task\TournamentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Турниры');
$this->params['breadcrumbs'][] = $this->title;

?>
  <?php if (Yii::$app->session->hasFlash('activated')): ?>
    <div class="alert" style="margin-bottom: 0; background: lightgreen;">
      <span style="font-size: 18px; color: #18982e;"><?= Yii::$app->session->getFlash('activated')?></span>
    </div>
  <?php endif ?>
<div class="box container">

    <h1><?= Html::encode($this->title) ?></h1>
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
         <a href="<?=Url::to('/admin/tournament/check-answers')?>">
          <div class="info-box bg-aqua">
            <span class="info-box-icon"><i class="fa fa-envelope"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Ответы</span>
              <span class="info-box-number"><?=TournAnswers::find()->where(['true' => null])->count()?></span>

              <div class="progress">
                <div class="progress-bar" style="width: 70%"></div>
              </div>
                  <span class="progress-description">
                    <?= ($countUser > 1) ? 'участники '. $countUser : 'участник '. $countUser ?> / <?= intval($countUser * 100 / $allUsers) .'%'; ?>
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
        </a>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        
        <div class="col-md-3 col-sm-6 col-xs-12">
         <a href="<?= !empty($tournament) ? Url::to(['/admin/tournament/view', 'id' => $tournament->id]) : ''?>">
          <div class="info-box bg-green">
            <span class="info-box-icon"><i class="fa fa-check"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Активный турнир</span>
              <span class="info-box-number"><?= empty($tournament) ? 0 : date('Y-M-d', strtotime($tournament->day))?></span>

              <div class="progress">
                <div class="progress-bar" style="width: 70%"></div>
              </div>
                  <span class="progress-description">
                    <?php $time = explode(',', $tournament->time);
                        echo !empty($time[0]) ? $time[0]. '/'.$time[1] : '00:00 / 00:00';       
                    ?>
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
        </a>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <?php $countT = Tournament::find()->where(['status' => Tournament::STATUS_DESACTIVE])->count() ?>
         <a href="<?=!empty($countT) ? Url::to('/admin/tournament/desactive') : ''?>">
          <div class="info-box bg-yellow">
            <span class="info-box-icon"><i class="fa fa-calendar"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Неактивные турниры</span>
              <span class="info-box-number"><?= $countT ?></span>

              <div class="progress">
                <div class="progress-bar" style="width: 70%"></div>
              </div>
                  <span class="progress-description">
                    70% Increase in 30 Days
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
        </a>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
         <a href="">
          <div class="info-box bg-red">
            <span class="info-box-icon"><i class="fa fa-comments-o"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Комментарии</span>
              <span class="info-box-number">41,410</span>

              <div class="progress">
                <div class="progress-bar" style="width: 70%"></div>
              </div>
                  <span class="progress-description">
                    70% Increase in 30 Days
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
        </a>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
      </div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Создать турнир'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'id',
                'label'=>'ID',
                'contentOptions' => ['style' => 'width:100px;'],
            ],
            [
                'attribute' => 'status',
                'label'=>'Статус',
                'value' => function($data) {
                  return Yii::t('lang', $data->status);
                },
                'contentOptions' => ['style' => 'width:100px;'],
            ],
            'name',
            [
                'attribute' => 'descrip',
                'format' => 'html',
                // 'label'=>'Description',
                'value' => function($data) {
                  return substr($data->descrip, 0, 250);
                },
                'contentOptions' => ['style' => 'width:450px;'],
            ],
            [
                'attribute' => 'day',
                // 'label'=>'Day',
                'contentOptions' => ['style' => 'width:100px;'],
            ],
            //'time',
            //'status',
            //'difficulty_lev',
            //'rules',
            //'sponsors',

            [
              'class' => 'yii\grid\ActionColumn',
              'header' => 'Actions',
              'headerOptions' => ['style' => 'color:#337ab7'],
              'template' => '{list-result}{view}{update}{delete}',
              'buttons' => [
                'list-result' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-check"></span>', $url, [
                                'title' => Yii::t('app', 'list-result'),
                    ]);
                },
                'view' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                'title' => Yii::t('app', 'lead-view'),
                    ]);
                },

                'update' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                'title' => Yii::t('app', 'lead-update'),
                    ]);
                },
                'delete' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                'title' => Yii::t('app', 'lead-delete'),
                                'data-method'  => 'post',
                    ]);
                }

              ],
              // 'urlCreator' => function ($action, $model, $key, $index) {
              //   if ($action === 'view') {
              //       $url ='index.php?r=client-login/lead-view&id='.$model->id;
              //       return $url;
              //   }

              //   if ($action === 'update') {
              //       $url ='index.php?r=client-login/lead-update&id='.$model->id;
              //       return $url;
              //   }
              //   if ($action === 'delete') {
              //       $url ='index.php?r=client-login/lead-delete&id='.$model->id;
              //       return $url;
              //   }

              // }
            ],
        ],
    ]); ?>
</div>
