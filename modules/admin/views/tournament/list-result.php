<?php 

use app\models\user\User;

?>

<div class="box box-info">
    <div class="box-header with-border">
      <h3 class="box-title"><?= $tourn->name?></h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <div class="table-responsive">
        <table class="table no-margin">
          <thead>
          <tr>
            <th>ID</th>
            <th>Участник</th>
            <th>Страна</th>
            <th>Балл</th>
            <th>Место</th>
          </tr>
          </thead>
          <tbody>
            <?php $num = 1; foreach ($listResult as $key => $result) { 
                $user = User::findOne($result->user_id);
            ?>
                <tr>
                  <td><?= $num; ?></td>
                  <td><a href="pages/examples/invoice.html"><?= $user->username ?></a></td>
                  <td><?= $result->country  ?></td>
                  <td><?= $result->score ?>  <span class="label label-success">(из 60)</span></td>
                  <td>
                    <div class="sparkbar" data-color="#00a65a" data-height="20"><?= $num?></div>
                  </td>
                </tr>
            <?php $num++; } ?>
          
          
          </tbody>
        </table>
      </div>
      <!-- /.table-responsive -->
    </div>
    <!-- /.box-body -->
    <div class="box-footer clearfix">
      <a href="/admin/tournament/" class="btn btn-sm btn-info btn-flat pull-left">Ortga</a>
      <!-- <a href="javascript:void(0)" class="btn btn-sm btn-default btn-flat pull-right">View All Orders</a> -->
    </div>
    <!-- /.box-footer -->
</div>