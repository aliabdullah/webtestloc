<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use app\models\category\Category;
use app\models\user\User;
use app\models\task\Tournament;

/* @var $this yii\web\View */
/* @var $model app\models\task\Tournament */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Турнир'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box container">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if ($model->status == Tournament::STATUS_PAST): ?>
    <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">NATIJALAR</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="table-responsive">
            <table class="table no-margin">
              <thead>
              <tr>
                <th>ID</th>
                <th>Участник</th>
                <th>Страна</th>
                <th>Балл</th>
                <th>Место</th>
              </tr>
              </thead>
              <tbody>
                <?php $num = 1; foreach ($listResult as $key => $result) { 
                    $user = User::findOne($result->user_id);
                ?>
                    <tr>
                      <td><?= $num; ?></td>
                      <td><?= $user->username ?></td>
                      <td><?= $result->country  ?></td>
                      <td><?= $result->score ?>  <span class="label label-success">(из 60)</span></td>
                      <td>
                        <div class="sparkbar" data-color="#00a65a" data-height="20"><?= $num?></div>
                      </td>
                    </tr>
                <?php $num++; } ?>
              
              
              </tbody>
            </table>
          </div>
          <!-- /.table-responsive -->
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">
          <a href="/admin/tournament/" class="btn btn-sm btn-info btn-flat pull-left">Ortga</a>
          <!-- <a href="javascript:void(0)" class="btn btn-sm btn-default btn-flat pull-right">View All Orders</a> -->
        </div>
        <!-- /.box-footer -->
    </div>
    <?php endif ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Tasks'), ['/admin/task/create', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'category_id',
                'value' => function($data){
                    $category = Category::findOne($data->category_id);
                    return $category->name;
                },
                'contentOptions' => ['style' => 'width:800px;'],
            ],
            'lang',
            'name',
            'descrip:html',
            'day',
            'time',
            'status',
            'difficulty_lev',
            'rules',
            'sponsors',
        ],
    ]) ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'cat_id',
                'value' => function($data){
                    $user = Category::findOne($data->cat_id);
                    return $user->name;
                },
                'contentOptions' => ['style' => 'width:100px;'],
            ],
            
            [
                'attribute' => 'sentBy_id',
                'value' => function($data){
                    $user = User::findOne($data->sentBy_id);
                    return $user->username;
                },
                'contentOptions' => ['style' => 'width:100px;'],
            ],
            [
                'attribute' => 'name',
                'contentOptions' => ['style' => 'width:250px;'],
            ],
            [
                'attribute' => 'task',
                'format' => 'html',
                'value' => function($data) {
                    return substr($data->task, 0, 200);
                },
                'contentOptions' => ['style' => 'width:250px;'],
            ],
            // 'short_answ',
            'lang',
            //'source',
            //'level',
            //'class',
            //'bal',
            'theme',
            'date',

            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Actions',
                'headerOptions' => ['style' => 'color:#337ab7'],
                'template' => '{view}{update}{delete}',
                'buttons' => [
                'view' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                'title' => Yii::t('app', 'lead-view'),
                    ]);
                },

                'update' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                'title' => Yii::t('app', 'lead-update'),
                    ]);
                },
                'delete' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                'title' => Yii::t('app', 'lead-delete'),
                    ]);
                }

                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'view') {
                        $url =['task/view', 'id' => $model->id];
                        return $url;
                }

                if ($action === 'update') {
                    $url =['task/update', 'id' => $model->id];
                    return $url;
                }
                if ($action === 'delete') {
                    $url =['task/delete', 'id' => $model->id];
                    return $url;
                }

                }
                  
            ],
        ],
    ]); ?>

</div>
