<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\cours\Cours;

/* @var $this yii\web\View */
/* @var $model app\models\cours\Cours */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cours-form" style="padding-left: 10px; padding-right: 10px;">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'lang')->dropDownList(Cours::getLang()) ?>


    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'content')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList(\app\models\cours\Cours::getStatus()) ?>

    <?= $form->field($model, 'cost')->textInput() ?>

    <?= $form->field($model, 'photo')->fileInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
