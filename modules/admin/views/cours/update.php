<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\cours\Cours */

$this->title = Yii::t('app', 'Обновить {modelClass}: ', [
    'modelClass' => 'Cours',
]) . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Тариф'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Обновить');
?>
<div class="box container">
	<div class="cours-update">

	    <h1><?= Html::encode($this->title) ?></h1>

	    <?= $this->render('_form', [
	        'model' => $model,
	    ]) ?>

	</div>
</div>