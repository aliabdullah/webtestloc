<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\cours\Cours */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Тариф'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box container " style="padding-left: 10px; padding-right: 10px;">
    <div class="cours-view">

        <h1><?= Html::encode($this->title) ?></h1>

        <p>
            <?= Html::a(Yii::t('app', 'Обновить'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) ?>
        </p>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'user_id',
                // 'test_id',
                'lang',
                'title',
                'content',
                [
                    'attribute' => 'status',
                    'value' => function($model){

                        $statuses = \app\models\cours\Cours::getStatus();
                        return $statuses[$model->status];
                    }
                ],
                'cost',
                'photo',
            ],
        ]) ?>

    </div>
</div>