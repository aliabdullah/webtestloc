<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\cours\Cours */

$this->title = Yii::t('app', 'Создать тариф');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Тариф'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box container">
	<div class="cours-create">

	    <h1><?= Html::encode($this->title) ?></h1>

	    <?= $this->render('_form', [
	        'model' => $model,
	    ]) ?>

	</div>
</div>