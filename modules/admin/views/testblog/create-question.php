<?php
use yii\helpers\Html;
use yii\helpers\Utils;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\LinkPager;
use yii\bootstrap\ActiveForm;
use kartik\datetime\DateTimePicker;
use app\models\User;
use app\models\Questions;
use zxbodya\yii2\elfinder\TinyMceElFinder;
use zxbodya\yii2\tinymce\TinyMce;
use zxbodya\yii2\elfinder\ElFinderInput;
use app\models\tests\Tests;


// $utils = new Utils;

$this->title = 'Создать вопрос';
$this->params['breadcrumbs'][] = $this->title;
?>

<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
    <div class="page-header pull-left">
        <div class="page-title"><?=$this->title;?></div>
    </div>
    <ol class="breadcrumb page-breadcrumb pull-right">
        <li><i class="fa fa-home"></i> <a href="<?=Yii::$app->urlManager->createUrl(['/admin/user'])?>">Админка</a> <i class="fa fa-angle-right"></i></li>
        <li class="hidden"><a href="#"><?=$this->title?></a> <i class="fa fa-angle-right"></i></li>
        <li class="active"><?=$this->title?></li>
    </ol>
    <div class="clearfix"></div>
</div>
<div class="page-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?=$this->title;?>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <?php $form = ActiveForm::begin([
                                    'id' => 'login-form',
                                    'options' => [['class' => 'row'],['enctype' => 'multipart/form-data']],
                                ]);?>

                                <?
                                // =$form->field($model_question, 'blog_id')->textInput()->input('hidden', ['value'=>Yii::$app->request->get('question_id')])->label(false);
                                ?>

                                <div class="col-sm-6">
                                    <div class="form-group field-category-parent_id has-success">
                                        <label class="control-label for-category_id">Raditelskaya kategoria</label>
                                        <select id="category-parent_id" class="form-control" name="Tests[category_id]">
                                            <?= \app\components\MenuWidget::widget(['tpl' => 'select']); ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <?= $form->field($model_question, 'level')->dropDownList(\app\models\tests\Tests::getLevels()) ?>
                                </div>

                                <div class="col-sm-12">
                                    <?=$form->field($model_question, 'question')->widget(
                                        TinyMce::className(), [
                                            'settings' => [   
                                                
                                                'toolbar' => "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media | forecolor backcolor | fontsizeselect",
                                                'fontsize_formats' => '8px 10px 12px 14px 18px 24px 36px',
                                            ],
                                            'fileManager' => [
                                                'class' => TinyMceElFinder::className(),
                                                'connectorRoute' => '/admin/tests/connector_test'
                                            ],
                                        ]
                                    )->label('Текст вопроса');?>
                                </div>
                                <div class="row">
                                    <div class="col-xs-4">
                                        <?
                                        // =$form->field($model_question, 'imageFiles[]')->fileInput(['class'=>'file-upload-ajax'])->label('Фото'); 
                                        ?>
                                    </div>
                                    <div class="col-xs-4">
                                        <img src="" width="230" class="photo-admin-user"/>
                                    </div>
                                </div>

                                <div class="col-sm-6 well">
                                    <?= $form->field($model_variant, "content[a]", ['template' => '{label}<div class=\'input-group\'>
                                    <span class="input-group-addon">
                                        <input type="checkbox" name="TestVariant[correct][a]">
                                    </span>{input}</div>{error}'])->widget(
                                    TinyMce::className(), [
                                        'settings' => [   
                                            
                                            'toolbar' => "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media | forecolor backcolor | fontsizeselect",
                                            'fontsize_formats' => '8px 10px 12px 14px 18px 24px 36px',
                                        ],
                                        'fileManager' => [
                                            'class' => TinyMceElFinder::className(),
                                            'connectorRoute' => '/admin/tests/connector_test'
                                        ],
                                    ])->label('A'); ?>
                                </div>

                                <div class="col-sm-6 well">
                                    <?= $form->field($model_variant, "content[b]", ['template' => '{label}<div class=\'input-group\'>
                                                <span class="input-group-addon">
                                                    <input type="checkbox" name="TestVariant[correct][b]">
                                                </span>{input}</div>{error}'])->widget(
                                                TinyMce::className(), [
                                                    'settings' => [   
                                                        
                                                        'toolbar' => "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media | forecolor backcolor | fontsizeselect",
                                                        'fontsize_formats' => '8px 10px 12px 14px 18px 24px 36px',
                                                    ],
                                                    'fileManager' => [
                                                        'class' => TinyMceElFinder::className(),
                                                        'connectorRoute' => '/admin/tests/connector_test'
                                                    ],
                                                ]
                                            )->label('B') ?>

                                    <? 
                                    // $form->field($model_question, "photo[b]")->fileInput()
                                    ?>
                                </div>

                                <div class="col-sm-6 well">
                                    <?= $form->field($model_variant, "content[c]", ['template' => '{label}<div class=\'input-group\'>
                                                <span class="input-group-addon">
                                                    <input type="checkbox" name="TestVariant[correct][c]">
                                                </span>{input}</div>{error}'])->widget(
                                                TinyMce::className(), [
                                                    'settings' => [   
                                                        
                                                        'toolbar' => "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media | forecolor backcolor | fontsizeselect",
                                                        'fontsize_formats' => '8px 10px 12px 14px 18px 24px 36px',
                                                    ],
                                                    'fileManager' => [
                                                        'class' => TinyMceElFinder::className(),
                                                        'connectorRoute' => '/admin/tests/connector_test'
                                                    ],
                                                ]
                                            )->label('C') ?>

                                    <? 
                                    // $form->field($model_question, "photo[c]")->fileInput() 
                                    ?>
                                </div>

                                <div class="col-sm-6 well">
                                    <?= $form->field($model_variant, "content[d]", ['template' => '{label}<div class=\'input-group\'>
                                                <span class="input-group-addon">
                                                    <input type="checkbox" name="TestVariant[correct][d]">
                                                </span>{input}</div>{error}'])->widget(
                                                TinyMce::className(), [
                                                    'settings' => [   
                                                        
                                                        'toolbar' => "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media | forecolor backcolor | fontsizeselect",
                                                        'fontsize_formats' => '8px 10px 12px 14px 18px 24px 36px',
                                                    ],
                                                    'fileManager' => [
                                                        'class' => TinyMceElFinder::className(),
                                                        'connectorRoute' => '/admin/tests/connector_test'
                                                    ],
                                                ]
                                            )->label('D') ?>

                                    <? 
                                    // $form->field($model_question, "photo[d]")->fileInput()
                                    ?>
                                </div>

                                <div class="col-sm-6 col-sm-offset-3 well">
                                    <?= $form->field($model_variant, "content[e]", ['template' => '{label}<div class=\'input-group\'>
                                        <span class="input-group-addon">
                                            <input type="checkbox" name="TestVariant[correct][e]">
                                        </span>{input}</div>{error}'])->widget(
                                        TinyMce::className(), [
                                            'settings' => [   
                                                
                                                'toolbar' => "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media | forecolor backcolor | fontsizeselect",
                                                'fontsize_formats' => '8px 10px 12px 14px 18px 24px 36px',
                                            ],
                                            'fileManager' => [
                                                'class' => TinyMceElFinder::className(),
                                                'connectorRoute' => '/admin/tests/connector_test'
                                            ],
                                        ]
                                    )->label('C') ?>
                                </div>

                                <div class="col-sm-12">
                                    <?= $form->field($model_question, 'answer')->widget(
                                        TinyMce::className(), [
                                            'settings' => [   
                                                
                                                'toolbar' => "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media | forecolor backcolor | fontsizeselect",
                                                'fontsize_formats' => '8px 10px 12px 14px 18px 24px 36px',
                                            ],
                                            'fileManager' => [
                                                'class' => TinyMceElFinder::className(),
                                                'connectorRoute' => '/admin/tests/connector_test',
                                            ],
                                        ]) ?>
                                </div>

                                <div class="col-sm-6">
                                        <?= $form->field($model_question, 'score')->dropDownList(\app\models\tests\Tests::getScores()) ?>
                                </div>
                                <div class="col-sm-6">
                                    <?= $form->field($model_question, 'type')->dropDownList(\app\models\tests\Tests::getTypes()) ?>
                                </div>
                                <div class="col-sm-12">
                                    <?= $form->field($model_question, 'date')->widget(DateTimePicker::className(), [
                                                'type' => DateTimePicker::TYPE_COMPONENT_PREPEND,
                                                'value' => date("Y-m-d H:i"),
                                                'pluginOptions' => [
                                                    'autoclose'=>true,
                                                    'format' => 'yyyy-mm-dd hh:ii'
                                                ]
                                            ]) ?>
                                </div>

                               
                               
                                
</div>




                                <?=Html::submitButton('Сохранить', ['class'=>'btn btn-success btn-block'])?>
                            <?php ActiveForm::end()?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
