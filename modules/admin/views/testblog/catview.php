<?php
use yii\helpers\Html;
use yii\helpers\Utils;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\LinkPager;
use yii\bootstrap\ActiveForm;
use app\models\user\User;
use app\models\tests\Tests;
use app\models\testblog\TestBlog;

// $utils = new Utils;

$page = 'TестБлог';

$this->title = $page;
$this->params['breadcrumbs'][] = $this->title;
?>

<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
    <div class="page-header pull-left">
        <div class="page-title"><?=$this->title;?></div>
    </div>
    <ol class="breadcrumb page-breadcrumb pull-right">
        <li><i class="fa fa-home"></i> <a href="<?=Yii::$app->urlManager->createUrl(['/admin/user'])?>">Админка</a> <i class="fa fa-angle-right"></i></li>
        <li class="hidden"><a href="#"><?=$this->title?></a> <i class="fa fa-angle-right"></i></li>
        <li class="active"><?=$this->title?></li>
    </ol>
    <div class="clearfix"></div>
</div>
<div class="page-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a href="<?=Yii::$app->urlManager->createUrl(['/admin/testblog'])?>"><?=$this->title;?> <?=$count;?></a>
                </div>



                <div class="panel-body">
                    
                    <div class="row">
                        <div class="col-sm-3">
                            
                                <a href='<?=Yii::$app->urlManager->createUrl(['/admin/testblog/create', 'cat_id' => $cat_id ])?>' class="btn btn-success"><i class="glyphicon glyphicon-plus"></i> Создать TестБлог</a>
                        </div>
                    </div>
<? 
  ?>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        // 'showFooter' => true,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            'id',
                            // 'parent_id',
                            'cat_id',
                            'lang',
                            'name',
                            'description:ntext',
                            'type',
                            'testcount',
                            // [
                            //     'attribute' => 'testcount',
                            //     'value' => function($model) {
                            //         $testcount = count($model->getTests());
                                    
                            //         return $testcount;
                            //     } 
                            // ],
                            // 'date',

                            ['class' => 'yii\grid\ActionColumn'],
                        ],
                    ]); ?>
                    
                    <?php if($pagination):?>
                        <?=LinkPager::widget(['pagination'=>$pagination]);?>
                    <?php endif;?> 
                </div>
            </div>
        </div>
    </div>
</div>
