<div class="table-responsive">
    <table class="table">
        <thead style="background: #fff0e2;">
            <tr>
                <th>
                    Matematika
                </th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($subcats as $key => $cat) { ?>
            
                <tr>
                    <td><a href="<?= yii\helpers\Url::to(['/admin/testblog/catview', 'category_id' => $cat->id]) ?>"><?= $cat->name ?></a></td>
                </tr>

            <?php } ?>    
        </tbody>
    </table>
</div>