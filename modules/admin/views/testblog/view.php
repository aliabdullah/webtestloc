<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\bootstrap\ActiveForm;
use app\models\User;
use yii\helpers\Utils;
use yii\grid\GridView;


// $utils = new Utils;

$page = Yii::$app->request->get('update_id') ? 'Редактировать' : 'Создать'; 

$this->title = ($model && $model->id) ? 'TестБлог #'.$model->id : 'Вопрос';
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
    <div class="page-header pull-left" style="margin-left: 15px; margin-bottom: 0;">
        <div class="page-title"><?=$this->title?></div>
    </div>
    <!-- <ol class="breadcrumb page-breadcrumb pull-right">
        <li><i class="fa fa-home"></i><a href="<?//=Yii::$app->urlManager->createUrl(['/'])?>"><?//=$this->title;?></a><i class=""></i></li>
        <li class="active"><a href="javascript:;">профиль</a></li>
    </ol> -->
    <div class="clearfix"></div>
</div>

<div class="page-content">
    <div class="row">
        <div class="col-lg-12">
            
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?=$this->title;?>
                </div>
                <div class="panel-body">
                    <?php if($model){?>


                        <div class="row">
                            <div class="col-sm-3">
                                <a href="<?=Yii::$app->urlManager->createUrl(['/admin/testblog/create-question', 'question_id'=>$model->id])?>" style="margin-bottom: 3px;" class="btn btn-info left-button-menu"><i class="glyphicon glyphicon-plus"></i> Добавить вопрос</a><br/>
                                <a href="<?=Yii::$app->urlManager->createUrl(['/admin/testblog/create', 'update_id'=>$model->id])?>" class="btn btn-info left-button-menu"><i class="glyphicon glyphicon-pencil"></i> Редактировать</a><br/>
                                
                            </div>
                            <div class="col-sm-9">
                                <div class="table-responsive">
                                    <table class="table text-center" style="border:1px solid #ceeaff; padding: 5px;">
                                        <thead style="background: #ceeaff;">
                                            <tr>
                                                <th>
                                                    <strong>Название</strong>
                                                </th>
                                                <th>
                                                    <strong>Подробный</strong>
                                                </th>
                                                <th>
                                                    <strong>Тип Блога</strong>
                                                </th>
                                                <th>
                                                    <strong>Категория</strong>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody style="background: #fff5e4;"" >
                                            <tr>
                                                <td style="border-right:1px solid #ceeaff; padding: 5px;"><?= $model->name ? $model->name : '-'; ?></td>
                                                <td style="border-right:1px solid #ceeaff; padding: 5px;"><?= $model->description ? $model->description : '-'; ?></td>
                                                <td style="border-right:1px solid #ceeaff; padding: 5px;"><?= $model->type ?></td>
                                                <td style="border-right:1px solid #ceeaff; padding: 5px;"><?= $model->cat_id ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                               
                            </div>
                        </div>
                        <!-- GRID VIEW -->
                        
                        <!-- GRID VIEW END -->
                        
                    <?php }else{?>
                        <div class="alert alert-warning text-center">
                            Страницы не существует
                        </div>
                    <?php }?>
                </div>


            </div>
        </div>
    </div>
</div>
<div class="panel panel-default" style="padding: 20px;">
    <div class="row">
        <div class="col-sm-12">
            <p style=" font-size: 22px;">Вопросы  блога <?= $model->name?>
</p>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'id',
                    'category_id',
                    'question:html',
                    // 'photo_quest',
                    // 'a',
                    // 'b',
                    // 'c',
                    // 'd',
                    'answer:html',
                    // 'answer_photo',
                    'score',
                    'level',
                    

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{view} {update} {delete}',
                        'buttons' => [
                            'view' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                            'title' => 'testview',
                                ]);
                            },

                            'update' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                            'title' => 'testupdate',
                                ]);
                            },
                            'delete' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                            'title' => 'testdelete',
                                ]);
                            }

                          ],
                          'urlCreator' => function ($action, $model, $key, $index) {
                            if ($action === 'view') {
                                $url ='/admin/testblog/tview?id='.$model->id;
                                return $url;
                            }

                            if ($action === 'update') {
                                $url ='/admin/testblog/tupdate?id='.$model->id;
                                return $url;
                            }
                            if ($action === 'delete') {
                                $url ='/admin/testblog/tdelete?id='.$model->id;
                                return $url;
                            }

                          }
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
