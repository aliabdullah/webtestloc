<?php
use yii\helpers\Html;
use yii\helpers\Utils;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\LinkPager;
use yii\bootstrap\ActiveForm;
use app\models\user\User;
use app\models\tests\Tests;
use yii\bootstrap\Modal;
use yii\helpers\Url;

// $utils = new Utils;

$page = 'Предметы Блогов';
$sub = 'Выберите Предмет';
$this->title = $page;
$this->params['breadcrumbs'][] = $this->title;
?>



<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
    <div class="page-header pull-left">
        <div class="page-title"><?=$this->title;?></div>
    </div>
    <ol class="breadcrumb page-breadcrumb pull-right">
        <li><i class="fa fa-home"></i> <a href="<?=Yii::$app->urlManager->createUrl(['/admin/user'])?>">Админка</a> <i class="fa fa-angle-right"></i></li>
        <li class="hidden"><a href="#"><?=$this->title?></a> <i class="fa fa-angle-right"></i></li>
        <li class="active"><?=$this->title?></li>
    </ol>
    <div class="clearfix"></div>
</div>
<div class="page-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a href="<?=Yii::$app->urlManager->createUrl(['/admin/testblog'])?>"><?=$sub;?> <?=$count;?></a>
                </div>
                <div class="panel-body">
                    
                    <hr/>
                    <div class="main__services services">
                        <div class="services__wrapper" >
                            <?php foreach ($cats as $key => $category) { ?>
                                <div class="services__box">
                              <div class="services__service service">
                                <div class="service__box">
                                  <a href="<?= Url::to(['/admin/testblog/catview', 'parent_id' => $category->id]) ?>"  class="service__link"></a>
                                  <img src="<?= $category->Photo ?>" class="service__icon">
                                  <div class="service__name"><?= $category->name ?></div>
                                </div>
                              </div>
                            </div>  
                            <?php } ?>
                        </div>
                    </div>
                        
                    <?php 

                        // Modal::begin([
                        //     'header' => '<h2 style="text-align:center;">Выберите под котегорию</h2>',
                        //     'id' => 'CatsModal',
                        //     'size' => 'modal-md',
                        // ]);
                        // echo "<div class='text-center'></div>";
                        

                        // Modal::end();

                     ?>
                     
                </div>
            </div>
        </div>
    </div>
</div>
