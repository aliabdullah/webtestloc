<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\bootstrap\ActiveForm;
use app\models\user\User;
use kartik\select2\Select2;
use app\models\testblog\TestBlog;

$update_id = Yii::$app->request->get('update_id');
$page = $update_id ? 'Редактировать' : 'Создать'; 

$this->title = $page.' TestBlog';
$this->params['breadcrumbs'][] = ['label' => 'TestBlog', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
    <div class="page-header pull-left" style="margin-left: 15px; margin-top: 15px;">
        <div class="page-title"><?=$model->name ? $model->name : 'Cоздать TестБлог'?></div>
    </div>
    <ol class="breadcrumb page-breadcrumb pull-right">
        <li><i class="fa fa-home"></i><a href="<?=Yii::$app->urlManager->createUrl(['/'])?>"><?=$this->title;?></a><i class="fa fa-angle-right"></i></li>
        <li class="active"><a href="javascript:;">профиль</a></li>
    </ol>
    <div class="clearfix"></div>
</div>

<div class="page-content">
    <?php if(Yii::$app->session->hasFlash('created')){?>
        <div class="alert alert-success text-center alert-bottom"><?=Yii::$app->session->getFlash('created');?></div>
    <?php }?>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading" >
                    <?=$this->title;?>
                </div>
                <div class="panel-body">
                    <?php if(Yii::$app->request->get('update_id')){?>
                        <a href="<?=Yii::$app->urlManager->createUrl(['/admin/testblog/create'])?>" class="btn btn-success"><i class="glyphicon glyphicon-plus"></i> Создать TестБлог</a>
                        <hr/>
                    <?php }?>
                    <div class="row">
                        <div class="col-xs-12">
                            <?php $form = ActiveForm::begin([
                                'id' => 'login-form',
                                'options' => ['class' => 'form-horizontal'],
                                'fieldConfig' => [
                                    'template' => "{label}\n<div class=\"col-sm-6\">{input}{error}</div>",
                                    'labelOptions' => ['class' => 'col-sm-3 control-label'],
                                    'inputOptions' => ['class' => 'form-control'],
                                    'errorOptions' => ['class' => 'help-block text-danger'],
                                ],]); ?>
                                
                                <?=$form->field($model, 'cat_id')->widget(Select2::classname(), [
                                    'data' => $cats,
                                    'options' => [
                                        'placeholder' => 'Веберите category',
                                        'id'=>'advertiser-select'
                                    ],
                                    'pluginOptions' => [
                                        'allowClear' => true
                                    ],
                                ])->label('Category');?>
                                <?= $form->field($model, 'lang')->dropDownList(TestBlog::getLang())?>
                                <!-- <div class="alert alert-warning text-center" id="stock-limit">
                                    Для этого рекламодателя исчерпан лимит количества квестов
                                </div> -->
                                <div id="stock-form" <?php if(!$update_id){?>class="stock-form"<?php }?>>
                                    <?=$form->field($model, 'name')->textInput()->input('text')->label('Название TестБлога');?>
                                    <?=$form->field($model, 'description')->textArea(['rows' => '8', 'cols'=>'40'])->label('Краткое описание');?>
                                    <?=$form->field($model, 'type')->dropDownList([
                                        ''=>'Выбрать...',
                                        'Легко'=>'Легко',
                                        'Нормальный'=>'Нормальный',
                                        'Сложный' => 'Сложный',
                                    ], [
                                        'id'=>'type-stock'
                                    ])->label('Тип TестБлога');?>
                                    
                                    <div class="row">
                                        <div class="col-sm-6 col-sm-offset-3">
                                            <?=Html::submitButton('Сохранить', ['class'=>'btn btn-warning btn-block'])?>
                                        </div>
                                    </div>
                                </div>
                            <?php ActiveForm::end()?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>