<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\bootstrap\ActiveForm;
use app\models\user\User;
use kartik\select2\Select2;
use app\models\tests\Tests;

$update_id = Yii::$app->request->get('update_id');
$page = $update_id ? 'Редактировать' : 'Создать'; 

$this->title = $page.' TестГруппа';
$this->params['breadcrumbs'][] = ['label' => 'TestBlog', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
    <div class="page-header pull-left">
        <div class="page-title" style="margin-left: 15px;"><?=$model->name ? $model->name : 'Cоздать TестГруппа'?></div>
    </div>
    <ol class="breadcrumb page-breadcrumb pull-right">
        <li><i class="fa fa-home"></i><a href="<?=Yii::$app->urlManager->createUrl(['/'])?>"><?=$this->title;?></a><i class="fa fa-angle-right"></i></li>
        <li class="active"><a href="javascript:;">профиль</a></li>
    </ol>
    <div class="clearfix"></div>
</div>

<div class="page-content">
    <?php if(Yii::$app->session->hasFlash('created')){?>
        <div class="alert alert-success text-center alert-bottom"><?=Yii::$app->session->getFlash('created');?></div>
    <?php }?>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?=$this->title;?>
                </div>
                <div class="panel-body">
                    <?php if(Yii::$app->request->get('update_id')){?>
                        <a href="<?=Yii::$app->urlManager->createUrl(['/admin/testblog/create'])?>" class="btn btn-success"><i class="glyphicon glyphicon-plus"></i> Создать TестГруппа</a>
                        <hr/>
                    <?php }?>
                    <div class="row">
                        <div class="col-xs-12">
                            <?php $form = ActiveForm::begin([
                                'id' => 'login-form',
                                'options' => ['class' => 'form-horizontal'],
                                'fieldConfig' => [
                                    'template' => "{label}\n<div class=\"col-sm-6\">{input}{error}</div>",
                                    'labelOptions' => ['class' => 'col-sm-3 control-label'],
                                    'inputOptions' => ['class' => 'form-control'],
                                    'errorOptions' => ['class' => 'help-block text-danger'],
                                ],]); ?>
                            <div class="cotainer">
                                <div class="row">
                                	<div class="table-responsive col-sm-8 col-sm-offset-2">
        								<table class="table text-center">
        								    <thead class="">
        								      <tr class="info">
        								        <th><?=$model->cat->name ?></th>
        								        <th>Кол-тест</th>
        								        <th>Виборь</th>
        								      </tr>
        								    </thead>
        								    <tbody>
                                                 
                                                <?php if ($model->cat->parent_id == 0){ 
                                                    foreach ($subChilds as $key => $subcat) { ?>
                                                        <tr class="warning">
                                                            <td><?= $subcat->name ?></td>
                                                            <?php $countCatTests = Tests::find()->where(['category_id' => $subcat->id])->andWhere(['lang' => $model->lang])->count(); ?>
                                                            <td><span class="badge"><?= $countCatTests?></span></td>
                                                            <td><input type="text" name="<?= $subcat->id ? $subcat->id : false?>" size="5" ></td>
                                                        </tr>
                                                    <?php }
                                                }else { ?>
            								    	<?php foreach ($model->subcats as $key => $subcat) { ?>
            									      <tr class="warning">
            									        <td><?= $subcat->name ?></td>
                                                        <?php $countCatTests = Tests::find()->where(['category_id' => $subcat->id])->andWhere(['lang' => $model->lang])->count(); ?>
            									        <td><span class="badge"><?= $countCatTests?></span></td>
            									        <td><input type="text" name="<?= $subcat->id ? $subcat->id : false?>" size="5" ></td>
            									      </tr>
            								      	<?php  } ?>
                                                <?php } ?>
        								    </tbody>      
        								</table>
                                    </div>
								</div>
								<div class="row">
                                    <div class="col-sm-6 col-sm-offset-3">
                                        <?=Html::submitButton('Сохранить', ['class'=>'btn btn-success btn-block'])?>
                                    </div>
                                </div>
							</div>
                        </div>
                        <?php ActiveForm::end()?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>