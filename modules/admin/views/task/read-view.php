 <?php 

use app\models\user\User;
use app\models\task\Tasks;
use yii\helpers\Html;
use yii\helpers\Url;

?>
<section class="content-header">
    <h1>
      <span style="color: #aaaaaa;"># <?=$task->id?></span> <?=$task->name?>
      <small style="color:<?=$task->solved ? '' : '#ff0000'?>"><?=$task->solved ? 'Yechilgan' : 'Yechilmagan' ?></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Mailbox</li>
    </ol>
</section>
<section class="cont" style="    min-height: 250px;
    padding: 15px;
    margin-right: auto;
    margin-left: auto;
    padding-left: 15px;
    padding-right: 15px;">
  <div class="">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?=$user->username?></h3>

              <div class="box-tools pull-right">
                <a href="#" class="btn btn-box-tool" data-toggle="tooltip" title="Previous"><i class="fa fa-chevron-left"></i></a>
                <a href="#" class="btn btn-box-tool" data-toggle="tooltip" title="Next"><i class="fa fa-chevron-right"></i></a>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <div class="mailbox-read-info">
                <h3><?=$task->name?></h3>
                <h5><span style="color: #aaaaaa;"><?=Yii::t('lang', 'Задача')?>:</span> <?=$task->task?>
                  <span class="mailbox-read-time pull-right">15 Feb. 2016 11:03 PM</span></h5>
              </div>
              <!-- /.mailbox-read-info -->
              <div class="mailbox-controls with-border text-center">
                <div class="btn-group">
                  <button type="button" class="btn btn-default btn-sm" data-toggle="tooltip" data-container="body" title="Delete">
                    <i class="fa fa-trash-o"></i></button>
                  <button type="button" class="btn btn-default btn-sm" data-toggle="tooltip" data-container="body" title="Reply">
                    <i class="fa fa-reply"></i></button>
                  <button type="button" class="btn btn-default btn-sm" data-toggle="tooltip" data-container="body" title="Forward">
                    <i class="fa fa-share"></i></button>
                </div>
                <!-- /.btn-group -->
                <button type="button" class="btn btn-default btn-sm" data-toggle="tooltip" title="Print">
                  <i class="fa fa-print"></i></button>
              </div>
              <!-- /.mailbox-controls -->
              <div class="mailbox-read-message" style="background: #fff;">
                <p><span style="color: #aaaaaa;"><?=Yii::t('lang', 'Ответ')?>:</span></p>
                 <?= $hold->full_answ ?>
                <!-- <p>Thanks,<br>Jane</p> -->
              </div>
              
              <?php if (!empty($hold->file)){ ?>
              	<div class="mailbox-read-message" style="background: #fff;">
	                <p><span style="color: #aaaaaa;"><?=Yii::t('lang', 'Ответ (файл)')?>:</span></p>
	                <?php $file = "<img src='/$hold->file '>"; ?>
	                 <?= $file ?>
	                <!-- <p>Thanks,<br>Jane</p> -->
	            </div>
              <?php } ?>
              
              
              <!-- /.mailbox-read-message -->
            </div>
            <!-- /.box-body -->
            <!-- <div class="box-footer">
              <ul class="mailbox-attachments clearfix">
                <li>
                  <span class="mailbox-attachment-icon"><i class="fa fa-file-pdf-o"></i></span>

                  <div class="mailbox-attachment-info">
                    <a href="#" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> Sep2014-report.pdf</a>
                        <span class="mailbox-attachment-size">
                          1,245 KB
                          <a href="#" class="btn btn-default btn-xs pull-right"><i class="fa fa-cloud-download"></i></a>
                        </span>
                  </div>
                </li>
                <li>
                  <span class="mailbox-attachment-icon"><i class="fa fa-file-word-o"></i></span>

                  <div class="mailbox-attachment-info">
                    <a href="#" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> App Description.docx</a>
                        <span class="mailbox-attachment-size">
                          1,245 KB
                          <a href="#" class="btn btn-default btn-xs pull-right"><i class="fa fa-cloud-download"></i></a>
                        </span>
                  </div>
                </li>
                <li>
                  <span class="mailbox-attachment-icon has-img"><img src="../../dist/img/photo1.png" alt="Attachment"></span>

                  <div class="mailbox-attachment-info">
                    <a href="#" class="mailbox-attachment-name"><i class="fa fa-camera"></i> photo1.png</a>
                        <span class="mailbox-attachment-size">
                          2.67 MB
                          <a href="#" class="btn btn-default btn-xs pull-right"><i class="fa fa-cloud-download"></i></a>
                        </span>
                  </div>
                </li>
                <li>
                  <span class="mailbox-attachment-icon has-img"><img src="../../dist/img/photo2.png" alt="Attachment"></span>

                  <div class="mailbox-attachment-info">
                    <a href="#" class="mailbox-attachment-name"><i class="fa fa-camera"></i> photo2.png</a>
                        <span class="mailbox-attachment-size">
                          1.9 MB
                          <a href="#" class="btn btn-default btn-xs pull-right"><i class="fa fa-cloud-download"></i></a>
                        </span>
                  </div>
                </li>
              </ul>
            </div> -->
            <!-- /.box-footer -->
            <div class="box-footer">
              <!-- <div class="pull-right">
                <button type="button" class="btn btn-default"><i class="fa fa-reply"></i> Reply</button>
                <button type="button" class="btn btn-default"><i class="fa fa-share"></i> Forward</button>
              </div> -->
              <a href="<?=Url::to(['/admin/task/delete-send', 'id' => $hold->id])?>" class="btn btn-default"><i class="fa fa-trash-o"></i> Delete</a>
              <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal"><i class="fa fa-check"></i> Правильно</button>
            </div>
            <!-- /.box-footer -->
        </div>
        <!-- modal start -->
        	<div id="myModal" class="modal fade" role="dialog">
			  <div class="modal-dialog">
			    <!-- Modal content-->
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal">&times;</button>
			        <h4 class="modal-title">Modal Header</h4>
			      </div>
			      <form method="post" action="<?=Url::to(['/admin/task/true-check', 'id' => $hold->id])?>">
			      	<?= Html::hiddenInput(\Yii :: $app->getRequest()->csrfParam, \Yii :: $app->getRequest()->getCsrfToken(), []);?>
			      <div class="modal-body">
		      		<label>Оценка</label><br>
		      		<input type="radio" name="auto" value="manual"><span style="margin-left: 5px;">Оцените 100 баллов</span><br>
		      		<input type="radio" name="auto" value="auto" checked="checked"><span style="margin-left: 5px;">Aвтоматически оценивать (в зависимости от того, насколько пользователь попытался)</span>
			      	
			        <!-- <p>Some text in the modal.</p> -->
			      </div>
			      <div class="modal-footer">
			        <?= Html::submitButton(Yii::t('app', 'Send'), ['class' => 'btn btn-success']) ?>
			      </div>
			      </form>
			    </div>
			  </div>
			</div>
        <!-- modal end -->
          <!-- /. box -->
    </div>
  </div>
</section>