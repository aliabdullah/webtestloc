<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\task\Tasks */

$this->title = Yii::t('app', 'Создание задач');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Задача'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box container">
    <div class="tests-index" style="margin: 15px;">
    	
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
