<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use zxbodya\yii2\elfinder\TinyMceElFinder;
use zxbodya\yii2\elfinder\ElFinderInput;
use zxbodya\yii2\tinymce\TinyMce;
use pudinglabs\tagsinput\TagsinputWidget;
use app\models\task\Tasks;

/* @var $this yii\web\View */
/* @var $model app\models\task\Tasks */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tasks-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-md-6">
        <div class="form-group field-category-parent_id has-success">
            <label class="control-label for-category_id">Raditelskaya kategoria</label>
            <select id="category-parent_id" class="form-control" name="Tasks[cat_id]">
                <?= \app\components\MenuWidget::widget(['tpl' => 'select']); ?>
            </select>
        </div>
    </div>

    <div class="col-md-6">
        <?= $form->field($model, 'lang')->dropDownList(Tasks::getLang()) ?>
    </div>

    <div class="col-md-6">
        <?= $form->field($model, 'source')->textInput() ?>
    </div>

    <div class="col-md-6">
        <?= $form->field($model, 'theme')->widget(pudinglabs\tagsinput\TagsinputWidget::classname(), [
                'options' => ['class' => 'tagInput'],
                'clientOptions' => ['class' => 'tagInput'],
                'clientEvents' => []
             ]); ?>
    </div>

     <div class="col-md-12">
        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="col-md-12">    
        <?= $form->field($model, 'task')->widget(
            TinyMce::className(), [
                'settings' => [
                    'plugins' => ["advlist autolink lists link image charmap print preview hr anchor pagebreak",
                    "searchreplace visualblocks visualchars code fullscreen",
                    "insertdatetime media nonbreaking save table contextmenu directionality",
                    "template paste textcolor "],
                    'toolbar' => "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media | forecolor backcolor | fontsizeselect | tiny_mce_wiris_formulaEditor tiny_mce_wiris_formulaEditorChemistry tiny_mce_wiris_CAS",
                    'fontsize_formats' => '8px 10px 12px 14px 18px 24px 36px',],
                'fileManager' => [
                    'class' => TinyMceElFinder::className(),
                    'connectorRoute' => '/admin/tests/connector_test'
                ],
            ]
        ) ?>
    </div>

    <div class="col-md-6">
        <?= $form->field($model, 'bal')->textInput(['type' => 'number']) ?>
    </div>

    <div class="col-md-6">
        <?= $form->field($model, 'short_answ')->widget(pudinglabs\tagsinput\TagsinputWidget::classname(), [
            'options' => ['class' => 'tagInput'],
            'clientOptions' => ['class' => 'tagInput'],
            'clientEvents' => []
         ]); ?>
    </div>

    <div class="col-md-6">
        <?= $form->field($model, 'class')->textInput(['type' => 'number']) ?>
    </div>

    <div class="col-md-6">
        <?= $form->field($model, 'level')->textInput(['type' => 'number']) ?>
    </div>
   

<div class="col-md-12">
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>
</div>
    <?php ActiveForm::end(); ?>

</div>
