<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\user\User;
use app\models\category\Category;

/* @var $this yii\web\View */
/* @var $searchModel app\models\task\TaskSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Задания');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box container">
    <div class="tests-index" style="margin: 15px;">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Создание задач'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // [
            //     'attribute' => 'id',
            //     'contentOptions' => ['style' => 'width:50px;'],
            // ],
            [
                'attribute' => 'cat_id',
                'value' => function($data){
                    $user = Category::findOne($data->cat_id);
                    return $user->name;
                },
                'contentOptions' => ['style' => 'width:100px;'],
            ],
            [
                'attribute' => 'sentBy_id',
                'value' => function($data){
                    $user = User::findOne($data->sentBy_id);
                    return $user->username;
                },
                'contentOptions' => ['style' => 'width:100px;'],
            ],
            [
                'attribute' => 'name',
                'contentOptions' => ['style' => 'width:250px;'],
            ],
            [
                'attribute' => 'task',
                'format' => 'html',
                'value' => function($data){
                    return substr($data->task, 0, 150);
                },
                'contentOptions' => ['style' => 'width:250px;'],
            ],
            'short_answ',
            //'source',
            //'level',
            //'class',
            //'bal',
            'theme',
            'date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
