<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\task\TaskSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tasks-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'cat_id') ?>

    <?= $form->field($model, 'sentBy_id') ?>

    <?= $form->field($model, 'task') ?>

    <?= $form->field($model, 'short_answ') ?>

    <?php // echo $form->field($model, 'source') ?>

    <?php // echo $form->field($model, 'level') ?>

    <?php // echo $form->field($model, 'class') ?>

    <?php // echo $form->field($model, 'bal') ?>

    <?php // echo $form->field($model, 'theme') ?>

    <?php // echo $form->field($model, 'date') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
