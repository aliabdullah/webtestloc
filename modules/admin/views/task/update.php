<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\task\Tasks */

$this->title = Yii::t('app', 'Обновить задачи: {nameAttribute}', [
    'nameAttribute' => $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Задачи'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Обновить');
?>
<div class="box container" style="padding-left: 30px; padding-right: 30px;">
    <div class="tests-index" style="margin: 15px;">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
