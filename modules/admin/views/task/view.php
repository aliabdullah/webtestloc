<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\category\Category;
use app\models\user\User;

/* @var $this yii\web\View */
/* @var $model app\models\task\Tasks */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Задачи'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box container">
    <div class="tests-index" style="margin: 15px;">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Обновить'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Вы уверены, что хотите удалить этот элемент?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'cat_id',
                'value' => function($data){
                    $category = Category::findOne($data->cat_id);
                    return $category->name;
                },
                'contentOptions' => ['style' => 'width:800px;'],
            ],
            [
                'attribute' => 'sentBy_id',
                'value' => function($data){
                    $user = User::findOne($data->sentBy_id);
                    return $user->username;
                },
                'contentOptions' => ['style' => 'width:100px;'],
            ],
            'name',
            'lang',
            'task:html',
            'short_answ',
            'source',
            'level',
            'class',
            'bal',
            'theme',
            'date',
        ],
    ]) ?>

</div>
