<?php 

use app\models\user\User;
use app\models\task\Tasks;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use app\models\task\AttemptFull;


$user = Yii::$app->user->identity;
$unchecked = AttemptFull::find()->with('attempt')->where(['checked' => null])->all();
?>
<section class="content-header">
    <h1>
      Ответы
      <small><?=count($unchecked)?> не проверенные</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Mailbox</li>
    </ol>
  </section>
<section class="cont" style="    min-height: 250px;
    padding: 15px;
    margin-right: auto;
    margin-left: auto;
    padding-left: 15px;
    padding-right: 15px;">
  <div class="">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Inbox</h3>

          <div class="box-tools pull-right">
            <div class="has-feedback">
              <input type="text" class="form-control input-sm" placeholder="Search Mail">
              <span class="glyphicon glyphicon-search form-control-feedback"></span>
            </div>
          </div>
          <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">
          <div class="mailbox-controls">
            <!-- Check all button -->
            
            <!-- <div class="btn-group">
              <button type="button" class="btn btn-default btn-sm"><i class="fa fa-trash-o"></i></button>
              <button type="button" class="btn btn-default btn-sm"><i class="fa fa-reply"></i></button>
              <button type="button" class="btn btn-default btn-sm"><i class="fa fa-share"></i></button>
            </div> -->
            <!-- /.btn-group -->
            <a href="<?=Url::to('/admin/task/messages')?>"><button type="submit" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i></button></a>
            <div class="pull-right">
              <?= LinkPager::widget([
                  'pagination' => $pages,
              ]); ?>
              <!-- <div class="btn-group">
                <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-left"></i></button>
                <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-right"></i></button>
              </div> -->
              <!-- /.btn-group -->
            </div>
            <!-- /.pull-right -->
          </div>
          <div class="table-responsive mailbox-messages">
            <table class="table table-hover table-striped">
              <tbody>
                <?php $num=1; foreach ($models as $key => $box) {
                  $user = User::findOne($box->attempt->user_id);
                  $task = Tasks::findOne($box->attempt->task_id); ?>
                  <tr>
                    <td><?=$num++?></td>
                    <td class="mailbox-star"><a href="#"><i class="fa fa-star text-yellow"></i></a></td>
                    <td class="mailbox-name"><a href="<?= Url::to(['/admin/task/read-view', 'id' => $box->id])?>"><?= $user->username ?></a></td>
                    <td class="mailbox-subject"><b><a href="<?= Url::to(['/admin/task/read-view', 'id' => $box->id])?>"><?= $task->name?></a></b> - <span><?= $box->full_answ ? "  Otvet otpravlyon vide texta" : "  Otvet otpravlyon vida fayl ili foto..." ?></span>
                    </td>
                    <td class="mailbox-attachment"></td>
                    <?php 
                      $d1=new DateTime($box->date); 
                      $d2=new DateTime(date('Y-m-d H:i:s')); 
                      $diff=$d2->diff($d1);
                    ?>
                    <td class="mailbox-date"><?= $diff->h ? $diff->h.' hour' : $diff->i.' mins'?> ago</td>
                  </tr>
                <?php } ?>
              </tbody>
            </table>
            <!-- /.table -->
          </div>
          <!-- /.mail-box-messages -->
        </div>
        <!-- /.box-body -->
        <div class="box-footer no-padding">
          <div class="mailbox-controls">
          </div>
        </div>
      </div>
          <!-- /. box -->
    </div>
  </div>
</section>
<?php 
// $script = <<< Js
//   $(function () {
//     //Enable iCheck plugin for checkboxes
//     //iCheck for checkbox and radio inputs
//     $('.mailbox-messages input[type="checkbox"]').iCheck({
//       checkboxClass: 'icheckbox_flat-blue',
//       radioClass: 'iradio_flat-blue'
//     });

//     //Enable check and uncheck all functionality
//     $(".checkbox-toggle").click(function () {
//       var clicks = $(this).data('clicks');
//       if (clicks) {
//         //Uncheck all checkboxes
//         $(".mailbox-messages input[type='checkbox']").iCheck("uncheck");
//         $(".fa", this).removeClass("fa-check-square-o").addClass('fa-square-o');
//       } else {
//         //Check all checkboxes
//         $(".mailbox-messages input[type='checkbox']").iCheck("check");
//         $(".fa", this).removeClass("fa-square-o").addClass('fa-check-square-o');
//       }
//       $(this).data("clicks", !clicks);
//     });

//     //Handle starring for glyphicon and font awesome
//     $(".mailbox-star").click(function (e) {
//       e.preventDefault();
//       //detect type
//       var $this = $(this).find("a > i");
//       var glyph = $this.hasClass("glyphicon");
//       var fa = $this.hasClass("fa");

//       //Switch states
//       if (glyph) {
//         $this.toggleClass("glyphicon-star");
//         $this.toggleClass("glyphicon-star-empty");
//       }

//       if (fa) {
//         $this.toggleClass("fa-star");
//         $this.toggleClass("fa-star-o");
//       }
//     });
//   });
// Js;
// $this->registerJs($script);
?>