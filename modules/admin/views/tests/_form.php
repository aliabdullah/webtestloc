<?php

use app\models\Tree;
use kartik\tree\TreeViewInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;
use zxbodya\yii2\elfinder\TinyMceElFinder;
use zxbodya\yii2\tinymce\TinyMce;
use zxbodya\yii2\elfinder\ElFinderInput;
use mihaildev\ckeditor\CKEditor;
use app\models\tests\Tests;

/* @var $this yii\web\View */
/* @var $model app\models\tests\tests */
/* @var $form yii\widgets\ActiveForm */

?>
<?php $form = ActiveForm::begin([
    'id' => 'login-form',
    'options' => [['class' => 'row'],['enctype' => 'multipart/form-data']],
]); ?>

<div class="col-sm-6">
    <?//= $form->field($model, 'category_id')->dropDownList(\app\models\Tree::getCats()); ?>

    <div class="form-group field-category-parent_id has-success">
        <label class="control-label for-category_id">Raditelskaya kategoria</label>
        <select id="category-parent_id" class="form-control" name="Tests[category_id]">
            <?= \app\components\MenuWidget::widget(['tpl' => 'select']); ?>
        </select>
    </div>
</div>
<div class="col-sm-6">
    <?= $form->field($model, 'lang')->dropDownList(Tests::getLang()) ?>
</div>
<div class="col-sm-12">
    <?= $form->field($model, 'question')->widget(
        TinyMce::className(), [
            'settings' => [
                'plugins' => ["advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "template paste textcolor "],
                'toolbar' => "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media | forecolor backcolor | fontsizeselect | tiny_mce_wiris_formulaEditor tiny_mce_wiris_formulaEditorChemistry tiny_mce_wiris_CAS",
                'fontsize_formats' => '8px 10px 12px 14px 18px 24px 36px',],
            'fileManager' => [
                'class' => TinyMceElFinder::className(),
                'connectorRoute' => '/admin/tests/connector_test'
            ],
        ]
    ) ?>
</div>
<?php
    // echo '<pre>';
    // print_r($variants);
    // echo '</pre>';
    // exit();
?>
<?php foreach ($variants as $key => $variant) { ?>
    <?php if ($variant->correct == 1) { $checked = 'checked'; }else { $checked = ''; } ?>
    <div class="col-sm-6 well">
        <?= $form->field($variant, "content[".$variant->type."]", ['template' => "{label}<div class='input-group'>
            <span class='input-group-addon'>
                <input type='checkbox' ".$checked." name=TestVariant[correct][".$variant->type."]>
            </span>{input}</div>{error}"])->widget(
            TinyMce::className(), [
                'options' => [
                    'class' => 'your_class',
                    'value' => (isset($variant->content))? $variant->content:'',
                ],
                
                'settings' => [
                    'plugins' => ["advlist autolink lists link image charmap print preview hr anchor pagebreak",
                    "searchreplace visualblocks visualchars code fullscreen",
                    "insertdatetime media nonbreaking save table contextmenu directionality",
                    "template paste textcolor"],
                    'toolbar' => "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media | forecolor backcolor | fontsizeselect ",
                    'fontsize_formats' => '8px 10px 12px 14px 18px 24px 36px',],
                'fileManager' => [
                    'class' => TinyMceElFinder::className(),
                    'connectorRoute' => '/admin/tests/connector_test'
                ],
            ]
        )->label(mb_strtoupper($variant->type)) ?>
    </div>  
<?php  } ?>
<div class="col-sm-12">
    <?= $form->field($model, 'answer')->widget(
        TinyMce::className(), [
            'settings' => [
                'plugins' => ["advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "template paste textcolor "],
                'toolbar' => "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media | forecolor backcolor | fontsizeselect ",
                'fontsize_formats' => '8px 10px 12px 14px 18px 24px 36px',],
            'fileManager' => [
                'class' => TinyMceElFinder::className(),
                'connectorRoute' => '/admin/tests/connector_test',
            ],
        ]
    ) ?>
</div>
<div class="col-sm-6">
    <?= $form->field($model, 'score')->dropDownList(\app\models\tests\Tests::getScores()) ?>
</div>
<div class="col-sm-6">
    <?= $form->field($model, 'type')->dropDownList(\app\models\tests\Tests::getTypes()) ?>
</div>
<div class="col-sm-6">
    <?= $form->field($model, 'date')->widget(DateTimePicker::className(), [
        'type' => DateTimePicker::TYPE_COMPONENT_PREPEND,
        'value' => date("Y-m-d H:i"),
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'yyyy-mm-dd hh:ii'
        ]
    ]) ?>
</div>
<div class="col-sm-6">
    <?= $form->field($model, 'level')->dropDownList(\app\models\tests\Tests::getLevels()) ?>
</div>
<div class="col-sm-6">
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
</div>

    <?php ActiveForm::end(); ?>


