<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\tests\tests */

$this->title = Yii::t('app', 'Создание тестов');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Тести'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box container" style="margin:15px; ">
	
    <h1><?= Html::encode($this->title) ?></h1>
    <hr style="margin-bottom: 30px; margin-top: 0;">

    <?= $this->render('_forms', [
        'model' => $model,
        'model_variant' => $model_variant
    ]) ?>

</div>
