<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\tests\tests */

$this->title = Yii::t('app', 'Обновить {modelClass}: ', [
    'modelClass' => 'Tests',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tests'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Обновить');
?>

<div class="box container" style="padding-left: 30px; padding-right: 30px;">
	<div class="tests-update">

	    <h1><?= Html::encode($this->title) ?></h1>

	    <?= $this->render('_form', [
	        'model' => $model,
	        'variants' => $variants,
	        // 'model_variant' => $model_variant
	    ]) ?>

	</div>
</div>