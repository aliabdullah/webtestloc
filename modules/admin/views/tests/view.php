<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\tests\TestVariant;

/* @var $this yii\web\View */
/* @var $model app\models\tests\tests */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Тести'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box container" style="padding-left: 30px; padding-right: 30px;">
    <div class="tests-view">

        <h1><?= Html::encode($this->title) ?></h1>

        <p>
            <?= Html::a(Yii::t('app', 'Обновить'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Вы уверены, что хотите удалить этот элемент?'),
                    'method' => 'post',
                ],
            ]) ?>
        </p>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'category_id',
                'question:html',
                // 'photo_quest',
                // 'a',
                // 'b',
                // 'c',
                // 'd',
                'answer:html',
                // 'answer_photo',
                'score',
                'level',
                'lang',
                'type',
            ],
        ]) ?>

    </div>
    <?php $variants = TestVariant::find()->where(['test_id' => $model->id])->all(); ?>
    <div class="col-md-12" style="padding: 20px;">
        <label>Варианты</label>
        <table class="table table-striped table-bordered detail-view">
            <tbody>
                <?php foreach ($variants as $key => $variant) { ?>
                    <tr style="background: <?= $variant->correct ? '#a3ffa3' : ''?>">
                        <th style="width: 50px;"><?=$variant->type?></th>
                        <td><?=$variant->content?></td>
                        <!-- <td><?=$variant->id?></td> -->
                    </tr>
                <?php } ?>
                
            </tbody>
        </table>
    </div>
</div>