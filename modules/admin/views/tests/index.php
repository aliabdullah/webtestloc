<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\tests\testsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Тесты');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box container">
    <div class="tests-index" style="margin: 15px;">

        <h1><?= Html::encode($this->title) ?></h1>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <p>
            <button type="button" class="btn btn-success" id="create-test" data-target="#myModal" data-toggle="modal">Быстро добавить</button>
            <a class="btn btn-default" href="<?=Url::to(['/admin/tests/index', 'invalid' => true])?>">Инвалидни вопросы</a>
            <?//= Html::a(Yii::t('app', 'Create Tests'), ['create'], ['class' => 'btn btn-success', 'id' => 'create-test', "data-toggle" => "modal", "data-target" => "#myModal"]) ?>
        </p>
        <div id='cssmenu' class="col-md-3" style="margin-right: 800px; ">
            <ul>
                <li class='active has-sub'><a href='#'><i class="fa fa-fw fa-bars"></i><span>Категории</span></a>
                   <ul>
                    <?php  foreach (app\models\category\Category::getTree() as $key => $parentCat) {
                      if(isset($parentCat['childs'])) { $childs = 'has-sub'; }else $childs = '';
                        if (Yii::$app->controller->action->id == 'view') {
                              $currentUrl = Yii::$app->controller->id.'/index';
                         }else $currentUrl = '/'.Yii::$app->controller->route; ?>
                      <li class='<?=$childs?>' style="background: #d0fbe6;"><a href='<?= Url::to([$currentUrl, 'TestsSearch[category_id]' => $key]) ?>'><span><?= Yii::t('lang', $parentCat['name']) ?></span></a>
                        <?php if (isset($parentCat['childs'])): ?>
                          <ul>
                            <?php foreach ($parentCat['childs'] as $key => $child) { 
                              if(isset($parentCat['childs'])) { $childs = 'has-sub'; }else $childs = ''; ?>
                                <li class="<?=$childs?>" style="background: #76cfd8;"><a href='<?= Url::to([$currentUrl, 'TestsSearch[category_id]' => $key]) ?>'><span><?= Yii::t('lang', $child['name']) ?></span></a>
                                  <ul>
                                    <?php if (isset($child['childs'])): ?>
                                    <?php foreach ($child['childs'] as $id => $subchild) { 
                                      if(isset($subchild['childs'])) { $childs = 'has-sub'; }else $childs = ''; ?>
                                        <li class="<?=$childs?>" style="background: #3cb1bd;"><a href='<?= Url::to([$currentUrl, 'TestsSearch[category_id]' => $id]) ?>'><span><?= Yii::t('lang', $subchild['name']) ?></span></a></li>
                                     <?php } ?>
                                     <?php endif ?>
                                  </ul>
                                </li>
                             <?php } ?>
                          </ul>
                      <?php endif ?>
                      </li>
                      <?php } ?>
                   </ul>
                </li>
            </ul>
        </div><br>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                'category_id',
                'lang',
                // 'question:text',
                [
                    'attribute'=>'question',
                    'label'=>'Родительская категория',
                    'format'=>'text', // Возможные варианты: raw, html
                    'content'=>function($data){
                        return $data->question;
                    },
                ],
                'answer:html',
                // 'answer_photo',
                'score',
                'level',
                // 'blog_id',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
</div>
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><?= Html::a(Yii::t('app', 'Отдельный добавлять'), ['create'], ['class' => 'btn btn-success']) ?></h4>
      </div>
      <div class="modal-body">
        <form action="<?=Url::to('/ru/admin/tests/fast-create')?>" method="post" enctype="multipart/form-data">
            <?= Html::hiddenInput(\Yii::$app->getRequest()->csrfParam, \Yii::$app->getRequest()->getCsrfToken(), []);?>
            <div class="col-md-12 well">
                <h5>Bu tanlaydigan ko'rsatkichlaringiz fayldagi hamma test savollarga tegishli bo'ladi!</h5>
            </div>
            <div class="col-sm-12">
                <div class="form-group field-category-parent_id has-success">
                    <label class="control-label for-category_id">Raditelskaya kategoria</label>
                    <select id="category-parent_id" class="form-control" name="Tests[category_id]">
                        <?= \app\components\MenuWidget::widget(['tpl' => 'select']); ?>
                    </select>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group field-tests-lang required has-success">
                    <label class="control-label" for="tests-lang">Language</label>
                    <select id="tests-lang" class="form-control" name="Tests[lang]" aria-required="true" aria-invalid="false">
                        <option value="ru">ru</option>
                        <option value="uz">uz</option>
                    </select>
                    <div class="help-block"></div>
                </div>
            </div>

            <div class="col-sm-12">
                <div class="form-group field-tests-type required has-success">
                    <label class="control-label" for="tests-type">Type</label>
                    <select id="tests-type" class="form-control" name="Tests[type]" aria-required="true" aria-invalid="false">
                        <option value="Базовый">Базовый</option>
                        <option value="Продвинутый">Продвинутый</option>
                        <option value="Премиум">Премиум</option>
                        <option value="Платинум">Платинум</option>
                    </select>

                <div class="help-block"></div>
                </div>
            </div>

            <div class="col-sm-12">
                <div class="form-group field-tests-score required has-success">
                <label class="control-label" for="tests-score">Score</label>
                <select id="tests-score" class="form-control" name="Tests[score]" aria-required="true" aria-invalid="false">
                    <option value="0">0</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                </select>
                <div class="help-block"></div>
                </div>
            </div>

            <div class="col-sm-12">
                <div class="form-group field-tests-level required has-success">
                    <label class="control-label" for="tests-level">Level</label>
                    <select id="tests-level" class="form-control" name="Tests[level]" aria-required="true" aria-invalid="false">
                        <option value="Легко">Легко</option>
                        <option value="Нормальный">Нормальный</option>
                        <option value="Сложный">Сложный</option>
                        <option value="Эксперт">Эксперт</option>
                    </select>
                    <div class="help-block"></div>
                </div>
            </div>

            <div class="input-file-container text-center" style="margin-top: 15px;">
                <h6>Bu yerga faqat <strong>.xml (2003)</strong> versiyada saqlangan faylni yuklashingiz mumkin</h6>
                <input class="input-file" id="my-file" type="file" style="display: none;" name="file">
                <label tabindex="0" for="my-file" class="input-file-trigger"><?=Yii::t('lang', 'Выберите файл...')?></label>
                <p class="file-return"></p>
            </div>
            <div class="form-group" style="margin-top: 15px;">
                <?= Html::submitButton(Yii::t('app', 'Send'), ['class' => 'btn btn-success']) ?>
            </div>
        </form>
        <!-- <p>Some text in the modal.</p> -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>