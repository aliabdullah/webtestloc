<?php

use app\models\Tree;
use kartik\tree\TreeViewInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;
use zxbodya\yii2\elfinder\TinyMceElFinder;
use zxbodya\yii2\tinymce\TinyMce;
use zxbodya\yii2\elfinder\ElFinderInput;
use mihaildev\ckeditor\CKEditor;
use app\models\tests\Tests;

/* @var $this yii\web\View */
/* @var $model app\models\tests\tests */
/* @var $form yii\widgets\ActiveForm */

// echo "<pre>";
// print_r(\app\models\Tree::findOne(['name' => 'Cats'])->children()->all());
// echo "</pre>";
// exit();
?>



<?php $form = ActiveForm::begin([
    'id' => 'login-form',
    'options' => [['class' => 'row'],['enctype' => 'multipart/form-data']],
]); ?>


<div class="col-sm-6">
    <?//= $form->field($model, 'category_id')->dropDownList(\app\models\tests\Tests::getCats()) ?>

    <div class="form-group field-category-parent_id has-success">
        <label class="control-label for-category_id">Raditelskaya kategoria</label>
        <select id="category-parent_id" class="form-control" name="Tests[category_id]">
            <?= \app\components\MenuWidget::widget(['tpl' => 'select']) ?>
        </select>
    </div>

</div>

<div class="col-sm-6">
    <?= $form->field($model, 'lang')->dropDownList(Tests::getLang()) ?>
</div>
<div class="col-sm-12">
    <?= $form->field($model, 'question')->widget(
                TinyMce::className(), [
                    'settings' => [   
                        'plugins' => ["advlist autolink lists link image charmap print preview hr anchor pagebreak ",
                        "searchreplace visualblocks visualchars code fullscreen",
                        "insertdatetime media nonbreaking save table contextmenu directionality",
                        "template paste textcolor "],
                        'toolbar' => "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media | forecolor backcolor | fontsizeselect ",
                        'fontsize_formats' => '8px 10px 12px 14px 18px 24px 36px',
                    ],
                    'fileManager' => [
                        'class' => TinyMceElFinder::className(),
                        'connectorRoute' => '/admin/tests/connector_test'
                    ],
                ]
            ) ?>
</div>
<!-- <div class="col-sm-12"> -->
    <?
     // $form->field($model, 'photo_quest')->fileInput() 
    ?>
<!-- </div> -->



<div class="col-sm-6 well">
    
            <?= $form->field($model_variant, "content[a]", ['template' => "{label}<div class='input-group'>
                <span class='input-group-addon'>
                    <input type='checkbox' name=TestVariant[correct][a]>
                </span>{input}</div>{error}"])->widget(
                TinyMce::className(), [
                    'settings' => [   
                        'plugins' => ["advlist autolink lists link image charmap print preview hr anchor pagebreak",
                        "searchreplace visualblocks visualchars code fullscreen",
                        "insertdatetime media nonbreaking save table contextmenu directionality",
                        "template paste textcolor "],
                        'toolbar' => "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media | forecolor backcolor | fontsizeselect",
                        'fontsize_formats' => '8px 10px 12px 14px 18px 24px 36px',
                    ],
                    'fileManager' => [
                        'class' => TinyMceElFinder::className(),
                        'connectorRoute' => '/admin/tests/connector_test'
                    ],
                ]
            )->label('A') ?>

            <? 
            // $form->field($model->variants, "photo[a]")->fileInput() 
            ?>
</div>

<div class="col-sm-6 well">
    
            <?= $form->field($model_variant, "content[b]", ['template' => "{label}<div class='input-group'>
                <span class='input-group-addon'>
                    <input type='checkbox' name=TestVariant[correct][b]>
                </span>{input}</div>{error}"])->widget(
                TinyMce::className(), [
                    'settings' => [   
                        'plugins' => ["advlist autolink lists link image charmap print preview hr anchor pagebreak",
                        "searchreplace visualblocks visualchars code fullscreen",
                        "insertdatetime media nonbreaking save table contextmenu directionality",
                        "template paste textcolor "],
                        'toolbar' => "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media | forecolor backcolor | fontsizeselect",
                        'fontsize_formats' => '8px 10px 12px 14px 18px 24px 36px',
                    ],
                    'fileManager' => [
                        'class' => TinyMceElFinder::className(),
                        'connectorRoute' => '/admin/tests/connector_test'
                    ],
                ]
            )->label('B') ?>

            <? 
            // $form->field($model->variants, "photo[a]")->fileInput() 
            ?>
</div> 

<div class="col-sm-6 well">
    
            <?= $form->field($model_variant, "content[c]", ['template' => "{label}<div class='input-group'>
                <span class='input-group-addon'>
                    <input type='checkbox' name=TestVariant[correct][c]>
                </span>{input}</div>{error}"])->widget(
                TinyMce::className(), [
                    'settings' => [   
                        'plugins' => ["advlist autolink lists link image charmap print preview hr anchor pagebreak",
                        "searchreplace visualblocks visualchars code fullscreen",
                        "insertdatetime media nonbreaking save table contextmenu directionality",
                        "template paste textcolor "],
                        'toolbar' => "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media | forecolor backcolor | fontsizeselect ",
                        'fontsize_formats' => '8px 10px 12px 14px 18px 24px 36px',
                    ],
                    'fileManager' => [
                        'class' => TinyMceElFinder::className(),
                        'connectorRoute' => '/admin/tests/connector_test'
                    ],
                ]
            )->label('C') ?>

            <? 
            // $form->field($model->variants, "photo[a]")->fileInput() 
            ?>
</div> 

<div class="col-sm-6 well">
    
            <?= $form->field($model_variant, "content[d]", ['template' => "{label}<div class='input-group'>
                <span class='input-group-addon'>
                    <input type='checkbox' name=TestVariant[correct][d]>
                </span>{input}</div>{error}"])->widget(
                TinyMce::className(), [
                    'settings' => [   
                        'plugins' => ["advlist autolink lists link image charmap print preview hr anchor pagebreak",
                        "searchreplace visualblocks visualchars code fullscreen",
                        "insertdatetime media nonbreaking save table contextmenu directionality",
                        "template paste textcolor "],
                        'toolbar' => "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media | forecolor backcolor | fontsizeselect",
                        'fontsize_formats' => '8px 10px 12px 14px 18px 24px 36px',
                    ],
                    'fileManager' => [
                        'class' => TinyMceElFinder::className(),
                        'connectorRoute' => '/admin/tests/connector_test'
                    ],
                ]
            )->label('D') ?>

            <? 
            // $form->field($model->variants, "photo[a]")->fileInput() 
            ?>
</div> 

<div class="col-sm-6 well">
    
            <?= $form->field($model_variant, "content[e]", ['template' => "{label}<div class='input-group'>
                <span class='input-group-addon'>
                    <input type='checkbox' name=TestVariant[correct][e]>
                </span>{input}</div>{error}"])->widget(
                TinyMce::className(), [
                    'settings' => [   
                        'plugins' => ["advlist autolink lists link image charmap print preview hr anchor pagebreak",
                        "searchreplace visualblocks visualchars code fullscreen",
                        "insertdatetime media nonbreaking save table contextmenu directionality",
                        "template paste textcolor "],
                        'toolbar' => "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media | forecolor backcolor | fontsizeselect",
                        'fontsize_formats' => '8px 10px 12px 14px 18px 24px 36px',
                    ],
                    'fileManager' => [
                        'class' => TinyMceElFinder::className(),
                        'connectorRoute' => '/admin/tests/connector_test'
                    ],
                ]
            )->label('E') ?>

            <? 
            // $form->field($model->variants, "photo[a]")->fileInput() 
            ?>
</div> 



<div class="col-sm-12">
    <?= $form->field($model, 'answer')->widget(
                TinyMce::className(), [
                    'settings' => [   
                        'plugins' => ["advlist autolink lists link image charmap print preview hr anchor pagebreak",
                        "searchreplace visualblocks visualchars code fullscreen",
                        "insertdatetime media nonbreaking save table contextmenu directionality",
                        "template paste textcolor "],
                        'toolbar' => "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media | forecolor backcolor | fontsizeselect",
                        'fontsize_formats' => '8px 10px 12px 14px 18px 24px 36px',
                    ],
                    'fileManager' => [
                        'class' => TinyMceElFinder::className(),
                        'connectorRoute' => '/admin/tests/connector_test',
                    ],
                ]
            ) ?>
</div>

<!-- <div class="col-sm-12"> -->
    <?
     // $form->field($model, 'answer_photo')->fileInput() 
     ?>
<!-- </div> -->

<div class="col-sm-6">
    <?= $form->field($model, 'score')->dropDownList(\app\models\tests\Tests::getScores()) ?>
</div>
<div class="col-sm-6">
    <?= $form->field($model, 'type')->dropDownList(\app\models\tests\Tests::getTypes()) ?>
</div>

<div class="col-sm-12">
    <?= $form->field($model, 'level')->dropDownList(\app\models\tests\Tests::getLevels()) ?>
</div>
<div class="col-sm-6">
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
</div>

    <?php ActiveForm::end(); ?>


