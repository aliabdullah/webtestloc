<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\statistics\Statistics */

$this->title = Yii::t('app', 'Create Statistics');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Statistics'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box container">
	<div class="statistics-create">

	    <h1><?= Html::encode($this->title) ?></h1>

	    <?= $this->render('_form', [
	        'model' => $model,
	    ]) ?>

	</div>
</div>