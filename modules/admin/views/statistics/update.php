<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\statistics\Statistics */

$this->title = Yii::t('app', 'Обновить {modelClass}: ', [
    'modelClass' => 'Statistics',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Statistics'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Обновить');
?>
<div class="box container" style="padding-left: 10px; padding-right: 10px;">
	<div class="statistics-update">

	    <h1><?= Html::encode($this->title) ?></h1>

	    <?= $this->render('_form', [
	        'model' => $model,
	    ]) ?>

	</div>
</div>