<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\category\Category;
use zxbodya\yii2\elfinder\TinyMceElFinder;
use zxbodya\yii2\tinymce\TinyMce;
use yii\helpers\ArrayHelper;
// use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\category\Category */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <?
    // = $form->field($model, 'parent_id')->dropDownList(Category::getParentCats(), ['prompt'=>'Select...', 'options'=> [$model['parent_id'] => 0]]) ?>
    <?
    //= $form->field($model, 'parent_id')->dropDownList($tree, ['prompt'=>'Select...']) ?>

    <div class="form-group field-category-parent_id has-success">
        <label class="control-label for-category_id">Raditelskaya kategoria</label>
        <select id="category-parent_id" class="form-control" name="Category[parent_id]">
            <?= \app\components\MenuWidget::widget(['tpl' => 'select']) ?>
        </select>
    </div>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'keywords')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->widget(
                TinyMce::className(), [
                    'settings' => [   
                        'plugins' => ["advlist autolink lists link image charmap print preview hr anchor pagebreak",
                        "searchreplace visualblocks visualchars code fullscreen",
                        "insertdatetime media nonbreaking save table contextmenu directionality",
                        "template paste textcolor "],
                        'toolbar' => "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media | forecolor backcolor | fontsizeselect | tiny_mce_wiris_formulaEditor tiny_mce_wiris_formulaEditorChemistry tiny_mce_wiris_CAS",
                        'fontsize_formats' => '8px 10px 12px 14px 18px 24px 36px',
                    ],
                    'fileManager' => [
                        'class' => TinyMceElFinder::className(),
                        'connectorRoute' => '/admin/default/connector',
                    ],
                ])
                 ?>

    <?= $form->field($model, 'test_count')->textInput(['maxlength' => true, 'type' => 'number']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
