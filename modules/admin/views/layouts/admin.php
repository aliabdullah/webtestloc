 <?php 
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use app\models\task\Attempts;
use app\models\task\AttemptFull;
use app\models\user\UserPoints;
use app\models\user\User;
use app\models\task\Tasks;
use app\models\task\UserComplaint;

\app\assets\AdminAsset::register($this);

$admin = Yii::$app->user->identity;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
  <meta charset="<?= Yii::$app->charset ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  
  <?= Html::csrfMetaTags() ?>
  <title><?= Html::encode($this->title) ?></title>
  <?php $this->head() ?>
  <!-- Tell the browser to be responsive to screen width -->
  
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<?php $this->beginBody() ?>
<?php
$script = <<< JS

      // var myUrl = window.location.pathname.split('/');
      // $(document).ready(function(){  
      //     setInterval(mode, 15000);
      // });

      function mode() {
        if(myUrl[1] == 'ru') {
          $.ajax({
           url: '/ru/admin/task/check',
           success: function(response) {
            
            resrponse = JSON.parse(response);
            // var rr = response[1];
            console.log(resrponse.length);
           }
          });
        }else {
          $.ajax({
           url: '/uz/admin/task/check',
           success: function(response) {
            resrponse = JSON.parse(response);
            // var rr = response[1];
            console.log(resrponse);
           }
          });
        }
      };

      
    
JS;
$this->registerJs($script);
?>
    <div class="wrapper">
      <header class="main-header">
        <!-- Logo -->
        <a href="<?= Url::to(['/admin'])?>" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>A</b>LT</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>Admin</b>LTE</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>

          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->
              <?php
                $user = Yii::$app->user->identity;
                $unchecked = AttemptFull::find()->with('attempt')->where(['checked' => null])->limit(7)->all();
              ?>
              <li class="dropdown messages-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-flag-o"></i>
                  <?= $unchecked ? "<span class='label label-success'>".count($unchecked)."</span>" : '' ?>
                </a>
                <ul class="dropdown-menu">
                  <li class="header"><?=count($unchecked)?> ta javob</li>
                  <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu">
                      <?php foreach ($unchecked as $key => $uncheck) {
                        $task = Tasks::find()->where(['id' => $uncheck->attempt->task_id])->one();
                        $user = User::findOne($uncheck->attempt->user_id);
                      ?>
                        <?php if (!empty($uncheck->attempt)) { ?>
                            <li><!-- start message -->
                            <a href="<?=Url::to(['/admin/task/read-view', 'id' => $uncheck->id])?>">
                              <div class="pull-left" style="margin-right: 10px;">
                                <!-- <img src="<?='/'.$user->photo?>" class="img-circle" alt=" Ali"> -->
                                <label><?= $user->username?></label>
                              </div>
                              <h4>
                                <?= $task->name ?>
                                <?php 
                                  $d1=new DateTime($uncheck->date); 
                                  $d2=new DateTime(date('Y-m-d H:i:s')); 
                                  $diff=$d2->diff($d1);
                                ?>
                                
                                <small><i class="fa fa-clock-o"></i> <?= $diff->h ? $diff->h.' hour' : $diff->i.' mins'?></small>
                              </h4>
                              <p><?= $uncheck->full_answ ? $uncheck->full_answ : Yii::t('lang', 'Файловой ответ...') ?></p>
                            </a>
                          </li>
                        <?php } ?>
                      <?php } ?>
                    </ul>
                  </li>
                  <li class="footer"><a href="<?=Url::to('/admin/task/messages')?>">See All Messages</a></li>
                </ul>
              </li>
              <li class="dropdown tasks-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-bell-o"></i>
                  <?php $complaint = UserComplaint::find()->where(['checked' => null])->orWhere(['checked' => ''])->limit(7)->all() ?>
                  <?= $complaint ? "<span class='label label-danger'>".count($complaint)."</span>" : '' ?>
                </a>
                <ul class="dropdown-menu">
                  <li class="header"><?= count($complaint)?> shikoyat bor</li>
                  <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu">
                      <?php foreach ($complaint as $key => $message) {
                        // $task = Tasks::find()->where(['id' => $uncheck->attempt->task_id])->one();
                        $user = User::findOne($message->user_id);
                      ?>
                        <?php if (!empty($message)) { ?>
                            <li><!-- start message -->
                            <a href="<?=Url::to(['/admin/task/complaint-view', 'id' => $message->id])?>">
                              <div class="pull-left" style="margin-right: 10px;">
                                <label><?= $user->username?></label>
                              </div>
                              <h5>
                                <?= $message->complaint ?>
                                <?php 
                                  $d1=new DateTime($message->date); 
                                  $d2=new DateTime(date('Y-m-d H:i:s')); 
                                  $diff=$d2->diff($d1);
                                ?>
                                <small style="margin-left: 10px;"><i class="fa fa-clock-o"></i> <?= $diff->h.' hour'?></small>
                              </h5>
                            </a>
                          </li>
                        <?php } ?>
                      <?php } ?>
                    </ul>
                  </li>
                  <!-- <li class="footer">
                    <a href="#">View all tasks</a>
                  </li> -->
                </ul>
              </li>
              <!-- Notifications: style can be found in dropdown.less -->
            
             
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  
                  <span class="hidden-xs"><?= $admin->username ?>
                   <?
                   // = $admin->lastname 
                   ?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="<?= '/'.$admin->photo?>" class="img-circle" alt="User Image">

                    <p>
                      <?= $admin->username ?>
                      <small>Member since Nov. 2012</small>
                    </p>
                  </li>
                  <!-- Menu Body -->
                  <li class="user-body">
                    <div class="row">
                      <div class="col-xs-4 text-center">
                        <a href="#">Followers</a>
                      </div>
                      <div class="col-xs-4 text-center">
                        <a href="#">Sales</a>
                      </div>
                      <div class="col-xs-4 text-center">
                        <a href="#">Friends</a>
                      </div>
                    </div>
                    <!-- /.row -->
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="<?= yii\helpers\Url::to(['/cabinet/']) ?>" class="btn btn-default btn-flat">Profile</a>
                    </div>
                    <div class="pull-right">
                      <a href="<?= yii\helpers\Url::to(['/site/logout']) ?>" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->
              <li>
                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image" >
              <img class="img-circle " style="height: 60px; " alt="User Image" src="<?= '/'.$admin->photo?>">
            </div>
            <div class="pull-left info" style="margin-left: 15px;">
              <h4><?= $admin->username?> </h4>
              <!-- <a href="#"><i class="fa fa-circle text-success"></i> Online</a> -->
            </div>
          </div>
          <!-- search form -->
          <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                    </button>
                  </span>
            </div>
          </form>
          <!-- /.search form -->

          <?php $adminRole = \Yii::$app->authManager->getRolesByUser($admin->id); ?>

          <?php if (!Yii::$app->user->isGuest && isset($adminRole['BossRole'])) { ?>
            <ul class="sidebar-menu" data-widget="tree">
              <!-- <li class="header">MAIN NAVIGATION</li> -->
              <li>
                <a href="<?= Url::to(['/rbac']) ?>">
                  <i class="fa fa-users"></i> <span>Пользователи</span>
                  <span class="pull-right-container">
                  </span>
                </a>
              </li>
              <li>
                <a href="<?= Url::to(['/admin/category/index']) ?>">
                  <i class="fa fa-list"></i> <span>Категорий</span>
                  <span class="pull-right-container"></span>
                </a>
              </li>
              <li>
                <a href="<?= Url::to(['/admin/tests/index']) ?>">
                  <i class="fa fa-question-circle"></i> <span>Тесты</span>
                  <span class="pull-right-container"></span>
                </a>
              </li>
              <li>
                <a href="<?= Url::to(['/admin/task/index']) ?>">
                  <i class="fa fa-stack-exchange"></i> <span>Задачи</span>
                  <span class="pull-right-container"></span>
                </a>
              </li>
              <li>
                <a href="<?= Url::to(['/admin/tournament/index']) ?>">
                  <i class="fa fa-trophy"></i> <span>Турниры</span>
                  <span class="pull-right-container"></span>
                </a>
              </li>
              <li>
                <a href="<?= Url::to(['/admin/library/index']) ?>">
                  <i class="fa fa-book"></i> <span>Библиотека</span>
                  <span class="pull-right-container"></span>
                </a>
              </li>
              <li>
                <a href="<?= Url::to(['/admin/cours/index']) ?>">
                  <i class="fa fa-credit-card"></i> <span>Подписки</span>
                  <span class="pull-right-container"></span>
                </a>
              </li>
              <li>
                <a href="<?= Url::to(['/admin/db_info/index']) ?>">
                  <i class="fa fa-folder"></i> <span>Новости</span>
                  <span class="pull-right-container"></span>
                </a>
              </li>
              <li>
                <a href="<?= Url::to(['/admin/comment/index']) ?>">
                  <i class="fa fa-comment"></i> <span>Коментарии</span>
                  <span class="pull-right-container"></span>
                </a>
              </li>
            </ul>
          <?php } ?>

          <?php if (!Yii::$app->user->isGuest && isset($adminRole['aditorRole']) && !isset($adminRole['BossRole'])) { ?>
            <ul class="sidebar-menu" data-widget="tree">
              <li>
                <a href="<?= Url::to(['/admin/category/index']) ?>">
                  <i class="fa fa-list"></i> <span>Категорий</span>
                  <span class="pull-right-container"></span>
                </a>
              </li>
              <li>
                <a href="<?= Url::to(['/admin/tests/index']) ?>">
                  <i class="fa fa-list"></i> <span>Тесты</span>
                  <span class="pull-right-container"></span>
                </a>
              </li>
              <li>
                <a href="<?= Url::to(['/admin/task/index']) ?>">
                  <i class="fa fa-stack-exchange"></i> <span>Задачи</span>
                  <span class="pull-right-container"></span>
                </a>
              </li>
              <li>
                <a href="<?= Url::to(['/admin/tournament/index']) ?>">
                  <i class="fa fa-trophy"></i> <span>Турниры</span>
                  <span class="pull-right-container"></span>
                </a>
              </li>
              <li>
                <a href="<?= Url::to(['/admin/library/index']) ?>">
                  <i class="fa fa-list"></i> <span>Библиотека</span>
                  <span class="pull-right-container"></span>
                </a>
              </li>
               <li>
                <a href="<?= Url::to(['/admin/comment/index']) ?>">
                  <i class="fa fa-comment"></i> <span>Коментарии</span>
                  <span class="pull-right-container"></span>
                </a>
              </li>
            </ul>
          <?php } ?>
          <!-- sidebar menu: : style can be found in sidebar.less -->
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
          
            <!-- <div class="content"> -->
              <?= $content ?>
            <!-- </div> -->
          
        </section>
        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 2.4.0
        </div>
        <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
        reserved.
      </footer>

      <!-- Control Sidebar -->
      <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
          <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
          <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
          <!-- Home tab content -->
          <div class="tab-pane" id="control-sidebar-home-tab">
            <h3 class="control-sidebar-heading">Recent Activity</h3>
            <ul class="control-sidebar-menu">
              <li>
                <a href="javascript:void(0)">
                  <i class="menu-icon fa fa-birthday-cake bg-red"></i>

                  <div class="menu-info">
                    <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                    <p>Will be 23 on April 24th</p>
                  </div>
                </a>
              </li>
              <li>
                <a href="javascript:void(0)">
                  <i class="menu-icon fa fa-user bg-yellow"></i>

                  <div class="menu-info">
                    <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>

                    <p>New phone +1(800)555-1234</p>
                  </div>
                </a>
              </li>
              <li>
                <a href="javascript:void(0)">
                  <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

                  <div class="menu-info">
                    <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>

                    <p>nora@example.com</p>
                  </div>
                </a>
              </li>
              <li>
                <a href="javascript:void(0)">
                  <i class="menu-icon fa fa-file-code-o bg-green"></i>

                  <div class="menu-info">
                    <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>

                    <p>Execution time 5 seconds</p>
                  </div>
                </a>
              </li>
            </ul>
            <!-- /.control-sidebar-menu -->

            <h3 class="control-sidebar-heading">Tasks Progress</h3>
            <ul class="control-sidebar-menu">
              <li>
                <a href="javascript:void(0)">
                  <h4 class="control-sidebar-subheading">
                    Custom Template Design
                    <span class="label label-danger pull-right">70%</span>
                  </h4>

                  <div class="progress progress-xxs">
                    <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
                  </div>
                </a>
              </li>
              <li>
                <a href="javascript:void(0)">
                  <h4 class="control-sidebar-subheading">
                    Update Resume
                    <span class="label label-success pull-right">95%</span>
                  </h4>

                  <div class="progress progress-xxs">
                    <div class="progress-bar progress-bar-success" style="width: 95%"></div>
                  </div>
                </a>
              </li>
              <li>
                <a href="javascript:void(0)">
                  <h4 class="control-sidebar-subheading">
                    Laravel Integration
                    <span class="label label-warning pull-right">50%</span>
                  </h4>

                  <div class="progress progress-xxs">
                    <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
                  </div>
                </a>
              </li>
              <li>
                <a href="javascript:void(0)">
                  <h4 class="control-sidebar-subheading">
                    Back End Framework
                    <span class="label label-primary pull-right">68%</span>
                  </h4>

                  <div class="progress progress-xxs">
                    <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
                  </div>
                </a>
              </li>
            </ul>
            <!-- /.control-sidebar-menu -->

          </div>
          <!-- /.tab-pane -->
          <!-- Stats tab content -->
          <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
          <!-- /.tab-pane -->
          <!-- Settings tab content -->
          <div class="tab-pane" id="control-sidebar-settings-tab">
            <form method="post">
              <h3 class="control-sidebar-heading">General Settings</h3>

              <div class="form-group">
                <label class="control-sidebar-subheading">
                  Report panel usage
                  <input type="checkbox" class="pull-right" checked>
                </label>

                <p>
                  Some information about this general settings option
                </p>
              </div>
              <!-- /.form-group -->

              <div class="form-group">
                <label class="control-sidebar-subheading">
                  Allow mail redirect
                  <input type="checkbox" class="pull-right" checked>
                </label>

                <p>
                  Other sets of options are available
                </p>
              </div>
              <!-- /.form-group -->

              <div class="form-group">
                <label class="control-sidebar-subheading">
                  Expose author name in posts
                  <input type="checkbox" class="pull-right" checked>
                </label>

                <p>
                  Allow the user to show his name in blog posts
                </p>
              </div>
              <!-- /.form-group -->

              <h3 class="control-sidebar-heading">Chat Settings</h3>

              <div class="form-group">
                <label class="control-sidebar-subheading">
                  Show me as online
                  <input type="checkbox" class="pull-right" checked>
                </label>
              </div>
              <!-- /.form-group -->

              <div class="form-group">
                <label class="control-sidebar-subheading">
                  Turn off notifications
                  <input type="checkbox" class="pull-right">
                </label>
              </div>
              <!-- /.form-group -->

              <div class="form-group">
                <label class="control-sidebar-subheading">
                  Delete chat history
                  <a href="javascript:void(0)" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
                </label>
              </div>
              <!-- /.form-group -->
            </form>
          </div>
          <!-- /.tab-pane -->
        </div>
      </aside>
      <!-- /.control-sidebar -->
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div>
<!-- ./wrapper -->


<!-- <script type="text/javascript">
  $.widget.bridge('uibutton', $.ui.button);
</script> -->
<!-- Bootstrap 3.3.7 -->


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
