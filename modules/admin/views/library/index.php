<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\library\LibrarySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Библиотека');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box container" style="padding-left: 30px; padding-right: 30px;">
    <div class="tests-index" style="margin: 15px;">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Создать библиотеку'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
              'attribute' => 'id',
               'contentOptions' => ['style' => 'width:50px;'],
            ],
            'cat_id',
            'lang',
            'author',
            'name',
            [
              'attribute' => 'year',
               'contentOptions' => ['style' => 'width:50px;'],
            ],
            [
              'attribute' => 'page',
               'contentOptions' => ['style' => 'width:50px;'],
            ],
            [
              'attribute' => 'djvu',
              'value' => function($data){
                    return ($data->djvu == '') ? 0 : 1;   
                },
               'contentOptions' => ['style' => 'width:50px;'],
            ],
            [
              'attribute' => 'pdf',
               'contentOptions' => ['style' => 'width:50px;'],
               'value' => function($data){
                    return ($data->pdf == '') ? 0 : 1;
                  },
            ],
            [
              'attribute' => 'ps',
               'contentOptions' => ['style' => 'width:50px;'],
               'value' => function($data){
                    return ($data->ps == '') ? 0 : 1;
                  },
            ],
            [
              'attribute' => 'html',
               'contentOptions' => ['style' => 'width:50px;'],
               'value' => function($data){
                    return ($data->html == '') ? 0 : 1;
                  },
            ],
            [
              'attribute' => 'TeX',
               'contentOptions' => ['style' => 'width:50px;'],
               'value' => function($data){
                    return ($data->TeX == '') ? 0 : 1;
                  },
            ],
            //'link',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
</div>
