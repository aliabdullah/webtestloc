<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\library\Library */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Библиотека'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box container" style="padding-left: 30px; padding-right: 30px;">
    <div class="tests-index" style="margin: 15px;">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Обновить'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Вы уверены, что хотите удалить этот элемент?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'cat_id',
            'lang',
            'author',
            'name',
            'year',
            'page',
            'djvu',
            'pdf',
            'ps',
            'html',
            'TeX',
            
        ],
    ]) ?>

</div>
</div>
