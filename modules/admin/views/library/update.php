<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\library\Library */

$this->title = Yii::t('app', 'Обновить библиотеку: {nameAttribute}', [
    'nameAttribute' => $model->name,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Библиотека'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Обновить
');
?>
<div class="box container" style="padding-left: 30px; padding-right: 30px;">
    <div class="tests-index" style="margin: 15px;">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
</div>
