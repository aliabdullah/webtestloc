<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use wbraganca\tagsinput\TagsinputWidget;
use app\models\library\Library;

/* @var $this yii\web\View */
/* @var $model app\models\library\Library */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="library-form">

    <?php $form = ActiveForm::begin(); ?>


    <div class="col-sm-6">
    <?//= $form->field($model, 'category_id')->dropDownList(\app\models\Tree::getCats()); ?>

    <div class="form-group field-category-parent_id has-success">
        <label class="control-label for-category_id">Raditelskaya kategoria</label>
        <select id="category-parent_id" class="form-control" name="Library[cat_id]">
            <?= \app\components\MenuWidget::widget(['tpl' => 'select']); ?>
        </select>
    </div>
</div>
    <div class="col-md-6">
        <?= $form->field($model, 'lang')->dropDownList(Library::getLang()) ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($model, 'author')->textInput() ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($model, 'year')->textInput(['type' => 'number']) ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($model, 'page')->textInput(['type' => 'number']) ?>
    </div>
    <div class="col-md-12">
        <?= $form->field($model, 'djvu')->fileInput() ?>
    </div>
    <div class="col-md-12">
        <?= $form->field($model, 'pdf')->fileInput() ?>
    </div>
    <div class="col-md-12">
        <?= $form->field($model, 'ps')->fileInput() ?>
    </div>
    <div class="col-md-12">
        <?= $form->field($model, 'html')->fileInput() ?>
    </div>
    <div class="col-md-12">
        <?= $form->field($model, 'TeX')->fileInput() ?>
    </div>

    <div class="form-group col-md-12">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
