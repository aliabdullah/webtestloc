<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\library\LibrarySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Izohlar');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box container" style="padding-left: 30px; padding-right: 30px;">
    <div class="tests-index" style="margin: 15px;">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php if (!empty($comments)): ?>
        <table class="table">
          <thead>
            <tr>
              <th>ID</th>
              <th>Foydalanuvchi</th>
              <th>Izoh</th>
              <th>Masala</th>
              <th></th>
            </tr>
          </thead>

          <tbody>
            <?php foreach ($comments as $key => $comment) { ?>
              <tr>
                <td><?=$comment->id?></td>
                <td><?=$comment->user->username?></td>
                <td><?=$comment->text?></td>
                <td><?=$comment->task->name?></td>
                <td>
                  <?php if ($comment->status == 1){ ?>
                    <a href="<?=Url::to(['/admin/comment/check',  'id' => $comment->id, 'select' => 'block'])?>" class="btn btn-warning">Bloklash</a>  
                  <?php }else { ?>
                    <a href="<?=Url::to(['/admin/comment/check', 'id' => $comment->id, 'select' => 'active'])?>" class="btn btn-success">Faollash</a>
                  <?php } ?>
                  <a href="<?=Url::to(['/admin/comment/delete', 'id' => $comment->id])?>" class="btn btn-danger">O'chirish</a>
                </td>
              </tr>
            <?php } ?>
            
          </tbody>
        </table>
    <?php endif ?>

    
</div>
</div>
