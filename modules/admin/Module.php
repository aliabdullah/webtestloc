<?php

namespace app\modules\admin;

/**
 * admin module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\admin\controllers';

    /**
     * @var string
     */
    public $layout = "admin";

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->modules = [
            'menu' => [
                'class' => '\pceuropa\menu\Menu',
                'layout' => '@app/modules/admin/views/layouts/admin'
            ],
        ];

        // custom initialization code goes here
    }
}
