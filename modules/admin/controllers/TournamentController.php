<?php

namespace app\modules\admin\controllers;

use Yii;
use yii\helpers\Url;
use app\models\task\Tournament;
use app\models\task\TournamentSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\task\TaskSearch;
use app\models\task\TournAnswers;
use app\models\user\User;
use app\models\user\UserInfo;
use app\models\user\UserPoints;
use yii\filters\AccessControl;
use app\models\task\TournResults;

/**
 * TournamentController implements the CRUD actions for Tournament model.
 */
class TournamentController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Tournament models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TournamentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $tournament = Tournament::find()->where(['status' => Tournament::STATUS_ACTIVE])->one();
        $countUser = TournAnswers::find()->where(['true' => null])->andWhere(['tourn_id' => $tournament->id])->groupBy(['user_id'])->count();
        $allUsers = User::find()->count();
        // if (empty($tournament)) {
        //     $tournament = Tournament::find()->where(['status' => Tournament::STATUS_PAST])->orderBy(['day' => SORT_DESC])->one();
        // }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'countUser' => $countUser,
            'allUsers' => $allUsers,
            'tournament' => $tournament
        ]);
    }

    public function actionCheckAnswers()
    {
        $tournAnswers = TournAnswers::find()->where(['true' => null])->groupBy(['user_id'])->limit(10)->all();
        if ($get = Yii::$app->request->get()) {
            $tournament = Tournament::findOne($get['tourn_id']);
            $secDay = date('Y-m-d', strtotime($tournament->day.'+1 day'));
            
            if ((date('Y-m-d', strtotime($tournament->day)) == date('Y-m-d') || $secDay == date('Y-m-d')) && $tournament->status == Tournament::STATUS_ACTIVE) {
                $selected = TournAnswers::find()->where(['user_id' => $get['user_id']])->andWhere(['tourn_id' => $get['tourn_id']])->andWhere(['true' => null])->all();
            }else {
                echo '<pre>';
                print_r('Activ bo\'lmagan Turnirni tanladingiz');
                echo '</pre>';
                exit();
            }
            
        }
        return $this->render('check-answers', [
            'tournAnswers' => $tournAnswers,
            'selected' => $selected ? $selected : '',
        ]);
    }

    public function actionCheck()
    {
        $get = Yii::$app->request->get();
        if ($get['wrong']) {
            $tournAnswer = TournAnswers::findOne($get['wrong']);
            $tournAnswer->true = 0;
            $tournAnswer->save();    
        }else {
            echo '<pre>';
            print_r('Javobni to\'g\'ri notogriligini aniqlashda hatolik');
            echo '</pre>';
            exit();
        }
        
        return $this->redirect(Url::previous());
    }

    public function actionTrueAnswer()
    {
        $post = Yii::$app->request->post('score');
        $tAnswId = Yii::$app->request->get('id');
        if (!empty($post) && !empty($tAnswId)) {
            $tournAnswer = TournAnswers::findOne($tAnswId);
            $tournAnswer->true = $post;
            $tournAnswer->save();
        }else {
            echo '<pre>';
            print_r('Javobni to\'g\'ri notogriligini aniqlashda hatolik');
            echo '</pre>';
            exit();
        }
        return $this->redirect(Url::previous());
    }

    public function actionResult()
    {
        $tournId = Yii::$app->request->get('id');
        $tournAswers = TournAnswers::find()->where(['tourn_id' => $tournId])->groupBy('user_id')->all();
        foreach ($tournAswers as $key => $answer) {
            $resultsTourn = TournAnswers::find()->where(['user_id' => $answer->user_id])->andWhere(['!=', 'true', 0])->all();
            if (!empty($resultsTourn)) {
                foreach ($resultsTourn as $key => $res) {
                    $total += $res->true;
                }
            }
            $userInfo = UserInfo::find()->where(['user_id' => $answer->user_id])->one();
            $resultModel = new TournResults();
            $resultModel->user_id = $answer->user_id;
            $resultModel->country = $userInfo->countries ? $userInfo->countries : '----';
            $resultModel->score = $total;
            $resultModel->place = $answer->tourn_id;
            $resultModel->date = date('Y-m-d');
            if ($resultModel->save()) {
                $userPoints = UserPoints::findOne($answer->user_id);
                if (!empty($userPoints)) {
                    $userPoints->point += $total;
                    $userPoints->save();
                }else {
                    $newUserPoints = new UserPoints();
                    $newUserPoints->user_id = $answer->user_id;
                    $newUserPoints->subject = UserPoints::SUBJECT_TOURNAMENT;
                    $newUserPoints->point = $total;
                    $newUserPoints->save();
                }
                $total = 0;
            }else {
                echo '<pre>';
                print_r('tournament/result, da model saqlanmadi');
                echo '</pre>';
                exit();
            }    
        }
        $tourn = Tournament::findOne($tournId);
        $tourn->status = Tournament::STATUS_PAST;
        $tourn->save();
        return $this->redirect(['/admin/tournament/index']);
    }

    public function actionListResult()
    {
        $get = Yii::$app->request->get('id');
        if (!empty($get)) {
            $listResult = TournResults::find()->where(['place' => $get])->orderBy(['score' => SORT_DESC])->all();
            $tourn = Tournament::findOne($get);
        }
        return $this->render('list-result', [
            'listResult' => $listResult,
            'tourn' => $tourn
        ]);
    }

    public function actionDesactive()
    {

        $tournDesAct = Tournament::find()->where(['status' => Tournament::STATUS_DESACTIVE])->all();
        $tournAct = Tournament::find()->where(['status' => Tournament::STATUS_ACTIVE])->one();
        $post = Yii::$app->request->post();
        $get = Yii::$app->request->get();
        
        
        if (!empty($post)) {
            if (empty($tournAct)) {
                $updateTourn = Tournament::findOne($post['tournId']);
                $updateTourn->day = $post['new_date'];
                $updateTourn->status = Tournament::STATUS_ACTIVE;
                if ($updateTourn->save()) {
                    $newD = $post['new_date'];
                    Yii::$app->session->setFlash('activated', "Turnir $newD sanaga aktivlashtirildi!");
                    return $this->redirect(['/admin/tournament/']);
                }    
            }else {
                $name = $tournAct->name;
                $id = $tournAct->id;
                Yii::$app->session->setFlash('has-active', $post['tournId'].','.$post['new_date']);
                return $this->redirect(['/admin/tournament/desactive']);
            }
            
        }elseif (!empty($get)) {
            if (!empty($tournAct) && (date('Y-m-d', strtotime($tournAct->day)) > date('Y-m-d'))) {
                $tournAct->status = Tournament::STATUS_DESACTIVE;
                
                if ($tournAct->save()) {
                    $choosen = Tournament::findOne($get['checkedId']);
                    $choosen->status = Tournament::STATUS_ACTIVE;
                    $choosen->day = $get['checkedDate'];
                    if ($choosen->save()) {
                        
                        Yii::$app->session->setFlash('done-active');
                        return $this->redirect(['/admin/tournament/desactive']);    
                    }else {
                        echo '<pre>';
                        print_r($choosen);
                        echo '</pre>';
                        exit();
                    }
                }
            }elseif (!empty($tournAct) && date('Y-m-d', strtotime($tournAct->day)) < date('Y-m-d')) {
                $tournAct->status = Tournament::STATUS_PAST;
                
                if ($tournAct->save()) {
                    $choos = Tournament::findOne($get['checkedId']);
                    $choos->status = Tournament::STATUS_ACTIVE;
                    $choos->day = $get['checkedDate'];
                    if ($choos->save()) {
                        Yii::$app->session->setFlash('done-active');
                        return $this->redirect(['/admin/tournament/desactive']);    
                    }else {
                        echo '<pre>';
                        print_r('saxranit qilinmadi');
                        echo '</pre>';
                        exit();
                    }                    
                }
            }else {
                echo '<pre>';
                print_r('hatolik - tournament/desactive');
                echo '</pre>';
                exit();
            }
        }
        return $this->render('desactive', [
            'tournDesAct' => $tournDesAct,
            'tournAct' => $tournAct
        ]);
    }

    /**
     * Displays a single Tournament model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $searchModel = new TaskSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $id);
        $listResult = TournResults::find()->where(['place' => $id])->orderBy(['score' => SORT_DESC])->all();

        return $this->render('view', [
            'model' => $this->findModel($id),
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'listResult' => $listResult
        ]);
    }

    /**
     * Creates a new Tournament model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Tournament();

        if ($model->load(Yii::$app->request->post())) {
            $model->rules = Tournament::RULE;
            $model->sponsors = Tournament::SPONSOR;
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Tournament model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->rules = Tournament::RULE;
            $model->sponsors = Tournament::SPONSOR;
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Tournament model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Tournament model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Tournament the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Tournament::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
