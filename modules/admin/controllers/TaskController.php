<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\task\Tasks;
use app\models\task\TaskSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\task\AttemptFull;
use app\models\task\Attempts;
use app\models\task\TaskRating;
use app\models\task\UserComplaint;
use app\models\task\UserMessages;
use app\models\task\Tournament;
use app\models\user\User;
use app\models\user\UserPoints;
use yii\filters\AccessControl;
use yii\data\Pagination;

/**
 * TaskController implements the CRUD actions for Tasks model.
 */
class TaskController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST','GET'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Tasks models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TaskSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCheck()
    {
        $unchecked = AttemptFull::find()->with('attempt')->where(['checked' => null])->asArray()->all();
        return json_encode($unchecked);
    }

    public function actionMessages()
    {   
        $unchecked = AttemptFull::find()->with('attempt')->where(['checked' => null]);

        $countQuery = clone $unchecked;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'defaultPageSize' => 15,
            'forcePageParam' => false, 
            'pageSizeParam' => false,
        ]);
        $models = $unchecked->offset($pages->offset)
            ->limit($pages->limit)
            ->orderBy('id DESC')
            ->all();

        return $this->render('messages', [
            'models' => $models,
            'pages' => $pages,
        ]);
    }

    public function actionReadView($id)
    {
        $hold = AttemptFull::find()->with('attempt')->where(['id' => $id])->one();
        $user = User::findOne($hold->attempt->user_id);
        $task = Tasks::findOne($hold->attempt->task_id);
        
        return $this->render('read-view', [
            'hold' => $hold,
            'user' => $user,
            'task' => $task,
        ]);
    }

    

    public function actionComplaintView()
    {
        $complaintId = Yii::$app->request->get('id');
        $complaint = UserComplaint::findOne($complaintId);
        $task = Tasks::findOne($complaint->task_id);
        $user = User::findOne($complaint->user_id);
        if (!empty(Yii::$app->request->get('accept'))) {
            $get = Yii::$app->request->get();
            $compl = UserComplaint::findOne($get['accept']);
            $compl->checked = 1;
            if ($compl->save()) {
                return $this->redirect(['/admin/task/index']);
            }else {
                echo '<pre>';
                print_r('checked column in UserComplaint table does\'t check');
                echo '</pre>';
                exit();
            }
        }

        return $this->render('complaint-view', [
            'complaint' => $complaint,
            'task' => $task,
            'user' => $user,
        ]);
    }

    public function actionUserMessage()
    {
        $user = Yii::$app->user->identity;
        $messages = UserMessages::find()->where(['user_id' => $user->id])->andWhere(['checked' => null])->asArray()->all();
        return json_encode($messages);

    }

    public function actionGetTask($id)
    {
        $task = Tasks::find()->where(['id' => $id])->asArray()->one();
        return json_encode($task);
    }

    public function actionDeleteSend($id)
    {
        $full = AttemptFull::findOne($id);
        $attempt = Attempts::findOne($full->attempt_id);
        $user = User::findOne($attempt->user_id);
        $task = Tasks::findOne($attempt->task_id);
        $task->attempts++;
        $task->save();
        $message = new UserMessages();
        $message->user_id = $attempt->user_id;
        $message->task_id = $attempt->task_id;
        $message->message = "Afsus! $user->username siz javobni to'g'ri topa olmadingiz. Xarakat qiling va to'g'ri javobni toping!";
        $message->date = date('Y-m-d H:i:s');
        $message->save();
        if ($attempt->complete != 1) {
            $full->checked = 1;
            $full->save();
            if ($attempt->attempt > 0) {
                $attempt->attempt--;
                $attempt->save();
            }
            return $this->redirect(['/admin/task/messages']);
        }
        

    }

    public function actionTrueCheck($id)
    {
        $post = Yii::$app->request->post('auto');
        $full = AttemptFull::findOne($id);
        $attempt = Attempts::find()->where(['id' => $full->attempt_id])->one();
        $user = User::findOne($attempt->user_id);
        $task = Tasks::find()->where(['id' => $attempt->task_id])->one();
        $taskMainCat = $task::getMainCat($task->cat_id);
        $userPoint = UserPoints::find()->where(['user_id' => $attempt->user_id])->one();
        if (!empty($full)) {
            $full->true = 1;
            $full->checked = 1;
            if ($post == 'auto') {
                if ($attempt->attempt == 6) {
                    $attempt->score = 100;
                    $point = 100;
                }elseif ($attempt->attempt == 5) {
                    $attempt->score = 90;
                    $point = 90;
                }elseif ($attempt->attempt == 4) {
                    $attempt->score = 70;
                    $point = 70;
                }elseif ($attempt->attempt == 3) {
                    $attempt->score = 50;
                    $point = 50;
                }elseif ($attempt->attempt == 2) {
                    $attempt->score = 20;
                    $point = 20;
                }elseif ($attempt->attempt == 1) {
                    $attempt->score = 10;
                    $point = 10;
                }elseif ($attempt->attempt == 0) {
                    $attempt->score = 0;
                }
            }elseif ($post == 'manual') {
                $attempt->score = 100;
                $point = 100;
            }
            if (!empty($userPoint)) {
                $userPoint->point += $point; 
            }else {
                $newUserPoint = new UserPoints();
                $newUserPoint->user_id = $user->id;
                $newUserPoint->subject = UserPoints::SUBJECT_TASK;
                $newUserPoint->point = $point;
                $newUserPoint->save();
            }
            $attempt->date = date('Y-m-d H:i:s');
            $attempt->complete = 1;
            $task->solved = 1;
            $task->attempts++;
            
            if ($full->save() && $attempt->save() && $userPoint->save() && $task->save()) {
                // Send message to user
                $message = new UserMessages();
                $message->user_id = $attempt->user_id;
                $message->task_id = $attempt->task_id;
                $message->message = "Tabriklaymiz! $user->username siz javobni to'g'ri topdingiz
                qo'lga kiritilgan ball: $attempt->score ni tashkil qiladi.";
                $message->date = date('Y-m-d H:i:s');
                $message->true = 1;
                $message->save();

                $rating = new TaskRating();
                $rating->user_id = $attempt->user_id;
                $rating->attempt = ((6 - $attempt->attempt) + 1);
                $rating->task_id = $attempt->task_id;
                $rating->date = date('Y-m-d H:i:s');
                $rating->full = 1;
                $rating->save();
                return $this->redirect(['/admin/task/messages']);
            }
        }        
        
    }


    /**
     * Displays a single Tasks model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Tasks model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Tasks();
        $user = Yii::$app->user->identity;
        $get = Yii::$app->request->get();
        
        if ($model->load(Yii::$app->request->post())) {
            if (empty($get) || !isset($get['id'])) {
                    $model->date = date('Y-m-d');
                    $model->sentBy_id = $user->id;
                    $model->type = $model::TYPE_TASK;
                    if ($model->save()) {
                        return $this->redirect(['view', 'id' => $model->id]);   
                    }       
               }elseif (!empty($get)) {
                    $tourModel = Tournament::findOne($get['id']);
                    $model->type = $model::TYPE_TOURNAMENT;
                    $model->tournament = $tourModel->id;
                    $model->date = date('Y-m-d');
                    $model->sentBy_id = $user->id;
                    $model->cat_id = $tourModel->category_id;
                    if ($model->save()) {
                        return $this->redirect(['tournament/view', 'id' => $tourModel->id]);   
                    } 
               }
            
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Tasks model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $user = Yii::$app->user->identity;
        $get = Yii::$app->request->get();
        
        
        if ($model->load(Yii::$app->request->post())) {
            if (empty($get) || !isset($get['id'])) {
                    $model->date = date('Y-m-d');
                    $model->sentBy_id = $user->id;
                    $model->type = $model::TYPE_TASK;
                    // $model->tournament = null;
                    if ($model->save()) {
                        return $this->redirect(['view', 'id' => $model->id]);   
                    }       
               }elseif (!empty($get)) {
                    $tourModel = Tournament::findOne($model->tournament);

                    if (!empty($tourModel)) {
                        $model->type = $model::TYPE_TOURNAMENT;
                        $model->tournament = $tourModel->id;
                        $model->date = date('Y-m-d');
                        $model->sentBy_id = $user->id;
                        $model->cat_id = $tourModel->category_id;
            
                        if ($model->save()) {
                            return $this->redirect(['tournament/view', 'id' => $tourModel->id]);   
                        }    
                    }else {
                        echo '<pre>';
                        print_r('task/update da hatolik');
                        echo '</pre>';
                        exit();
                    }
                }
            
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Tasks model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {   
        // echo '<pre>';
        // print_r($this->findModel($id)->type);
        // echo '</pre>';
        // exit();
        if ($this->findModel($id)->type == 'tournament') {
            $url = ['tournament/view', 'id' => $this->findModel($id)->tournament];
        }else {
            $url = ['index'];
        }
        $this->findModel($id)->delete();

        return $this->redirect($url);
    }

    /**
     * Finds the Tasks model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Tasks the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Tasks::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
