<?php

namespace app\modules\admin\controllers;

use yii\web\Controller;
use zxbodya\yii2\elfinder\ConnectorAction;


/**
 * Default controller for the `admin` module
 */
class DefaultController extends AdminController
{

	public function actions()
    {
        return [
            'connector_test' => array(
                'class' => ConnectorAction::className(),
                'settings' => array(
                    'root' => \Yii::getAlias('@webroot') . '/uploads/tests',
                    'URL' => \Yii::getAlias('@web') . '/uploads/tests/',
                    'rootAlias' => 'Home',
                    'mimeDetect' => 'none'
                )
            ),
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->redirect('admin/tests/index');
    }

    public function actionTree() {
    	return $this->render('tree');
    }
}
