<?php 

namespace app\modules\admin\controllers;

use Yii;
use yii\web\Controller;
use app\models\Comments;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;




Class CommentController extends Controller
{
	/**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST', 'GET'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
    	$comments = Comments::find()->with('task', 'user')->orderBy('id DESC')->all();

    	return $this->render('index', [
    		'comments' => $comments
    	]);
    }

    public function actionCheck()
    {
    	$id = Yii::$app->request->get('id');
    	$select = Yii::$app->request->get('select');

    	$comment = Comments::findOne($id);
    	if ($select == 'block') {
    		$comment->status = 0;
    	}elseif ($select == 'active') {
    		$comment->status = 1;
    	}

    	if ($comment->save()) {
    		return $this->redirect(['/admin/comment/index']);
    	}
    }

    public function actionDelete($id)
    {
    	$comment = Comments::findOne($id);
    	if ($comment->delete()) {
    		return $this->redirect(['/admin/comment/index']);
    	}
    }
}