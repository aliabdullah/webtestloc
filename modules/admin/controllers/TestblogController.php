<?php

namespace app\modules\admin\controllers;

use Yii;
use yii\helpers\ArrayHelper;
use app\models\testblog\TestBlog;
use app\models\testblog\TestGroup;
use app\models\testblog\TestGroupSearch;
use app\models\testblog\TestBlogSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\category\Category;
use yii\data\Pagination;
use app\models\tests\Tests;
use app\models\tests\TestVariant;
use yii\web\UploadedFile;

class TestblogController extends \yii\web\Controller
{

    
    public function actionIndex()
    {
        $cats = Category::getCats();

        return $this->render('index', [
            'cats' => $cats,
        ]);
    }

    public function actionCatview($parent_id)
    {
        $searchModel = new TestBlogSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $cat_id = $parent_id;

        return $this->render('catview', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'cat_id' => $cat_id,
        ]);
    }



    // public function actionSub()
    // {
    //     $id = Yii::$app->request->get('id');
    //     $subcats = Category::findAll(['parent_id' => $id]);
        
    //     $this->layout = false;
    //     return $this->render('sub', [
    //         'subcats' => $subcats,
    //     ]);
    // }

    public function actionCreate(){
        $model = new TestBlog;
        $update_id = null;

        $cat_id = Yii::$app->request->get('cat_id');
        
        if(Yii::$app->request->get('update_id')){
            $update_id = Yii::$app->request->get('update_id');
            $model = TestBlog::findOne($update_id);
        }

        if($model->load(Yii::$app->request->post()) && $model->validate()){
            $model->parent_id = $cat_id;
        	$model->date = date('Y-m-d H:i');
            if ($model->cat_id === "") {
                $model->cat_id = $cat_id;
            }
        
            if($redirect_id = $model->save()){
                Yii::$app->session->setFlash('testblog_created', 'TestBlog успешно сохранен');
            }
            
            return $this->redirect(['/admin/testblog/add-test', 'id'=>$update_id ? $update_id : $model->id]);
        }

        if ($update_id) {
            $cats = ArrayHelper::map(Category::find()->all(), 'id', 'name');
        }else {
            $cats = ArrayHelper::map(Category::find()->where(['parent_id' => $cat_id])->all(), 'id', 'name');
        }
        return $this->render('create', [
            'model'=>$model,
            'cats' => $cats
        ]);
    }

    public function actionAddTest($id)
    {
        $model = TestBlog::find()->with('subcats', 'cat', 'tests')->where(['id' => $id])->one();
        $cats = ArrayHelper::map($model->subcats, 'id', 'name');

        $group = TestGroup::find()->where(['blog_id' => $id])->all();
        $idtests = ArrayHelper::map($group, 'test_id', 'test_id');

        $subCats = $model->subcats;

        // If choosen main category work this
        if ($model->cat->parent_id == 0) {
            $subCatId = ArrayHelper::map($model->subcats, 'id', 'id');
            $subChilds = Category::find()->where(['parent_id' => $subCatId])->indexBy('id')->all();
            
            $subCats = $subChilds;
          
        }

        $post = Yii::$app->request->post($subcat->id);
            if (!$post == '') {
                foreach ($subCats as $key => $subcat) {
                    $post = Yii::$app->request->post($subcat->id);
                    if ($post != '') {
                        $tests = Tests::find()
                        ->where(['category_id' => $subcat->id])
                        ->andWhere(['not in', 'id', $idtests])
                        ->andWhere(['level' => $model->type])
                        ->andWhere(['lang' => $model->lang])
                        ->limit($post)
                        ->orderBy('rand()')
                        ->all();

                        foreach ($tests as $key => $test) {
                            $testGroup = new TestGroup();
                            $testGroup->test_id = $test->id;
                            $testGroup->blog_id = $model->id;
                            $testGroup->sub_cat_id = $subcat->id;
                            $testGroup->date = date('Y-m-d H:i');
                            if ($exists = TestGroup::find()->where(['test_id' => $testGroup->test_id])->andWhere(['blog_id' => $testGroup->blog_id])->exists()) {
                                $testGroup->delete();
                            }else {
                                $testGroup->save();
                                
                            }
                        }
                    }
                }
                return $this->redirect(['/admin/testblog/view', 'id' => $model->id]);      
            }
        
        return $this->render('add-test', [
            'model' => $model,
            'cats' => $cats,
            'tests' => $tests,
            'subChilds' => $subChilds,

        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $cats = ArrayHelper::map(Category::find()->all(), 'id', 'name');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['/admin/testblog/add-test', 'id'=>$update_id ? $update_id : $model->id]);
        } else {
            return $this->render('update', [
                'model'=>$model,
                'cats' => $cats,
                
            ]);
        }
    }

    protected function findModel($id)
    {
        if (($model = TestBlog::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionDelete($id){
        if($id){
            $model = TestBlog::find()->where(['id'=>$id])->one();
            if($model && $model->delete()){
                Yii::$app->session->setFlash('blog_deleted', 'Blog успешно удален');
            }
            return $this->redirect(Yii::$app->request->referrer);
        }
    }

    public function actionView($id){
        if($id){
            $model = TestBlog::find()->with('cat', 'tests')->where(['id'=>$id])->one();

            $searchModel = new TestGroupSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        // $cat_id = $category_id;
           return $this->render('view', [
                'model'=>$model,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
           ]);
        }
    }

    public function actionMake($id, $action){
        if($id && $action){
            $model = TestBlog::find()->where(['id'=>$id])->one();
            $model->makeAction($action);
            return $this->redirect(Yii::$app->request->referrer);
        }
    }

    public function actionCreateQuestion($question_id){
        if($question_id){
            $model_question = new Tests;
            $categories = Category::find()->orderBy('id','sort')->all();
            $model_variant = new TestVariant();

            if($model_question->load(Yii::$app->request->post()) && $model_question->validate()){
            	$model_question->blog_id = $question_id;
                if($redirect_id = $model_question->save()){
                    Yii::$app->session->setFlash('question_created', 'Вопрос успешно создан');
                    $model_variant_post = Yii::$app->request->post('TestVariant');
	                $types = ['a', 'b', 'c', 'd', 'e'];
	                foreach ($types as $key => $type) {
	                    $variant = new TestVariant;
	                    $variant->type = $type;
	                    $variant->test_id = $model_question->id;
	                    $variant->correct = $model_variant_post['correct'][$type] ? true : false;
	                    $variant->content = $model_variant_post['content'][$type];
	                    if ($model_variant_post['content'][$type] == '') continue;
	                    $variant->photo = UploadedFile::getInstance($variant, 'photo['.$type.']');
	                    $variant->UploadFile();
	                    $variant->save(false);
	                }
                }
                if ($model_question->save()) {
                    $addToBlog = new TestGroup();
                    $addToBlog->test_id = $model_question->id;
                    $addToBlog->blog_id = $question_id;
                    $addToBlog->sub_cat_id = $model_question->category_id;
                    $addToBlog->date = date('Y-m-d, H:i');
                    $addToBlog->save();
                }

                return $this->redirect(['/admin/testblog/view', 'id'=>$question_id]);
            }
            return $this->render('create-question', [
                'model_question'=>$model_question,
                'categories'=>$categories,
                'model_variant' => $model_variant
            ]);
        }
    }



    // Test VIEW, UPDATE, DELETE

    public function actionTview($id)
    {
        return $this->render('tview', [
            'model' => Tests::findOne($id),
        ]);
    }

    public function actionTupdate($id)
    {
        $model = Tests::findOne($id);
       
        if ($model->load(Yii::$app->request->post()) && $model->save()) {

           $model_variant_post = Yii::$app->request->post('TestVariant');
            foreach ($model->variants as $key => $variant) {

                
                
                $variant->type = $variant->type;
                $variant->test_id = $model->id;
                $variant->correct = $model_variant_post['correct'][$variant->type] ? true : false;
                $variant->content = $model_variant_post['content'][$variant->type];
                if ($model_variant_post['content'][$variant->type] == '') continue;
                $variant->photo = UploadedFile::getInstance($variant, 'photo['.$variant->type.']');
                $variant->UploadFile();
                $variant->save(false);
                
            }
            
            return $this->redirect(['view', 'id' => $model->id]);

        }else {
                return $this->render('tupdate', [
                    'model' => $model,
                    // 'model_variant' => $model_variant
                ]);
        }
    }

    public function actionTdelete($id)
    {
        $model = Tests::findOne($id)->delete();

        return $this->redirect(Yii::$app->request->referrer);
    }

    // public function findModel($id)
    // {
    //     if (($model = Tests::findOne($id)) !== null) {
    //         return $model;
    //     } else {
    //         throw new NotFoundHttpException('The requested page does not exist.');
    //     }
    // }
}
