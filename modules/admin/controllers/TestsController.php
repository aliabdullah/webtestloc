<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\tests\Tests;
use app\models\tests\TestVariant;
use app\models\tests\TestsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use zxbodya\yii2\tinymce\TinyMce;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;
use zxbodya\yii2\elfinder\ConnectorAction;
use app\components\helpers\TestParser;
use yii\filters\AccessControl;



/**
 * TestsController implements the CRUD actions for tests model.
 */
class TestsController extends AdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'connector_test' => array(
                'class' => ConnectorAction::className(),
                'settings' => array(
                    'root' => \Yii::getAlias('@webroot') . '/uploads/tests',
                    'URL' => \Yii::getAlias('@web') . '/uploads/tests/',
                    'rootAlias' => 'Home',
                    'mimeDetect' => 'none'
                )
            ),
        ];
    }



    /**
     * Lists all tests models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TestsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single tests model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionFastCreate()
    {
        $post = Yii::$app->request->post();
        $file = UploadedFile::getInstanceByName('file');

        if (is_object($file)) {
            $name = 'uploads/xml/' . md5($file->baseName.rand())  . '.' . $file->extension;
            if ($file->saveAs($name)) {
                chmod($name, 0777);
            }
        }
        // Parsing file
        $parser = new TestParser();
        $parser->load($name);
        foreach ($parser->result as $key => $question) {
            $test = new Tests();
            $test->question = $question['question'];
            $test->category_id = $post['Tests']['category_id'];
            $test->score = $post['Tests']['score'];
            $test->level = $post['Tests']['level'];
            $test->type = $post['Tests']['type'];
            $test->lang = $post['Tests']['lang'];
            $test->date = date('Y-m-d H:i:s');
            $test->answer = 'unchecked';
            if ($test->save(false)) {
                // to save variants
                foreach ($question['answers'] as $key => $answer) {
                    $variant = new TestVariant();
                    $variant->test_id = $test->id;
                    $variant->type = Tests::changeEncodingOption($answer['key']);
                    if (empty($answer['key']) || empty($answer['text'])) {
                        $variant->invalid = 1;
                    }                    
                    $variant->correct = $answer['correct'] ? $answer['correct'] : 0;
                    $variant->content = $answer['text'];
                    $variant->photo = 'unchecked';
                    $variant->save(false);
                }

            }else {
                echo '<pre>';
                print_r('fast_create actionida hatolik');
                echo '</pre>';
                exit();
            }
        }
            return $this->redirect(['/admin/tests/index']);
        
    }

    public function actionInvalidTests()
    {

        $variants = TestVariant::find()->where(['invalid' => 1])->groupBy('test_id')->all();
        $idArr = [];
        foreach ($variants as $key => $value) {
            $idArr[$value->test_id] = $value->test_id;   
        }
        $invalidTests = Tests::find()->where(['in', 'id', $idArr])->all();
        
        return $this->render('invalid-tests', [
            'invalidTests' => $invalidTests,
        ]);
    }

    /**
     * Creates a new tests model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Tests();
        $model_variant = new TestVariant();

        if ($model->load(Yii::$app->request->post())) {
            $model->date = date('Y-m-d');

            if ($model->save(false)) {
                $model_variant_post = Yii::$app->request->post('TestVariant');                  
                
                $types = ['a', 'b', 'c', 'd', 'e'];
                foreach ($types as $key => $type) {
                    
                    $variant = new TestVariant;
                    $variant->type = $type;
                    $variant->test_id = $model->id;
                    $variant->correct = $model_variant_post['correct'][$type] ? true : false;
                    $variant->content = $model_variant_post['content'][$type];
                    if ($model_variant_post['content'][$type] == '') continue;
                    $variant->photo = UploadedFile::getInstance($variant, 'photo['.$type.']');
                    $variant->UploadFile();
                    $variant->save(false);
                    
                }
               
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
                'model_variant' => $model_variant
            ]);
        }
    }

    /**
     * Updates an existing tests model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $variants = TestVariant::findAll(['test_id' => $id]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model_variant_post = Yii::$app->request->post('TestVariant');
            foreach ($model->variants as $key => $variant) {
                $variant->type = $variant->type;
                $variant->test_id = $model->id;
                $variant->correct = $model_variant_post['correct'][$variant->type] ? 1 : 0;
                $variant->content = $model_variant_post['content'][$variant->type];
                if ($model_variant_post['content'][$variant->type] == '') {continue;}
                $variant->photo = UploadedFile::getInstance($variant, 'photo['.$variant->type.']');
                $variant->UploadFile();
                $variant->save();
            }
            
            return $this->redirect(['view', 'id' => $model->id]);

        }else {
                return $this->render('update', [
                    'model' => $model,
                    'variants' => $variants,
                    // 'model_variant' => $model_variant
                ]);
        }
    }
    /**
     * Deletes an existing tests model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        foreach (TestVariant::find()->where(['test_id' => $id])->all() as $variant) {
             $variant->delete();
         } 

        return $this->redirect(['index']);
    }

    /**
     * Finds the tests model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return tests the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = tests::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

  
}
