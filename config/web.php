<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'language' => 'ru',
    'sourceLanguage' => 'en',
    'timeZone' => 'Asia/Tashkent',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],

    

    'modules' => [
        'rbac' => [
            'class' => 'mdm\admin\Module',
            'controllerMap' => [
                 'assignment' => [
                    'class' => 'mdm\admin\controllers\AssignmentController',
                    /* 'userClassName' => 'app\models\User', */
                    'idField' => 'id',
                    'usernameField' => 'username',
                    'searchClass' => 'app\models\user\UserSearch'
                ],
            ],

            'layout' => 'left-menu',
            'mainLayout' => '@app/modules/admin/views/layouts/admin.php',
        ],

        'admin' => [
            'class' => 'app\modules\admin\Module',
            'layout' => 'admin'
        ],

        'treemanager' =>  [
            'class' => '\kartik\tree\Module',
            // other module settings, refer detailed documentation
        ],
    ],

    'as access' => [
        'class' => 'mdm\admin\components\AccessControl',
        'allowActions' => [
            'admin/*',
            // 'rbac/*',
            'site/*',
            'test/*',
            // 'test/modal',
            // 'test/premium-filter',
            // 'test/premium-test',
            // 'test/free-test',
            'results/*',
            // 'results/result',
            // 'results/add-to-results',
            // 'results/stats',
            // 'results/delete',
            'materials/*',
            'tariffs/*',
            '/tariffs/pay-form',
            'cabinet/*',
            'woywo/*',
            'footer/*',
            'gii/*',
            'library/*',
            'task/*',

        ],
    ],

    'components' => [

       'authClientCollection' => [
              'class' => 'yii\authclient\Collection',
              'clients' => [
                  'google' => [
                      'class' => 'yii\authclient\clients\Google',
                      'clientId' => '556588844168-cdg32sod0rbn22ighi89ggl92uckcctu.apps.googleusercontent.com',
                      'clientSecret' => 'jyYgzrMMMbLuujsx6PiYrn-h',
                  ],
                  'facebook' => [
                        'class' => 'yii\authclient\clients\Facebook',
                        'clientId' => '751222858381800',
                        'clientSecret' => '202e153fd7eb9a98c94fc117fb4ada52',
                        //'attributeNames' => ['name', 'email', 'first_name', 'last_name'],
                    ],
              ],
        ],

       'formatter' => [
               'class' => 'yii\i18n\Formatter',
               'defaultTimeZone' => 'Uzbekistan/Tashkent',
               // 'timeZone' => 'GMT+3',
               'dateFormat' => 'd MMMM yyyy',
               'datetimeFormat' => 'd-M-Y H:i:s',
               'timeFormat' => 'H:i:s', 
        ],

        'authManager' => [
            'class' => 'yii\rbac\DbManager', // or use 'yii\rbac\DbManager'
        ],

        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'hwkfy22xZVjH-e7fAm0cNFR4DbZureIY',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],

        "image" => [
            'class' => '\app\components\helpers\SimpleImage'
        ],
        
        'user' => [
            'identityClass' => 'app\models\user\User',
            'loginUrl' => ['site/login'],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                    'logFile' => '@app/runtime/logs/eauth.log',
                    'categories' => ['nodge\eauth\*'],
                    'logVars' => [],
                ],
            ],
        ],
        'db' => $db,

        'i18n' => [
            'translations' => [
                'lang' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/messages',
                ],
            ],
        ],
        
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'class' => 'codemix\localeurls\UrlManager',
            'languages' => ['uz', 'ru', 'cryl'],
            'enableDefaultLanguageUrlCode' => true,
            'rules' => [
                // 'tariffs/<id:.+>' => 'tariffs/pay-form',
                '<language:ru|uz|cryl>/<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ],
        ],
        
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
