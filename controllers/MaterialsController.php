<?php 

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use app\models\db_info\Db_info;

/**
* 
*/
class MaterialsController extends Controller
{
	
	public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post', 'get'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $lang = Yii::$app->language;
        $modelPosts = Db_info::find()->with('cat')->where(['type' => Db_info::DATA_POST])->andWhere(['lang' => $lang])->orderBy('id DESC')->limit(6)->all();

        // $modelVideos = Db_info::find()->with('cat')->where(['type' => Db_info::DATA_VIDEO])->andWhere(['lang' => $lang])->orderBy('id DESC')->limit(3)->all();

        if ($id = Yii::$app->request->get('id')) {
            $catFindes = Db_info::find()->with('cat', 'user')->where(['parent_id' => $id])->all();
        
            $this->layout = 'taskLayout';
            return $this->render('find', [
                'catFindes' => $catFindes,
            ]);
        }

        $this->layout = 'taskLayout';
        return $this->render('index', [
           'posts' => $modelPosts,
           'videos' => $modelVideos,
        ]);
    }

    public function actionFind($id)
    {   

        $catFindes = Db_info::find()->with('cat', 'user')->where(['parent_id' => $id])->all();
        
        $this->layout = 'taskLayout';
        return $this->render('find', [
            'catFindes' => $catFindes,
        ]);
    }

    public function actionView($id)
    {
        $model = Db_info::find()->with('cat', 'user')->where(['id' => $id])->one();
        $model->views++;
        $model->save();


        $this->layout = 'taskLayout';
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    public function actionAddLink()
    {
        $id = \Yii::$app->request->post('id');
        $post = Db_info::findOne($id);
        $post->likes++;
        $post->save();

        return $post->likes;
    }

    public function actionVideo()
    {
        $lang = Yii::$app->language;
        $modelVideos = Db_info::find()->with('cat', 'user')->where(['type' => Db_info::DATA_VIDEO])->andWhere(['lang' => $lang])->all();

        if ($id = Yii::$app->request->get('id')) {
            $catFindes = Db_info::find()->with('cat', 'user')->where(['parent_id' => $id])->where(['type' => Db_info::DATA_VIDEO])->all();
        
            $this->layout = 'taskLayout';
            return $this->render('find', [
                'catFindes' => $catFindes,
            ]);
        }

        $this->layout = 'taskLayout';
        return $this->render('video', [
            'videos' => $modelVideos,
        ]);
    }

    public function actionAudio()
    {
        $modelAudios = Db_info::find()->with('cat', 'user')->where(['type' => Db_info::DATA_AUDIO])->all();

        if ($id = Yii::$app->request->get('id')) {
            $catFindes = Db_info::find()->with('cat', 'user')->where(['parent_id' => $id])->where(['type' => Db_info::DATA_AUDIO])->all();
        
            $this->layout = 'taskLayout';
            return $this->render('find', [
                'catFindes' => $catFindes,
            ]);
        }

        $this->layout = 'taskLayout';
        return $this->render('audio', [
            'audios' => $modelAudios,
        ]);
    }

    public function actionLibrary()
    {
        $modelLibraryes = Db_info::find()->with('cat', 'user')->where(['type' => Db_info::DATA_LIBRARY])->limit(8)->all();

        $this->layout = 'taskLayout';
        return $this->render('library', [
            'libraryes' => $modelLibraryes,
        ]);
    }

    public function actionLink()
    {

        $modelLinks = Db_info::find()->with('cat', 'user')->where(['type' => Db_info::DATA_LINK])->all();

        $this->layout = 'taskLayout';
        return $this->render('link', [
            'links' => $modelLinks,
        ]);
    }

    public function actionRecomended()
    {

        $modelRecomends = Db_info::find()->with('cat', 'user')->where(['type' => Db_info::DATA_RECOMENDED])->all();

        $this->layout = 'taskLayout';
        return $this->render('recomended', [
            'recomends' => $modelRecomends,
        ]);
    }

}