<?php 

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\user\User;
use app\models\library\Library;
use yii\data\Pagination;



/**
* 
*/
class LibraryController extends Controller
{
	
	public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post', 'get'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $books = Library::find()->where(['lang' => Yii::$app->language]);

        if ($id = Yii::$app->request->get('id')) {
            $books = Library::find()->where(['cat_id' => $id]);
        }

        if ($letter = Yii::$app->request->get('letter')) {
            $books->andWhere(['like', 'name', $letter.'%',false ]);
            
        }

        $countQuery = clone $books;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'defaultPageSize' => 20,
            'forcePageParam' => false, 
            'pageSizeParam' => false,
        ]);
        $books = $books->offset($pages->offset)
            ->limit($pages->limit)
            ->orderBy('id DESC')
            ->all();

        $this->layout = 'taskLayout';
        return $this->render('index', [
            'books' => $books,
            'pages' => $pages,
        ]);
    }
}