<?php 

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use app\models\user\Order;
/**
* 
*/
class WoywoController extends Controller
{
	public $enableCsrfValidation = false;	
	public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post', 'get'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
    	$rawData = \Yii::$app->request->getRawBody();
    	// $uu = '{"order_id":"[account]","amount":"[amount]","tran_id":"[tran_id]","tran_date":"[tran_date]","mac":"[mac]","billType":"PAY"}';
    	$request = json_decode($rawData, true);    	
    	
		if ($request['billType'] == "CHECK") {
		    if (!isset($request['amount']) || !isset($request['order_id'])) {
		        header("HTTP/1.1 500");
		        header("Status: 500");
		        header("Content-Type:application/json; charset=UTF-8");
		        echo json_encode(['status' => -2, 'message' => 'Incorrect parameter amount']);
		    } else {
		        $order = Order::findOne($request['order_id']);
		        if (empty($order)) {
		            header("HTTP/1.1 500");
		            header("Status: 500");
		            header("Content-Type:application/json; charset=UTF-8");
		            echo json_encode(['status' => -4, 'message' => 'Transaction does not exist ']);
		        } else {
		            if ($order->sum != $request['amount']) {
		                header("HTTP/1.1 500");
		                header("Status: 500");
		                header("Content-Type:application/json; charset=UTF-8");
		                echo json_encode(['status' => -2, 'message' => 'Incorrect parameter amount']);
		            } elseif ($order->amount_sum == $request['amount'] && $order->status == 1) {
		                header("HTTP/1.1 500");
		                header("Status: 500");
		                header("Content-Type:application/json; charset=UTF-8");
		                echo json_encode(['status' => -3, 'message' => 'Transaction already paid']);
		            } else {
		                header("HTTP/1.1 200");
		                header("Status: 200");
		                header("Content-Type:application/json; charset=UTF-8");
		                echo json_encode(['status' => 0, 'message' => 'Success']);
		            }
		        }
		    }
		}
		elseif ($request['billType'] == "PAY") {
		    if (!isset($request['amount']) ||
		        !isset($request['order_id']) ||
		        !isset($request['tran_id']) ||
		        !isset($request['tran_date']) ||
		        !isset($request['mac'])
		    ) {
		        header("HTTP/1.1 500");
		        header("Status: 500");
		        header("Content-Type:application/json; charset=UTF-8");
		        echo json_encode(['status' => -2, 'message' => 'Incorrect parameter amount']);
		    } else {
		        $order = Order::findOne($request['order_id']);
		        if (empty($order)) {
		            header("HTTP/1.1 500");
		            header("Status: 500");
		            header("Content-Type:application/json; charset=UTF-8");
		            echo json_encode(['status' => -4, 'message' => 'Transaction does not exist ']);
		        } else {
		            if ($order->sum != $request['amount']) {
		                header("HTTP/1.1 500");
		                header("Status: 500");
		                header("Content-Type:application/json; charset=UTF-8");
		                echo json_encode(['status' => -2, 'message' => 'Incorrect parameter amount']);
		            } elseif ($order->amount_sum == $request['amount'] && $order->status == 1) {
		                header("HTTP/1.1 500");
		                header("Status: 500");
		                header("Content-Type:application/json; charset=UTF-8");
		                echo json_encode(['status' => -3, 'message' => 'Transaction already paid']);
		            } elseif ($request['mac'] != self::getMac($order::UNIQUE_KEY, $order::MERCHANT_ID, $request['order_id'], $request['tran_date'], $request['amount'])) {
		                header("HTTP/1.1 500");
		                header("Status: 500");
		                header("Content-Type:application/json; charset=UTF-8");
		                echo json_encode(['status' => -2, 'message' => 'Incorrect parameter mac']);
		            } else {
		                $order->status = 1;
		                $order->amount_sum = $request['amount'];
		                $order->tran_id = $request['tran_id'];
		                $order->tran_date = $request['tran_date'];
		                $order->date_start = date('Y-m-d');
		                $order->date_end = date('Y-m-d', strtotime('+30 days'));
		                $order->save();
		                header("HTTP/1.1 200");
		                header("Status: 200");
		                header("Content-Type:application/json; charset=UTF-8");
		                echo json_encode(['status' => 0, 'message' => 'Success']);
		            }
		        }
		    }
		}
		else {
		    header("HTTP/1.1 500");
		    header("Status: 500");
		    header("Content-Type:application/json; charset=UTF-8");
		    echo json_encode(['status' => -2, 'message' => 'Incorrect parameter billType']);
		}

		
	}

	public function getMac($key, $merchantId, $account, $date, $amount)
	{
	    $str = $key . $merchantId . $account . $date . $amount;
	    return sha1($str);
	}
}