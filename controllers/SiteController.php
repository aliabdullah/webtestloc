<?php

namespace app\controllers;

use Yii;
use app\models\user\User;
use app\models\user\UserInfo;
use app\models\user\Order;
use app\models\category\Category;
use app\models\cours\Cours;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm as Login;
use app\models\Signup;
use app\models\ContactForm;
use yii\web\UploadedFile;
use app\components\AuthHandler;

class SiteController extends Controller
{
/**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'onAuthSuccess'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {

        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],

            'eauth' => [
                        // required to disable csrf validation on OpenID requests
                        'class' => \nodge\eauth\openid\ControllerBehavior::className(),
                        'only' => ['login'],
                    ],

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post', 'get'],
                ],
            ],

        ];
    }

    public function onAuthSuccess($client)
    {
        (new AuthHandler($client))->handle();
    }



    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $model = Category::getCats();
        $model_cours = Cours::getCours();

        // Check user orders
        $user = Yii::$app->user->identity;
        $order = Order::find()->where(['user_id' => $user->id, 'status' => 1])->one();
        if (!empty($order)) {
            if ($order->date_end < date('Y-m-d')) {
                $order->status = 0;
                $order->save();
            }   
        }

        return $this->render('index', [
            'model' => $model,
            'model_cours' => $model_cours
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    /**
     * Login
     * @return string
     */
    public function actionLogin()
    {   

        if ($data = $_POST['response']) {
            echo "<pre>";
            print_r($data);
            echo "</pre>";
            exit();
        }

        $this->layout = 'loginLayout';
        if (!Yii::$app->getUser()->isGuest) {
            return $this->goHome();
        }

        $model = new Login();
        if ($model->load(Yii::$app->getRequest()->post()) && $model->login()) {
            $user_info = UserInfo::find()->where(['user_id' => Yii::$app->user->identity->id])->all();
            if (empty($user_info) || $user_info == '') {
                Yii::$app->session->setFlash('user_info', 'add-info');
            }
            return $this->goBack();
        } else {
            return $this->render('login', [
                    'model' => $model,
            ]);
        }
    }

    public function actionSignup()
    {
        $this->layout = 'loginLayout';
        $model = new Signup();
        if ($model->load(Yii::$app->getRequest()->post())) {
            $model->photo = UploadedFile::getInstance($model, 'photo');
            $model->uploadFile();
            if ($user = $model->signup()) {
                return $this->goHome();
            }
        }

        return $this->render('signup', [
                'model' => $model,
        ]);
    }

    public function actionUpload()
    {
        $this->layout = 'loginLayout';
       
        $user = Yii::$app->user->identity;
        // $model = User::findOne($user->id);
        $modelSignup = new Signup();

        if ($modelSignup->load(Yii::$app->getRequest()->post())) {
            $modelSignup->photo = UploadedFile::getInstance($modelSignup, 'photo');
            $modelSignup->uploadFile();
            if ($user = $modelSignup->upload($user)) {
                return $this->goHome();
            }
            
        }

        return $this->render('upload', [
                'model' => $modelSignup,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }


    /**
     * Displays outab page.
     *
     * @return string
     */
    public function actionPolitics()
    {
        return $this->render('politics');
    }

    public function actionUserInfo()
    {
        $user = Yii::$app->user->identity;
        $model = new UserInfo();
        $post = Yii::$app->request->post();

        if ($model->load($post)) {
            $model->user_id = $user->id;
            $model->date = date('Y-m-d H:i:s');
            $model->public = $post['UserInfo']['public'] ? $post['UserInfo']['public'] : 0;
            if (empty(UserInfo::find()->where(['user_id' => $user->id])->all()) && $model->save()) {
                Yii::$app->session->setFlash('user-info', "Rahmat! $user->username sizning ma'lumotlaringiz muvoffaqiyatli saqlandi. Ma'lumotlaringizni o'zgartirishni hohlasangiz \"Shaxsiy bo'lim\" sahifasidan foydalanishingiz mumkin!");
                return $this->redirect(['/']);
            }elseif (empty(UserInfo::find()->where(['user_id' => $user->id])->all())) {
                Yii::$app->session->setFlash('writed', "Avval ma'lumotlaringiz kiritilgan!");
                return $this->redirect(['/']);
            }
        }
        $this->layout = 'loginLayout';
        return $this->render('user-info', [
            'model' => $model,
        ]);
    }
}
