<?php 

namespace app\controllers;

use Yii;
use DateTime;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use app\models\user\User;
use app\models\user\UserStats;
use app\models\task\Tasks;
use app\models\tests\TestVariant;
use app\models\task\TaskRating;
use app\models\Result;
use app\models\cours\Cours;
use app\models\testblog\TestBlog;
use app\models\statistics\Statistics;
use yii\helpers\ArrayHelper;
use yii\data\Pagination;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use app\models\task\Attempts;
use app\models\task\AttemptFull;
use app\models\task\UserComplaint;
use app\models\task\Tournament;
use app\models\task\UserMessages;
use app\models\task\TournAnswers;
use app\models\user\UserPoints;
use app\models\category\Category;
use app\models\CommentForm;
use app\models\Comments;

/**
* 
*/
class TaskController extends Controller
{
	
	public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post', 'get'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }


    public function actionIndex()
    {
        $tasks = Tasks::find()->with('editor')->where(['!=', 'type', 'tournament'])->orderBy(['id' => SORT_DESC])->all();
        $attempt = Attempts::find()->with('user', 'task')->where(['complete' => 1])->orderBy('date DESC')->one();
        

        if ($cat_id = Yii::$app->request->get('id')) {
            $tasks = Tasks::find()->with('editor')->where(['cat_id' => $cat_id])->orderBy(['id' => SORT_DESC])->all();
            $category = Category::findOne($cat_id);
        }elseif ($all = Yii::$app->request->get('all') == 1) {
            $tasks = $tasks;
        }elseif ($unsolved = Yii::$app->request->get('solved') == 0 && null !== Yii::$app->request->get('solved')) {
            $tasks = Tasks::find()->with('editor')->where(['solved' => null])->orderBy(['id' => SORT_DESC])->all();
        }elseif ($solved = Yii::$app->request->get('solved') == 1) {
            $tasks = Tasks::find()->with('editor')->where(['solved' => 1])->orderBy(['id' => SORT_DESC])->all();
        }
        

        $this->layout = 'taskLayout';
        return $this->render('index', [
            'tasks' => $tasks,
            'attempt' => $attempt,
            'category' => $category
        ]);
    }

    public function actionView($id)
    {
        $task = Tasks::find()->with('comments', 'user')->where(['id' => $id])->one();
        $rating = TaskRating::find()->with('user')->where(['task_id' => $id])->orderBy('id DESC')->limit(7)->all();
        $complated = Attempts::find()->where(['task_id' => $task->id])->andWhere(['complete' => 1])->count();

        // for comments
        $commentForm = new CommentForm();

        
        $this->layout = 'taskLayout';
        return $this->render('view', [
            'task' => $task,
            'rating' => $rating,
            'complated' => $complated,
            'commentForm' => $commentForm,
        ]);
    }

    public function actionComments($id)
    {
        $commentForm = new CommentForm();
        if (Yii::$app->request->isPost) {
            $commentForm->load(Yii::$app->request->post());
            if ($commentForm->saveComment($id)) {
                return $this->redirect(['task/view', 'id' => $id]);
            }
        }
    }

    public function actionGetTournDate()
    {
        $tournDate = [];
        $tourn = Tournament::find()->where(['status' => Tournament::STATUS_ACTIVE])->one();
        $tournDate['date'] = date('Y-m-d', strtotime($tourn->day));
        $tournDate['timeUp'] = explode(',', $tourn->time)[0];
        $tournDate['timeDown'] = explode(',', $tourn->time)[1];
        $tournTime = explode(',', $tourn->time);
        if (!empty($tourn) && $tournDate >= date('Y-m-d')) {
            $trueDate = $tournDate;
            $date = date('F d, Y', strtotime($tourn->day));
            return json_encode($tournDate);
        }
        return false;
    }

    public function actionAnswer()
    {
        $post = Yii::$app->request->post();
        $attempt = Attempts::find()->with('fullAttempts')->where(['user_id' => $post['user']])->andWhere(['task_id' => $post['task_id']])->one();
        if ($attempt->complete == '') {
            $file = UploadedFile::getInstanceByName('answer[file]');
            if (!empty($post['answer']['short_answ']) && empty($post['answer']['full_answ']) && empty($file)) {
                $task_model = Tasks::findOne($post['task_id']);
                $arrAnsw = explode(',', $task_model->short_answ);
                $attempt = Attempts::find()->where(['user_id' => $post['user']])->andWhere(['task_id' => $post['task_id']])->one(); // perevod Popitka
                if (!in_array($post['answer']['short_answ'], $arrAnsw)) {
                    if ($attempt === null) {
                        $attempt = new Attempts();
                        $attempt->user_id = $post['user'];
                        $attempt->task_id = $post['task_id'];
                        $attempt->attempt = 5;
                        $attempt->save(false);
                    }else {
                        if (!empty($attempt)) {
                            if ($attempt->attempt > 0) {
                                $attempt->attempt--;
                                $attempt->save();
                            }                        
                        }
                    }
                    $task_model->attempts++;
                    if ($task_model->save()) {
                        if ($attempt->attempt > 0) {
                            $num = 6 - $attempt->attempt;
                           $count = "$num ta urinishni amalga oshirdingiz.(Umumiy 5 - urinish beriladi)";
                        }else {
                            $count = "O\'z urunishlaringizni amalga oshirib bo\'ldingiz endi bu masala uchun bal ololmaysiz.";
                        }
                        
                        Yii::$app->session->setFlash('incorrect', "$count");
                        return $this->redirect(['/task/view', 'id' => $post['task_id']]);
                    }else{
                        echo '<pre>';
                        print_r('Qisqa javob yuborish qismida hatolik (agar notogri bolsa');
                        echo '</pre>';
                        exit();
                    }
                    
                }elseif (in_array($post['answer']['short_answ'], $arrAnsw)) {
                    $task_model->solved = 1; 
                    $task_model->attempts++; 
                    $task_model->save(); // add to completed tasks
                    $taskMainCat = $task_model->getMainCat($post['task_cat']);
                    $userPoint = UserPoints::find()->where(['user_id' => $post['user']])->one();
                    if ($attempt === null) {
                        if (empty($userPoint)) {
                            $newPoint = new UserPoints();
                            $newPoint->user_id = $post['user'];
                            $newPoint->subject = UserPoints::SUBJECT_TASK;
                            $newPoint->point = 100;
                            $newPoint->save();
                        }else {
                            $userPoint->point += 100;
                            $userPoint->save(); 
                        }
                        $newAttempt = new Attempts();
                        $newAttempt->user_id = $post['user'];
                        $newAttempt->task_id = $post['task_id'];
                        $newAttempt->answer = $post['answer']['short_answ'];
                        $newAttempt->score = 100;
                        $newAttempt->complete = 1;
                        $newAttempt->date = date('Y-m-d H:i:s');
                        $newAttempt->save();

                        // add to rating for statistiks
                        $rating = new TaskRating();
                        $rating->user_id = $newAttempt->user_id;
                        $rating->attempt = ((6 - $newAttempt->attempt) + 1);
                        $rating->task_id = $post['task_id'];
                        $rating->date = date('Y-m-d H:i:s');
                        $rating->save();
                    }else {
                        if (empty($userPoint)) {
                            $newPoint = new UserPoints();
                            $newPoint->user_id = $post['user'];
                            $newPoint->subject = UserPoints::SUBJECT_TASK;
                            if ($attempt->attempt == 6) {
                                $newPoint->point = 100;
                                $attempt->score = 100;
                            }elseif ($attempt->attempt == 5) {
                                $newPoint->point = 90;
                                $attempt->score = 90;
                            }elseif($attempt->attempt == 4) {
                                $newPoint->point = 70;
                                $attempt->score = 70;
                            }elseif($attempt->attempt == 3) {
                                $newPoint->point = 50;
                                $attempt->score = 50;
                            }elseif($attempt->attempt == 2) {
                                $newPoint->point = 10;
                                $attempt->score = 10;
                            }else { $newPoint->point = 0; $attempt->score = 0; }
                            $newPoint->save();
                        }else {
                            if ($attempt->attempt == 6) {
                                $userPoint->point += 100;
                                $attempt->score = 100;
                            }elseif ($attempt->attempt == 5) {
                                $userPoint->point += 90;
                                $attempt->score = 90;
                            }elseif($attempt->attempt == 4) {
                                $userPoint->point += 70;
                                $attempt->score = 70;
                            }elseif($attempt->attempt == 3) {
                                $userPoint->point += 50;
                                $attempt->score = 50;
                            }elseif($attempt->attempt == 2) {
                                $userPoint->point += 10;
                                $attempt->score = 10;
                            }else { $userPoint->point += 0; $attempt->score = 0; }
                            $userPoint->save();
                        }
                        $attempt->answer = $post['answer']['short_answ'];
                        $attempt->complete = 1;
                        $attempt->date = date('Y-m-d H:i:s');
                        $attempt->save();

                        $rating = new TaskRating();
                        $rating->user_id = $attempt->user_id;
                        $rating->attempt = ((6 - $attempt->attempt) + 1);
                        $rating->task_id = $attempt->task_id;
                        $rating->date = date('Y-m-d H:i:s');
                        $rating->save();
                    }
                    return $this->redirect(['/task/view', 'id' => $post['task_id']]);
                }
            }elseif (!empty($post['answer']['full_answ']) || $file) {
                if (!empty($attempt)) {
                    foreach ($attempt->fullAttempts as $key => $fullAtte) {
                        if ($fullAtte->checked == '' || $fullAtte->checked === null) {
                            Yii::$app->session->setFlash('unchecked', 'Yuborgan javobingiz hali tekshirilmagan, tekshirilgandan so\'ng keyingi javobni yuborishingiz mumkin. Tez orada javobingiz ko\'rib chiqiladi...!');
                            return $this->redirect(['/task/view', 'id' => $post['task_id']]);
                        }
                    }
                    $full = new AttemptFull();
                    $full->attempt_id = $attempt->id;
                    $full->full_answ = $post['answer']['full_answ'] ? $post['answer']['full_answ'] : '';
                    $full->file = $file;
                    $full->date = date('Y-m-d H:i:s');
                    $full->uploadFile();
                    $full->save();
                }elseif (empty($attempt)) {
                    $newAttempt = new Attempts();
                    $newAttempt->user_id = $post['user'];
                    $newAttempt->task_id = $post['task_id'];
                    $newAttempt->attempt = 6;
                    if ($newAttempt->save()) {
                        $newFull = new AttemptFull();
                        $newFull->attempt_id = $newAttempt->id;
                        $newFull->full_answ = $post['answer']['full_answ'] ? $post['answer']['full_answ'] : '';
                        $newFull->file = $file;
                        $newFull->date = date('Y-m-d H:i:s');
                        $newFull->uploadFile();
                        $newFull->save();
                    }  
                }
                $message = Yii::$app->session->setFlash('send-answer', "Sizning javobingiz yuborildi");
                return $this->redirect(['/task/view', 'id' => $post['task_id']]);
            }else {
                echo '<pre>';
                print_r($post .'  <br>  to\'liq javob yoki file da hatolik bor');
                echo '</pre>';
                exit();
            }
        }else {
            return $this->redirect(['/task/view', 'id' => $post['task_id']]);
        }
        

         return $this->render('answer');

    }

    public function actionAnswTourn()
    {
        $post = Yii::$app->request->post();
        $tournAnswModel = TournAnswers::find()->where(['user_id' => $post['user']])->andWhere(['tourn_id' => $post['tourn_id']])->andWhere(['task_id' => $post['task_id']])->one();

        if (empty($tournAnswModel)) {
            $file = UploadedFile::getInstanceByName('answer[file]');
            if (!empty($post['answer']['full_answ']) || $file) {
                $tournAnsw = new TournAnswers();
                $tournAnsw->user_id = $post['user'];
                $tournAnsw->tourn_id = $post['tourn_id'];
                $tournAnsw->task_id = $post['task_id'];
                $tournAnsw->time = date('H:i:s');
                $tournAnsw->answer = $post['answer']['full_answ'];
                $tournAnsw->file = $file;
                $tournAnsw->uploadFile();
                if ($tournAnsw->save()) {
                    $message = Yii::$app->session->setFlash('send-answer-$post["task_id"]', "Sizning javobingiz yuborildi! Tez orada tekshiriladi.");
                    return $this->redirect(['/task/tourn-view', 'id' => $post['tourn_id']]);
                }
                
               
            }else {
                $message = Yii::$app->session->setFlash('empty-inputs', "Javob kritilmagan!");
                    return $this->redirect(['/task/view', 'id' => $post['task_id']]);
            }
        }else {
            echo '<pre>';
            print_r('Bu masalaga javob bergansiz.');
            echo '</pre>';
            exit();
        }
        

         return $this->render('answer');

    }

    public function actionReadMessage()
    {
        $id = Yii::$app->request->post('id');
        $userMessage = UserMessages::findOne($id);
        $userMessage->checked = 1;
        $userMessage->save();
        return true;
    }

    public function actionComplaint()
    {
        $post = Yii::$app->request->post();
        $complaint = new UserComplaint();
        $complaint->user_id = $post['user'];
        $complaint->task_id = $post['task'];
        $complaint->complaint = $post['complaint'];
        $complaint->date = date('Y-m-d');
        if ($complaint->save()) {
            Yii::$app->session->setFlash('complaint', 'Sizning shikoyatingiz adminga yuborildi tez orada sizning shikoyatingiz ko`ribchiqiladi!');
            return $this->redirect(['/task/view', 'id' => $post['task']]);
        }else {
            echo '<pre>';
            print_r('task/complaint qismida hatolik');
            echo '</pre>';
            exit();
        }

    }

    public function actionEvents() 
    {
        $events = Attempts::find()->with('user', 'task')->where(['complete' => 1])->orderBy('id DESC')->limit(10)->all();

        $this->layout = 'taskLayout';
        return $this->render('events', [
            'events' => $events,
        ]);
    }

    public function actionAddLike()
    {
        $id = Yii::$app->request->post('id');
        $attempt = Attempts::findOne($id);
        $attempt->like++;
        if ($attempt->save()) {
            return true;
        }
        return false;
    }

    public function actionTournament()
    {
        $tournModels = Tournament::find()->where(['!=', 'status', Tournament::STATUS_DESACTIVE])->orderBy(['id' => SORT_DESC])->all();
        foreach ($tournModels as $key => $tourn) {
            if ($tourn->status == Tournament::STATUS_ACTIVE && date('Y-m-d', strtotime($tourn->day.'+1 day')) < date('Y-m-d')) {
                $tourn->status = Tournament::STATUS_PAST;
                $tourn->save();
            }
        }

        $this->layout = 'taskLayout';
        return $this->render('tournament', [
            'tournModels' => $tournModels,                            
        ]);
    }

    public function actionTournView($id)
    {
        $tournModel = Tournament::find()->with('tasks')->where(['id' => $id])->one();
        $first = date('Y-m-d', strtotime($tournModel->day));
        $secDay = date('Y-m-d', strtotime($first.'+1 day'));
        $time = explode(',', $tournModel->time);
        $tourn_tasks = $tournModel->tasks;
        if (date('Y-m-d') == $first && $time[0] <= date('H:i') && $time[1] >= date('H:i')) {
            $cut = array_slice($tourn_tasks, 0, 3);
        }elseif (date('Y-m-d') == $secDay && $time[0] <= date('H:i') && $time[1] >= date('H:i')) {
            $cut = array_slice($tourn_tasks, 3);
        }
        
        $this->layout = 'taskLayout';
        return $this->render('index', [
            'tournModel' => $tournModel,
            'tasks' => $cut
        ]);
    }

    public function actionRating()
    {   
        $points = UserPoints::find()->orderBy(['point' => SORT_DESC])->limit(20)->all();
        $this->layout = 'taskLayout';
        return $this->render('rating', [
            'points' => $points
        ]);
    }

}