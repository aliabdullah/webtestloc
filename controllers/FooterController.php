<?php 

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use app\models\cours\Cours;


Class FooterController extends Controller
{
	public function behaviors()
	{
		return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post', 'get'],
                ],
            ],
        ];
	}

	/**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionTarrif()
    {
    	$model_cours = Cours::getCours();

        return $this->render('tarrif', [
            'model_cours' => $model_cours
        ]);
    }

    public function actionPayment()
    {
    	return $this->render('payment');
    }

    public function actionOferta()
    {
    	return $this->render('oferta');
    }

    public function actionPolitic()
    {
    	return $this->render('politic');
    }

    public function actionPartner()
    {
    	return $this->render('Partner');
    }

    public function actionContact()
    {
    	return $this->render('contact');
    }
}