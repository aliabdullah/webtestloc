<?php 

namespace app\controllers;

use Yii;
use DateTime;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use app\models\user\User;
use app\models\user\UserStats;
use app\models\tests\Tests;
use app\models\tests\TestVariant;
use app\models\Result;
use app\models\cours\Cours;
use app\models\testblog\TestBlog;
use app\models\statistics\Statistics;
use yii\helpers\ArrayHelper;
use yii\data\Pagination;

/**
* 
*/
class ResultsController extends Controller
{
	
	public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post', 'get'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $user = Yii::$app->user->identity;
        $modelUser = User::find()->with('results', 'useBlogs')->where(['id' => $user->id])->one();
        $userResults = Statistics::find()->where(['user_id' => $user->id]);
        
        $countQuery = clone $userResults;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'defaultPageSize' => 14,
            'forcePageParam' => false, 
            'pageSizeParam' => false,
        ]);
        $models = $userResults->offset($pages->offset)
            ->limit($pages->limit)
            ->orderBy('date DESC')
            ->all();

        return $this->render('index', [
            'user' => $user,
            'modelUser' => $modelUser,
            'models' => $models,
            'pages' => $pages
            //'modelStats' => $modelStats,
        ]);
    }

    public function actionStats() 
    {
        $user = Yii::$app->user->identity;
        $modelStats = Statistics::find()
        ->select(['DATE_FORMAT(date, "%Y %m" ) as month, SUM(correct_answer) as sum_correct, 
            SUM(`interval`) as sum_interval, SUM(wrong_answer) as sum_wrong'])
        ->where(['user_id' => $user->id])
        // ->andWhere(['<=','DATE(date)', date('Y')."-03-30"])
        ->groupBy('month')
        ->asArray()
        ->all();

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        return $modelStats;
    }

    public function actionAddToResults()
    {
        $this->layout = false;
        $user = \Yii::$app->user->identity;
        $var_id = Yii::$app->request->get('var_id');
        $quest_id = Yii::$app->request->get('quest_id');
        $pred_name = Yii::$app->request->get('pred_name');

        $result = new Result();
        $result->addResults($quest_id, $pred_name, $var_id, $user);
        
        echo "<pre>";
        print_r($_SESSION['result']);
        echo "</pre>";

    }

    public function actionResult()
    {

        $user = \Yii::$app->user->identity;
        if ($_SESSION['result']['stopTime'] == 0) {
            $_SESSION['result']['stopTime'] = date("H:i:s");
        }
        if (empty($_SESSION['result']['all'])) {
            if (isset($_SESSION['result'][$user->username])) {
                $check = [];
                foreach ($_SESSION['result'][$user->username] as $pred => $arrAnsw) {
                    $check[$pred];
                    foreach ($arrAnsw as $quest_id => $var_id) {
                        $correct = TestVariant::find()->where(['test_id' => $quest_id])->andWhere(['correct' => 1])->one();
                        $selected = TestVariant::find()->where(['test_id' => $quest_id])->andWhere(['id' => $var_id])->one();
                        if ($correct->id == $selected->id) {
                                $check[$pred]['true'] += 1;
                                $_SESSION['result']['all']['true'] += 1;
                        }else {
                                $check[$pred]['false'] += 1;
                                $_SESSION['result']['all']['false'] += 1;
                        }
                    }
                }
                $_SESSION['result']['checked'] = $check;
            }else {
                return $this->redirect(['/site/login']);
            }
        }
        return $this->render('result', [
                'result' => $result,
                // 'predmet_1' => $predmet_1,
                // 'predmet_2' => $predmet_2,
                // 'predmet_3' => $predmet_3,
                // 'blogModels' => $blogModels,
            ]);
    }

    public function actionSave()
    {
        $user = \Yii::$app->user->identity;

        if (!$_SESSION['result']['saved'] == true) {
            $predmets = [];
            foreach ($_SESSION['result'][$user->username] as $namePred => $arrAnsw) {
                if (empty($predmets)){ $predmets[0] = $namePred;}elseif (!empty($predmets[0]) && empty($predmets[1])) { $predmets[1] = $namePred; }elseif (!empty($predmets[0]) && !empty($predmets[1])) { $predmets[2] = $namePred; }
                
                foreach ($arrAnsw as $quest => $value) {
                    $userstatsModel = new UserStats();
                    $userstatsModel->user_id = $user->id;
                    $userstatsModel->blog_id = $namePred;
                    $userstatsModel->quest_id = $quest;
                    $userstatsModel->select_id = $value;
                    $userstatsModel->start_time = $_SESSION['result']['startTime'];
                    $userstatsModel->stop_time = $_SESSION['result']['stopTime'];
                    $userstatsModel->date = date('Y-m-d, H:i:s');
                    $userstatsModel->save();
                }
            }

            $date1 = new DateTime($_SESSION['result']['startTime']);
            $date2 = new DateTime($_SESSION['result']['stopTime']);
            $interval = $date2->diff($date1);
            $time = $date2->getTimestamp() - $date1->getTimestamp();

            $statsModel = new Statistics();
            $statsModel->user_id = $user->id;
            $statsModel->start_time = $_SESSION['result']['startTime'];
            $statsModel->stop_time = $_SESSION['result']['stopTime'];
            $statsModel->interval = $time;
            $statsModel->correct_answer = $_SESSION['result']['all']['true'] ? $_SESSION['result']['all']['true'] : 0 ;
            $statsModel->wrong_answer = $_SESSION['result']['all']['false'] ? $_SESSION['result']['all']['false'] : 0 ;
            $statsModel->date = date('Y-m-d, H:i:s');
            $statsModel->predmet1 = mb_substr($predmets[0], 0, -1);
            $statsModel->predmet2 = mb_substr($predmets[1], 0, -1);
            $statsModel->predmet3 = mb_substr($predmets[2], 0, -1);
            if ($statsModel->save()) {
                $session = Yii::$app->session;
                $session->open();
                $session->remove('result');
                $_SESSION['result'] = ['saved' => 'true'];
                
            }

            \Yii::$app->getSession()->setFlash('success', Yii::t('lang', 'Ваши результаты сохранены, и вы можете увидеть свои результаты в разделе «Мои результаты».'));
            return $this->redirect(['/test/premium-filter']);
        }
        return $this->redirect(['/test/premium-filter']);
         
    }

    public function actionDelete()
    {
        $session = Yii::$app->session;
        $session->open();
        $session->remove('result');
        if (empty($_SESSION['result'])) {
            \Yii::$app->getSession()->setFlash('deleted', Yii::t('lang', "Результаты теста удалены, вы можете увидеть сохраненные результаты в разделе «Мои результаты»"));
            return $this->redirect(['/test/premium-filter']);
        }
        // $this->layout = false;
        // return $this->render('result', [
        //         'result' => $result,
        // ]);
    }
}