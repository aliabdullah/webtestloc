<?php 

namespace app\controllers;

use Yii;
use app\models\tests\Tests;
use app\models\testblog\TestGroup;
use app\models\testblog\TestBlog;
use app\models\category\Category;
use app\models\cours\Cours;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\helpers\ArrayHelper;

/**
* 
*/
class TestController extends Controller
{
	
	public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post', 'get'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionModal()
    {

        $modelTestGroup = TestGroup::find()->all();

        $free = Yii::$app->request->post(2);
        $premium = Yii::$app->request->post(3);

        if ($free) {
            echo $free;
        }else {
            echo $premium;
        }


        $this->layout = false;
        return $this->render('modal', [
            'modelTestGroup' => $modelTestGroup,
        ]);
    }

    public function actionIndex()
    {

        // $model = Category::getCats();
        $model_cours = Cours::getCours();

        return $this->render('index', [
            // 'model' => $model,
            'model_cours' => $model_cours
        ]);
    }

    public function actionFreeTest()
    {
        $user = \Yii::$app->user->identity;        
        $test = Tests::find()->with('variants')->where(['lang' => Yii::$app->language])->limit(30)->orderBy('rand()')->all();
        $session = Yii::$app->session;
        $session->open();

        $_SESSION['result'] = [
                'user_id' => $user->id,
                'type' => 'free',
                'startTime' => date('H:i:s'),
                'stopTime' => 0,
                'max_ball' => 0,
                // $user->username => [],
            ];

        return $this->render('free-test', [
                'test' => $test,
            ]);

    }



    public function actionPremiumFilter()
    {
        $test = Tests::find()->all();
        $test_count = count($test);
        

        unset($_SESSION['tests']);
        return $this->render('premium-filter', [
            'test_count' => $test_count,
        ]);
    }



    public function actionPremiumTest()
    {
        $user = \Yii::$app->user->identity;
        $lang = Yii::$app->language;
        
        if (Yii::$app->request->post()) {
            $lang = Yii::$app->request->post('lang');
            $level = Yii::$app->request->post('level');
            $predmet1 = Yii::$app->request->post('predmet1');
            $predmet2 = Yii::$app->request->post('predmet2');
            $predmet3 = Yii::$app->request->post('predmet3');
            
            if (!empty($lang) && !empty($level) && !empty($predmet1) && empty($predmet2) && empty($predmet3)) {
                $test = [];
                $mainCat = Category::findOne($predmet1);
                $tests = Tests::find()->with('variants')->where(['category_id' => $predmet1])->andWhere(['lang' => $lang])->andWhere(['level' => $level])->orderBy('rand()')->limit(30)->all();
                $test['mainCat'] = $mainCat;
                $test['tests'] = $tests;
                
                $session = Yii::$app->session;
                $session->open();
                $_SESSION['result'] = [
                    'user_id' => $user->id,
                    'type' => 'school',
                    'startTime' => date('H:i:s'),
                    'stopTime' => 0,
                    'bal' => 0,
                    // $user->username => [],
                ];

                return $this->render('premium-test', [
                    'test' => $test
                ]);

            }elseif (!empty($lang) && !empty($level) && !empty($predmet1) && !empty($predmet2) && !empty($predmet3)) {
                    $test1 = Tests::getTests($predmet1, $level, $lang);
                    $used_ids_1 = ArrayHelper::map($test1['tests'], 'id', 'id');
                    $test2 = Tests::getTests($predmet2, $level, $lang, $used_ids_1);
                    $used_ids_2 = ArrayHelper::map($test2['tests'], 'id', 'id');
                    $test3 = Tests::getTests($predmet3, $level, $lang, $used_ids_1, $used_ids_2);
                    
                    $session = Yii::$app->session;
                    $session->open();
                    $_SESSION['result'] = [
                        'user_id' => $user->id,
                        'type' => 'student',
                        'startTime' => date('H:i:s'),
                        'stopTime' => 0,
                        'bal' => 0,
                        // $user->username => [],
                    ];
                    
                    return $this->render('premium-test', [
                        'test1' => $test1,
                        'test2' => $test2,
                        'test3' => $test3,
                    ]);
            }else {
                echo '<pre>';
                print_r('error! at the abiturient request');
                echo '</pre>';
                exit();
            }

        }else {
            return $this->redirect(["/test/premium-filter"]); 
        }
        
    }

    public function actionRemoveVariant()
    {
        $user = Yii::$app->user->identity;
        $questId = Yii::$app->request->post();
        $sessionArr = $_SESSION['result'][$user->username]; 
        foreach ($sessionArr as $key => $pred) {
            foreach ($pred as $quest_id => $varId) {
                if ($quest_id == $questId['questId']) {
                    unset($_SESSION['result'][$user->username][$key][$quest_id]);
                }
            }
        }
        
        return json_encode($questId);
    }

    public function actionTests()
    {
        return $this->render('tests');
    }

    public function actionCatId()
    {
        $mainCats = Category::find()->where(['parent_id' => 0])->all();
        $arr = ArrayHelper::map($mainCats, 'id', 'id');
        return json_encode(['kkk' => $arr]);
        // foreach ($mainCats as $key => $value) {
        //     $subCats = Category::find()->where(['parent_id' => $value->id])->all();
        // }
    }
}