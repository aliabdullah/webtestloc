<?php 

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use app\models\cours\Cours;
use app\models\user\Order;

/**
* 
*/
class TariffsController extends Controller
{
	
	public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post', 'get'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $user = Yii::$app->user->identity;
        $lang = Yii::$app->language;
        $tarrifs = Cours::find()->where(['lang' => $lang])->all();
        // echo '<pre>';
        // print_r($tarrifs);
        // echo '</pre>';
        // exit();
        $userOrder = Order::find()->with('tarif')->where(['user_id' => $user->id])->andWhere(['status' => 1])->one();
        
        
        return $this->render('index', [
                'tariffsModal' => $tariffsModal,
                'tarrifs' => $tarrifs,
                'userOrder' => $userOrder,
            ]);
    }

    public function actionPayForm($id)
    {
        $tarifModel = Cours::findOne($id);

        return $this->render('pay-form', [
                'tarifModel' => $tarifModel
            ]);
    }

    public function actionModal($tariff_id)
    {
        $user = \Yii::$app->user->identity;
        $tariff = Cours::findOne($tariff_id);

        $old_orders = Order::deleteAll(['user_id' =>  $user->id, 'status' => null, 'amount_sum' => null]);

        $orderModel = new Order();
        $orderModel->user_id = $user->id;
        $orderModel->tariff_id = $tariff->id;
        $orderModel->sum = $tariff->cost;
        // $orderModel->date_start = date('Y-m-d');
        $orderModel->save();

        $this->layout = false;
        return $this->render('modal', [
            'tariff' => $tariff,
            'orderModel' => $orderModel
        ]);
    }



    public function actionModalForm()
    {
        // $modelOrder = Order::UNIQUE_KEY;
        $form = \Yii::$app->request->post();
        // echo "<pre>";
        // print_r($form);
        // echo "</pre>";
        // exit();

        $this->layout = false;
        return $this->render('modal-form');
    }

}